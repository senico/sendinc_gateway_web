/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.components;

import java.util.regex.Pattern;

import mitm.common.util.RegExprUtils;
import mitm.common.ws.WebServiceCheckedException;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.corelib.components.Form;
import org.apache.tapestry5.corelib.components.TextField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@IncludeStylesheet("context:styles/components/regExprFilter.css")
public class RegExprFilter 
{
	private final static Logger logger = LoggerFactory.getLogger(RegExprFilter.class);
	
	/*
	 * The value to filter on. Can be a regular expression if filter is true.
	 */
	@Persist
	private String filter;
	
	/*
	 * True if the filter is a regular expression
	 */
	@Persist
	private boolean regExpr;

	@Component
	private Form filterForm;

	@Component(id = "filter")
	private TextField filterTextField;
	
	public boolean isFilterDisabled() {
		return filter == null || filter.length() == 0;
	}

	public boolean isRegExpr() {
		return regExpr;
	}

	public void setRegExpr(boolean regExpr) {
		this.regExpr = regExpr;
	}

	public String getFilter() {
		return filter;
	}

	public void setFilter(String filter) {
		this.filter = filter;
	}

	/**
	 * Returns the regular expression pattern. If the user did not select the regular expression checkbox the regular
	 * expression that's being returned is prepended with (?i) (ie. case insensitive) search. If escapeHTML is true
	 * the search expression is HTML escaped before a pattern is build. This is needed when you want to match
	 * the pattern against a HTML file but you want the user to be able to search for HTML special characters (like <)
	 * without the user having to change it into the HTML entity.
	 */
	public Pattern getPattern(boolean escapeHTML)
	{
		/*
		 * We will always return a Pattern but if the user did not select the regular expression 
		 * checkbox we will create a pattern from the filter 
		 */
		String expression = filter;

		Pattern pattern = null;
		
		if (expression != null && expression.length() > 0)
		{
			if (escapeHTML) {
				expression = StringEscapeUtils.escapeHtml(expression);
			}

			if (!regExpr)
			{
				/*
				 * We need to escape all characters that have a special meaning for regular expressions 
				 */
				expression = "(?i)" + RegExprUtils.escape(expression);
			}
			
			try {
				pattern = Pattern.compile(expression);
			}
			catch(IllegalArgumentException e) {
				/*
				 * Even though the for does not validate getPattern is still called so we should
				 * catch any compile errors.
				 */
				logger.error("Error in regular expression.");
			}
		}
		
		return pattern;
	}
	
	public void onValidateForm() 
	throws WebServiceCheckedException 
	{
		/*
		 * Skip checks if the filter is empty
		 */
		if (filter == null || filter.length() == 0) {
			return;
		}
		
		/*
		 * Check that the filter is a valid regular expression
		 */
		if (isRegExpr()) 
		{
			try {
				Pattern.compile(filter);
			}
			catch(IllegalArgumentException e) 
			{
				filterForm.recordError(filterTextField, 
						"The filter is not a valid regular expression.");
			}
		}
	}
}
