/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 *
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar,
 * wsdl4j-1.6.1.jar (or modified versions of these libraries),
 * containing parts covered by the terms of Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to
 * convey the resulting work.
 */
package mitm.djigzo.web.common;

/**
 * The templates that can be edited
 *
 * @author Martijn Brinkers
 *
 */
public enum TemplateType
{
	ENCRYPTION_FAILED_NOTIFICATION  ("Encryption failed notification", true, true, true),
	ENCRYPTION_NOTIFICATION         ("Encryption notification", true, true, true),

  DLP_WARNING                     ("DLP warning", true, true, true),
  DLP_QUARANTINE                  ("DLP quarantine", true, true, true),
  DLP_BLOCK                       ("DLP block", true, true, true),
  DLP_ERROR                       ("DLP error", true, true, true),
  DLP_RELEASE_NOTIFICATION        ("DLP release notification", true, true, true),
  DLP_DELETE_NOTIFICATION         ("DLP delete notification", true, true, true),
  DLP_EXPIRE_NOTIFICATION         ("DLP expire notification", true, true, true);

	private final String friendlyName;

	/*
	 * A template can only be specific for user, domain or global or a combination.
	 */
    private final boolean userTemplate;
    private final boolean domainTemplate;
    private final boolean globalTemplate;

	private TemplateType(String friendlyName, boolean userTemplate, boolean domainTemplate, boolean globalTemplate)
	{
	    this.friendlyName = friendlyName;
	    this.userTemplate = userTemplate;
	    this.domainTemplate = domainTemplate;
	    this.globalTemplate = globalTemplate;
	}

	public String getFriendlyName() {
	    return friendlyName;
	}

	public static TemplateType fromString(String value)
	{
	    for (TemplateType type : TemplateType.values())
	    {
	        if (type.toString().equalsIgnoreCase(value)) {
	            return type;
	        }
	    }

	    return null;
	}

	public static TemplateType fromFriendlyName(String friendlyName)
    {
        for (TemplateType type : TemplateType.values())
        {
            if (type.friendlyName.equalsIgnoreCase(friendlyName)) {
                return type;
            }
        }

        return null;
    }

    public boolean isUserTemplate() {
        return userTemplate;
    }

    public boolean isDomainTemplate() {
        return domainTemplate;
    }

    public boolean isGlobalTemplate() {
        return globalTemplate;
    }
}
