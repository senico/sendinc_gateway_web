Name: sendinc-gateway-web
Version: @VERSION@
Release: @RELEASE@
Summary: SendInc Email Encryption Gateway Web Management
License: AGPLv3
Group: System Environment/Daemons
URL: https://www.sendinc.com/
Vendor: Send Technology, Inc.
Requires: java-1.6.0-openjdk
Requires: mktemp

%define _rpmdir .
%define _rpmfilename %%{NAME}-%%{VERSION}-%%{RELEASE}.%%{ARCH}.rpm

%post
#!/bin/bash -e
# postinst script for djigzo

# set a sane/secure path
PATH='/bin:/usr/bin:/sbin:/usr/sbin'
# it's almost certainly already marked for export, but make sure
export PATH

# remove all aliases (start with \ to prevent unalias from being aliased)
\unalias -a

# clean command hash
hash -r

# set a sane/secure IFS (note this is bash & ksh93 syntax only--not portable!)
IFS=$' \t\n'

DJIGZO_HOME=/usr/share/djigzo
DJIGZO_WEB_HOME=/usr/share/djigzo-web
DJIGZO_USER=djigzo
DJIGZO_GROUP=djigzo

# disabled since this depends too much on the actual Java version of OpenJDK
#update_java()
#{
#    /usr/sbin/alternatives --set java /usr/lib/jvm/jre-1.6.0-openjdk/bin/java
#}

finish_message()
{
    echo "****************************************************************************"
    echo "* The installation has finished. Tomcat (or Jetty) should now be manually  *"
    echo "* configured.                                                              *"
    echo "****************************************************************************"
}

# add user djigzo if user does not exist
if ! id "$DJIGZO_USER" > /dev/null 2>&1 ; then
    adduser --home-dir "$DJIGZO_HOME" -m  --shell /sbin/nologin "$DJIGZO_USER"
fi

# disabled since this depends too much on the actual Java version of OpenJDK
#update_java

# Note: If a sslCertificate.p12 file exists, the owner and mode will be reset. We cannot store
# the owner and mode like the .deb installer because when %post (this script) is called the 
# file sslCertificate.p12 is already overwritten (and the owner is set to root:root)

# make djigzo-web files owned by djgzo
chown -R $DJIGZO_GROUP:$DJIGZO_USER $DJIGZO_WEB_HOME

# add djigzo dir in etc and create soft links
mkdir -p /etc/djigzo
ln -s $DJIGZO_WEB_HOME/conf/djigzo-web.properties /etc/djigzo/
    
finish_message

exit 0

%postun
#!/bin/bash -e
# postrm script for djigzo
#

# set a sane/secure path
PATH='/bin:/usr/bin:/sbin:/usr/sbin'
# it's almost certainly already marked for export, but make sure
export PATH

# remove all aliases (start with \ to prevent unalias from being aliased)
\unalias -a

# clean command hash
hash -r

# set a sane/secure IFS (note this is bash & ksh93 syntax only--not portable!)
IFS=$' \t\n'

# only continue if last instance is removed
if [ -z $1 ] || [ $1 != '0' ]; then
  exit 0
fi

rm /etc/djigzo/djigzo-web.properties

exit 0

%description

Djigzo email encryption gateway is an email server (MTA) that encrypts and decrypts
your incoming and outgoing email. Djigzo currently supports two encryption standards: 
S/MIME and PDF encryption. S/MIME provides authentication, message integrity and 
non-repudiation (using X.509 certificates) and protection against message 
interception. S/MIME uses public key encryption (PKI) for encryption and signing. 

%files
%dir "/usr/share/djigzo-web/"
%dir "/usr/share/djigzo-web/conf"
%dir "/usr/share/djigzo-web/conf/jetty"
%dir "/usr/share/djigzo-web/licenses"
%dir "/usr/share/djigzo-web/ssl"

%config "/usr/share/djigzo-web/conf/djigzo-web.properties"

"/usr/share/djigzo-web/djigzo-portal.war"
"/usr/share/djigzo-web/djigzo.war"
"/usr/share/djigzo-web/LIBRARIES-WEB.txt"
"/usr/share/djigzo-web/LICENSE.txt"
"/usr/share/djigzo-web/conf/jetty/djigzo-jetty-context.xml"
"/usr/share/djigzo-web/conf/jetty/djigzo-jetty-ssl.xml"
"/usr/share/djigzo-web/conf/tomcat/server.xml"
"/usr/share/djigzo-web/conf/tomcat/server-T6.xml"
"/usr/share/djigzo-web/licenses/agpl.txt"
"/usr/share/djigzo-web/licenses/apache-v2.txt"
"/usr/share/djigzo-web/licenses/bouncycastle.txt"
"/usr/share/djigzo-web/licenses/bsd.txt"
"/usr/share/djigzo-web/licenses/cddl.txt"
"/usr/share/djigzo-web/licenses/cpl.txt"
"/usr/share/djigzo-web/licenses/objectweb.txt"
"/usr/share/djigzo-web/licenses/ognl.txt"
"/usr/share/djigzo-web/licenses/slf4j.txt"
"/usr/share/djigzo-web/ssl/sslCertificate.p12"

