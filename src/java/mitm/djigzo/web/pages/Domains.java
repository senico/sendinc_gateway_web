/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.pages;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import mitm.application.djigzo.UserLocality;
import mitm.application.djigzo.admin.FactoryRoles;
import mitm.common.ws.WebServiceCheckedException;
import mitm.djigzo.web.beans.DomainBean;
import mitm.djigzo.web.entities.DomainManager;
import mitm.djigzo.web.grid.DomainGridDataSource;

import org.apache.commons.lang.StringUtils;
import org.apache.tapestry5.Asset;
import org.apache.tapestry5.Block;
import org.apache.tapestry5.ComponentResources;
import org.apache.tapestry5.PersistenceConstants;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Path;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.beaneditor.BeanModel;
import org.apache.tapestry5.corelib.components.Grid;
import org.apache.tapestry5.grid.ColumnSort;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.json.JSONObject;
import org.apache.tapestry5.services.BeanModelSource;
import org.apache.tapestry5.services.Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.annotation.Secured;

@IncludeStylesheet("context:styles/pages/domains.css")
public class Domains
{
	private static final Logger logger = LoggerFactory.getLogger(Domains.class);
	
	private final static String DOMAIN_COLUMN = "domain";
	
	@Inject
	private DomainManager domainManager;
	
	@Inject
	private Request request;
	
	/*
	 * The domain currently being drawn by the grid (is set by the grid component)
	 */
	@Property
	private DomainBean domain;
	
	/*
	 * Set of the selected domains
	 */
	@Persist
	private Set<String> selected;
	
	/*
	 * True if an error occurred deleting a domain
	 */
	@Persist(PersistenceConstants.FLASH)
	private boolean deletionError;
	
	@Component(parameters = {"source = gridDataSource", "model = model", "row = domain", "volatile = true", 
			"reorder = select,delete,domain"})
    private Grid grid;

	@Inject
    private BeanModelSource beanModelSource;

    @Inject
    private ComponentResources resources;

    @SuppressWarnings("unused")
	@Inject
    @Path("context:icons/cross.png")
    @Property
    private Asset deleteAsset;

    @SuppressWarnings("unused")
	@Inject
    @Path("context:icons/internal.png")
    @Property
    private Asset internalDomainAsset;

	@Inject
	private Block deletionErrorBlock;
    
    @SetupRender
    public void setupGrid() 
    {
        if (selected == null) {
            selected = Collections.synchronizedSet(new HashSet<String>());
        }
        else {
            selected.clear();
        }
    	
    	/*
    	 * set initial sorting if sorting is not yet set
    	 */
    	if (grid.getSortModel().getColumnSort(DOMAIN_COLUMN) == ColumnSort.UNSORTED) 
    	{
    		grid.getSortModel().updateSort(DOMAIN_COLUMN);
    	}
    }
	
	/*
	 * Returns true if the domain is in use (called by the template to hide the delete icon when
	 * the domain is in use)
	 */
	public boolean isInUse()
	throws WebServiceCheckedException
	{
		return domain.isInUse();
	}
	
	public boolean isInternalDomain()
	throws WebServiceCheckedException 
	{
		return domain.getUserLocality() == UserLocality.INTERNAL;
	}
	
    public BeanModel<DomainBean> getModel() 
    {
        BeanModel<DomainBean> model = beanModelSource.createDisplayModel(DomainBean.class, resources.getMessages());
        
        model.exclude("userLocality", "inUse");
        model.add("delete", null);
        model.add("select", null);
        
        return model;
    }
        
	public boolean getSelect() {
		return selected.contains(domain.getDomain());
	}
	
	public void setSelect(boolean value) 
	{
		/*
		 * Ignored. Checkbox values will be handled using EventCheckbox (see onEventActionLink)
		 */
	}
    	
    @OnEvent(component = "deleteDomain")
	@Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_DOMAIN_MANAGER})
    protected void deleteDomain(String domain) 
    {
    	try {
			domainManager.deleteDomain(domain);
		} 
    	catch (WebServiceCheckedException e) 
    	{
    		logger.error("Error deleting domain " + domain, e);
    		
    		deletionError = true;
		}
    }
    
	public DomainGridDataSource getGridDataSource()	{
		return new DomainGridDataSource(domainManager);
	}
	
    /*
     * Event handler called when the checkbox is clicked. 
     */
	@Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_DOMAIN_MANAGER})
    public void onCheckboxEvent()
    {
    	String data = request.getParameter("data");

    	if (StringUtils.isEmpty(data)) {
    		throw new IllegalArgumentException("data is missing.");
    	}
    	
    	JSONObject json = new JSONObject(data);
    	    	    	
    	String element = json.getString("element");
    	boolean checked = json.getBoolean("checked"); 
    	
    	if (checked) {
    		selected.add(element);
    	}
    	else {
    		selected.remove(element);
    	}
    }
    
	@Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_DOMAIN_MANAGER})
    @OnEvent(component = "deleteSelected")
    protected void deleteSelected() 
    {
        /*
         * Note: we need to clone the set to make sure that concurrent modifications
         * are possible.
         */
        String[] domains = selected.toArray(new String[]{});

        for (String domain : domains) 
	    {
	        try {
	            domainManager.deleteDomain(domain);
	        } 
	        catch (WebServiceCheckedException e) 
	        {
	            logger.error("Error deleting domain " + domain, e);

	            deletionError = true;
	        }
	    }
    }
    
    public Block getDeletionErrorBlock()
    {
    	/*
    	 * Only return the error block if an error actually occurred
    	 */
    	return deletionError ? deletionErrorBlock : null;
    }
}
