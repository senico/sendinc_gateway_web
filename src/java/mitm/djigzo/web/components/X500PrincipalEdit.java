/*
 * Copyright (c) 2009-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.components;

import java.util.Locale;

import mitm.common.mail.EmailAddressUtils;
import mitm.djigzo.web.common.StaticValidationConstraintGenerator;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.tapestry5.ComponentResources;
import org.apache.tapestry5.FieldValidator;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.corelib.components.TextField;
import org.apache.tapestry5.internal.services.FieldValidatorDefaultSourceImpl;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.FieldValidatorDefaultSource;
import org.apache.tapestry5.services.FieldValidatorSource;
import org.apache.tapestry5.services.ValidationConstraintGenerator;

public class X500PrincipalEdit
{
    @Inject 
    private Locale locale;
    
    @Inject
    private ComponentResources resources;

    @Inject
    private FieldValidatorSource fieldValidatorSource;

    @Persist
    private boolean more;

    @Parameter
    private boolean emailRequired;

    @Parameter
    private boolean emailReadOnly;
    
    private String email;

    private String organisation;
    
    private String commonName;

    private String firstName;
    
    private String lastName;
    
    public FieldValidator<?> getEmailValidator()
    {
        String emailValidator = "maxlength=255, emailmitm=1";
        
        if (emailRequired) {
            emailValidator = emailValidator + ", required";
        }
        
        ValidationConstraintGenerator constraintGenerator = new StaticValidationConstraintGenerator(emailValidator);
        
        FieldValidatorDefaultSource fieldValidatorDefaultSource = new FieldValidatorDefaultSourceImpl(
                constraintGenerator, fieldValidatorSource);
        
        return fieldValidatorDefaultSource.createDefaultValidator(emailField, resources.getId(), 
                resources.getContainerMessages(), locale, null, null);
    }
    
    @Component(id = "email", parameters = {"value = email", "validate = prop:emailValidator", "disabled=prop:emailReadOnly"})
    private TextField emailField;

    @SuppressWarnings("unused")
    @Component(id = "commonName", parameters = {"value = commonName", "validate = required, maxlength = 255, regexp"})
    private TextField commonNameField;
    
    @SuppressWarnings("unused")
    @Component(id = "organisation", parameters = {"value = organisation", "validate = maxlength = 255, regexp"})
    private TextField organisationField;

    @SuppressWarnings("unused")
    @Component(id = "firstName", parameters = {"value = firstName", "validate = maxlength = 255, regexp"})
    private TextField firstNameField;

    @SuppressWarnings("unused")
    @Component(id = "lastName", parameters = {"value = lastName", "validate = maxlength = 255, regexp"})
    private TextField lastNameField;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = EmailAddressUtils.canonicalizeAndValidate(email, true);
    }

    public String getOrganisation() {
        return organisation;
    }

    public void setOrganisation(String organisation) {
        this.organisation = organisation;
    }

    public String getCommonName() {
        return commonName;
    }

    public void setCommonName(String commonName) {
        this.commonName = commonName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public boolean isMore() {
        return more;
    }

    public void setMore(boolean more) {
        this.more = more;
    }

    public boolean isEmailRequired() {
        return emailRequired;
    }

    public void setEmailRequired(boolean emailRequired) {
        this.emailRequired = emailRequired;
    }

    public boolean isEmailReadOnly() {
        return emailReadOnly;
    }

    public void setEmailReadOnly(boolean readOnly) {
        this.emailReadOnly = readOnly;
    }
    
    @Override
    public boolean equals(Object other)
    {
        if (other == null) { return false; }
        if (other == this) { return true; }

        if (!(other instanceof X500PrincipalEdit)) {
            return false;
        }
        
        X500PrincipalEdit rhs = (X500PrincipalEdit) other;

        return new EqualsBuilder()
                      .append(email, rhs.email)
                      .append(organisation, rhs.organisation)
                      .append(commonName, rhs.commonName)
                      .append(firstName, rhs.firstName)
                      .append(lastName, rhs.lastName)
                      .isEquals();
    }

    @Override
    public int hashCode()
    {
        return new HashCodeBuilder()
            .append(email)
            .append(organisation)
            .append(commonName)
            .append(firstName)
            .append(lastName).toHashCode();
    }
}
