/*
 * Copyright (c) 2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.beans;

import mitm.application.djigzo.relay.RelayBounceMode;
import mitm.common.properties.HierarchicalPropertiesException;

/**
 * Represents the Mobile properties
 * 
 * @author Martijn Brinkers
 *
 */
public interface MobilePropertiesBean
{
	/**
	 * If true, the recipient will be considered to be a blackberry recipient
	 */
	public BooleanProperty getBlackberryRecipient();

    /**
     * If true, unsupported message formats are stripped
     */
    public BooleanProperty getStripUnsupportedFormats();

    /**
     * If true, the sender is allowed to relay mail from a Blackberry
     */
    public BooleanProperty getRelayAllowed();

    /**
     * The email address to which email from a BlackBerry should be sent for 
     * relaying through Djigzo.
     */
    public StringProperty getRelayEmail();
        
    /**
     * The number of minutes a BlackBerry relay email is valid
     */
    public LongProperty getRelayValidityInterval();
    
    /**
     * The bounce mode for a BlackBerry relay message
     */
    public EnumProperty<RelayBounceMode> getRelayBounceMode();
	
	/**
	 * Saves (makes persistent) the user properties
	 */
	public void save()
	throws HierarchicalPropertiesException;
}
