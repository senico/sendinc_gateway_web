/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.pages.domain;

import mitm.application.djigzo.admin.FactoryRoles;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.util.DomainUtils;
import mitm.common.util.DomainUtils.DomainType;
import mitm.common.ws.WebServiceCheckedException;
import mitm.djigzo.web.entities.DomainManager;
import mitm.djigzo.web.pages.Domains;

import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.InjectPage;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.beaneditor.Validate;
import org.apache.tapestry5.corelib.components.Form;
import org.apache.tapestry5.corelib.components.TextField;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.springframework.security.annotation.Secured;

@IncludeStylesheet("context:styles/pages/domain/domainAdd.css")
@Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_DOMAIN_MANAGER})
public class DomainAdd
{
	@Inject
	private DomainManager domainManager;
	
	@Component
    private Form form;
	
	@InjectPage
	private DomainEdit domainEdit;
	
	@Property
	@Validate("required,maxlength=255,domain")
	private String domain;
	
	@Component(id = "domain", parameters = {"value = domain"})
	private TextField domainField;

	@SetupRender
	@Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_DOMAIN_MANAGER})
	protected void setupRender() {
		/*
		 * Empty on purpose
		 */
	}
	
	public void onValidateFromDomain(String domain)
	throws WebServiceCheckedException
	{
		String filteredDomain = DomainUtils.canonicalizeAndValidate(domain, DomainType.WILD_CARD);

		/* should already be handled by the validator */
		assert(filteredDomain != null);
		
		if (domainManager.isDomain(filteredDomain))
		{
            form.recordError(domainField, "The domain " + filteredDomain + 
            		" already exists.");
            
            return;
		}		
	}
	
	public Object onSuccess()
	throws HierarchicalPropertiesException, WebServiceCheckedException 
	{
		String filteredDomain = DomainUtils.canonicalizeAndValidate(domain, DomainType.WILD_CARD);

		assert(filteredDomain != null);
		
		domainManager.addDomain(filteredDomain);
		
		domainEdit.setDomain(filteredDomain);
		
		return domainEdit;
	}
	
    /*
     * Event handler called when the cancel button is pressed.
     */
	public Object onCancel() {
		return Domains.class;
	}
}
