/*
 * Copyright (c) 2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.common.security;

import org.apache.commons.lang.StringUtils;
import org.springframework.security.GrantedAuthority;
import org.springframework.security.userdetails.UserDetails;

/**
 * Extension of UserDetails used for loggin into the portal
 * 
 * @author Martijn Brinkers
 *
 */
public class PortalUserDetails implements UserDetails
{
    private static final long serialVersionUID = -6282307672355208103L;

    /*
     * The username (for a portal user this should be an email address)
     */
    private final String username;

    /*
     * The (possibly hashed) password
     */
    private final String password;

    /*
     * The salt used to hash the password with
     */
    private final String salt;
    
    /*
     * The authorites the user is granted
     */
    private final GrantedAuthority[] authorities;

    /*
     * If true, the account is enabled
     */
    private final boolean enabled;
    
    PortalUserDetails(String username, String password, String salt, GrantedAuthority[] authorities, 
            boolean enabled)
    {
        this.username = username;
        this.password = password;
        this.salt = salt;
        this.authorities = authorities;
        this.enabled = enabled;
    }
    
    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public String getPassword() {
        return password;
    }

    public String getSalt() {
        return salt;
    }
    
    @Override
    public GrantedAuthority[] getAuthorities() {
        return authorities;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }        
    
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }
    
    @Override
    public String toString() {
        return StringUtils.defaultString(username);
    }    
}