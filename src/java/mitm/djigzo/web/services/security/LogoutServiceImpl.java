/*
 * Copyright 2007 Ivan Dubrov
 * Copyright 2007 Robin Helgelin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package mitm.djigzo.web.services.security;

import java.util.List;

import mitm.djigzo.web.services.LogoutService;
import mitm.djigzo.web.utils.ASOUtils;

import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.RequestGlobals;
import org.springframework.security.Authentication;
import org.springframework.security.context.SecurityContextHolder;
import org.springframework.security.ui.logout.LogoutHandler;

/**
 * @author Ivan Dubrov
 * 
 * Martijn Brinkers
 * 	- 15/07/08 Updated to make it compatible with Spring security and Tapestry 5.0.13
 */
public class LogoutServiceImpl implements LogoutService 
{
    private List<LogoutHandler> handlers;
    private RequestGlobals requestGlobals;

    public LogoutServiceImpl(final List<LogoutHandler> handlers,
            @Inject final RequestGlobals requestGlobals) 
    {
        this.handlers = handlers;
        this.requestGlobals = requestGlobals;
    }

    @Override
    public final void logout() 
    {
        /*
         * This is a dirty workaround for https://issues.apache.org/jira/browse/TAP5-413 when running
         * in Tomcat.
         * 
         * SessionApplicationStatePersistenceStrategy needs to access the session to store the
         * ASOs but this results in the following exception:
         * 
         * java.lang.IllegalStateException: Cannot create a session after the response has been committed
         */
        ASOUtils.clearASOs(requestGlobals.getHTTPServletRequest());
        
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        
        for (LogoutHandler handler : handlers) 
        {
            handler.logout(requestGlobals.getHTTPServletRequest(), 
            		requestGlobals.getHTTPServletResponse(), auth);
        }
    }
}
