/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.common.security;

import java.util.List;

import mitm.common.hibernate.SortDirection;
import mitm.common.ws.WebServiceCheckedException;

public interface AdminManager 
{
	/**
	 * Returns a set of admins. 
	 */
	public List<Admin> getAdmins(Integer firstResult, Integer maxResults, SortDirection sortDirection)
	throws WebServiceCheckedException;

	/**
	 * Adds an Authority. The Admin with username should be non existing otherwise a RuntimeException
	 * can be thrown (the type of exception depends on the implementation and can be for example 
	 * org.hibernate.exception.ConstraintViolationException). The admin is disabled. Use the Admin
	 * object to set the password etc. and enable the admin
	 */
	public Admin addAdmin(String username, boolean builtIn)
	throws WebServiceCheckedException;

	/**
	 * Returns the Admin. Returns null if admin with username does not exist.
	 */
	public Admin getAdmin(String username)
	throws WebServiceCheckedException;

	/**
	 * Deletes the Admin with the given name. Returns true if deleted.
	 */
	public boolean deleteAdmin(String username)
	throws WebServiceCheckedException;
	
	/**
	 * Deletes all Admins
	 */
	public void deleteAll()
	throws WebServiceCheckedException;
	
	/**
	 * Returns the total number of admins.
	 */
	public int getAdminCount()
	throws WebServiceCheckedException;
}
