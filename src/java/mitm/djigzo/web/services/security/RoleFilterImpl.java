/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.services.security;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import mitm.common.util.Check;

import org.apache.tapestry5.internal.services.RequestPageCache;
import org.apache.tapestry5.internal.structure.Page;
import org.apache.tapestry5.services.ComponentClassResolver;
import org.apache.tapestry5.services.ComponentEventRequestHandler;
import org.apache.tapestry5.services.ComponentEventRequestParameters;
import org.springframework.security.ConfigAttribute;
import org.springframework.security.ConfigAttributeDefinition;
import org.springframework.security.SecurityConfig;
import org.springframework.security.annotation.Secured;
import org.springframework.security.intercept.InterceptorStatusToken;

/**
 * ComponentEventRequestFilter that checks if the page contains a @Secured class annotation and if it does
 * it checks whether the user has the required roles to access the page
 * 
 * Note: This filter only protects actions (like component actions and posts). It does not protect
 * a page render request but that should not be problematic because a page render should not
 * have any side effects just render the page.
 * 
 * @author Martijn Brinkers
 *
 */
public class RoleFilterImpl implements RoleFilter
{
	private final RequestPageCache requestPageCache;
	private final ComponentClassResolver componentClassResolver;
	private final SecurityChecker securityChecker;
	
	public RoleFilterImpl(RequestPageCache requestPageCache, ComponentClassResolver componentClassResolver, 
			SecurityChecker securityChecker)
	{
		Check.notNull(requestPageCache, "requestPageCache");
		Check.notNull(componentClassResolver, "componentClassResolver");
		Check.notNull(securityChecker, "securityChecker");
		
		this.requestPageCache = requestPageCache;
		this.componentClassResolver = componentClassResolver;
		this.securityChecker = securityChecker;
	}
	
    @Override
	public void handle(ComponentEventRequestParameters parameters, 
			ComponentEventRequestHandler handler)
	throws IOException 
	{
        Page page = requestPageCache.get(parameters.getActivePageName());

        try {
	        checkRequiredRoles(page); 
        }
        catch(ClassNotFoundException e) {
        	throw new IOException(e);
        }
        
        /*
         * User has required role
         */
        handler.handle(parameters);
	}
	
	private void checkRequiredRoles(Page page)
	throws ClassNotFoundException
	{
		String className = componentClassResolver.resolvePageNameToClassName(page.getLogicalName());
		
		Class<?> clazz = Class.forName(className);
		
		Secured securedAnnotation = clazz.getAnnotation(Secured.class);
		
		if (securedAnnotation != null)
		{
	    	List<ConfigAttribute> configAttributes = new LinkedList<ConfigAttribute>();

	    	for (String auth : securedAnnotation.value())
	    	{
	    		configAttributes.add(new SecurityConfig(auth));
	        }
	        
	        ConfigAttributeDefinition configAttributeDefinition = new ConfigAttributeDefinition(configAttributes);
	        
	        InterceptorStatusToken token = null;
	        
	        try {
	        	 token = securityChecker.checkBefore(configAttributeDefinition);
	        }
	        finally {
	        	securityChecker.checkAfter(token, null);
	        }
		}
	}
}
