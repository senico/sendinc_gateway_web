/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.pages.dlp.patterns;

import java.util.List;

import mitm.application.djigzo.admin.FactoryRoles;
import mitm.application.djigzo.ws.PolicyPatternManagerWS;
import mitm.application.djigzo.ws.PolicyPatternNodeDTO;
import mitm.application.djigzo.ws.PolicyPatternNodeWS;
import mitm.common.ws.WebServiceCheckedException;
import mitm.djigzo.web.components.DLPPatternGrid;

import org.apache.tapestry5.ComponentResources;
import org.apache.tapestry5.PersistenceConstants;
import org.apache.tapestry5.ValidationException;
import org.apache.tapestry5.annotations.Cached;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.corelib.components.TextField;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.ioc.annotations.Value;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.annotation.Secured;

@Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_DLP_MANAGER})
@IncludeStylesheet("context:styles/pages/dlp/patterns/group.css")
public class PatternsGroup
{
    private final static Logger logger = LoggerFactory.getLogger(PatternsGroup.class);
    
    @Inject
    private PolicyPatternManagerWS policyPatternManagerWS;

    @Inject
    private PolicyPatternNodeWS policyPatternNodeWS;
    
    @Inject
    private ComponentResources resources;
    
    @Inject
    @Value("${dlp.patterns.rowsPerPage}")
    private int rowsPerPage;    
    
    /*
     * The name of this group
     */
    private String name;

    /*
     * Will be set if a PolicyPatternNode is being edited
     */
    private PolicyPatternNodeDTO policyPatternNode;

    /*
     * True if the group was added
     */
    @Persist(PersistenceConstants.FLASH)
    private boolean added;
 
    /*
     * True if an error has occurred
     */
    @Persist(PersistenceConstants.FLASH)
    private boolean error;

    /*
     * Error message if an error has occurred
     */
    @Persist(PersistenceConstants.FLASH)
    private String errorMessage;

    @SuppressWarnings("unused")
    @Component(id = "name", parameters = {"value = name", "validate = required"})
    private TextField nameField;
    
    @Component(parameters = {"source = source", "rowsPerPage = prop:rowsPerPage"})
    private DLPPatternGrid patternGrid; 
    
    @SetupRender
    @Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_DLP_MANAGER})
    public void setupRender() {
        /*
         * Empty on purpose
         */
    }
    
    @Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_DLP_MANAGER})
    public Object onActivate(String name)
    throws WebServiceCheckedException 
    {
        policyPatternNode = policyPatternManagerWS.getPattern(name);
        
        if (policyPatternNode == null) {
            return PatternsView.class;
        }
     
        this.name = policyPatternNode.getName();
        
        return null;
    }
    
    public String onPassivate() {
        return policyPatternNode != null ? policyPatternNode.getName() : null;
    }
    
    private List<PolicyPatternNodeDTO> getSelectedNodes()
    throws WebServiceCheckedException
    {
        return policyPatternNode != null ? policyPatternNodeWS.getChilds(
                policyPatternNode.getName()) : null;
    }
    
    @Cached
    public Object getSource()
    throws WebServiceCheckedException
    {
        return getSelectedNodes(); 
    }
    
    public void onValidateFromName(String name)
    throws ValidationException, WebServiceCheckedException
    {
        if (name != null && !isEditing())
        {
            if (policyPatternManagerWS.getPattern(name) != null) {
                throw new ValidationException("A pattern with this name already exists.");
            }
        }
    }
    
    public Object onSuccess()
    {
        Object result = null;

        if (!isEditing())
        {
            try {
                PolicyPatternNodeDTO node = !isEditing() ? 
                        policyPatternManagerWS.createPattern(name) : policyPatternNode;
                
                if (node != null) {
                    /*
                     * Redirect to the same page if adding new group but now 
                     * with the name as parameter
                     */
                    if (!isEditing())
                    {
                        result = resources.createPageLink(PatternsGroup.class, false, name);
                        
                        added = true;
                    }
                    else {
                        result = PatternsView.class;
                    }
                }
            }
            catch(WebServiceCheckedException e)
            {
                logger.error("Error during submit.", e);
                
                errorMessage = e.getMessage();
                error = true;
            }
        }
        
        return result;
    }
    
    /*
     * Event handler for delete event from the DLPPatternGrid
     */
    @OnEvent(component = "patternGrid", value = DLPPatternGrid.DELETE_PATTERN_EVENT)
    @Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_DLP_MANAGER})
    protected void removePattern(String name) 
    {
        if (policyPatternNode == null || name == null) {
            return;
        }
        
        try {
            policyPatternNodeWS.removeChild(policyPatternNode.getName(), name);
        }
        catch(WebServiceCheckedException e)
        {
            logger.error("Error removing pattern: " + name, e);

            errorMessage = e.getMessage();
            error = true;
        }
    }
    
    @OnEvent(component = "removeSelected")
    @Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_DLP_MANAGER})
    protected void removeSelectedPatterns()
    {
        /*
         * Note: we need to clone the set to make sure that concurrent modifications
         * are possible.
         */
        String[] selected = patternGrid.getSelected().toArray(new String[]{});
        
        for (String name : selected)
        {
            try {
                policyPatternNodeWS.removeChild(policyPatternNode.getName(), name);
            }
            catch(WebServiceCheckedException e)
            {
                logger.error("Error removing pattern: " + name, e);

                errorMessage = e.getMessage();
                error = true;
            }
        }
    }
    
    /*
     * Event handler called when the cancel button is pressed.
     */
    protected Object onCancel() {
        return PatternsView.class;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isEditing() {
        return policyPatternNode != null;
    }

    public boolean isError() {
        return error;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public int getRowsPerPage() {
        return rowsPerPage;
    }

    public boolean isAdded() {
        return added;
    }
}
