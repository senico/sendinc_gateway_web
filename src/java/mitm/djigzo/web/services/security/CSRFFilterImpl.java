/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.services.security;

import java.io.IOException;

import mitm.common.util.Check;
import mitm.djigzo.web.asos.CSRF;

import org.apache.tapestry5.Link;
import org.apache.tapestry5.internal.services.LinkFactory;
import org.apache.tapestry5.internal.services.LinkFactoryListener;
import org.apache.tapestry5.internal.services.RequestPageCache;
import org.apache.tapestry5.internal.structure.Page;
import org.apache.tapestry5.services.ApplicationStateManager;
import org.apache.tapestry5.services.ComponentEventRequestHandler;
import org.apache.tapestry5.services.ComponentEventRequestParameters;
import org.apache.tapestry5.services.Request;
import org.apache.tapestry5.services.Response;

/**
 * ComponentEventRequestFilter that checks if the request contains the correct secureID. Without
 * a secureID the application is vulnerable to 'Cross Site Request Forgeries' (CSRF).
 * 
 * Note: This filter only protects actions (like component actions and posts). It does not protect
 * a page render request but that should not be problematic because a page render should not
 * have any side effects just render the page.
 * 
 * @author Martijn Brinkers
 *
 */
public class CSRFFilterImpl implements CSRFFilter, LinkFactoryListener
{
	public static String CSRF_SECURITY_PARAMETER = "sid";

	private final ApplicationStateManager asm;
	private final Request request;
	private final Response response;
	private final LinkFactory linkFactory;
	private final RequestPageCache requestPageCache;
	
	/*
	 * The page to redirect to when secureID is incorrect
	 */
	private final String redirectTo;
	
	public CSRFFilterImpl(ApplicationStateManager asm, Request request, Response response, 
			LinkFactory linkFactory, RequestPageCache requestPageCache, String redirectTo)
	{
		Check.notNull(asm, "asm");
		Check.notNull(request, "request");
		Check.notNull(response, "response");
		Check.notNull(linkFactory, "linkFactory");
		Check.notNull(requestPageCache, "requestPageCache");
		Check.notNull(redirectTo, "redirectTo");
		
		this.asm = asm;
		this.request = request;
		this.response = response;
		this.linkFactory = linkFactory;
		this.requestPageCache = requestPageCache;
		this.redirectTo = redirectTo;
	}
	
	private String getSecurityCode() {
		return asm.get(CSRF.class).getSecurityCode();
	}
	
    @Override
	public void createdComponentEventLink(Link link)
	{
		link.addParameter(CSRF_SECURITY_PARAMETER, getSecurityCode());
	}

    @Override
	public void createdPageRenderLink(Link link)
	{
		/*
		 * Page links do not need the SecureID because pages should only show the page and
		 * not have any side effects.
		 */
	}

    @Override
	public void handle(ComponentEventRequestParameters parameters, 
			ComponentEventRequestHandler handler)
	throws IOException 
	{
        Page page = requestPageCache.get(parameters.getActivePageName());

        if (isCSRFProtected(page)) 
        {
        	/*
        	 * Check if the secureID is equal to the secureID in the session
        	 */
        	String secureID = request.getParameter(CSRF_SECURITY_PARAMETER);
        	
        	CSRF csrf = null;
        	
        	/*
        	 * Do not create ASO if it does not exist
        	 */
        	if (asm.exists(CSRF.class))	{
        		csrf = asm.get(CSRF.class);
        	}
        	
        	if (secureID == null || csrf == null || !csrf.getSecurityCode().equals(secureID)) 
        	{
        		/*
        		 * Illegal secure ID so redirect
        		 */
                Link link = linkFactory.createPageRenderLink(redirectTo, false);

                response.sendRedirect(link);
                
                return;
        	}
        }
        
        /*
         * Not protected or secureID is correct so continue
         */
        handler.handle(parameters);
	}
	
	private boolean isCSRFProtected(Page page)
	{
		/*
		 * For now all actions are protected. We can always create a special Annotation when
		 * we need to specify which pages/actions are protected.
		 */
		return true;
	}
}
