/*
 * Copyright (c) 2009-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.pages.ca;

import java.util.Set;

import mitm.application.djigzo.admin.FactoryRoles;
import mitm.application.djigzo.ws.CACertStoreViewFilter;
import mitm.application.djigzo.ws.CAWS;
import mitm.application.djigzo.ws.X509CertificateDTO;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.security.certificate.KeyUsageType;
import mitm.common.ws.WebServiceCheckedException;
import mitm.djigzo.web.beans.CertificateType;
import mitm.djigzo.web.beans.X509CertificateBean;
import mitm.djigzo.web.beans.impl.X509CertificateBeanImpl;
import mitm.djigzo.web.components.CACertificateSelect;

import org.apache.commons.lang.StringUtils;
import org.apache.tapestry5.ComponentResources;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.annotation.Secured;

@Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_PKI_MANAGER})
@IncludeStylesheet("context:styles/pages/ca/caCRLSelect.css")
public class CACRLSelect
{    
    private final static Logger logger = LoggerFactory.getLogger(CACRLSelect.class);
    
    @Inject
    private CAWS caWS;

    @Inject
    private ComponentResources resources;
    
    @Component(parameters = {"selectedCertificate = prop:selectedCertificate", "filter=prop:filter"})
    private CACertificateSelect certificateSelect;
    
    @SetupRender
    @Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_PKI_MANAGER})
    protected void setupRender()
    {
        /*
         * Empty on purpose
         */
    }

    public CACertStoreViewFilter getFilter()
    {
        /*
         * We only want CAs with a CRLSign key usage
         */
        return CACertStoreViewFilter.CRL_SIGN;
    }
    
    public X509CertificateBean getSelectedCertificate()
    throws HierarchicalPropertiesException, WebServiceCheckedException
    {
        X509CertificateBean selectedCertificate = null;
        
        X509CertificateDTO certificateDTO = caWS.getActiveCA();
        
        /*
         * We will only use the selected certificate if it can be used to sign CRLs with
         */
        if (certificateDTO != null) 
        {
            Set<String> keyUsage = certificateDTO.getKeyUsage();
            
            if (keyUsage == null || keyUsage.contains(KeyUsageType.CRLSIGN.toString()))
            {
                selectedCertificate = new X509CertificateBeanImpl(certificateDTO, 
                        CertificateType.SELECTED);
            }
            else {
                logger.warn("The CA certificate is not allowed to create CRLs.");
            }
        }
        else {
            logger.warn("select CA certificate with not found.");
        }
        
        return selectedCertificate;
    }

    public Object onSuccess()
    {
        Object result = null;
        
        String thumbprint = certificateSelect.getSelected();
        
        if (StringUtils.isNotBlank(thumbprint))
        {
            result = resources.createPageLink(CACRLCreate.class, false, 
                    certificateSelect.getSelected(), false);
        }
        
        return result;
    }
    
    /*
     * Event handler called when the cancel button is pressed.
     */
    protected Object onCancel() {
        return CAIssue.class;
    }    
}
