/*
 * Copyright (c) 2009-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.utils;

import java.io.IOException;
import java.util.Map;
import java.util.Properties;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * ServletContextListener imports a property file into the system properties. The properties are imported
 * into the system properties if the system property is not already set. The property file read is 
 * retrieved from a context-param with name "djigzo.systemproperties.default".
 * 
 * Example:
 * 
 *   &lt;context-param&gt;
 *       &lt;param-name&gt;djigzo.systemproperties.default&lt;/param-name&gt;
 *       &lt;param-value&gt;
 *           /WEB-INF/djigzo-system.properties
 *       &lt;/param-value&gt;
 *   &lt;/context-param&gt;
 * 
 * @author Martijn Brinkers
 *
 */
public class SetDefaultSystemPropertiesServletContextListener implements ServletContextListener
{
    private final static Logger logger = LoggerFactory.getLogger(SetDefaultSystemPropertiesServletContextListener.class);

    @Override
    public void contextInitialized(ServletContextEvent contextEvent)
    {
        String propertiesParam = contextEvent.getServletContext().getInitParameter("djigzo.system.properties");

        if (propertiesParam != null)
        {
            Properties properties = new Properties();
            
            try {
                properties.load(IOUtils.toInputStream(propertiesParam, "UTF-8"));
                
                for (Map.Entry<Object, Object> entry : properties.entrySet())
                {
                    if (System.getProperty((String) entry.getKey()) == null) {
                        System.setProperty((String) entry.getKey(), (String) entry.getValue());
                    }
                }
            }
            catch (IOException e) {
                logger.error("Error loading properties.", e);
            }
        }
        else {
            logger.warn("property djigzo.systemproperties.default is missing.");
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent contextEvent) {
        // empty on purpose
    }
}
