var CheckboxEvent = Class.create();

CheckboxEvent.prototype = 
{
    initialize: function(elementID, event, linkURL, onCompleteCallback)
    {
        this.elementID = elementID;
        this.event = event;
		this.linkURL = linkURL;

        this.onCompleteCallback = null;

        if (onCompleteCallback.length > 0) {
            this.onCompleteCallback = onCompleteCallback;
        }

        Event.observe($(this.elementID), event, this.onEvent.bindAsEventListener(this));        
    },
    onEvent: function(e)
    {
        var json = Object.toJSON({element: this.elementID, checked: ($(this.elementID).getValue() == null ? "false" : "true")});
        
        new Ajax.Request(this.linkURL,
        {
            method: 'post',
            parameters: {data: json},
            onFailure: function(t)
            {
            	Tapestry.ajaxError('Error communication with the server.');
            	
            	if (Tapestry.DEBUG_ENABLED) {
            		alert(t.responseText);
            	}
            },
            onException: function(t, exception)
            {
            	Tapestry.ajaxError('Error communication with the server.');

            	if (Tapestry.DEBUG_ENABLED) {
            		alert(exception);
            	}
            },
            onSuccess: function(t)
            {
               if (this.onCompleteCallback != null) {
                    eval(this.onCompleteCallback + "('" + t.responseText + "')");
               }
             }.bind(this)
        });
    }
}