/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.services;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Properties;

import mitm.common.util.ResourceLocator;

import org.apache.tapestry5.ioc.services.SymbolProvider;
import org.apache.tapestry5.ioc.util.CaseInsensitiveMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Tapestry symbol provider which reads the properties from a properties file.
 * 
 * @author Martijn Brinkers
 *
 */
public class PropertiesFileSymbolProvider implements SymbolProvider
{
	private final static Logger logger = LoggerFactory.getLogger(PropertiesFileSymbolProvider.class);
	
    private final Map<String, String> propertiesMap = new CaseInsensitiveMap<String>();

    public PropertiesFileSymbolProvider(String resourceName, String basesystemProperty)
    {
    	ResourceLocator locator = new ResourceLocator();
    	
    	locator.setSystemProperty(basesystemProperty);

    	loadProperties(resourceName, locator);
    }

    public PropertiesFileSymbolProvider(String resourceName, File baseDir)
    {
    	ResourceLocator locator = new ResourceLocator();
    	
    	locator.setBaseDir(baseDir.getPath());

    	loadProperties(resourceName, locator);
    }
    
    private void loadProperties(String resourceName, ResourceLocator locator)
    {
    	try {
	    	InputStream input = locator.getResourceAsStream(resourceName);
	    	
	    	if (input == null) {
	    		throw new IOException("Resource " + resourceName + " not found.");
	    	}
	    	
	    	logger.info("Resource " + resourceName + " found. Loading properties.");
	    	
	        Properties properties = new Properties();
	
	        properties.load(input);
	
	        for(Object key : properties.keySet()) {
	            propertiesMap.put((String) key, properties.getProperty((String) key));
	        }
    	}
    	catch(IOException e) {
    		logger.warn("Properties with resource with name " + resourceName + " not found.", e);
    	}
    }
    
    @Override
    public String valueForSymbol(String name) {
        return propertiesMap.get(name);
    }
}
