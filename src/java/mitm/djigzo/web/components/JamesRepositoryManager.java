/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.components;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import mitm.application.djigzo.james.JamesRepository;
import mitm.application.djigzo.ws.JamesRepositoryManagerWS;
import mitm.application.djigzo.ws.MailDTO;
import mitm.common.ws.WebServiceCheckedException;
import mitm.djigzo.web.grid.JamesRepositoryGridDataSource;

import org.apache.commons.lang.StringUtils;
import org.apache.tapestry5.Asset;
import org.apache.tapestry5.ComponentResources;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.annotations.Path;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.beaneditor.BeanModel;
import org.apache.tapestry5.corelib.components.Grid;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.json.JSONObject;
import org.apache.tapestry5.services.BeanModelSource;
import org.apache.tapestry5.services.Request;

@IncludeStylesheet("context:styles/components/jamesRepositoryManager.css")
public class JamesRepositoryManager
{
    public static final String MAIL_NAME_CLICKED_EVENT = "JamesRepositoryManager-mailName-clicked";
    public static final String MOVE_SELECTED_CLICKED_EVENT = "JamesRepositoryManager-moveSelected-clicked";
    public static final String RESPOOL_SELECTED_CLICKED_EVENT = "JamesRepositoryManager-respoolSelected-clicked";
	
    @Parameter(required = true)
    private JamesRepository repository;
    
    @Parameter(value = "false")
    private boolean showMoveSelected;

    @Parameter(value = "false")
    private boolean showRespoolSelected;
    
	@Inject
	private JamesRepositoryManagerWS repositoryManager;
	
	@Inject
    private BeanModelSource beanModelSource;

    @Inject
    private ComponentResources resources;

    @Inject
    private Request request;
    
	/*
	 * Set of the selected mail names (the ID of the mail item)
	 */
	@Persist
	private Set<String> selected;
    
    /*
     * The mail item currently being rendered
     */
    private MailDTO mailItem;
    
	@SuppressWarnings("unused")
	@Component(id = "jamesRepositoryGrid", parameters = {"source=gridDataSource", "volatile=true", 
			"model=model", "row=mailItem", "reorder=delete,select,name,state," +
					"messageSize,lastUpdated,sender,recipients,remoteAddr,errorMessage"})
    private Grid grid;
	
    @SuppressWarnings("unused")
	@Inject
    @Path("context:icons/cross.png")
    @Property
    private Asset deleteAsset;

    @SetupRender
    public void setupRender() 
    {
        if (selected == null) {
            selected = Collections.synchronizedSet(new HashSet<String>());
        }
        else {
            selected.clear();
        }
    }
    
    public JamesRepositoryGridDataSource getGridDataSource()	{
		return new JamesRepositoryGridDataSource(repositoryManager, repository);
	}

    public BeanModel<MailDTO> getModel() 
    {
        BeanModel<MailDTO> model = beanModelSource.createDisplayModel(MailDTO.class, resources.getMessages());
        
        model.add("recipients", null);
        model.add("delete", null);
        model.add("select", null);
        
        /*
         * Disable all sorting
         */
        for (String column : model.getPropertyNames()) {
        	model.get(column).sortable(false);
        }
        
        return model;
    }

	public MailDTO getMailItem() {
		return mailItem;
	}

	public void setMailItem(MailDTO mailItem) {
		this.mailItem = mailItem;
	}
	
	public Set<String> getSelected() {
		return selected;
	}
	
	public boolean getSelect() {
		return selected.contains(mailItem.getName());
	}
	
	public void setSelect(boolean value) 
	{
		/*
		 * Ignored. Checkbox values will be handled using EventCheckbox (see onEventActionLink)
		 */
	}
	
	public JamesRepository getRepository() {
		return repository;
	}
	
	public boolean isShowMoveSelected() {
		return showMoveSelected;
	}

	public boolean isShowRespoolSelected() {
		return showRespoolSelected;
	}
	
    /*
     * Ajax event handler called when the checkbox is clicked. 
     */
    public void onCheckboxEvent()
    {
    	String data = request.getParameter("data");

    	if (StringUtils.isEmpty(data)) {
    		throw new IllegalArgumentException("data is missing.");
    	}
    	
    	JSONObject json = new JSONObject(data);
    	    	    	
    	String element = json.getString("element");
    	boolean checked = json.getBoolean("checked"); 
    	
    	if (checked) {
    		selected.add(element);
    	}
    	else {
    		selected.remove(element);
    	}
    }
    
    @OnEvent(component="deleteMessage")
    protected void deleteMessage(String mailName) 
    throws WebServiceCheckedException 
    {
    	repositoryManager.removeMail(repository, mailName);
    }
    
    @OnEvent(component="deleteSelected")
    protected void deleteSelected() 
    throws WebServiceCheckedException 
    {
    	if (selected != null) 
    	{
            /*
             * We assume that the id's of the checkboxes are the id's of the items.
             * 
             * Note: we need to clone the set to make sure that concurrent modifications
             * are possible.
             */
            String[] names = selected.toArray(new String[]{});
    	    
    	    for (String mailName : names) {
		        repositoryManager.removeMail(repository, mailName);
			}
    	}
    }

    @OnEvent(component="moveSelected")
    protected void moveSelected() 
    {
		resources.triggerEvent(MOVE_SELECTED_CLICKED_EVENT, null, null);
    }

    @OnEvent(component="respoolSelected")
    protected void respoolSelected() 
    {
		resources.triggerEvent(RESPOOL_SELECTED_CLICKED_EVENT, null, null);
    }
    
    @OnEvent(component="mailNameLink")
    protected void mailNameClicked(String mailName) 
    {
		resources.triggerEvent(MAIL_NAME_CLICKED_EVENT, new Object[]{mailName}, null);
    }
}
