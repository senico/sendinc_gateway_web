/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.components;

import org.apache.commons.lang.math.NumberUtils;
import org.apache.tapestry5.Asset;
import org.apache.tapestry5.BindingConstants;
import org.apache.tapestry5.ComponentResources;
import org.apache.tapestry5.MarkupWriter;
import org.apache.tapestry5.annotations.Environmental;
import org.apache.tapestry5.annotations.Mixin;
import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.corelib.base.AbstractField;
import org.apache.tapestry5.corelib.mixins.RenderDisabled;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.FormSupport;
import org.apache.tapestry5.services.Heartbeat;
import org.apache.tapestry5.services.Request;

/**
 */
public class ImageSubmit extends AbstractField
{
    static final String SELECTED_EVENT = "imageSubmit";

    /*
     * x-coordinate that was clicked on
     */
    private int x;

    /*
     * y-coordinate that was clicked on
     */
    private int y;
    
    /**
     * If true (the default), then any notification sent by the component will be deferred until the end of the form
     * submission (this is usually desirable).
     */
    @Parameter
    private boolean defer = true;

    /**
     * The name of the event that will be triggered if this component is the cause of the form submission. The default
     * is "selected".
     */
    @Parameter(allowNull = false, defaultPrefix = BindingConstants.LITERAL)
    private String event = SELECTED_EVENT;

    @Parameter(required = true, defaultPrefix = BindingConstants.ASSET)
    private Asset src;
    
    @Environmental
    private FormSupport formSupport;

    @Environmental
    private Heartbeat heartbeat;

    @Inject
    private ComponentResources resources;

    @Inject
    private Request request;

    @SuppressWarnings("unused")
    @Mixin
    private RenderDisabled renderDisabled;

    void beginRender(MarkupWriter writer)
    {
        writer.element("input", "type", "image", "name", getControlName(), "id", getClientId(), "src", src.toClientURL());

        resources.renderInformalParameters(writer);
    }

    void afterRender(MarkupWriter writer)
    {
        writer.end();
    }

    @Override
    protected void processSubmission(String elementName)
    {
    	String paramX = request.getParameter(elementName + ".x");
    	String paramY = request.getParameter(elementName + ".y");
    	
    	if (paramX == null || paramY == null) {
    		return;
    	}
    	
        x = NumberUtils.toInt(paramX);
        y = NumberUtils.toInt(paramY);

        final String finalElementName = elementName;
        
        Runnable sendNotification = new Runnable()
        {
            @Override
            public void run()
            {
                resources.triggerEvent(event, new Object[]{x, y, finalElementName}, null);
            }
        };

        if (defer) {
        	formSupport.defer(sendNotification);
        }
        else {
        	heartbeat.defer(sendNotification);
        }
    }
}
