/*
 * Copyright (c) 2009-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.pages.domain;

import java.util.Set;

import mitm.application.djigzo.NamedCertificateCategories;
import mitm.application.djigzo.admin.FactoryRoles;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.util.DomainUtils;
import mitm.common.util.DomainUtils.DomainType;
import mitm.common.ws.WebServiceCheckedException;
import mitm.djigzo.web.beans.DomainBean;
import mitm.djigzo.web.beans.X509CertificateBean;
import mitm.djigzo.web.beans.impl.DomainBeanImpl;
import mitm.djigzo.web.components.ValidForSigningMultiSelectCertificates;
import mitm.djigzo.web.entities.Domain;
import mitm.djigzo.web.entities.DomainManager;
import mitm.djigzo.web.pages.Domains;

import org.apache.tapestry5.EventConstants;
import org.apache.tapestry5.PersistenceConstants;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.internal.services.LinkFactory;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.springframework.security.annotation.Secured;

@Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_DOMAIN_MANAGER})
@IncludeStylesheet("context:styles/pages/domain/domainRelay.css")
public class DomainRelay
{
	@Inject
	private DomainManager domainManager;

	@Inject
	private LinkFactory linkFactory;
	
	@Property
	private DomainBean domainBean;

    /*
     * True if changes were applied
     */
    @Persist(PersistenceConstants.FLASH)
    private boolean applied;
	
	@Component(parameters = {
			"inheritedCertificates = prop:inheritedCertificates",
			"userSelectedCertificates = prop:selectedCertificates"})
	private ValidForSigningMultiSelectCertificates relayCertificates;

    public boolean isApplied() {
        return applied;
    }
	
	@SetupRender
	@Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_DOMAIN_MANAGER})
	protected void setupRender() {
		/*
		 * Empty on purpose
		 */
	}
	
    public Set<X509CertificateBean> getSelectedCertificates()
    throws WebServiceCheckedException, HierarchicalPropertiesException
    {
        return domainBean.getPreferences().getNamedCertificates(NamedCertificateCategories.RELAY);
    }

    public Set<X509CertificateBean> getInheritedCertificates()
    throws WebServiceCheckedException, HierarchicalPropertiesException
    {
        return domainBean.getPreferences().getInheritedNamedCertificates(NamedCertificateCategories.RELAY);
    }
	
	private DomainBean createDomainBean(String domainName) 
	throws HierarchicalPropertiesException, WebServiceCheckedException
	{
		String filteredDomain = DomainUtils.canonicalizeAndValidate(domainName, DomainType.WILD_CARD);
		
		if (filteredDomain == null)
		{
		    /* 
		     * domain is not valid 
		     */
			return null;
		}
		
		Domain domain = domainManager.getDomain(filteredDomain);
		
		if (domain == null) {
			return null;			
		}
		
		return new DomainBeanImpl(domain);
	}
	
    @Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_DOMAIN_MANAGER})
	public Object onActivate(String domainName)
	throws WebServiceCheckedException, HierarchicalPropertiesException 
	{
		domainBean = createDomainBean(domainName);
		
		if (domainBean == null)	{
			return Domains.class;
		}
		
		return null;
	}

	public String onPassivate() 
	{
		String domain = null;
		
		if (domainBean != null) {
			domain = domainBean.getDomain();
		}
		
		return domain;
	}
	
	@OnEvent(value = EventConstants.SUCCESS, component = "buttonForm")
	public void onSuccess() 	
	throws HierarchicalPropertiesException, WebServiceCheckedException
	{
        Set<String> selectedThumbprints = relayCertificates.getCertificateGrid().getSelected();
        
        if (selectedThumbprints != null)
        {
            domainBean.getPreferences().setNamedCertificates(NamedCertificateCategories.RELAY, selectedThumbprints);
            
            domainBean.save();
        }
        
        applied = true;
	}
	
    /*
     * Event handler called when the cancel button is pressed.
     */
	protected Object onCancel()
	{
		if (domainBean == null) {
			return Domains.class;
		}
		
        return linkFactory.createPageRenderLink("domain/edit", false, domainBean.getDomain());
	}
}
