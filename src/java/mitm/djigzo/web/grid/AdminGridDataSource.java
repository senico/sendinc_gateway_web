/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.grid;

import java.util.ArrayList;
import java.util.List;

import mitm.application.djigzo.DjigzoRuntimeException;
import mitm.common.hibernate.SortDirection;
import mitm.common.util.Check;
import mitm.common.ws.WebServiceCheckedException;
import mitm.djigzo.web.beans.AdminBean;
import mitm.djigzo.web.beans.impl.AdminBeanImpl;
import mitm.djigzo.web.common.security.Admin;
import mitm.djigzo.web.common.security.AdminManager;

import org.apache.tapestry5.grid.ColumnSort;
import org.apache.tapestry5.grid.GridDataSource;
import org.apache.tapestry5.grid.SortConstraint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AdminGridDataSource implements GridDataSource 
{
	private final static Logger logger = LoggerFactory.getLogger(AdminGridDataSource.class);
	
	private List<AdminBean> adminBeans;
	
	/*
	 * we need to keep track of base index because the index in getRowValue is the absolute
	 * index and not relative.
	 */
	private int startIndex;
	
	private final AdminManager adminManager;
	
	public AdminGridDataSource(AdminManager adminManager)
	{
		Check.notNull(adminManager, "adminManager");
		
		this.adminManager = adminManager;
	}
	
    @Override
	public int getAvailableRows() 
	{
		try {
			return adminManager.getAdminCount();
		} 
		catch (WebServiceCheckedException e) {
			throw new DjigzoRuntimeException(e);
		}
	}

    @Override
	public Class<AdminBean> getRowType()
	{
		return AdminBean.class;
	}

    @Override
	public AdminBean getRowValue(int index) 
	{
		int rowIndex = index - startIndex;
		
		AdminBean value = null;
		
		if (adminBeans != null && rowIndex < adminBeans.size()) {
			value = adminBeans.get(rowIndex);
		}
		else {
			logger.warn("Not enough rows.");
			/* 
			 * We do not have enough Bean objects. This can happen when AvailableRows
			 * returned more rows than were actually available when prepared was called. 
			 */ 
			value = null;
		}
		
		return value;
	}

    @Override
	public void prepare(int startIndex, int endIndex, List<SortConstraint> sortConstraints) 
	{
		try {
			SortDirection sortDirection = sortConstraints.size() > 0 && 
				sortConstraints.get(0).getColumnSort() == ColumnSort.DESCENDING ? 
						SortDirection.DESC : SortDirection.ASC;
			
			List<Admin> admins = adminManager.getAdmins(startIndex, endIndex - startIndex + 1, 
					sortDirection);
			
			if (admins != null) 
			{
				adminBeans = new ArrayList<AdminBean>(admins.size());
				
				for (Admin admin : admins) {
					adminBeans.add(new AdminBeanImpl(admin));
				}
			}
			else {
				adminBeans = new ArrayList<AdminBean>();
			}
		} 
		catch (WebServiceCheckedException e) {
			throw new DjigzoRuntimeException(e);
		} 
		
		this.startIndex = startIndex;
	}
}
