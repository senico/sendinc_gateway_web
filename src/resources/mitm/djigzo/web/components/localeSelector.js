// called after a new locale is selected
function afterSelectLocale(data)
{
    // reload page to make it use the new locale
    window.location.reload();
}        
