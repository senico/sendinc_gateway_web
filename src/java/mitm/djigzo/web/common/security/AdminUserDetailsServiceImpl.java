/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.common.security;

import java.util.concurrent.atomic.AtomicReference;

import mitm.application.djigzo.ws.AdminDTO;
import mitm.application.djigzo.ws.LoginWS;
import mitm.common.security.crypto.RandomGenerator;
import mitm.common.util.Check;
import mitm.common.ws.WebServiceCheckedException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.security.BadCredentialsException;
import org.springframework.security.providers.dao.SaltSource;
import org.springframework.security.providers.encoding.PasswordEncoder;
import org.springframework.security.userdetails.UserDetails;
import org.springframework.security.userdetails.UsernameNotFoundException;

public class AdminUserDetailsServiceImpl implements AdminUserDetailsServiceExt 
{
	private static final Logger logger = LoggerFactory.getLogger(AdminUserDetailsServiceImpl.class);
	
	/*
	 * Webservice for getting the login credentials
	 */
	private final AtomicReference<LoginWS> loginWSRef = new AtomicReference<LoginWS>();
	
	/*
	 * Used to encode the password when the password is not encoded
	 */
	private final PasswordEncoder passwordEncoder;
	
	/*
	 * Used to salt the password when the password is not encoded
	 */
	private final SaltSource saltSource;
	
	/*
	 * Used to generate the Salt
	 */
	private RandomGenerator randomGenerator;

	public AdminUserDetailsServiceImpl(PasswordEncoder passwordEncoder, SaltSource saltSource)
	{
		this(passwordEncoder, saltSource, null);
	}
	
	public AdminUserDetailsServiceImpl(PasswordEncoder passwordEncoder, SaltSource saltSource,
			RandomGenerator randomGenerator)
	{
		Check.notNull(passwordEncoder, "passwordEncoder");
		Check.notNull(saltSource, "saltSource");

		this.passwordEncoder = passwordEncoder;
		this.saltSource = saltSource;
		this.randomGenerator = randomGenerator;
	}
	
    @Override
	public void setLoginWS(LoginWS loginWS) {
		this.loginWSRef.set(loginWS);
	}

    @Override
	public UserDetails loadUserByUsername(String username)
	throws UsernameNotFoundException, DataAccessException 
	{		
		LoginWS loginWS = loginWSRef.get();
		
		if (loginWS == null) 
		{
			/*
			 * Can happen when a request is made directly to the validation URL without
			 * opening the login page first.
			 */
			throw new UsernameNotFoundException("loginWS is not set.");
		}

		AdminDTO adminDTO = null;
		
		try {
			adminDTO = loginWS.getAdmin(username);
		} 
		catch (WebServiceCheckedException e) 
		{
			logger.error("Error getting admin.", e);
			
			throw new BadCredentialsException("Error getting admin.", e);
		}
		catch(RuntimeException e) {
			/*
			 * Thrown when there is something wrong with the soap connection
			 */
			logger.error("Error getting admin.", e);
			
			throw new BadCredentialsException("Error getting admin.", e);
		}

		if (adminDTO == null) {
			throw new UsernameNotFoundException("User " + username + " not found.");
		}

		Admin admin = new AdminImpl(adminDTO, passwordEncoder, saltSource, randomGenerator,
				null);
		
		return admin;
	}
}
