/*
 * Copyright (c) 2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.djigzo.web.services.security;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mitm.common.mail.EmailAddressUtils;
import mitm.djigzo.web.services.LoginBanService;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.security.AuthenticationException;
import org.springframework.security.ui.webapp.AuthenticationProcessingFilter;

/**
 * AuthenticationProcessingFilter extension that checks whether the user from a specific IP
 * address is banned from logging in
 * 
 * @author Martijn Brinkers
 *
 */
public class BannedAuthenticationProcessingFilter extends AuthenticationProcessingFilter 
    implements ApplicationEventPublisherAware
{
    /*
     * The LoginBanService is used for the prevention of brute force login attacks
     */
    private LoginBanService loginBanService;
    
    /*
     * For publishing application events
     */
    private ApplicationEventPublisher applicationEventPublisher;
    
    /*
     * The URL to open when the user is banned
     */
    private String bannedURL;
    
    @Override
    protected void onPreAuthentication(HttpServletRequest request, HttpServletResponse response)
    throws AuthenticationException, IOException
    {
        String email = EmailAddressUtils.canonicalize(obtainUsername(request));
        
        if (loginBanService != null)
        {
            if (loginBanService.isBanned(request.getRemoteAddr(), email)) {
                throw new BannedException(email, request.getRemoteAddr());
            }
        }
        
        super.onPreAuthentication(request, response);
    }
    
    @Override
    protected String determineFailureUrl(HttpServletRequest request, AuthenticationException failed)
    {
        if (failed instanceof BannedException)
        {
            applicationEventPublisher.publishEvent(new BannedApplicationEvent((BannedException) failed));
                        
            return bannedURL;
        }
        
        return super.determineFailureUrl(request, failed);
    }
    
    @Override
    public void afterPropertiesSet()
    throws Exception
    {
        super.afterPropertiesSet();
        
        if (StringUtils.isEmpty(bannedURL)) {
            throw new IllegalArgumentException("bannedURL is not set"); 
        }        
    }
    
    @Override
    public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher)
    {
        this.applicationEventPublisher = applicationEventPublisher;
    }
    
    public void setLoginBanService(LoginBanService loginBanService) {
        this.loginBanService = loginBanService;
    }

    public String getBannedUrl() {
        return bannedURL;
    }

    public void setBannedUrl(String bannedURL) {
        this.bannedURL = bannedURL;
    }
}
