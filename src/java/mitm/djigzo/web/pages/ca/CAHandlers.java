/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.pages.ca;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Set;

import mitm.application.djigzo.admin.FactoryRoles;
import mitm.common.ws.WebServiceCheckedException;
import mitm.djigzo.web.common.CertificateRequestHandlerConfigPageRegistry;
import mitm.djigzo.web.common.GenericSelectionModel;

import org.apache.tapestry5.ComponentResources;
import org.apache.tapestry5.Link;
import org.apache.tapestry5.SelectModel;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.corelib.components.Select;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.Response;
import org.springframework.security.annotation.Secured;

@Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_PKI_MANAGER})
@IncludeStylesheet("context:styles/pages/ca/caHandlers.css")
public class CAHandlers
{
    @Inject
    private CertificateRequestHandlerConfigPageRegistry handlerRegistry;
    
    @Inject
    private ComponentResources resources;
    
    @Inject
    private Response response;
    
    @SuppressWarnings("unused")
    @Component(id = "certificateRequestHandler", parameters = {"value = certificateRequestHandler", 
            "model = certificateRequestHandlerModel", "blankOption = NEVER", "validate = required"})
    private Select certificateRequestHandlerField;
    
    /*
     * The name of the selected request handler
     */
    private String certificateRequestHandler;
    
    @SetupRender
    @Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_PKI_MANAGER})
    public void setupRender() {
        /*
         * Empty on purpose
         */
    }

    public Set<String> getCertificateRequestHandlers() {
        return handlerRegistry.getPages().keySet();
    }
    
    public SelectModel getCertificateRequestHandlerModel()
    throws WebServiceCheckedException
    {
        return new GenericSelectionModel<String>(new ArrayList<String>(
                getCertificateRequestHandlers()), null, null);
    }

    public String getCertificateRequestHandler() {
        return certificateRequestHandler;
    }

    public void setCertificateRequestHandler(String certificateRequestHandler) {
        this.certificateRequestHandler = certificateRequestHandler;
    }

    public void onSuccess()
    throws IOException
    {
        Class<?> toPage = handlerRegistry.getPages().get(certificateRequestHandler);
        
        if (toPage != null)
        {
            Link link = resources.createPageLink(toPage, false);
            
            response.sendRedirect(link);
        }
    }
    
    /*
     * Event handler called when the cancel button is pressed.
     */
    protected Object onCancel() {
        return CAIssue.class;
    }
}
