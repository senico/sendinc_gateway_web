/*
 * Copyright (c) 2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.pages.dlp.patterns;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.util.ByteArrayDataSource;

import mitm.application.djigzo.admin.FactoryRoles;
import mitm.application.djigzo.ws.BinaryDTO;
import mitm.application.djigzo.ws.PolicyPatternManagerWS;
import mitm.common.util.SizeLimitedInputStream;
import mitm.common.util.SizeUtils;

import org.apache.commons.fileupload.FileUploadException;
import org.apache.tapestry5.PersistenceConstants;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.corelib.components.Checkbox;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.upload.components.Upload;
import org.apache.tapestry5.upload.services.UploadEvents;
import org.apache.tapestry5.upload.services.UploadedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.annotation.Secured;

@Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_DLP_MANAGER})
@IncludeStylesheet("context:styles/pages/dlp/patterns/import.css")
public class PatternsImport
{
    private final static Logger logger = LoggerFactory.getLogger(PatternsImport.class);
    
    /*
     * Some sane upper limit on the size of the XML to import.
     */
    private final static int MAX_XML_SIZE = SizeUtils.MB;
    
    @Inject
    private PolicyPatternManagerWS policyPatternManagerWS;
    
    /*
     * 'Handle' to the file that's uploaded
     */
    private UploadedFile file;

    /*
     * If true existing patterns are skipped
     */
    @Property
    @Persist
    private Boolean skipExisting;
    
    /*
     * True when uploading a file resulted in an error (mostly when file size exceeds maximum)
     */
    @Persist(PersistenceConstants.FLASH)
    private boolean importError;

    /*
     * True when uploading was successful
     */
    @Persist(PersistenceConstants.FLASH)
    private boolean importSuccess;
    
    /*
     * The error message accompanying the uploadError property.
     */
    @Persist(PersistenceConstants.FLASH)
    private String importErrorMessage;
    
    @SuppressWarnings("unused")
    @Component(id = "upload", parameters = {"value=file", "validate=required"})
    private Upload upload;

    @SuppressWarnings("unused")
    @Component(id = "skipExisting", parameters = {"value=skipExisting"})
    private Checkbox skipExistingCheckbox;
    
    @SetupRender
    @Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_DLP_MANAGER})
    protected void setupRender()
    {
        if (skipExisting == null) {
            skipExisting = false;
        }
    }

    /*
     * Event handler that gets called when the uploaded file exceeds the maximum size
     */
    @OnEvent(UploadEvents.UPLOAD_EXCEPTION)
    protected Object onUploadException(FileUploadException uploadException) 
    {
        logger.error("Error uploading file", uploadException);
        
        importError = true;
        importErrorMessage = uploadException.getMessage();
        
        return PatternsImport.class;
    }
    
    public void onSuccess()
    {
        try {
            if (file == null) {
                throw new IllegalStateException("file is null");
            }
            
            SizeLimitedInputStream limit = new SizeLimitedInputStream(file.getStream(), MAX_XML_SIZE, 
                    true /* exception */);
            
            DataSource source = new ByteArrayDataSource(limit, file.getContentType());
            DataHandler dataHandler = new DataHandler(source);
            
            BinaryDTO xml = new BinaryDTO(dataHandler);
                    
            policyPatternManagerWS.importFromXML(xml, skipExisting);
            
            importSuccess = true;
        }
        catch (Exception e)
        {
            logger.error("Error importing patterns", e);

            importError = true;
            importErrorMessage = e.getMessage();
        }
    }
    
    /*
     * Event handler called when the cancel button is pressed.
     */
    protected Object onCancel() {
        return PatternsView.class;
    }
    
    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }
    
    public boolean isImportError() {
        return importError;
    }

    public String getImportErrorMessage() {
        return importErrorMessage;
    }
    
    public boolean isImportSuccess() {
        return importSuccess;
    }    
}
