/*
 * Copyright (c) 2011-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.pages.portal.secure;

import mitm.application.djigzo.ws.LoginWS;
import mitm.application.djigzo.ws.SystemManagerWS;
import mitm.common.mail.EmailAddressUtils;
import mitm.djigzo.web.common.security.PortalUserDetailsServiceExt;
import mitm.djigzo.web.utils.PasswordCodec;

import org.apache.commons.lang.StringUtils;
import org.apache.tapestry5.Asset;
import org.apache.tapestry5.annotations.BeginRender;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.Path;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.ioc.annotations.Value;
import org.apache.tapestry5.services.Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@IncludeStylesheet("context:styles/pages/portal/secure/login.css")
public class Login 
{	
    private final static Logger logger = LoggerFactory.getLogger(Login.class);
    
	@Inject
	@Value("${login-processing-url}")
	private String loginProcessingURL;

    @SuppressWarnings("unused")
    @Inject
    @Path("context:images/logo.png")
    @Property
    private Asset logoAsset;
	
    @Inject
    private Request request;
        
    @Inject
    private SystemManagerWS systemManagerWS;

    @Inject
    private LoginWS loginWS;
    
    @Inject
    private PortalUserDetailsServiceExt userDetailsService;

    @Inject
    private PasswordCodec passwordCodec;

    /*
     * True if login failed
     */
    private boolean failed = false; 

    /*
     * The email address from the email parameter
     */
    private String email;
    
    public boolean isFailed() {
        return failed;
    }
    
    public String getEmail() {
        return email;
    }
    
    public String getLoginCheckUrl() {
        return request.getContextPath() + loginProcessingURL;
    }
    
    protected void onActivate()
    {
        if (email == null) {
            email = EmailAddressUtils.canonicalize(StringUtils.trimToNull(request.getParameter("email")));
        }
    }
    
    void onActivate(String extra) 
    {
        if ("failed".equalsIgnoreCase(extra)) {
            failed = true;
        }
        else {
            /*
             * Assume it's an email address
             */
            email = EmailAddressUtils.canonicalizeAndValidate(extra, true);
        }
    }
    
    public boolean isBackendRunning()
    {
        boolean running;
        
        try {
            running = systemManagerWS.isRunning();
        }
        catch (Exception e)
        {
            logger.error("Error in isBackendRunning. Backend is propably not running.", e);
            
            running = false;
        }
        
        return running;
    }
    
    @BeginRender
    public void beginRender() 
    {
    	/* 
    	 * Make sure userDetailsService has access to the required serviced
    	 */
    	userDetailsService.setPasswordCodec(passwordCodec);
    	userDetailsService.setLoginWS(loginWS);
    }
}
