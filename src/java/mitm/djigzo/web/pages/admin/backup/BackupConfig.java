/*
 * Copyright (c) 2009-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.pages.admin.backup;

import java.text.ParseException;

import mitm.application.djigzo.admin.FactoryRoles;
import mitm.application.djigzo.ws.BackupWS;
import mitm.common.backup.Strategy;
import mitm.common.cifs.StaticSMBFileParameters;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.ws.WebServiceCheckedException;
import mitm.djigzo.web.beans.BackupPreferencesBean;
import mitm.djigzo.web.beans.FilenameStrategySettingsBean;
import mitm.djigzo.web.beans.SMBFileParametersBean;
import mitm.djigzo.web.beans.impl.BackupPreferencesBeanImpl;
import mitm.djigzo.web.beans.impl.FilenameStrategySettingsBeanImpl;
import mitm.djigzo.web.beans.impl.SMBFileParametersBeanImpl;
import mitm.djigzo.web.components.NonClearingPassword;
import mitm.djigzo.web.entities.GlobalPreferencesManager;
import mitm.djigzo.web.services.DisableHttpCache;

import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.tapestry5.PersistenceConstants;
import org.apache.tapestry5.annotations.Cached;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.Mixins;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.corelib.components.Checkbox;
import org.apache.tapestry5.corelib.components.Form;
import org.apache.tapestry5.corelib.components.Select;
import org.apache.tapestry5.corelib.components.TextField;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.quartz.CronExpression;
import org.springframework.security.annotation.Secured;

@IncludeStylesheet("context:styles/pages/admin/backup/backupConfig.css")
@Secured({FactoryRoles.ROLE_ADMIN})
public class BackupConfig
{
    @Inject
    private GlobalPreferencesManager globalPreferencesManager;

    @Inject
    private BackupWS backupWS;

    /*
     * The SMB domain
     */
    @Persist(PersistenceConstants.FLASH)
    @Property
    private String domain;

    /*
     * The SMB user
     */
    @Persist(PersistenceConstants.FLASH)
    @Property
    private String username;

    /*
     * The SMB password
     */
    @Persist(PersistenceConstants.FLASH)
    @Property
    private String sharePassword;

    /*
     * The SMB server
     */
    @Persist(PersistenceConstants.FLASH)
    @Property
    private String server;

    /*
     * The SMB port
     */
    @Persist(PersistenceConstants.FLASH)
    @Property
    private Integer port;

    /*
     * The SMB share
     */
    @Persist(PersistenceConstants.FLASH)
    @Property
    private String share;

    /*
     * The Filename strategy
     */
    @Persist(PersistenceConstants.FLASH)
    @Property
    private Strategy filenameStrategy;
    
    /*
     * The SMB dir
     */
    @Persist(PersistenceConstants.FLASH)
    @Property
    private String dir;

    /*
     * True if automatic backup is enabled
     */
    @Persist(PersistenceConstants.FLASH)
    @Property
    private boolean automaticBackupEnabled;
    
    /*
     * The cron expression used by the automatic backup
     */
    @Persist(PersistenceConstants.FLASH)
    @Property
    private String cronExpression;
    
    /*
     * The password use by the automatic backup
     */
    @Persist(PersistenceConstants.FLASH)
    @Property
    private String backupPassword;
            
    @Persist(PersistenceConstants.FLASH)
    private SMBFileParametersBean sMBFileParameters;

    @Persist(PersistenceConstants.FLASH)
    private FilenameStrategySettingsBean filenameStrategySettings;

    @Persist(PersistenceConstants.FLASH)
    private BackupPreferencesBean backupPreferences;
    
    @Persist(PersistenceConstants.FLASH)
    private boolean connectionOK;

    @Persist(PersistenceConstants.FLASH)
    private boolean connectionFailed;

    @Persist(PersistenceConstants.FLASH)
    private String connectionFailure;
    
    /*
     * Set to true if the cron expression was changed so we can warn the user
     * that a restart is required. We make this setting persistent so to warn
     * the user that the server must be restarted. This is not completely 
     * correct because James should be restarted and not Jetty but this
     * is good enough because restarting from the web admin restarts James
     * and Jetty.
     */
    @Persist
    private boolean restartRequired;
        
    /*
     * true if the settings were applied
     */
    @Persist(PersistenceConstants.FLASH)
    private boolean applied;
    
    @Property
    private boolean authenticate;
    
    @Component
    private Form form;
    
    @SuppressWarnings("unused")
    @Component(id = "domain", parameters = {"value = domain"})
    private TextField domainField;
    
    @SuppressWarnings("unused")
    @Component(id = "user", parameters = {"value = username"})
    private TextField userField;
    
    @SuppressWarnings("unused")
    @Component(id = "sharePassword", parameters = {"value = sharePassword"})
    private NonClearingPassword sharePasswordField;

    @SuppressWarnings("unused")
    @Component(id = "server", parameters = {"value = server"})
    private TextField serverField;

    @SuppressWarnings("unused")
    @Component(id = "port", parameters = {"value = port", "validate = min=1, max=65535"})
    private TextField portField;

    @SuppressWarnings("unused")
    @Component(id = "share", parameters = {"value = share"})
    private TextField shareField;

    @SuppressWarnings("unused")
    @Component(id = "dir", parameters = {"value = dir"})
    private TextField dirField;

    @SuppressWarnings("unused")
    @Component(id = "strategy", parameters = {"value = filenameStrategy", "blankOption = NEVER"})
    private Select strategyField;
    
    @SuppressWarnings("unused")
    @Component(id = "authenticate", parameters = {"value = authenticate", "idsToDisable = literal:user sharePassword", "invert = true"})
    @Mixins(value={"DisableOnCheck"})
    private Checkbox authenticateCheckbox;

    @SuppressWarnings("unused")
    @Component(id = "automaticBackupEnabled", parameters = {"value = automaticBackupEnabled"})
    private Checkbox automaticBackupEnabledField;

    @Component(id = "cronExpression", parameters = {"value = cronExpression", "validate = required"})
    private TextField cronExpressionField;
    
    @SuppressWarnings("unused")
    @Component(id = "backupPassword", parameters = {"value = backupPassword"})
    private NonClearingPassword backupPasswordField;
    
    /*
     * true if apply button was pressed
     */
    private boolean apply;
    
    @SetupRender
    @DisableHttpCache /* disable cache for security purposes */
    @Secured({FactoryRoles.ROLE_ADMIN})
    protected void setupRender()
    throws HierarchicalPropertiesException, WebServiceCheckedException
    {
        if (sMBFileParameters == null)
        {
            sMBFileParameters = getSMBFileParameters();
    
            domain = sMBFileParameters.getDomain();
            username = sMBFileParameters.getUsername();
            /*
             * When loaded the first time the Dummy password will be returned to make sure the real password is
             * not revealed.
             */
            sharePassword = NonClearingPassword.DUMMY_PASSWORD;
            server = sMBFileParameters.getServer();
            port = sMBFileParameters.getPort();
            share = sMBFileParameters.getShare();
            dir = sMBFileParameters.getDir();
        }
        
        if (filenameStrategySettings == null)
        {
            filenameStrategySettings = getFilenameStrategySettings();
            
            filenameStrategy = filenameStrategySettings.getStrategy();
        }

        if (backupPreferences == null)
        {
            backupPreferences = getBackupPreferencesBean();
         
            automaticBackupEnabled = backupPreferences.isAutomaticBackupEnabled();
            cronExpression = backupPreferences.getCronExpression();
            /*
             * When loaded the first time the Dummy password will be returned to make sure the real password is
             * not revealed.
             */
            backupPassword = NonClearingPassword.DUMMY_PASSWORD;
        }
        
        authenticate = StringUtils.isNotBlank(username);
    }
    
    @Cached
    public SMBFileParametersBean getSMBFileParameters()
    throws HierarchicalPropertiesException, WebServiceCheckedException
    {
        return new SMBFileParametersBeanImpl(globalPreferencesManager.getProperties());
    }

    @Cached
    public FilenameStrategySettingsBean getFilenameStrategySettings()
    throws HierarchicalPropertiesException, WebServiceCheckedException
    {
        return new FilenameStrategySettingsBeanImpl(globalPreferencesManager.getProperties());
    }

    @Cached
    public BackupPreferencesBean getBackupPreferencesBean()
    throws HierarchicalPropertiesException, WebServiceCheckedException
    {
        return new BackupPreferencesBeanImpl(globalPreferencesManager.getProperties());
    }
    
    public boolean isConnectionOK() {
        return connectionOK;
    }
    
    public boolean isConnectionFailed() {
        return connectionFailed;
    }
        
    public String getConnectionFailure() {
        return StringUtils.isNotBlank(connectionFailure) ? connectionFailure : "No details.";
    }
    
    private void connectionFailed(Throwable t)
    {
        connectionFailed = true;

        connectionFailure = ExceptionUtils.getRootCauseMessage(t);
    }
    
    public boolean isrestartRequired() {
        return restartRequired;
    }

    public boolean isApplied() {
        return applied;
    }
    
    private void testConnection()
    {
        connectionFailed = false;
        connectionOK = false;
        
        try {
            StaticSMBFileParameters sMBFileParametersDTO = new StaticSMBFileParameters();
            
            sMBFileParametersDTO.setDomain(sMBFileParameters.getDomain());
            /*
             * We need to explicitly check authenticate because FF keeps on filling in the username
             * and password (using FF built-in fill-password-feature) if the username and password
             * is blank.
             */
            if (!authenticate)
            {
                username = null;
                sharePassword = null;
            }
            
            sMBFileParametersDTO.setUsername(sMBFileParameters.getUsername());
            sMBFileParametersDTO.setPassword(sMBFileParameters.getPassword());
            sMBFileParametersDTO.setServer(sMBFileParameters.getServer());
            sMBFileParametersDTO.setPort(sMBFileParameters.getPort());
            sMBFileParametersDTO.setShare(sMBFileParameters.getShare());
            sMBFileParametersDTO.setDir(sMBFileParameters.getDir());
            
            if (backupWS.testCIFSConnection(sMBFileParametersDTO))
            {
                connectionOK = true;
            }
            else {
                connectionFailed = true;
                
                connectionFailure = "Resource does not exist.";
            }
        }
        catch (WebServiceCheckedException e) {
            connectionFailed(e);
        }
        catch (HierarchicalPropertiesException e) {
            connectionFailed(e);
        }
    }
    
    public void onValidateForm() 
    {
        /*
         * Create a cron expression to check if it's a valid cron expression
         */
        try {
            new CronExpression(cronExpression);
        }
        catch (ParseException e) {
            form.recordError(cronExpressionField, "The expression is not a valid cron expression.");
        }
    }
    
    public void onSuccess()
    throws HierarchicalPropertiesException, WebServiceCheckedException
    {
        sMBFileParameters = getSMBFileParameters();        
        sMBFileParameters.setDomain(domain);
        /*
         * We need to explicitly check authenticate because FF keeps on filling in the username
         * and password (using FF built-in fill-password-feature) if the username and password
         * is blank.
         */
        if (!authenticate)
        {
            username = null;
            sharePassword = null;
        }
        sMBFileParameters.setUsername(username);
        
        /*
         * Only set the password if the initial dummy password has been changed.
         */
        if (!NonClearingPassword.DUMMY_PASSWORD.equals(sharePassword)) {
            sMBFileParameters.setPassword(sharePassword);
        }
        
        sMBFileParameters.setServer(server);
        sMBFileParameters.setPort(port);
        sMBFileParameters.setShare(share);
        sMBFileParameters.setDir(dir);

        filenameStrategySettings = getFilenameStrategySettings();        
        filenameStrategySettings.setStrategy(filenameStrategy);
        
        backupPreferences = getBackupPreferencesBean();
        backupPreferences.setAutomaticBackupEnabled(automaticBackupEnabled);

        /*
         * Only set the password if the initial dummy password has been changed.
         */
        if (!NonClearingPassword.DUMMY_PASSWORD.equals(backupPassword)) {
            backupPreferences.setPassword(backupPassword);
        }

        /*
         * We need to know if the cron expression has been changed so we can warn
         * the user that a restart is required. We only want to change 
         * cronExpressionChanged when it is not yet set.
         */
        if (apply && !restartRequired) {
            restartRequired = !ObjectUtils.equals(backupPreferences.getCronExpression(), cronExpression);
        }
        
        backupPreferences.setCronExpression(cronExpression);

        if (apply)
        {
            sMBFileParameters.save();
            filenameStrategySettings.save();
            backupPreferences.save();
            
            applied = true;
        }
        else {
            testConnection();
        }
    }
    
    public void onSelectedFromApply() {
        apply = true;
    }
    
    /*
     * Event handler called when the cancel button is pressed.
     */
    public Object onCancel() {
        return BackupManager.class;
    }
}
