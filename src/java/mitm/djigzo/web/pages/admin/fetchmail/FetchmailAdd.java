/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.pages.admin.fetchmail;

import java.util.UUID;

import mitm.application.djigzo.admin.FactoryRoles;
import mitm.application.djigzo.ws.FetchmailManagerWS;
import mitm.common.fetchmail.Authentication;
import mitm.common.fetchmail.FetchmailConfig;
import mitm.common.fetchmail.Poll;
import mitm.common.fetchmail.Protocol;
import mitm.common.util.DomainUtils;
import mitm.common.util.DomainUtils.DomainType;
import mitm.common.ws.WebServiceCheckedException;

import org.apache.cxf.common.util.StringUtils;
import org.apache.tapestry5.annotations.Cached;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.corelib.components.Checkbox;
import org.apache.tapestry5.corelib.components.Form;
import org.apache.tapestry5.corelib.components.PasswordField;
import org.apache.tapestry5.corelib.components.Select;
import org.apache.tapestry5.corelib.components.TextField;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.ioc.annotations.Value;
import org.springframework.security.AccessDeniedException;
import org.springframework.security.annotation.Secured;

@IncludeStylesheet("context:styles/pages/admin/fetchmail/fetchmailAdd.css")
@Secured({FactoryRoles.ROLE_ADMIN})
public class FetchmailAdd
{
    @Inject
    private FetchmailManagerWS fetchmailManager;

    /*
     * If true the Fetchmail pages are enabled
     */
    @Inject 
    @Value("${fetchmail.enabled}")
    private boolean fetchmailEnabled;
    
    @Property
    private String server;

    @Property
    private Integer port;
    
    @Property
    private Protocol protocol;

    @Property
    private Authentication authentication;

    @Property
    private String username;

    @Property
    private String password;
    
    @Property
    private boolean uidl;

    @Property
    private String principal;

    @Property
    private boolean ssl;

    @Property
    private boolean keep;

    @Property
    private boolean idle;

    @Property
    private String folder;

    @Property
    private String forwardTo;
    
    @Component
    private Form form;
    
    @Component(id = "server", parameters = {"value = server", "validate=required,maxlength=255"})
    private TextField serverField;

    @SuppressWarnings("unused")
    @Component(id = "port", parameters = {"value = port", "validate=min=1,max=65535"})
    private TextField portField;

    @SuppressWarnings("unused")
    @Component(id = "protocol", parameters = {"value = protocol", "validate=required"})
    private Select protocolField;

    @SuppressWarnings("unused")
    @Component(id = "authentication", parameters = {"value = authentication", "validate=required"})
    private Select authenticationField;

    @SuppressWarnings("unused")
    @Component(id = "username", parameters = {"value = username", "validate=required,maxlength=255"})
    private TextField usernameField;

    @SuppressWarnings("unused")
    @Component(id = "password", parameters = {"value = password", "validate=required,maxlength=255"})
    private PasswordField passwordField;
    
    @SuppressWarnings("unused")
    @Component(id = "uidl", parameters = {"value = uidl"})
    private Checkbox uidlField;

    @SuppressWarnings("unused")
    @Component(id = "principal", parameters = {"value = principal", "maxlength=50"})
    private TextField principalField;

    @SuppressWarnings("unused")
    @Component(id = "ssl", parameters = {"value = ssl"})
    private Checkbox sslField;

    @SuppressWarnings("unused")
    @Component(id = "keep", parameters = {"value = keep"})
    private Checkbox keepField;

    @SuppressWarnings("unused")
    @Component(id = "idle", parameters = {"value = idle"})
    private Checkbox idleField;

    @SuppressWarnings("unused")
    @Component(id = "folder", parameters = {"value = folder", "validate=maxlength=50"})
    private TextField folderField;

    @SuppressWarnings("unused")
    @Component(id = "forwardTo", parameters = {"value = forwardTo", "validate=required,maxlength=255,emailmitm=1"})
    private TextField forwardToField;
    
    /*
     * Checks if Fetmail is enabled
     */
    private void checkAccess()
    {
        if (!fetchmailEnabled) {
            throw new AccessDeniedException("Fetchmail is not enabled.");
        }
    }
    
    @SetupRender
    @Secured({FactoryRoles.ROLE_ADMIN})
    protected void setupRender()
    {
        checkAccess();

        if (protocol == null) {
            protocol = Protocol.POP3;
        }

        if (authentication == null) {
            authentication = Authentication.ANY;
        }
    }

    @Cached
    public FetchmailConfig getFetchmailConfig()
    throws WebServiceCheckedException
    {
        return fetchmailManager.getConfig();
    }
    
    @Cached
    private Poll getPoll()
    {
        Poll poll = new Poll();
        
        poll.setID(UUID.randomUUID().toString());
        poll.setServer(server);
        poll.setPort(port);
        poll.setProtocol(protocol);
        poll.setAuthentication(authentication);
        poll.setUsername(username);
        poll.setPassword(password);
        poll.setUIDL(uidl);
        poll.setPrincipal(principal);
        poll.setSSL(ssl);
        poll.setKeep(keep);
        poll.setIdle(idle);
        poll.setFolder(folder);
        poll.setForwardTo(forwardTo);
        
        return poll;
    }
    
    public void onValidateForm() 
    throws WebServiceCheckedException 
    {
        String filteredServer = DomainUtils.validate(server, DomainType.FRAGMENT);
            
        if (StringUtils.isEmpty(filteredServer))
        {
            form.recordError(serverField, "The server is not valid.");
            return;
        }
        
        server = filteredServer;
        
        FetchmailConfig config = getFetchmailConfig();
        
        if (config.getPolls().contains(getPoll())) {
            form.recordError("The server is already being polled for the given user.");
        }
    }
    
    protected Object onSuccess()
    throws WebServiceCheckedException
    {
        FetchmailConfig config = getFetchmailConfig();

        config.getPolls().add(getPoll());
        
        fetchmailManager.setConfig(config);
        
        return FetchmailManager.class;
    }
    
    /*
     * Event handler called when the cancel button is pressed.
     */
    public Object onCancel() {
        return FetchmailManager.class;
    }
}
