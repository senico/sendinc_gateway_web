/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.components;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import mitm.application.djigzo.ws.CertificateStore;
import mitm.application.djigzo.ws.CertificateValidatorResult;
import mitm.application.djigzo.ws.CertificateValidatorWS;
import mitm.application.djigzo.ws.KeyAndCertStoreWS;
import mitm.common.ws.WebServiceCheckedException;
import mitm.djigzo.web.beans.CertificateType;
import mitm.djigzo.web.beans.CertificateTypeMarker;
import mitm.djigzo.web.beans.X509CertificateBean;
import mitm.djigzo.web.common.CertificateFilterSettings;
import mitm.djigzo.web.common.ValidityCheck;
import mitm.djigzo.web.common.YesNo;
import mitm.djigzo.web.grid.AbstractCertificateGridDataSource;
import mitm.djigzo.web.grid.CertStoreCertificateGridDataSource;
import mitm.djigzo.web.grid.DelegatedStaticCertificateGridDataSource;
import mitm.djigzo.web.services.CertificateStoreMarker;

import org.apache.tapestry5.ComponentResources;
import org.apache.tapestry5.EventConstants;
import org.apache.tapestry5.Link;
import org.apache.tapestry5.annotations.BeginRender;
import org.apache.tapestry5.annotations.Cached;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.grid.GridDataSource;
import org.apache.tapestry5.internal.services.LinkFactory;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.Response;

@IncludeStylesheet("context:styles/components/signingCertificate.css")
public class SigningCertificate 
{
    private static final String SC_HIDDEN_CLASS = "sc-hidden"; 
    private static final String SC_INHERITED_CLASS = "sc-inherited";
    
    public static final String APPLY_EVENT = "SigningCertificate-apply";
	
	@Parameter
	private X509CertificateBean selectedCertificate;
	
	@Inject
	@CertificateStoreMarker
	private KeyAndCertStoreWS certStore;
	
	@Inject
	private CertificateValidatorWS certificateValidatorWS; 
	
	@Inject
	private Response response;
	
	@Inject
	private LinkFactory linkFactory;
	
    @Inject
    private ComponentResources resources;

	/*
	 * The user selected filter settings
	 */
	@Persist
	private CertificateFilterSettings filterSettings;
    
	@Component(parameters = {"source = source",	"exclude=" + CertificateGrid.DELETE_COLUMN, "legendBlock = inherit:legendBlock"})
    private SingleSelectCertificateGrid certificateGrid;
	
	@SuppressWarnings("unused")
	@Component(parameters = {"filterSettings = filterSettings", "disableMissingKeyAlias=true"})
	private CertificateFilter certificateFilter;
		
	@Cached
	private X509CertificateBean getSelectedCertificate() {
		return selectedCertificate;
	}
	
	@BeginRender
	public void beginRender() 
	{
		String selectedThumbprint = null;
		
		if (getSelectedCertificate() != null) {
			selectedThumbprint = getSelectedCertificate().getThumbprint();
		}
		
		certificateGrid.setSelected(selectedThumbprint);
	}

	public GridDataSource getSource()
	{
		AbstractCertificateGridDataSource delegate = new CertStoreCertificateGridDataSource(
				certStore, getFilterSettings());
		
		List<X509CertificateBean> list = new LinkedList<X509CertificateBean>();

		if (getSelectedCertificate() != null) {
			list.add(getSelectedCertificate());
		}
		
		return new DelegatedStaticCertificateGridDataSource(list, delegate);
	}
	
	@OnEvent(value=CertificateGrid.CERTIFICATE_VALIDATOR_RESULT_EVENT, 
			component="certificateGrid")
	protected CertificateValidatorResult onGetCertificateValidatorResult(
			X509CertificateBean certificate) 
	throws WebServiceCheckedException
	{
		return certificateValidatorWS.checkValidityForSigning(CertificateStore.CERTIFICATES, 
				certificate.getThumbprint());
	}
    
	public SingleSelectCertificateGrid getCertificateGrid() {
		return certificateGrid;
	}
	
	public KeyAndCertStoreWS getCertStore() {
		return certStore;
	}

	public void setCertStore(KeyAndCertStoreWS certStore) {
		this.certStore = certStore;
	}

	public CertificateFilterSettings getFilterSettings() 
	{
		if (filterSettings == null)	{
			filterSettings = new CertificateFilterSettings();
		}

		/*
		 * We only want entries with associated keys
		 */
		filterSettings.setAllowMissingKeyAlias(false);

		return filterSettings;
	}
	
	/*
	 * Returns true if the certificate equals the selected but it's not the certificate
	 * we want to be shown as the first item (the certificate that is added as static item
	 * in getSource)
	 */
	private boolean isSelectedAndGeneral(X509CertificateBean certificate)
	{
		boolean result = false;
		
		if (certificate instanceof CertificateTypeMarker)
		{
			CertificateType certificateType = ((CertificateTypeMarker) certificate).getCertificateType();
			
			if (getSelectedCertificate() != null && getSelectedCertificate().equals(certificate)) 
			{
				result = certificateType == CertificateType.GENERAL;
			}
		}
		
		return result;
	}
	
	boolean isValidForSigning(X509CertificateBean certificate) 
	throws WebServiceCheckedException
	{
		CertificateValidatorResult validatorResult = certificateGrid.getCertificateValidatorResult(
				certificate);
		
		return validatorResult.isValid();
	}
	
	private void apply() {
		resources.triggerEvent(APPLY_EVENT, null, null);
	}
	
	@OnEvent(CertificateGrid.ROW_CLASS_EVENT)
	protected String getRowClass(X509CertificateBean certificate, CertificateValidatorResult validatorResult) 
	{
		/*
		 * We cannot return null because returning null has a special meaning for events so I will
		 * return "" if no class should be specified (the default class will be used instead defined by
		 * the CertificateGrid)
		 */
		String rowClass = "";
		
		if (isSelectedAndGeneral(certificate)) {
			rowClass = SC_HIDDEN_CLASS;
		}
		else {
			/*
			 * If the certificate is an inherited, we will use a special class
			 */
			if (certificate.isInherited()) {
				rowClass = SC_INHERITED_CLASS;
			}
		}
		
        if (!SC_HIDDEN_CLASS.equals(rowClass))
        {
            /* 
             * If the certificate is not hidden and the certificate is not 
             * valid we will fallback on the CertificateGrid CSS class 
             * handling
             */
            if (!validatorResult.isValid()) {
                rowClass = "";
            }
        }		
		
		return rowClass;
	}
	
	@OnEvent(CertificateGrid.SUBJECT_CLICKED_EVENT)
	protected void onSubjectClicked(String thumbprint) 
	throws IOException
	{
        Link link = linkFactory.createPageRenderLink("certificate/view", false, CertificateStore.CERTIFICATES,
        		thumbprint, ValidityCheck.SIGNING);

        response.sendRedirect(link);
	}
	
	@OnEvent(CertificateGrid.IS_DISABLED_EVENT)
	protected YesNo isDisabled(X509CertificateBean certificate, String column) 
	throws WebServiceCheckedException 
	{
		YesNo disabled = YesNo.NO;
		
		if (CertificateGrid.SELECT_COLUMN.equals(column)) 
		{
			/*
			 * Disable the checkbox if the certificate is not valid for signing
			 */
			if (!isValidForSigning(certificate)) {
				disabled = YesNo.YES;
			}
		}
		
		return disabled;
	}
	
	/*
	 * Because we would like to have the selected certificate to be the first one in the list
	 * the selected item is added as a static item (see getSource). The problem now is that 
	 * the same item will be added again because it's an item in the CertStore. The value
	 * of the Radio button will be set to the thumbprint of the certificate. Two radio buttons
	 * now have the same value and therefore the last one is selected whereas we want the first
	 * one to be selected. We will therefore 'override' the value of the item not in the static
	 * list by appending .FromStore to make sure it's not selected. 
	 */
	@OnEvent(SingleSelectCertificateGrid.SELECT_VALUE__EVENT)
	protected String onSelectValue(X509CertificateBean certificate)
	{
		String value = certificate.getThumbprint();
		
		if (isSelectedAndGeneral(certificate)) {
			value = value + ".FromStore";
		}
		
		return value;
	}
	
    @OnEvent(component="autoSelect")
    protected void clearCertificate() 
    {
    	/*
    	 * Set the selected certificate to null and apply. This will make sure that the signing
    	 * certificate will be auto selected.
    	 */
    	certificateGrid.setSelected(null);

    	apply();
    }
	
	@OnEvent(value = EventConstants.SUCCESS, component = "certificateGridForm")
	public void onSuccess() 
	{
    	apply();
	}
}
