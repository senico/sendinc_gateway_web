/*
 * Copyright (c) 2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.components;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import mitm.djigzo.web.common.GenericSelectionModel;
import mitm.djigzo.web.mixins.AjaxEvent;
import mitm.djigzo.web.services.Locales;

import org.apache.tapestry5.SelectModel;
import org.apache.tapestry5.annotations.IncludeJavaScriptLibrary;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.json.JSONObject;
import org.apache.tapestry5.services.PersistentLocale;
import org.apache.tapestry5.services.Request;

/**
 * Component for the selection of the locale (language) by the user.
 * 
 * @author Martijn Brinkers
 *
 */
@IncludeJavaScriptLibrary("localeSelector.js")
public class LocaleSelector
{
    @Inject 
    private Locales locales;

    @Inject
    private Request request;

    @Inject
    private PersistentLocale persistentLocaleService;

    public SelectModel getLocaleModel()
    {
        List<Locale> supportedLocales = locales.getSupportedLocales();

        List<String> localeNames;
        
        if (supportedLocales != null)
        {
            localeNames = new ArrayList<String>(supportedLocales.size());
            
            for (Locale locale : locales.getSupportedLocales()) {
                localeNames.add(locale.getLanguage());
            }            
        }
        else {
            localeNames = Collections.emptyList();
        }
                
        return new GenericSelectionModel<String>(localeNames, null, null);
    }

    @OnEvent(component = "localeSelector", value="AjaxEvent")
    public Object onLocaleSelected()
    {
        String data = request.getParameter(AjaxEvent.REQUEST_PARAM_NAME);

        JSONObject json = new JSONObject(data);
        
        Locale locale = new Locale(json.getString("value"));

        if (locale != null) {
            persistentLocaleService.set(locale);
        }
        
        return null;
    }
 
    public Locale getCurrentLocale() {
        return persistentLocaleService.get();
    }

    public void setCurrentLocale(Locale locale) {
        this.persistentLocaleService.set(locale);
    }    
}
