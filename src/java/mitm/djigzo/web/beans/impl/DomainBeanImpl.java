/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.beans.impl;

import mitm.application.djigzo.UserLocality;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.util.Check;
import mitm.common.ws.WebServiceCheckedException;
import mitm.djigzo.web.beans.DomainBean;
import mitm.djigzo.web.beans.UserPreferencesBean;
import mitm.djigzo.web.entities.Domain;

public class DomainBeanImpl implements DomainBean
{
	private final Domain domain;
	
	private UserPreferencesBean userPreferences;
	
	public DomainBeanImpl(Domain domain) 
	{
		Check.notNull(domain, "domain");

		this.domain = domain;
	}

    @Override
	public String getDomain() {
		return domain.getDomain();
	}

    /**
     * The user locality (internal, external)
     */
    @Override
    public UserLocality getUserLocality()
    throws WebServiceCheckedException
    {
        return domain.getUserLocality();
    }

    /**
     * True if the domain is in use
     */
    @Override
    public boolean isInUse()
    throws WebServiceCheckedException
    {
        return domain.isInUse();
    }
	
    @Override
    public UserPreferencesBean getPreferences() 
    throws HierarchicalPropertiesException, WebServiceCheckedException 
    {
    	if (userPreferences == null) {
    		this.userPreferences = new UserPreferencesBeanImp(domain.getPreferences());
    	}
    	
    	return userPreferences;
    }

    @Override
	public void save() 
	throws HierarchicalPropertiesException, WebServiceCheckedException
	{
		if (userPreferences != null) {
			userPreferences.save();
		}
	}
}