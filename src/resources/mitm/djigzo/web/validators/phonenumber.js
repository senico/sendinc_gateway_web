Tapestry.Validator.phonenumber = function(field, message)
{
    field.addValidator(function(value)
    {
        if (value == null || value.length == 0) {
            return;
        }
            
        if (isNaN(Number(value)) || value.indexOf(".")!=-1 || value.length < 5 || value.indexOf("-")!=-1) 
        {
            throw message;
        }
    });
};