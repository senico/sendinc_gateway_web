/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.pages.dlp.patterns;

import mitm.application.djigzo.admin.FactoryRoles;
import mitm.application.djigzo.ws.PolicyPatternManagerWS;
import mitm.application.djigzo.ws.PolicyPatternNodeDTO;
import mitm.common.ws.WebServiceCheckedException;

import org.apache.commons.lang.StringUtils;
import org.apache.tapestry5.PersistenceConstants;
import org.apache.tapestry5.ValidationException;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.corelib.components.TextField;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.annotation.Secured;

@Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_DLP_MANAGER})
@IncludeStylesheet("context:styles/pages/dlp/patterns/rename.css")
public class PatternsRename
{
    private final static Logger logger = LoggerFactory.getLogger(PatternsRename.class);
    
    /*
     * The new name of this pattern
     */
    private String newName;

    @Inject
    private PolicyPatternManagerWS policyPatternManagerWS;

    /*
     * Set to the PolicyPatternNode that is being edited
     */
    private PolicyPatternNodeDTO policyPatternNode;
    
    /*
     * True if and error has occurred
     */
    @Persist(PersistenceConstants.FLASH)
    private boolean error;

    /*
     * Error message if an error has occurred
     */
    @Persist(PersistenceConstants.FLASH)
    private String errorMessage;

    @SuppressWarnings("unused")
    @Component(id = "currentName", parameters = {"value = policyPatternNode.name"})
    private TextField currentNameField;
    
    @SuppressWarnings("unused")
    @Component(id = "newName", parameters = {"value = newName", "validate = required, regexp"})
    private TextField newNameField;

    @SetupRender
    @Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_DLP_MANAGER})
    public void setupRender()
    {
        /*
         * prefill with current name
         */
        if (newName == null && policyPatternNode != null) {
            newName = policyPatternNode.getName();
        }
    }
    
    @Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_DLP_MANAGER})
    public Object onActivate(String name)
    throws WebServiceCheckedException 
    {
        policyPatternNode = policyPatternManagerWS.getPattern(name);
        
        if (policyPatternNode == null) {
            return PatternsView.class;
        }
     
        return null;
    }
    
    public String onPassivate() {
        return policyPatternNode != null ? policyPatternNode.getName() : null;
    }
        
    public void onValidateFromNewName(String newName)
    throws ValidationException, WebServiceCheckedException
    {
        if (newName != null)
        {
            if (policyPatternManagerWS.getPattern(StringUtils.trim(newName)) != null) {
                throw new ValidationException("A pattern with this name already exists.");
            }
        }
    }
    
    public Object onSuccess()
    {
        Object result = null;

        try {
            policyPatternManagerWS.renamePattern(policyPatternNode.getName(), 
                    StringUtils.trim(newName));
        }
        catch(WebServiceCheckedException e)
        {
            logger.error("Error during submit.", e);
            
            errorMessage = e.getMessage();
            error = true;
        }

        return result;
    }
    
    /*
     * Event handler called when the cancel button is pressed.
     */
    protected Object onCancel() {
        return PatternsView.class;
    }
    
    public String getNewName() {
        return newName;
    }

    public void setNewName(String newName) {
        this.newName = newName;
    }

    public boolean isError() {
        return error;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public PolicyPatternNodeDTO getPolicyPatternNode() {
        return policyPatternNode;
    }
}
