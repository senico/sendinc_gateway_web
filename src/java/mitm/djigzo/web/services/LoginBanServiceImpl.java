/*
 * Copyright (c) 2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.services;

import java.util.concurrent.atomic.AtomicLong;

import org.apache.commons.lang.StringUtils;

import mitm.common.cache.RateCounter;
import mitm.common.util.Check;

public class LoginBanServiceImpl implements LoginBanService
{
    private final static String BASE_KEY = "LoginBanService";
    
    /*
     * The RateCounter is used to keep track of the login failures
     */
    private final RateCounter rateCounter;
    
    /*
     * Counter that is increased with every failure. The counter is used to make
     * sure that every subkey is unique
     */
    private final AtomicLong failureCounter = new AtomicLong();
    
    /*
     * The time in milliseconds a login failure stays in the cache
     */
    private final long lifetime;
    
    /*
     * If the number of failures for an IP/Username exceeds maxFailures, the IP/Username will
     * be banned for some time 
     */
    private final int maxFailures;
    
    /*
     * If true, the Ban filter will be active
     */
    private final boolean enabled;
    
    public LoginBanServiceImpl(RateCounter rateCounter, long lifetime, int maxFailures, boolean enabled)
    {
        Check.notNull(rateCounter, "rateCounter");
        
        this.rateCounter = rateCounter;
        this.lifetime = lifetime;
        this.maxFailures = maxFailures;
        this.enabled = enabled;
    }
    
    @Override
    public void addFailure(String remoteAddress, String loginName)
    {
        if (enabled) {
            rateCounter.addKey(getKey(remoteAddress, loginName), getSubKey(), lifetime);
        }
    }

    @Override
    public boolean isBanned(String remoteAddress, String loginName) {
        return enabled && rateCounter.getItemCount(getKey(remoteAddress, loginName)) >= maxFailures;
    }

    private String getSubKey() {
        return Long.toString(failureCounter.incrementAndGet());
    }
    
    private String getKey(String remoteAddress, String loginName) {
        return BASE_KEY + ":" + StringUtils.defaultString(remoteAddress) + ":" + StringUtils.defaultString(loginName);
    }    
}
