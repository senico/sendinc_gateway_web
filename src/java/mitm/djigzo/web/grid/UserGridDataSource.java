/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.grid;

import java.util.ArrayList;
import java.util.List;

import mitm.application.djigzo.DjigzoRuntimeException;
import mitm.common.hibernate.SortDirection;
import mitm.common.util.Check;
import mitm.common.ws.WebServiceCheckedException;
import mitm.djigzo.web.beans.UserBean;
import mitm.djigzo.web.beans.impl.UserBeanImpl;
import mitm.djigzo.web.entities.User;
import mitm.djigzo.web.entities.UserManager;

import org.apache.commons.lang.StringUtils;
import org.apache.tapestry5.grid.ColumnSort;
import org.apache.tapestry5.grid.GridDataSource;
import org.apache.tapestry5.grid.SortConstraint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UserGridDataSource implements GridDataSource 
{
	private final static Logger logger = LoggerFactory.getLogger(UserGridDataSource.class);
	
	private List<UserBean> userBeans;
	
	/*
	 * we need to keep track of base index because the index in getRowValue is the absolute
	 * index and not relative.
	 */
	private int startIndex;
	
	private final UserManager userManager;
	
	/*
	 * Filter used for filtering the user
	 */
	private final String filter;

	public UserGridDataSource(UserManager userManager, String filter)
	{
		Check.notNull(userManager, "userManager");
		
		this.userManager = userManager;
		this.filter = filter;
	}
	
	private String getSearch()
	{
		String search = filter;
		
		if (search != null) 
		{
			if (search.trim().length() == 0) {
				search = null;
			}
			else {
				/*
				 * If the filter string does not contain a LIKE special symbol ('%' or '_') the search
				 * string will be embedded in %%.
				 */
				if (!StringUtils.containsAny(search, "%_")) {
					search = "%" + search + "%";
				}
			}
		}
		
		return search;
	}
	
	private List<User> getUsers(int startIndex, int maxItems, SortDirection sortDirection) 
	throws WebServiceCheckedException
	{
		String search = getSearch();
		
		return search != null ? 
				userManager.searchUsers(search, startIndex, maxItems, sortDirection) :
				userManager.getUsers(startIndex, maxItems, sortDirection);
	}
	
	private int getUserCount() 
	throws WebServiceCheckedException
	{
		int count;
		
		String search = getSearch();
		
		if (search != null)
		{
			count = userManager.getSearchUsersCount(search);
		}
		else  {
			count = userManager.getUserCount();
		}
		
		return count;
	}
	
    @Override
	public int getAvailableRows() 
	{
		try {
			return getUserCount();
		} 
		catch (WebServiceCheckedException e) {
			throw new DjigzoRuntimeException(e);
		}
	}

    @Override
	public Class<UserBean> getRowType() 
	{
		return UserBean.class;
	}

    @Override
	public UserBean getRowValue(int index) 
	{
		int rowIndex = index - startIndex;
		
		UserBean value = null;
		
		if (userBeans != null && rowIndex < userBeans.size()) {
			value = userBeans.get(rowIndex);
		}
		else {
			logger.warn("Not enough rows.");
			/* 
			 * We do not have enough Bean objects. This can happen when AvailableRows
			 * returned more rows than were actually available when prepared was called. 
			 */ 
			value = null;
		}
		
		return value;
	}

    @Override
	public void prepare(int startIndex, int endIndex, List<SortConstraint> sortConstraints) 
	{
		try {
			SortDirection sortDirection = sortConstraints.size() > 0 && 
				sortConstraints.get(0).getColumnSort() == ColumnSort.DESCENDING ? 
						SortDirection.DESC : SortDirection.ASC;
			
			List<User> users = getUsers(startIndex, endIndex - startIndex + 1, 
					sortDirection);

			if (users != null)
			{
				userBeans = new ArrayList<UserBean>(users.size());
				
				for (User user : users) {
					userBeans.add(new UserBeanImpl(user));
				}
			}
			else {
				userBeans = new ArrayList<UserBean>();
			}
		} 
		catch (WebServiceCheckedException e) {
			throw new DjigzoRuntimeException(e);
		} 
		
		this.startIndex = startIndex;
	}
}
