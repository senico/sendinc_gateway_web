/*
 * Copyright (c) 2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.components.propertyeditor;

import mitm.common.security.smime.SMIMEEncryptionAlgorithm;

import org.apache.tapestry5.annotations.Parameter;

/**
 * Somehow I cannot create a generic version of EnumPropery so I need to extend it for every enum
 * I need a property for :(
 *  
 * @author Martijn Brinkers
 *
 */
public class SMIMEEncryptionAlgorithmPropertyEdit extends AbstractEnumPropertyEdit
{	
	@Parameter(required = true)
	private SMIMEEncryptionAlgorithm value;
	
	public void setValue(SMIMEEncryptionAlgorithm value) {
		this.value = value;
	}

	public SMIMEEncryptionAlgorithm getValue() {
		return value;
	}
	
	@Override
	protected Class<SMIMEEncryptionAlgorithm> getEnumClass() {
		return SMIMEEncryptionAlgorithm.class;
	}
}