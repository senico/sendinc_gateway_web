var frame = $('attachmentsFrame');

// initially start with a small frame (Chrome does not make a frame smaller, only larger)
frame.style.height='10px';

function resizeFrame()
{
	if(frame.contentWindow) {
		var framebody = frame.contentWindow.document.body;
	} 
	else if(frame.contentDocument) {
		var framebody = frame.contentDocument.document.body;
	}

    // +6 to prevent scrollbars
	var newheight = framebody.scrollHeight + 6;

	frame.style.height = newheight + "px";
}

// called after a new locale is selected
function afterSelectLocale(data)
{
	// we need enable the saveDraft button before we can click it
	$('saveDraft').disabled = false;

	// postpone clicking the saveDraft button because if it was initially disabled, click won't work right away
	// note: I tried to use defer() but this did not work reliable on Firefox. On Chrome it does not work if 
	// the delay is set to 1 so we set it to 100 (what's the best way to do this?????)
	window.setTimeout(saveDraft, 100);
}        

function saveDraft()
{
	$('saveDraft').click();
}