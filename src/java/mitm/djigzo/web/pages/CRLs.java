/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.pages;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import mitm.application.djigzo.admin.FactoryRoles;
import mitm.application.djigzo.ws.CRLTrustCheckResult;
import mitm.application.djigzo.ws.X509CRLStoreWS;
import mitm.common.security.asn1.ObjectEncoding;
import mitm.common.ws.WebServiceCheckedException;
import mitm.djigzo.web.beans.X509CRLBean;
import mitm.djigzo.web.common.CRLDownloadConst;
import mitm.djigzo.web.common.ContentTypes;
import mitm.djigzo.web.common.streamresponse.ByteArrayFileResponse;
import mitm.djigzo.web.grid.CRLGridDataSource;

import org.apache.commons.lang.StringUtils;
import org.apache.tapestry5.Asset;
import org.apache.tapestry5.BindingConstants;
import org.apache.tapestry5.ComponentResources;
import org.apache.tapestry5.Link;
import org.apache.tapestry5.StreamResponse;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.annotations.Path;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.beaneditor.BeanModel;
import org.apache.tapestry5.corelib.components.Grid;
import org.apache.tapestry5.grid.GridDataSource;
import org.apache.tapestry5.internal.services.LinkFactory;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.ioc.annotations.Value;
import org.apache.tapestry5.json.JSONObject;
import org.apache.tapestry5.services.BeanModelSource;
import org.apache.tapestry5.services.Request;
import org.apache.tapestry5.services.Response;
import org.springframework.security.annotation.Secured;

@IncludeStylesheet("context:styles/pages/crls.css")
public class CRLs
{
    @Inject
    private X509CRLStoreWS crlStore;
    
	@Inject
	private Request request;
    
	@Inject
	private Response response;
	
	@Inject
	private LinkFactory linkFactory;
    
    @Inject
    @Value("${crls.rowsPerPage}")
    private int rowsPerPage;    
	
	/*
	 * The CRL currently being drawn by the grid (is set by the grid component)
	 */
	private X509CRLBean crl;
	
	/*
	 * Set of the selected crls (thumbprints).
	 */
	@Persist
	private Set<String> selected;
	
	@SuppressWarnings("unused")
	@Component(parameters = {"source = source", "model = model", "row = crl", "rowClass = rowClass",
			"reorder = select,delete,issuer,thisUpdate,nextUpdate,version,crlNumber," +
			"deltaIndicator,deltaCRL,thumbprintSHA1,thumbprint,signatureAlgorithm,info", "volatile = true", 
			"rowsPerPage = prop:rowsPerPage"})
    private Grid grid;

	@Parameter(value = "crl-not-trusted", required = false, defaultPrefix = BindingConstants.LITERAL)
    private String notTrustedRowClass;

	@Parameter(required = false, defaultPrefix = BindingConstants.LITERAL)
    private String trustedRowClass;
	
	@Inject
    private BeanModelSource beanModelSource;

    @Inject
    private ComponentResources resources;

	@Inject
    @Path("context:icons/cross.png")
    private Asset deleteAsset;

	/*
	 * Stores the CRLTrustCheckResult for the current CRL because we
	 * need it more than once and it is an expensive call.
	 */
	private CRLTrustCheckResult trustCheckResult;
	
    @SetupRender
    public void setupGrid() 
    {
        if (selected == null) {
            selected = Collections.synchronizedSet(new HashSet<String>());
        }
        else {
            selected.clear();
        }
    }
	
    public void setCRL(X509CRLBean crl)
    {
    	this.crl = crl;
    	
    	/*
    	 * Reset the cached value because we have a new CRL
    	 */
    	trustCheckResult = null;
    }
    
    public X509CRLBean getCRL() {
    	return crl;
    }

	public boolean getSelect() {
		return selected.contains(crl.getThumbprint());
	}
	
	public CRLTrustCheckResult getTrustCheckResult() 
	throws WebServiceCheckedException
	{
		if (trustCheckResult == null)
		{
			/*
			 * There is not cached result
			 */
			trustCheckResult = crlStore.checkTrust(crl.getThumbprint());
		}
		
		return trustCheckResult;
	}
	
	public void setSelect(boolean value) 
	{
		/*
		 * Ignored. Checkbox values will be handled using EventCheckbox (see onCheckboxClick)
		 */
	}
	
	public GridDataSource getSource() {
		return new CRLGridDataSource(crlStore);
	}

	public Asset getDeleteAsset() {
		return deleteAsset;
	}
	
    public String getRowClass() 
    throws WebServiceCheckedException
    {
    	String rowClass = null;

    	rowClass = !getTrustCheckResult().isTrusted() ?	notTrustedRowClass : trustedRowClass;

    	return rowClass;
    }
    
    public BeanModel<X509CRLBean> getModel()
    {
        BeanModel<X509CRLBean> model = beanModelSource.createDisplayModel(X509CRLBean.class, 
        		resources.getMessages());
        
        model.add("delete", null);
        model.add("select", null);
        model.add("info", null);

        /*
         * Disable all sorting
         */
        for (String column : model.getPropertyNames()) {
        	model.get(column).sortable(false);
        }
        
        return model;
    }

    @OnEvent(component = "deleteCRL")
	@Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_PKI_MANAGER})
    protected void deleteCRL(String thumbprint) 
    throws WebServiceCheckedException 
    {
    	crlStore.remove(thumbprint);
    }

    /*
     * Event handler called when the checkbox is clicked. 
     */
    public void onCheckboxEvent()
    {
    	String data = request.getParameter("data");

    	if (StringUtils.isEmpty(data)) {
    		throw new IllegalArgumentException("data is missing.");
    	}
    	
    	JSONObject json = new JSONObject(data);
    	    	    	
    	String element = json.getString("element");
    	boolean checked = json.getBoolean("checked"); 
    	
    	if (checked) {
    		selected.add(element);
    	}
    	else {
    		selected.remove(element);
    	}
    }
    
    @OnEvent(component = "deleteSelected")
	@Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_PKI_MANAGER})
    protected void deleteSelected()
    throws WebServiceCheckedException 
    {
        /*
         * Note: we need to clone the set to make sure that concurrent modifications
         * are possible.
         */
        String[] thumbprints = selected.toArray(new String[]{});
        
        for (String thumbprint : thumbprints)	{
            crlStore.remove(thumbprint);
        }
    }

    @OnEvent(component = "downloadSelected")
    protected StreamResponse downloadSelected() 
    throws WebServiceCheckedException 
    {
    	StreamResponse response = null;
    	
        /*
         * Note: we need to clone the set to make sure that concurrent modifications
         * are possible.
         */
        String[] thumbprints = selected.toArray(new String[]{});
        
        /*
         * We assume that the id's of the checkboxes are thumbprints of certificates
         */

        if (thumbprints.length > 0)
        {
            ObjectEncoding crlEncoding = CRLDownloadConst.DEFAUL_ENCODING;

            String filename;

            byte[] encodedCertificates;

            if (thumbprints.length == 1)
            {
                filename = CRLDownloadConst.getFilename(true /* single CRL */, crlEncoding);

                encodedCertificates = crlStore.getEncodedCRL(
                        thumbprints[0], crlEncoding);
            }
            else {
                filename = CRLDownloadConst.getFilename(false /* multiple CRLs */, crlEncoding);

                encodedCertificates = crlStore.getEncodedCRLs(
                        Arrays.asList(thumbprints), crlEncoding);
            }

            response = new ByteArrayFileResponse(encodedCertificates, ContentTypes.X_DOWNLOAD,
                    filename);
        }

        /*
         * We need to clear the selected. This is not visually reflected (we cannot refresh the page
         * and download the CRLs afaik) so the check boxes are unchecked using Javascript.
         * Another option would be to redirect to the same page and persist (flash) the  certificate 
         * byte array.
         */
        selected.clear();
    	
    	return response;
    }
    
    @OnEvent(component = "update")
	@Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_PKI_MANAGER})
    protected void update()
    throws WebServiceCheckedException 
    {
    	crlStore.update();
    }
    
    @OnEvent(component = "viewCRLLink")
    protected void viewCRL(String thumbprint) 
    throws IOException 
    {
        Link link = linkFactory.createPageRenderLink("crl/view", false, thumbprint);

        response.sendRedirect(link);
    }

    public int getRowsPerPage() {
        return rowsPerPage;
    }
}
