/*
 * Copyright (c) 2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.djigzo.web.services.security;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mitm.common.mail.EmailAddressUtils;
import mitm.common.util.Check;

import org.apache.commons.lang.StringUtils;
import org.springframework.security.Authentication;
import org.springframework.security.context.SecurityContextHolder;

/**
 * Spring Security Filter that checks whether the user identifed by the request parameter usernameRequestParameter 
 * is the same user as the logged in user and if not, the session will be invalidated en the request will be 
 * 'reloaded'.
 * 
 * @author Martijn Brinkers
 *
 */
public class InvalidateUserSpringSecurityFilter implements Filter
{
    /*
     * The request param which identifies the logged in user
     */
    private final String usernameRequestParameter;
    
    public InvalidateUserSpringSecurityFilter(String usernameRequestParameter)
    {
        Check.notNull(usernameRequestParameter, "usernameRequestParameter");
        
        this.usernameRequestParameter = usernameRequestParameter;
    }
    
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
    throws IOException, ServletException
    {
        if (!(request instanceof HttpServletRequest)) {
            throw new ServletException("HttpServletRequest expected.");
        }
        
        if (!(response instanceof HttpServletResponse)) {
            throw new ServletException("HttpServletResponse expected.");
        }
        
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;
        
        String loggedInUserName = null;

        Authentication loggedInUser = SecurityContextHolder.getContext().getAuthentication();
        
        if (loggedInUser != null) {
            loggedInUserName = loggedInUser.getName();
        }

        if (loggedInUserName != null)
        {
            String email = StringUtils.trimToNull(request.getParameter(usernameRequestParameter));
            
            if (email != null)
            {
                email = EmailAddressUtils.canonicalize(email);
                
                if (!email.equals(loggedInUserName))
                {
                    /*
                     * The user has changed, so invalidate session
                     */
                    HttpSession session = httpServletRequest.getSession(false);
                    
                    if (session != null) {
                        session.invalidate();
                    }
                    
                    SecurityContextHolder.clearContext();
                    
                    /*
                     * We need to 'reload' the complete request to make sure the user need to login
                     */
                    StringBuffer redirectURL = httpServletRequest.getRequestURL();
                    
                    if (httpServletRequest.getQueryString() != null) {
                        redirectURL.append("?").append(httpServletRequest.getQueryString());
                    }
                    
                    httpServletResponse.sendRedirect(httpServletResponse.encodeRedirectURL(redirectURL.toString()));
                    
                    return;
                }
            }
        }
        
        chain.doFilter(request, response);
    }
    
    @Override
    public void destroy() {
        // do nothing
    }

    @Override
    public void init(FilterConfig config)
    throws ServletException
    {
        // do nothing
    }
}
