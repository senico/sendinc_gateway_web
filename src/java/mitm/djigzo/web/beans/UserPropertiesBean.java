/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 *
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar,
 * wsdl4j-1.6.1.jar (or modified versions of these libraries),
 * containing parts covered by the terms of Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to
 * convey the resulting work.
 */
package mitm.djigzo.web.beans;

import mitm.application.djigzo.EncryptMode;
import mitm.application.djigzo.UserLocality;

import mitm.common.properties.HierarchicalPropertiesException;


/**
 * Represents the user properties of a user
 *
 * @author Martijn Brinkers
 *
 */
public interface UserPropertiesBean
{
	/**
	 * The UserLocality determines whether the user is an internal user or an external user
	 */
	public EnumProperty<UserLocality> getUserLocality();

	/**
	 * The encrypt mode for the user
	 */
	public EnumProperty<EncryptMode> getEncryptMode();

	/**
   * If true, a notification will be sent to the sender when the message is encrypted
   */
	public BooleanProperty getSendEncryptionNotification();

	/**
	 * The trigger is matched against the subject to see if encryption must be forced
	 */
	public StringProperty getSubjectTrigger();

	/**
	 * If false, subject trigger is not used (ie. encryption using the subject cannot be forced)
	 */
	public BooleanProperty getSubjectTriggerEnabled();

	/**
	 * If true, the subject trigger is used as a regular expression
	 */
	public BooleanProperty getSubjectTriggerIsRegExpr();

	/**
	 * If true, the matched subject pattern will be removed. Example: if subject is "test [encrypt]" and the
	 * subject trigger is [encrypt] the subject will be changed to "test "
	 */
	public BooleanProperty getSubjectTriggerRemovePattern();


	/**
	 * Returns the server secret which is used for generating URLs etc. (using HMAC)
	 */
  public StringProperty getServerSecret();

  /**
   * If true, messages a header can force encrypt of a message (see ForceEncryptHeaderTrigger)
   */
  public BooleanProperty getForceEncryptAllowed();

  /**
   * The trigger that can trigger encrypting the message when a header
   * matches the trigger
   */
  public StringProperty getForceEncryptHeaderTrigger();



    /**
     * If true, security info will be added to the subject when the message is handled by S/MIME handler
     */
    public BooleanProperty getAddSecurityInfo();

    /**
     * Tag that will be added to the subject if the messages was decrypted (only if addSecurityInfo is true)
     */
    public StringProperty getSecurityInfoDecryptedTag();

    /**
     * Tag that will be added to the subject if the messages was signed and valid (only if addSecurityInfo is true)
     */
    public StringProperty getSecurityInfoSignedValidTag();

    /**
     * Tag that will be added to the subject if the messages was signed and valid (only if addSecurityInfo is true).
     * Additionally, the email address of the signer will be added because the from is different from the signer.
     */
    public StringProperty getSecurityInfoSignedByValidTag();

    /**
     * Tag that will be added to the subject if the messages was signed but invalid (only if addSecurityInfo is true)
     */
    public StringProperty getSecurityInfoSignedInvalidTag();

    /**
     * If true, the subject filter will be enabled
     */
    public BooleanProperty getSubjectFilterEnabled();

    /**
     * The subject filter reg ex.
     */
    public StringProperty getSubjectFilterRegEx();

	/**
	 * Saves (makes persistent) the user properties
	 */
	public void save()
	throws HierarchicalPropertiesException;
}
