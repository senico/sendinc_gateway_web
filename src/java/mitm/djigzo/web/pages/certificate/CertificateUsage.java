/*
 * Copyright (c) 2009-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.pages.certificate;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import mitm.application.djigzo.ws.CertificateStore;
import mitm.application.djigzo.ws.KeyAndCertStoreWS;
import mitm.application.djigzo.ws.UserPreferencesWorkflowWS;
import mitm.application.djigzo.ws.X509CertificateDTO;
import mitm.common.ws.WebServiceCheckedException;
import mitm.djigzo.web.beans.CertificateUsageBean;
import mitm.djigzo.web.beans.X509CertificateBean;
import mitm.djigzo.web.beans.impl.X509CertificateBeanImpl;
import mitm.djigzo.web.common.ValidityCheck;
import mitm.djigzo.web.grid.CertificateUsageGridDataSource;
import mitm.djigzo.web.services.CertificateStoreMarker;

import org.apache.commons.lang.StringUtils;
import org.apache.tapestry5.ComponentResources;
import org.apache.tapestry5.Link;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.beaneditor.BeanModel;
import org.apache.tapestry5.corelib.components.Grid;
import org.apache.tapestry5.internal.services.LinkFactory;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.ioc.annotations.Value;
import org.apache.tapestry5.services.BeanModelSource;
import org.apache.tapestry5.services.Response;

@IncludeStylesheet("context:styles/pages/certificate/certificateUsage.css")
public class CertificateUsage
{
    @Inject
    @Property
    @CertificateStoreMarker
    private KeyAndCertStoreWS keyAndCertStoreWS;

    @Inject
    private BeanModelSource beanModelSource;

    @Inject
    private ComponentResources resources;

    @Inject
    private UserPreferencesWorkflowWS userPreferencesWorkflowWS;

    @Inject
    private Response response;
    
    @Inject
    private LinkFactory linkFactory;
    
    @Inject
    @Value("${certificateUsage.rowsPerPage}")
    private int rowsPerPage;    
    
    private X509CertificateBean certificate;

    @SuppressWarnings("unused")
    @Component(parameters = {"source = gridDataSource", "model = model", "volatile = true", "rowsPerPage = prop:rowsPerPage",
            "reorder = type, usage, identifier"})
    private Grid grid;
    
    public BeanModel<CertificateUsageBean> getModel()
    {
        BeanModel<CertificateUsageBean> model = beanModelSource.createDisplayModel(CertificateUsageBean.class, 
                resources.getMessages());
        
        /*
         * Disable all sorting
         */
        for (String column : model.getPropertyNames()) {
            model.get(column).sortable(false);
        }
        
        return model;
    }
    
    public CertificateUsageGridDataSource getGridDataSource() {
        return new CertificateUsageGridDataSource(userPreferencesWorkflowWS, certificate.getThumbprint());
    }
    
    public void onActivate(String thumbprint)
    throws WebServiceCheckedException 
    {
        if (thumbprint != null)
        {
            X509CertificateDTO certificateDTO = keyAndCertStoreWS.getCertificate(thumbprint);
            
            if (certificateDTO != null) {
                certificate = new X509CertificateBeanImpl(certificateDTO);
            }
        }
    }
    
    public List<Object> onPassivate()
    {
        List<Object> context = new LinkedList<Object>();
        
        if (certificate != null) {
            context.add(certificate.getThumbprint());
        }
        
        return context;
    }

    @OnEvent(component="subjectLink")
    protected void onSubjectClicked(String thumbprint) 
    throws IOException
    {
        if (StringUtils.isNotBlank(thumbprint))
        {
            Link link = linkFactory.createPageRenderLink("certificate/view", false, CertificateStore.CERTIFICATES,
                    thumbprint, ValidityCheck.TRUST);
    
            response.sendRedirect(link);
        }
    }
    
    public X509CertificateBean getCertificate() {
        return certificate;
    }

    public int getRowsPerPage() {
        return rowsPerPage;
    }
}
