/*
 * Copyright (c) 2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.utils;

import mitm.common.util.Check;

import org.apache.commons.lang.StringUtils;
import org.springframework.security.providers.encoding.PasswordEncoder;

/**
 * Implementation of PasswordCodec that uses {@link org.springframework.security.providers.encoding.PasswordEncoder}
 * for hashing the password.
 * 
 * @author Martijn Brinkers
 *
 */
public class PasswordCodecImpl implements PasswordCodec
{
    /*
     * Used for hashing the password
     */
    private final PasswordEncoder passwordEncoder;

    private static class PasswordAndSaltImpl implements PasswordAndSalt
    {
        final String password;
        final String salt;
        
        PasswordAndSaltImpl(String password, String salt)
        {
            this.password = password;
            this.salt = salt;
        }
        
        @Override
        public String getPassword() {
            return password;
        }
        @Override
        public String getSalt() {
            return salt;
        }
    }
    
    public PasswordCodecImpl(PasswordEncoder passwordEncoder)
    {
        Check.notNull(passwordEncoder, "passwordEncoder");
        
        this.passwordEncoder = passwordEncoder;
    }

    private String generateSalt() {
        return Long.toString(System.currentTimeMillis());
    }
    
    @Override
    public PasswordAndSalt getPasswordAndSalt(String encodedPassword)
    {
        if (encodedPassword == null) {
            return null;
        }
        
        int i = encodedPassword.indexOf(':');
        
        if (i == -1) {
            throw new IllegalArgumentException("Password seems not to be encoded");
        }
        
        String salt = StringUtils.substring(encodedPassword, 0, i);
        String hashedPassword = StringUtils.substring(encodedPassword, i + 1);
        
        if (salt == null || StringUtils.isEmpty(hashedPassword)) {
            throw new IllegalArgumentException("Password seems not to be encoded");
        }
        
        return new PasswordAndSaltImpl(hashedPassword, salt);
    }
    
    @Override
    public String encodePassword(String password)
    {
        if (password == null) {
            return null;
        }
        
        String salt = generateSalt();
        
        String encodedPassword = passwordEncoder.encodePassword(password, salt);
        
        return salt + ":" + encodedPassword;
    }
    
    @Override
    public boolean isPasswordValid(String currentEncodedPassword, String rawPassword)
    {
        Check.notNull(currentEncodedPassword, "currentEncodedPassword");
        Check.notNull(rawPassword, "rawPassword");
        
        PasswordAndSalt passwordAndSalt = getPasswordAndSalt(currentEncodedPassword);
        
        return passwordEncoder.isPasswordValid(passwordAndSalt.getPassword(), rawPassword, 
                passwordAndSalt.getSalt());
    }
}
