/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.pages.dlp.patterns;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import mitm.application.djigzo.admin.FactoryRoles;
import mitm.application.djigzo.ws.DomainWS;
import mitm.application.djigzo.ws.GlobalPreferencesManagerWS;
import mitm.application.djigzo.ws.PolicyPatternNodeDTO;
import mitm.application.djigzo.ws.UserPreferencesDTO;
import mitm.application.djigzo.ws.UserPreferencesPolicyPatternManagerWS;
import mitm.application.djigzo.ws.UserWS;
import mitm.common.ws.WebServiceCheckedException;
import mitm.djigzo.web.common.PropertyType;
import mitm.djigzo.web.common.YesNo;
import mitm.djigzo.web.components.DLPPatternGrid;
import mitm.djigzo.web.pages.dlp.DLPSettings;

import org.apache.tapestry5.ComponentResources;
import org.apache.tapestry5.Link;
import org.apache.tapestry5.PersistenceConstants;
import org.apache.tapestry5.annotations.Cached;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.ioc.annotations.Value;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.annotation.Secured;

@Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_DLP_MANAGER})
@IncludeStylesheet("context:styles/pages/dlp/patterns/userView.css")
public class PatternsUserView
{
    private final static Logger logger = LoggerFactory.getLogger(PatternsUserView.class);
    
    @Inject
    @Value("${dlp.patterns.rowsPerPage}")
    private int rowsPerPage;    

    @Inject
    private UserWS userWS;

    @Inject
    private DomainWS domainWS;
    
    @Inject
    private GlobalPreferencesManagerWS globalPreferencesManagerWS;
    
    @Inject
    private UserPreferencesPolicyPatternManagerWS userPreferencesPolicyPatternManagerWS;

    @Inject
    private ComponentResources resources;
    
    /*
     * The property type being edited (user, domain etc.)
     */
    private PropertyType propertyType;

    /*
     * The UserPreferences (user, domain or global).
     */
    private UserPreferencesDTO userPreferences;
    
    /*
     * If true, the patterns are inherited
     */
    private boolean inherited;
    
    /*
     * True if the settings were applied
     */
    @Persist(PersistenceConstants.FLASH)
    private boolean applied;
    
    @Component(parameters = {"source = source", "rowsPerPage = prop:rowsPerPage"})
    private DLPPatternGrid patternGrid; 
    
    @SetupRender
    @Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_DLP_MANAGER})
    public void setupRender() {
        /*
         * Empty on purpose
         */
    }

    private UserPreferencesDTO loadDomainPreferences(String domain)
    throws WebServiceCheckedException
    {
        return domainWS.getDomainPreferences(domain);
    }

    private UserPreferencesDTO loadUserPreferences(String email)
    throws WebServiceCheckedException
    {
        return userWS.getUserPreferences(email);
    }

    private UserPreferencesDTO loadGlobalPreferences()
    throws WebServiceCheckedException
    {
        return globalPreferencesManagerWS.getGlobalUserPreferences();
    }
    
    @Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_DLP_MANAGER})
    protected Object onActivate(Object[] context)
    throws WebServiceCheckedException 
    {
        Object result = PatternsView.class;

        if (context == null || context.length < 1) {
            return result;
        }

        if (!(context[0] instanceof String)) {
            return result;
        }
        
        propertyType = PropertyType.fromString((String) context[0]);
        
        if (propertyType == null) {
            return result;
        }
        
        String id = null;
        
        if (context.length > 1)
        {
            if (!(context[1] instanceof String)) {
                return result;
            }
            
            id = (String) context[1];
        }

        switch(propertyType)
        {
        case DOMAIN : userPreferences = loadDomainPreferences(id); break;
        case USER   : userPreferences = loadUserPreferences(id); break;
        case GLOBAL : userPreferences = loadGlobalPreferences(); break;
        default:
            return result;
        }
        
        if (userPreferences == null) {
            return result;
        }
        
        initInherited();
        
        return null;
    }

    private void initInherited()
    throws WebServiceCheckedException
    {
        /*
         * If one the the patterns are inherited, make them all inherit.
         */
        List<PolicyPatternNodeDTO> patterns = getSource();

        if (patterns != null)
        {
            for (PolicyPatternNodeDTO pattern : patterns) {
                if (pattern.isInherited())
                {
                    inherited = true;
                    break;
                }
            }
        }
        else {
            /* 
             * if there are not patterns than the patterns are inherited. This can
             * happen when the global patterns are empty.
             */
            inherited = true;
        }
    }
    
    protected Object[] onPassivate()
    {
        List<Object> context = new ArrayList<Object>(2);

        if (propertyType != null) {
            context.add(propertyType);
        }
        
        if (userPreferences != null) {
            context.add(userPreferences.getName());
        }
        
        return context.toArray();
    }
    

    @Cached
    public List<PolicyPatternNodeDTO> getSource()
    throws WebServiceCheckedException
    {
        return userPreferencesPolicyPatternManagerWS.getPatterns(userPreferences);
    }

    /*
     * Event handler for delete event from the DLPPatternGrid
     */
    @OnEvent(component = "patternGrid", value = DLPPatternGrid.DELETE_PATTERN_EVENT)
    @Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_DLP_MANAGER})
    protected void removePattern(String name) 
    {
        if (userPreferences == null || name == null) {
            return;
        }
        
        try {
            userPreferencesPolicyPatternManagerWS.removePatterns(userPreferences, Collections.singleton(name));
        }
        catch(WebServiceCheckedException e) {
            logger.error("Error removing pattern: " + name, e);
        }
    }
    
    @OnEvent(component = "removeSelected")
    @Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_DLP_MANAGER})
    protected void removeSelectedPatterns()
    {
        /*
         * Note: we need to clone the set to make sure that concurrent modifications
         * are possible.
         */
        List<String> selected = Arrays.asList(patternGrid.getSelected().toArray(new String[]{})); 
        
        try {
            userPreferencesPolicyPatternManagerWS.removePatterns(userPreferences, selected);
        }
        catch(WebServiceCheckedException e) {
            logger.error("Some patterns could not be removed.", e);
        }
    }

    @OnEvent(component = "addPatterns")
    @Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_DLP_MANAGER})
    protected Link addPatterns()
    {
        return resources.createPageLink(PatternsUserAdd.class, false, onPassivate());
    }
    
    /*
     * Event handler for isDisabled event from the DLPPatternGrid
     */
    @OnEvent(component = "patternGrid", value = DLPPatternGrid.IS_DISABLED_EVENT)
    @Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_DLP_MANAGER})
    protected YesNo isDisabledEvent(PolicyPatternNodeDTO patternNode)
    throws WebServiceCheckedException 
    {
        /*
         * Disable if inherited
         */
        return patternNode != null && patternNode.isInherited() ? YesNo.YES : YesNo.NO;
    }
    
    public int getRowsPerPage() {
        return rowsPerPage;
    }
    
    public void onSuccess()
    throws WebServiceCheckedException
    {
        List<String> patternNames = null;
        
        if (!inherited)
        {
            /*
             * If it's not inherited, we should set the patterns explitly to make sure
             * the patterns are no longer inherited.
             */
            List<PolicyPatternNodeDTO> patterns = getSource();
            
            if (patterns != null)
            {
                patternNames = new ArrayList<String>(patterns.size());
                
                for (PolicyPatternNodeDTO pattern : patterns) {
                    patternNames.add(pattern.getName());
                }
            }
        }
            
        userPreferencesPolicyPatternManagerWS.setPatterns(userPreferences, patternNames);
        
        applied = true;
    }

    /*
     * Event handler called when the cancel button is pressed.
     */
    protected Object onCancel()
    {
        return resources.createPageLink(DLPSettings.class, false, 
                userPreferences.getName(), propertyType);
    }
    
    public boolean isInherited() {
        return inherited;
    }

    public void setInherited(boolean inherited) {
        this.inherited = inherited;
    }
    
    public boolean isApplied() {
        return applied;
    }
    
    public boolean isUserType() {
        return propertyType == PropertyType.USER;
    }

    public boolean isDomainType() {
        return propertyType == PropertyType.DOMAIN;
    }

    public boolean isGlobalType() {
        return propertyType == PropertyType.GLOBAL;
    }

    public UserPreferencesDTO getUserPreferences() {
        return userPreferences;
    }

    public String getUserPreferencesName() {
        return userPreferences.getName();
    }
}
