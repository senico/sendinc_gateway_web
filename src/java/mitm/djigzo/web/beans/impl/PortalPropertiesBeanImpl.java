/*
 * Copyright (c) 2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.beans.impl;

import mitm.application.djigzo.PortalProperties;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.util.Check;
import mitm.djigzo.web.beans.BooleanProperty;
import mitm.djigzo.web.beans.PortalPropertiesBean;
import mitm.djigzo.web.beans.StringProperty;
import mitm.djigzo.web.entities.SaveableUserProperties;

public class PortalPropertiesBeanImpl implements PortalPropertiesBean
{
	private final StringProperty password = new StringProperty();
    private final BooleanProperty enabled = new BooleanProperty();
    private final StringProperty baseURL = new StringProperty();
    private final BooleanProperty autoInvite = new BooleanProperty();
	
    /*
     * We need a reference to the SaveableUserProperties to be able to save 
     * the DLP properties. 
     */
    private final SaveableUserProperties saveableProperties;
    
	private final PortalProperties portalProperties;
	
	public PortalPropertiesBeanImpl(SaveableUserProperties saveableProperties) 
	throws HierarchicalPropertiesException 
	{
		Check.notNull(saveableProperties, "saveableProperties");
		
		this.saveableProperties = saveableProperties;
		this.portalProperties = saveableProperties.getPortalProperties();
		
		initializeProperties();
	}
	
	private boolean isInherited(String property)
	throws HierarchicalPropertiesException 
	{
		return portalProperties.isInherited(portalProperties.getFullPropertyName(property));
	}
	
	private void initializeProperties() 
	throws HierarchicalPropertiesException 
	{
	    password.setValue(portalProperties.getPassword());
	    password.setInherit(isInherited(PortalProperties.PASSWORD));

	    enabled.setValue(portalProperties.isEnabled());
	    enabled.setInherit(isInherited(PortalProperties.ENABLED));
	    
        baseURL.setValue(portalProperties.getBaseURL());
        baseURL.setInherit(isInherited(PortalProperties.BASE_URL));
        
        autoInvite.setValue(portalProperties.isAutoInvite());
        autoInvite.setInherit(isInherited(PortalProperties.AUTO_INVITE));
	}
	
    @Override
    public StringProperty getPassword() {
        return password;
    }

	@Override
    public BooleanProperty getEnabled() {
    	return enabled;
    }

    @Override
    public StringProperty getBaseURL() {
        return baseURL;
    }

    @Override
    public BooleanProperty getAutoInvite() {
        return autoInvite;
    }

    @Override
	public void save()
	throws HierarchicalPropertiesException
	{
        portalProperties.setPassword(password.getUserValue());
        portalProperties.setEnabled(enabled.getUserValue());
        portalProperties.setBaseURL(baseURL.getUserValue());
        portalProperties.setAutoInvite(autoInvite.getUserValue());
        
        saveableProperties.save();
	}
}