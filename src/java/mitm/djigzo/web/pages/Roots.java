/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.pages;

import java.io.IOException;

import mitm.application.djigzo.ws.CertificateStore;
import mitm.application.djigzo.ws.CertificateValidatorResult;
import mitm.application.djigzo.ws.CertificateValidatorWS;
import mitm.application.djigzo.ws.KeyAndCertStoreWS;
import mitm.common.ws.WebServiceCheckedException;
import mitm.djigzo.web.beans.X509CertificateBean;
import mitm.djigzo.web.common.ValidityCheck;
import mitm.djigzo.web.components.CertificateGrid;
import mitm.djigzo.web.components.CertificateStoreGrid;
import mitm.djigzo.web.services.RootStoreMarker;

import org.apache.tapestry5.Link;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.internal.services.LinkFactory;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.ioc.annotations.Value;
import org.apache.tapestry5.services.Response;

public class Roots
{
	@SuppressWarnings("unused")
	@Inject
	@Property
	@RootStoreMarker
    private KeyAndCertStoreWS certStore;
	
	@Inject
	private CertificateValidatorWS certificateValidatorWS; 
	
	@Inject
	private Response response;
	
	@Inject
	private LinkFactory linkFactory;
	
    @Inject
    @Value("${certificates.rowsPerPage}")
    private int defaultRowsPerPage;
	
    @Persist
    private Integer rowsPerPage;    
    
	@SuppressWarnings("unused")
	@Component(parameters = {"certStore = certStore", "showDownloadKeys = false", "rowsPerPage = prop:rowsPerPage"})
    private CertificateStoreGrid certificateStoreGrid;
	
	@OnEvent(CertificateGrid.CERTIFICATE_VALIDATOR_RESULT_EVENT)
	protected CertificateValidatorResult onGetCertificateValidatorResult(
			X509CertificateBean certificate) 
	throws WebServiceCheckedException
	{
		return certificateValidatorWS.checkValidity(CertificateStore.ROOTS, 
				certificate.getThumbprint());
	}

	@OnEvent(CertificateGrid.SUBJECT_CLICKED_EVENT)
	protected void onSubjectClicked(String thumbprint) 
	throws WebServiceCheckedException, IOException
	{
        Link link = linkFactory.createPageRenderLink("certificate/view", false, CertificateStore.ROOTS,
        		thumbprint, ValidityCheck.TRUST);

        response.sendRedirect(link);
	}

    public int getRowsPerPage()
    {
        return rowsPerPage != null ? rowsPerPage : defaultRowsPerPage;
    }
    
    public void setRowsPerPage(int rowsPerPage) {
        this.rowsPerPage = rowsPerPage;
    }
}
