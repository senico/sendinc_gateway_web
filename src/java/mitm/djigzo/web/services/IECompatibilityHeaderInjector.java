package mitm.djigzo.web.services;

import java.io.IOException;

import org.apache.tapestry5.services.Request;
import org.apache.tapestry5.services.RequestFilter;
import org.apache.tapestry5.services.RequestHandler;
import org.apache.tapestry5.services.Response;

/**
 * Injects a X-UA-Compatible compatibility header to make IE9 compatible with Prototype used by DJIGZO.
 * 
 * @author Martijn Brinkers
 *
 */
public class IECompatibilityHeaderInjector implements RequestFilter
{
    private static final String HEADER_KEY = "X-UA-Compatible";
    
    @Override
    public boolean service(Request request, Response response, RequestHandler handler)
    throws IOException
    {
        response.setHeader(HEADER_KEY, "IE=8");

        return handler.service(request, response);             
    } 
}
