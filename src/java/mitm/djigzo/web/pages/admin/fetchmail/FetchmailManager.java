/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.pages.admin.fetchmail;

import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import mitm.application.djigzo.admin.FactoryRoles;
import mitm.application.djigzo.ws.FetchmailManagerWS;
import mitm.common.fetchmail.FetchmailConfig;
import mitm.common.fetchmail.Poll;
import mitm.common.ws.WebServiceCheckedException;
import mitm.djigzo.web.pages.Admins;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.tapestry5.Asset;
import org.apache.tapestry5.ComponentResources;
import org.apache.tapestry5.PersistenceConstants;
import org.apache.tapestry5.annotations.Cached;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Path;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.beaneditor.BeanModel;
import org.apache.tapestry5.corelib.components.Checkbox;
import org.apache.tapestry5.corelib.components.Grid;
import org.apache.tapestry5.corelib.components.TextField;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.ioc.annotations.Value;
import org.apache.tapestry5.json.JSONObject;
import org.apache.tapestry5.services.BeanModelSource;
import org.apache.tapestry5.services.Request;
import org.springframework.security.AccessDeniedException;
import org.springframework.security.annotation.Secured;

@IncludeStylesheet("context:styles/pages/admin/fetchmail/fetchmailManager.css")
@Secured({FactoryRoles.ROLE_ADMIN})
public class FetchmailManager
{
    @Inject
    private FetchmailManagerWS fetchmailManager;
    
    @Inject
    private BeanModelSource beanModelSource;

    @Inject
    private ComponentResources resources;
    
    @Inject
    private Request request;
    
    @Inject
    @Value("${fetchmail.rowsPerPage}")
    private int rowsPerPage;    

    @Inject
    @Value("${fetchmail.hidePasswords}")
    private boolean hidePasswords;    
    
    /*
     * If true the Fetchmail pages are enabled
     */
    @Inject 
    @Value("${fetchmail.enabled}")
    private boolean fetchmailEnabled;
    
    @Inject
    @Path("context:icons/cross.png")
    private Asset deleteAsset;
    
    @Persist(PersistenceConstants.FLASH)
    private boolean applied;

    @Persist(PersistenceConstants.FLASH)
    private boolean applyError;
    
    /*
     * The failure message when backup
     */    
    @Persist(PersistenceConstants.FLASH)
    private String failureMessage;
    
    /*
     * The current rendering row
     */
    private Poll row;
    
    /*
     * Set of the selected Polls
     */
    @Persist
    private Set<String> selected;
    
    @SuppressWarnings("unused")
    @Component(id = "postmaster", parameters = {"value = fetchmailConfig.postmaster", 
            "validate=required,maxlength=255,emailmitm=1"})
    private TextField postmasterField;

    @SuppressWarnings("unused")
    @Component(id = "pollInterval", parameters = {"value = fetchmailConfig.pollInterval", 
        "validate=required,min=1,max=9999999999"})
    private TextField pollIntervalField;

    @SuppressWarnings("unused")
    @Component(id = "checkCertificate", parameters = {"value = fetchmailConfig.checkCertificate"})
    private Checkbox checkCertificateField;
    
    @SuppressWarnings("unused")
    @Component(parameters = {"source = fetchmailConfig.polls", "model = model", "volatile = true", "row = row", 
            "reorder = select,delete", "exclude = id", "rowsPerPage = prop:rowsPerPage"})
    private Grid pollsGrid;
    
    /*
     * Checks if Fetmail is enabled
     */
    private void checkAccess()
    {
        if (!fetchmailEnabled) {
            throw new AccessDeniedException("Fetchmail is not enabled.");
        }
    }
    
    @SetupRender
    @Secured({FactoryRoles.ROLE_ADMIN})
    protected void setupRender() {
        checkAccess();
    }

    @SetupRender
    public void setupGrid() 
    {
        if (selected == null) {
            selected = Collections.synchronizedSet(new HashSet<String>());
        }
        else {
            selected.clear();
        }
    }
    
    public BeanModel<Poll> getModel()
    {
        BeanModel<Poll> model = beanModelSource.createDisplayModel(Poll.class, 
                resources.getMessages());
        
        model.add("delete", null).sortable(false);
        model.add("select", null).sortable(false);
        
        return model;
    }
    
    @Cached
    public FetchmailConfig getFetchmailConfig()
    throws WebServiceCheckedException
    {
        return fetchmailManager.getConfig();
    }
    
    public void onSuccess()
    {
        try {
            fetchmailManager.setConfig(getFetchmailConfig());
            fetchmailManager.applyConfig();
            
            applied = true;
        }
        catch (WebServiceCheckedException e)
        {
            applyError = true;
            failureMessage = ExceptionUtils.getRootCauseMessage(e);
        }
    }
        
    /*
     * Event handler called when the cancel button is pressed.
     */
    public Object onCancel() {
        return Admins.class;
    }
    
    @OnEvent(component = "deleteEntry")
    protected void deleteEntry(String id) 
    throws WebServiceCheckedException 
    {
        checkAccess();
        
        FetchmailConfig config = getFetchmailConfig();
        
        List<Poll> polls = config.getPolls();

        for (Poll poll : polls)
        {
            if (StringUtils.equals(id, poll.getID()))
            {
                polls.remove(poll);

                fetchmailManager.setConfig(config);
                
                break;
            }
        }
    }
    
    /*
     * Event handler called when the checkbox is clicked. 
     */
    public void onCheckboxEvent()
    {
        String data = request.getParameter("data");

        if (StringUtils.isEmpty(data)) {
            throw new IllegalArgumentException("data is missing.");
        }
        
        JSONObject json = new JSONObject(data);
                        
        String element = json.getString("element");
        boolean checked = json.getBoolean("checked"); 
        
        if (checked) {
            selected.add(element);
        }
        else {
            selected.remove(element);
        }
    }
    
    @OnEvent(component = "deleteSelected")
    protected void deleteSelected()
    throws WebServiceCheckedException 
    {
        checkAccess();
        
        if (selected != null)
        {
            FetchmailConfig config = getFetchmailConfig();
            
            List<Poll> polls = config.getPolls();

            Iterator<Poll> it = polls.iterator();
            
            boolean removed = false;
            
            while (it.hasNext())
            {
                Poll poll = it.next();
                
                if (selected.contains(poll.getID()))
                {
                    it.remove();
                    
                    selected.remove(poll.getID());
                 
                    removed = true; 
                    
                    if (selected.size() == 0) {
                        break;
                    }
                }
            }
            
            if (removed) {
                fetchmailManager.setConfig(config);
            }
        }
    }
    
    public boolean getSelect() {
        return selected.contains(row.getID());
    }
    
    public void setSelect(boolean value) 
    {
        /*
         * Ignored. Checkbox values will be handled using EventCheckbox (see onCheckboxClick)
         */
    }
    
    public String getPassword() {
        return hidePasswords ? "***" : row.getPassword();
    }
    
    public int getRowsPerPage() {
        return rowsPerPage;
    }
    
    public boolean isApplied() {
        return applied;
    }

    public boolean isApplyError() {
        return applyError;
    }

    public Poll getRow() {
        return row;
    }

    public void setRow(Poll row) {
        this.row = row;
    }
    
    public Asset getDeleteAsset() {
        return deleteAsset;
    }

    public String getFailureMessage() {
        return StringUtils.isNotBlank(failureMessage) ? failureMessage : "No details.";
    }
}
