/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.pages.ca;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import mitm.application.djigzo.admin.FactoryRoles;
import mitm.application.djigzo.ws.CAWS;
import mitm.application.djigzo.ws.CertificateRequestHandlerDTO;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.security.ca.CSVRequestConverter;
import mitm.common.security.ca.RequestParameters;
import mitm.common.security.ca.handlers.BuiltInCertificateRequestHandler;
import mitm.common.util.MiscStringUtils;
import mitm.common.ws.WebServiceCheckedException;
import mitm.djigzo.web.beans.CASettingsBean;
import mitm.djigzo.web.beans.impl.CASettingsBeanImpl;
import mitm.djigzo.web.common.GenericSelectionModel;
import mitm.djigzo.web.common.SignatureAlgorithm;
import mitm.djigzo.web.entities.GlobalPreferencesManager;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.tapestry5.PersistenceConstants;
import org.apache.tapestry5.SelectModel;
import org.apache.tapestry5.annotations.Cached;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.InjectPage;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.corelib.components.Checkbox;
import org.apache.tapestry5.corelib.components.Form;
import org.apache.tapestry5.corelib.components.Select;
import org.apache.tapestry5.corelib.components.TextField;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.ioc.annotations.Value;
import org.apache.tapestry5.json.JSONObject;
import org.apache.tapestry5.services.Request;
import org.apache.tapestry5.upload.components.Upload;
import org.apache.tapestry5.upload.services.UploadedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.annotation.Secured;

@Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_PKI_MANAGER})
@IncludeStylesheet("context:styles/pages/ca/caBulkRequest.css")
public class CABulkRequest
{
    private final static Logger logger = LoggerFactory.getLogger(CABulkRequest.class);

    private final static int MAX_ERROR_MESSAGE_LENGTH = 256;
    
    @Inject
    private CAWS caWS;
    
    @Inject
    private GlobalPreferencesManager globalPreferencesManager;
    
    @Inject
    private Request request;
    
    @InjectPage
    private CARequestsPreview requestsPreviewPage; 
    
    /*
     * Maximum number of lines that will be imported from the CSV
     */
    @Inject
    @Value("${caBulkRequest.maxLines}")
    private int maxLines;

    /*
     * Maximum length of an imported CSV item value
     */
    @Inject
    @Value("${caBulkRequest.maxValueLength}")
    private int maxValueLength;
    
    /*
     * 'Handle' to the file that's uploaded
     */
    private UploadedFile file;
    
    /*
     * Validity in days of the certificate
     */
    private Integer validity;

    /*
     * The length of the generated key
     */
    private Integer keyLength;

    /*
     * Algorithm for signing the certificate
     */
    private SignatureAlgorithm signatureAlgorithm;
       
    /*
     * If true, the crlDistributionPoint will be added
     */
    private Boolean addCRLDistributionPoint;
        
    /*
     * The CRL distribution point for the intermediate certificate
     */
    private String crlDistributionPoint;
    
    /*
     * The selected Certificate Request Handler
     */
    private String certificateRequestHandler;
            
    /*
     * If true a user object will be added when a certificate is ussued
     */
    @Persist
    private Boolean addUser;
    
    @SuppressWarnings("unused")
    @Component(id = "upload", parameters = {"value = file", "validate=required"})
    private Upload upload;
    
    @SuppressWarnings("unused")
    @Component(id = "validity", parameters = {"value = validity", "validate = required, min=1, max=14600"})
    private TextField validityField;

    @SuppressWarnings("unused")
    @Component(id = "keyLength", parameters = {"value = keyLength"})
    private Select keyLengthField;

    @SuppressWarnings("unused")
    @Component(id = "signatureAlgorithm", parameters = {"value = signatureAlgorithm", 
            "blankOption = NEVER", "validate = required"})
    private Select signatureAlgorithmField;
    
    @Component(id = "crlDistributionPoint", parameters = {"value = crlDistributionPoint", "validate = maxLength=1024"})
    private TextField crlDistributionPointField;

    @SuppressWarnings("unused")
    @Component(id = "addCRLDistributionPoint", parameters = {"value = addCRLDistributionPoint"})
    private Checkbox addCRLDistributionPointField;

    @SuppressWarnings("unused")
    @Component(id = "certificateRequestHandler", parameters = {"value = certificateRequestHandler", 
            "model = certificateRequestHandlerModel", "blankOption = NEVER", "validate = required"})
    private Select certificateRequestHandlerField;
    
    @SuppressWarnings("unused")
    @Component(id = "addUser", parameters = {"value = addUser"})
    private Checkbox addUserField;
    
    @Component
    private Form form;

    @Persist
    private boolean advanced;
    
    /*
     * True if an error has occurred
     */
    @Persist(PersistenceConstants.FLASH)
    private boolean error;

    /*
     * Error message if an error has occurred
     */
    @Persist(PersistenceConstants.FLASH)
    private String errorMessage;

    /*
     * True if a warning should be shown
     */
    @Persist(PersistenceConstants.FLASH)
    private boolean warning;

    /*
     * Error message if a warning should be showed
     */
    @Persist(PersistenceConstants.FLASH)
    private String warningMessage;
    
    @SetupRender
    @Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_PKI_MANAGER})
    public void setupRender()
    throws HierarchicalPropertiesException, WebServiceCheckedException
    {
        loadDefaults();
    }
    
    private void loadDefaults()
    throws HierarchicalPropertiesException, WebServiceCheckedException
    {
        if (validity == null) {
            validity = getCASettings().getValidity();
        }

        if (keyLength == null) {
            keyLength = getCASettings().getKeyLength();
        }
        
        if (addCRLDistributionPoint == null) {
            addCRLDistributionPoint = getCASettings().isAddCRLDistributionPoint();
        }
        
        if (crlDistributionPoint == null) {
            crlDistributionPoint = getCASettings().getCRLDistributionPoint();
        }
        
        if (signatureAlgorithm == null) {
            signatureAlgorithm = SignatureAlgorithm.fromName(getCASettings().getSignatureAlgorithm());
        }
        
        if (signatureAlgorithm == null) {
            /*
             * signatureAlgorithm not found to use default
             */
            signatureAlgorithm = SignatureAlgorithm.SHA1_WITH_RSA;
        }
        
        /*
         * Show advanced when a CRL dist point is added
         */
        if (StringUtils.isNotEmpty(crlDistributionPoint) && addCRLDistributionPoint) {
            advanced = true;
        }
        
        if (certificateRequestHandler == null) {
            certificateRequestHandler = getCASettings().getCertificateRequestHandler();
        }
        
        if (StringUtils.isEmpty(certificateRequestHandler)) {
            /*
             * Use default Certificate Request Handler
             */
            certificateRequestHandler = BuiltInCertificateRequestHandler.NAME;
        }
        
        if (addUser == null) {
            addUser = true;
        }
    }
    
    public SelectModel getCertificateRequestHandlerModel()
    throws WebServiceCheckedException
    {
        return new GenericSelectionModel<String>(new ArrayList<String>(getEnabledCertificateRequestHandlers()), 
                null, null);
    }
    
    @Cached
    public CASettingsBean getCASettings() 
    throws HierarchicalPropertiesException, WebServiceCheckedException
    {
        return new CASettingsBeanImpl(globalPreferencesManager.getProperties());
    }

    @Cached
    public List<CertificateRequestHandlerDTO> getCertificateRequestHandlers()
    throws WebServiceCheckedException
    {
        return caWS.getCertificateRequestHandlers();
    }
    
    @Cached
    public List<String> getEnabledCertificateRequestHandlers()
    throws WebServiceCheckedException
    {
        List<String> result = new LinkedList<String>();

        List<CertificateRequestHandlerDTO> handlers = getCertificateRequestHandlers();

        for (CertificateRequestHandlerDTO handler : handlers)
        {
            if (handler.isEnabled()) {
                result.add(handler.getName());
            }
        }

        return result;
    }
        
    public void onValidateForm()
    throws HierarchicalPropertiesException, WebServiceCheckedException 
    {
        if (StringUtils.isNotBlank(crlDistributionPoint) && addCRLDistributionPoint)
        {
            /*
             * Check if the crlDistributionPoint is a valid URL
             */
            try {
                new URL(crlDistributionPoint);
            }
            catch (MalformedURLException e)
            {
                form.recordError(crlDistributionPointField, "CRL distribution point is not a valid URL.");
                
                return;
            }
        }
    }
    
    private CertificateRequestHandlerDTO getSelectedCertificateRequestHandler()
    throws WebServiceCheckedException
    {
        List<CertificateRequestHandlerDTO> handlers = getCertificateRequestHandlers();

        for (CertificateRequestHandlerDTO handler : handlers)
        {
            if (StringUtils.equalsIgnoreCase(certificateRequestHandler, handler.getName())) {
                return handler;
            }
        }
        
        return null;
    }
    
    public Object onSuccess()
    {
        Object result = null;
        
        try {
            loadDefaults();
            
            CSVRequestConverter requestConverter = new CSVRequestConverter();
            
            requestConverter.setMaxLines(maxLines);
            requestConverter.setMaxValueLength(maxValueLength);
            
            List<RequestParameters> requests = requestConverter.convertCSV(file.getStream());
            
            if (requests != null && requests.size() > 0)
            {
                /*
                 * Not all values are set by convertCSV so we must set the default values
                 */
                for (RequestParameters request : requests) 
                {
                    if (StringUtils.isEmpty(request.getCertificateRequestHandler())) {
                        request.setCertificateRequestHandler(certificateRequestHandler);
                    }
                    
                    if (StringUtils.isEmpty(request.getCRLDistributionPoint())) {
                        request.setCRLDistributionPoint(crlDistributionPoint);
                    }

                    if (StringUtils.isEmpty(request.getSignatureAlgorithm())) {
                        request.setSignatureAlgorithm(signatureAlgorithm.getName());
                    }
                    
                    if (request.getKeyLength() == 0) {
                        request.setKeyLength(keyLength);
                    }

                    if (request.getValidity() == 0) {
                        request.setValidity(validity);
                    }
                }
                
                requestsPreviewPage.setCertificateRequestHandler(getSelectedCertificateRequestHandler());
                requestsPreviewPage.setAddUser(addUser);
                requestsPreviewPage.setCertificateRequests(requests);
                
                result = requestsPreviewPage;
            }
            else {
                warningMessage = "The file did not contain any requests.";
                warning = true;
            }
        }
        catch(Exception e)
        {
            logger.error("Error requesting certificates.", e);
            
            errorMessage = MiscStringUtils.restrictLength(ExceptionUtils.getRootCauseMessage(e), 
                    MAX_ERROR_MESSAGE_LENGTH, true);
            error = true;
        }
        
        return result;
    }

    public int getvalidity() {
        return validity;
    }

    public void setvalidity(int validity) {
        this.validity = validity;
    }

    public int getkeyLength() {
        return keyLength;
    }

    public void setKeyLength(int keyLength) {
        this.keyLength = keyLength;
    }
    
    public boolean isError() {
        return error;
    }
    
    public String getErrorMessage() {
        return StringUtils.isNotBlank(errorMessage) ? errorMessage : "No details.";
    }
    
    /*
     * Event handler called when the cancel button is pressed.
     */
    protected Object onCancel() {
        return CAIssue.class;
    }
    
    public boolean isAddCRLDistributionPoint() {
        return addCRLDistributionPoint;
    }

    public void setAddCRLDistributionPoint(boolean addCRLDistributionPoint) {
        this.addCRLDistributionPoint = addCRLDistributionPoint;
    }
    
    public SignatureAlgorithm getSignatureAlgorithm() {
        return signatureAlgorithm;
    }

    public void setSignatureAlgorithm(SignatureAlgorithm signatureAlgorithm) {
        this.signatureAlgorithm = signatureAlgorithm;
    }    
    
    public boolean isAdvanced() {
        return advanced;
    }

    public void setAdvanced(boolean advanced) {
        this.advanced = advanced;
    }

    public String getCrlDistributionPoint() {
        return crlDistributionPoint;
    }

    public void setCrlDistributionPoint(String crlDistributionPoint) {
        this.crlDistributionPoint = crlDistributionPoint;
    }
    
    public void setCertificateRequestHandler(String certificateRequestHandler) {
        this.certificateRequestHandler = certificateRequestHandler;
    }
    
    public String getCertificateRequestHandler() {
        return certificateRequestHandler;
    }
    
    /*
     * Ajax event handler called when the advanced settings checkbox is clicked. We want to store
     * the selected value without requiring a submit
     */
    @OnEvent(value="checkboxEvent", component="advanced")
    public void onCheckboxEvent()
    {
        String data = request.getParameter("data");

        if (StringUtils.isEmpty(data)) {
            throw new IllegalArgumentException("data is missing.");
        }
        
        JSONObject json = new JSONObject(data);
                        
        boolean checked = json.getBoolean("checked");
        
        advanced = checked;
    }

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public boolean isWarning() {
        return warning;
    }

    public String getWarningMessage() {
        return warningMessage;
    }

    public Boolean getAddUser() {
        return addUser;
    }

    public void setAddUser(Boolean addUser) {
        this.addUser = addUser;
    }    
}
