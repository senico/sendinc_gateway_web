/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.components;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import mitm.application.djigzo.admin.FactoryRoles;
import mitm.application.djigzo.ws.X509CertStoreWS;
import mitm.common.security.asn1.ObjectEncoding;
import mitm.common.ws.WebServiceCheckedException;
import mitm.djigzo.web.common.CertificateDownloadConst;
import mitm.djigzo.web.common.CertificateFilterSettings;
import mitm.djigzo.web.common.ContentTypes;
import mitm.djigzo.web.common.streamresponse.ByteArrayFileResponse;
import mitm.djigzo.web.grid.CertStoreCertificateGridDataSource;

import org.apache.tapestry5.BindingConstants;
import org.apache.tapestry5.ComponentEventCallback;
import org.apache.tapestry5.ComponentResources;
import org.apache.tapestry5.PersistenceConstants;
import org.apache.tapestry5.StreamResponse;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.grid.GridDataSource;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.annotation.Secured;

@IncludeStylesheet("context:styles/components/certificateStoreGrid.css")
public class CertificateStoreGrid
{
	private final static Logger logger = LoggerFactory.getLogger(CertificateStoreGrid.class);
	
    public static final String DOWNLOAD_KEYS_CLICKED_EVENT = "certificateStoreGrid-downloadKeysClicked";

	/*
	 * True when there was an error deleting a certificate
	 */
	@Persist(PersistenceConstants.FLASH)
	private boolean deletionError;
	
	@Property
	@Parameter(required = true)
	private X509CertStoreWS certStore;

	@Parameter(required = false, value = "true", defaultPrefix = BindingConstants.LITERAL)
	private boolean showDownloadKeys;

	@Parameter(required = false, value = "true", defaultPrefix = BindingConstants.LITERAL)
	private boolean showDownloadCertificates;

	@Parameter(required = false, value = "true", defaultPrefix = BindingConstants.LITERAL)
	private boolean showDeleteSelected;

	@Parameter(required = false, value = "true", defaultPrefix = BindingConstants.LITERAL)
	private boolean showInvertSelection;
	
	@Component(parameters = {"source = source", "rowsPerPage = inherit:rowsPerPage", 
	        "pagerPosition = inherit:pagerPosition"})
    private MultiSelectCertificateGrid certificateGrid;
		
	@SuppressWarnings("unused")
	@Component(parameters = {"filterSettings = filterSettings"})
	private CertificateFilter certificateFilter;
	
    @Inject
    private ComponentResources resources;
	
	/*
	 * The user selected filter settings
	 */
	@Persist
	private CertificateFilterSettings filterSettings;

	public GridDataSource getSource() {
		return new CertStoreCertificateGridDataSource(certStore, filterSettings);
	}

	public MultiSelectCertificateGrid getCertificateGrid() {
		return certificateGrid;
	}
	
    @SetupRender
    public void setupGrid() 
    {
    	/*
    	 * Clear the selected certificates
    	 */
        certificateGrid.clearSelected();
    	
		if (filterSettings == null) {
			filterSettings = new CertificateFilterSettings();
		}
    }
	
    private void removeCertificate(String thumbprint)
    {
		try {
			if (!certificateGrid.isInUse(thumbprint)) {
				certStore.removeCertificate(thumbprint);
			}
			else {
				/*
				 * Certificate is in use so it cannot be deleted
				 */
				deletionError = true;
			}
		} 
		catch (WebServiceCheckedException e) 
		{
			logger.error("Error removing certificate", e);
			
			deletionError = true;
		}
    }
    
	@OnEvent(value = CertificateGrid.DELETE_EVENT, component = "certificateGrid")
	@Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_PKI_MANAGER})
	protected void onDelete(String thumbprint) 
	{
		removeCertificate(thumbprint);
	}
	
    @OnEvent(component = "deleteSelected")
	@Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_PKI_MANAGER})
    protected void deleteSelected() 
    {
        /*
         * Note: we need to clone the set to make sure that concurrent modifications
         * are possible.
         */
        String[] selected = certificateGrid.getSelected().toArray(new String[]{});
        
        for (String thumbprint : selected)	{
            removeCertificate(thumbprint);
        }
    }

    @OnEvent(component = "downloadSelected")
    protected StreamResponse downloadSelected() 
    throws WebServiceCheckedException 
    {
    	StreamResponse response = null;
    	
        /*
         * Note: we need to clone the set to make sure that concurrent modifications
         * are possible.
         */
        String[] selected = certificateGrid.getSelected().toArray(new String[]{});

    	if (selected.length > 0)
    	{
    	    ObjectEncoding certificateEncoding = CertificateDownloadConst.DEFAUL_ENCODING;

    	    String filename;

    	    byte[] encodedCertificates;

    	    if (selected.length == 1)
    	    {
    	        filename = CertificateDownloadConst.getFilename(true /* single certificate */, 
    	                certificateEncoding);

    	        encodedCertificates = certStore.getEncodedCertificate(
    	                selected[0], certificateEncoding);
    	    }
    	    else {
    	        filename = CertificateDownloadConst.getFilename(false /* multiple certificates */, 
    	                certificateEncoding);

    	        encodedCertificates = certStore.getEncodedCertificates(
    	                Arrays.asList(selected), certificateEncoding);
    	    }

    	    response = new ByteArrayFileResponse(encodedCertificates, ContentTypes.X_DOWNLOAD,
    	            filename);
    	}

    	/*
    	 * We need to clear the selected. This is not visually reflected (we cannot refresh the page
    	 * and download the certificates afaik) so the check boxes are unchecked using Javascript.
    	 * Another option would be to redirect to the same page and persist (flash) the  certificate 
    	 * byte array.
    	 */
    	certificateGrid.getSelected().clear();
    	
    	return response;
    }

    @OnEvent(component = "downloadSelectedKeys")
	@Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_PKI_MANAGER})
    protected Object downloadSelectedKeys() 
    throws WebServiceCheckedException 
    {
		final List<Object> resultHolder = new LinkedList<Object>();
		
		ComponentEventCallback<Object> callback = new ComponentEventCallback<Object>() 
		{
		    @Override
			public boolean handleResult(Object result) 
			{
				resultHolder.add(result);
				
				return true;
			}
		};
		
		resources.triggerEvent(DOWNLOAD_KEYS_CLICKED_EVENT, new Object[]{}, callback);
		
		Object result = null;
		
		if (resultHolder.size() > 0) {
			result = resultHolder.get(0);
		}
    	
		return result;
    }

    public CertificateFilterSettings getFilterSettings() {
		return filterSettings;
	}

	public boolean isShowDownloadKeys() {
		return showDownloadKeys;
	}

	public void setShowDownloadKeys(boolean showDownloadKeys) {
		this.showDownloadKeys = showDownloadKeys;
	}

	public boolean isShowDownloadCertificates() {
		return showDownloadCertificates;
	}

	public void setShowDownloadCertificates(boolean showDownloadCertificates) {
		this.showDownloadCertificates = showDownloadCertificates;
	}

	public boolean isShowDeleteSelected() {
		return showDeleteSelected;
	}

	public void setShowDeleteSelected(boolean showDeleteSelected) {
		this.showDeleteSelected = showDeleteSelected;
	}

	public boolean isShowInvertSelection() {
		return showInvertSelection;
	}

	public void setShowInvertSelection(boolean showInvertSelection) {
		this.showInvertSelection = showInvertSelection;
	}

	public boolean isDeletionError() {
		return deletionError;
	}
}
