/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.pages.admin;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import mitm.application.djigzo.admin.FactoryRoles;
import mitm.application.djigzo.ws.LoggerDTO;
import mitm.application.djigzo.ws.LoggerManagerWS;
import mitm.common.util.LogLevel;
import mitm.common.ws.WebServiceCheckedException;
import mitm.djigzo.web.grid.LoggerManagerGridDataSource;
import mitm.djigzo.web.pages.Admins;

import org.apache.commons.lang.StringUtils;
import org.apache.tapestry5.ComponentResources;
import org.apache.tapestry5.PersistenceConstants;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.beaneditor.BeanModel;
import org.apache.tapestry5.beaneditor.Validate;
import org.apache.tapestry5.corelib.components.Grid;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.json.JSONObject;
import org.apache.tapestry5.services.BeanModelSource;
import org.apache.tapestry5.services.Request;
import org.springframework.security.annotation.Secured;

@IncludeStylesheet("context:styles/pages/admin/loggerManager.css")
@Secured({FactoryRoles.ROLE_ADMIN})
public class LoggerManager
{
	@Inject
	private LoggerManagerWS loggerManagerWS;
	
	@Inject
	private Request request;
	
	/*
	 * The current log item being drawn
	 */
	private LoggerDTO log;
	
	@Persist
	private LogLevel logLevel;
	
	/*
	 * Set of the selected log names
	 */
	@Persist
	private Set<String> selected;
		
    /*
     * True if changes were applied
     */
    @Persist(PersistenceConstants.FLASH)
    private boolean applied;
	
	@SuppressWarnings("unused")
	@Component(parameters = {"source=gridDataSource", "model=model", "row=log", "volatile=true", 
			"reorder=select,name,logLevel"})
    private Grid logLevelGrid;

	@Inject
    private BeanModelSource beanModelSource;

    @Inject
    private ComponentResources resources;
    
    public boolean isApplied() {
        return applied;
    }
    
    @SetupRender
	@Secured({FactoryRoles.ROLE_ADMIN})
    public void setupGrid() 
    {
        if (selected == null) {
            selected = Collections.synchronizedSet(new HashSet<String>());
        }
        else {
            selected.clear();
        }
    }
    
    public BeanModel<LoggerDTO> getModel() 
    {
        BeanModel<LoggerDTO> model = beanModelSource.createDisplayModel(LoggerDTO.class, resources.getMessages());
        
        model.add("select", null);
        
        /*
         * Disable all sorting
         */
        for (String column : model.getPropertyNames()) {
        	model.get(column).sortable(false);
        }
        
        return model;
    }
        
	public boolean getSelect() {
		return selected.contains(log.getName());
	}
	
	public void setSelect(boolean value) 
	{
		/*
		 * Ignored. Checkbox values will be handled using EventCheckbox (see onEventActionLink)
		 */
	}
    
	public LoggerManagerGridDataSource getGridDataSource()	{
		return new LoggerManagerGridDataSource(loggerManagerWS);
	}

    /*
     * Event handler called when the checkbox is clicked. 
     */
    public void onCheckboxEvent()
    {
    	String data = request.getParameter("data");

    	if (StringUtils.isEmpty(data)) {
    		throw new IllegalArgumentException("data is missing.");
    	}
    	
    	JSONObject json = new JSONObject(data);
    	    	    	
    	String loggerName = json.getString("element");
    	boolean checked = json.getBoolean("checked"); 
    	
    	if (checked) {
    		selected.add(loggerName);
    	}
    	else {
    		selected.remove(loggerName);
    	}
    }

    public void onSuccess()
    throws WebServiceCheckedException
    {
        /*
         * Note: we need to clone the set to make sure that concurrent modifications
         * are possible.
         */
        String[] names = selected.toArray(new String[]{});
        
        for (String loggerName : names)
        {
            LoggerDTO loggerDTO = new LoggerDTO(loggerName, logLevel);
            
            loggerManagerWS.setLogLevel(loggerDTO);
        }
        
        applied = true;
    }
    
    /*
     * Event handler called when the cancel button is pressed.
     */
    public Object onCancel() {
        return Admins.class;
    }
    
	@Validate("required")
    public LogLevel getLogLevel() { 
		return logLevel; 
	}

    public void setLogLevel(LogLevel logLevel) {
    	this.logLevel = logLevel; 
    }
	
	public LoggerDTO getLog() {
		return log;
	}

	public void setLog(LoggerDTO log) {
		this.log = log;
	}
}
