function beforeSecretKeyGenerated(data)
{
	var json = data.evalJSON(true);
	
	var field = $(json.params.context);
   
	// stop the event if the edit field is disabled
	return field.disabled;
}

function afterSecretKeyGenerated(data)
{
	var json = data.evalJSON(true);

	var field = $(json.params.context);
   
	field.value = json.result.key;
}