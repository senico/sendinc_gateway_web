/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.pages.certificate;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import mitm.application.djigzo.ws.CTLWS;
import mitm.application.djigzo.ws.CertificateStore;
import mitm.application.djigzo.ws.CertificateValidatorResult;
import mitm.application.djigzo.ws.CertificateValidatorWS;
import mitm.application.djigzo.ws.KeyAndCertStoreWS;
import mitm.application.djigzo.ws.X509CertStoreWS;
import mitm.application.djigzo.ws.X509CertificateDTO;
import mitm.common.security.asn1.ObjectEncoding;
import mitm.common.security.ctl.CTLManager;
import mitm.common.ws.WebServiceCheckedException;
import mitm.djigzo.web.beans.X509CertificateBean;
import mitm.djigzo.web.beans.impl.X509CertificateBeanImpl;
import mitm.djigzo.web.common.CertificateDownloadConst;
import mitm.djigzo.web.common.ContentTypes;
import mitm.djigzo.web.common.ValidityCheck;
import mitm.djigzo.web.common.streamresponse.ByteArrayFileResponse;
import mitm.djigzo.web.services.CertificateStoreMarker;
import mitm.djigzo.web.services.RootStoreMarker;

import org.apache.tapestry5.Asset;
import org.apache.tapestry5.ComponentResources;
import org.apache.tapestry5.Link;
import org.apache.tapestry5.StreamResponse;
import org.apache.tapestry5.annotations.Cached;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Path;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.beaneditor.BeanModel;
import org.apache.tapestry5.corelib.components.BeanDisplay;
import org.apache.tapestry5.internal.services.LinkFactory;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.BeanModelSource;
import org.apache.tapestry5.services.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@IncludeStylesheet("context:styles/pages/certificate/certificateView.css")
public class CertificateView 
{
	private final static Logger logger = LoggerFactory.getLogger(CertificateView.class);
	
	/*
	 * The X509 certificate that's being displayed
	 */
	private X509CertificateBean certificate;

	private ValidityCheck validityCheck = ValidityCheck.TRUST;
	
	@Inject
	@Property
	@CertificateStoreMarker
	private KeyAndCertStoreWS keyAndCertStoreWS;

    @Inject
    @Property
    @RootStoreMarker
    private KeyAndCertStoreWS rootWS;

    @Inject
    private BeanModelSource modelSource;
	
    @Inject
    private ComponentResources resources;

	@Inject
	private CertificateValidatorWS certificateValidator; 
    
    @Inject
    private CTLWS ctlWS;
	
    @Inject
    private Response response;
    
    @Inject
    private LinkFactory linkFactory;
	
    @Inject
    @Path("context:icons/key.png")
    private Asset keyAsset;
    
	@Inject
    @Path("context:icons/exclamation.png")
    private Asset keyNotAccessibleAsset;
    
	/*
	 * The store from which to get the certificate
	 */
	@Property
	private CertificateStore store = CertificateStore.CERTIFICATES;
	
	@SuppressWarnings("unused")
	@Component(parameters = "model = prop:model")
	private BeanDisplay certificateDisplay;
	
    private X509CertStoreWS getCertStoreWS()
    {
    	X509CertStoreWS certStore;
    	
    	switch(store)
    	{
    	case CERTIFICATES : certStore = keyAndCertStoreWS; break;
    	case ROOTS        : certStore = rootWS; break;
    	default:
    		throw new IllegalArgumentException("Unknown store.");
    	}
    	
    	return certStore;
    }

	public void onActivate(CertificateStore store, String thumbprint, ValidityCheck validityCheck)
	throws WebServiceCheckedException 
	{
		if (store != null) {
			this.store = store;
		}
		
		if (thumbprint != null)
		{
			X509CertificateDTO certificateDTO = getCertStoreWS().getCertificate(thumbprint);
			
			if (certificateDTO != null) {
				certificate = new X509CertificateBeanImpl(certificateDTO);
			}
		}

		if (validityCheck != null) {
			this.validityCheck = validityCheck;
		}
	}
	
	public List<Object> onPassivate()
	{
		List<Object> context = new LinkedList<Object>();
		
		if (store != null) {
			context.add(store);
		}
		
		if (certificate != null) {
			context.add(certificate.getThumbprint());
		}
		
		if (validityCheck != null) {
			context.add(validityCheck);
		}
		
		return context;
	}
	
    @OnEvent(component = "downloadCertificate")
    protected Object downloadCertificate() 
    throws WebServiceCheckedException 
    {
    	StreamResponse response = null;
    	
    	if (certificate == null) 
    	{
    		logger.warn("certificate is null.");
    		/*
    		 * Can happen when the certificate is deleted
    		 */
    		return null;
    	}
    	
    	ObjectEncoding certificateEncoding = CertificateDownloadConst.DEFAUL_ENCODING;
    	
		byte[] encodedCertificate = getCertStoreWS().getEncodedCertificate(certificate.getThumbprint(),
				certificateEncoding);

		if (encodedCertificate == null) 
		{
    		logger.warn("encoded certificate is null.");
    		
			/*
			 * The certificate was not found
			 */
			return null;
		}
		
		String filename = CertificateDownloadConst.getFilename(true /* single certificate */, 
				certificateEncoding);
		
		response = new ByteArrayFileResponse(encodedCertificate, ContentTypes.X_DOWNLOAD, filename);
		
    	return response;
    }
	
    @OnEvent(component = "usage")
    protected void usage()
    throws IOException 
    {
        Link link = linkFactory.createPageRenderLink("certificate/usage", false, certificate.getThumbprint());

        response.sendRedirect(link);
    }

	public BeanModel<X509CertificateBeanImpl> getModel()
	{
		BeanModel<X509CertificateBeanImpl> model = modelSource.createDisplayModel(X509CertificateBeanImpl.class, 
				resources.getMessages());
		
        model.add("ctlStatus", null);
        model.add("info", null);
		
		return model;
	}
	
	@Cached
	public CertificateValidatorResult getCertificateValidatorResult() 
	throws WebServiceCheckedException 
	{
		String thumbprint = certificate.getThumbprint();
		
		switch(validityCheck)
		{
		case TRUST      : return certificateValidator.checkValidity(store, thumbprint);
		case ENCRYPTION : return certificateValidator.checkValidityForEncryption(store, thumbprint);
		case SIGNING    : return certificateValidator.checkValidityForSigning(store, thumbprint);
		default:
			throw new IllegalArgumentException("Unknown validityCheck");
		}
	}

    public Asset getKeyAsset() {
        return keyAsset;
    }

	public Asset getKeyNotAccessibleAsset() {
		return keyNotAccessibleAsset;
	}
    
    public X509CertificateBean getCertificate() {
        return certificate;
    }
    
    public boolean isPrivateKeyAvailable() {
        return certificate != null && certificate.isPrivateKeyAvailable();
    }

    public boolean isPrivateKeyAccessible() {
        return certificate != null && certificate.isPrivateKeyAccessible();
    }
    
	public String getCTLStatus()
	throws WebServiceCheckedException
	{
	    CertificateValidatorResult validatorResult = getCertificateValidatorResult();
	    
	    if (validatorResult.isWhiteListed()) {
	        return "whitelisted";
	    }
	    else if (validatorResult.isBlackListed()) {
            return "blacklisted";
        }
	    
	    return null;
	}
	
	public boolean isShowAddToCTL()
	throws WebServiceCheckedException
	{
	    /*
	     * We do not support the CTL for the root store. If you don't trust the root remove it
	     */
	    return store == CertificateStore.CERTIFICATES && ctlWS.getEntry(
	            CTLManager.DEFAULT_CTL, certificate.getThumbprint()) == null;
	}
	
	@Cached
	private X509CertificateDTO getIssuerCertificate()
	{
	    X509CertificateDTO issuer = null;
	    
	    /*
	     * Only get the issuer if the current certificate is from the certificate store.
	     * There is no need to get the issuer if the certificate is from the roots.
	     */
	    if (store == CertificateStore.CERTIFICATES)
	    {
            try {
                 issuer = certificateValidator.getIssuerCertificate(store, certificate.getThumbprint());
            } 
            catch (WebServiceCheckedException e) {
                // ignore
            }
	    }
	    
        return issuer;
	}
	
	public boolean isIssuerCertificateAvailable() {
	    return getIssuerCertificate() != null;
	}
	
	public Link getIssuerCertificateLink()
	{
	    Link link = null;
	    
	    X509CertificateDTO issuer = getIssuerCertificate();

	    link = resources.createPageLink(CertificateView.class, false, issuer.getCertificateStore(), 
	            issuer.getThumbprint(), validityCheck);
	    
        return link;
	}
}
