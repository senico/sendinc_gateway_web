/*
 * Copyright (c) 2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.components;

import java.io.IOException;

import mitm.djigzo.web.mixins.AjaxEvent;

import org.apache.commons.lang.math.NumberUtils;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.IncludeJavaScriptLibrary;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.corelib.components.Select;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.json.JSONObject;
import org.apache.tapestry5.services.Request;

@IncludeJavaScriptLibrary("rowsPerPageSelector.js")
public class RowsPerPageSelector
{
    private static final int MIN_ROWS = 10;
    private static final int MAX_ROWS = 200;
    private static final int DEFAULT_ROWS = 25;
    
    @Parameter(required = true)
    private int rowsPerPage;    
    
    @Inject
    private Request request;
    
    @SuppressWarnings("unused")
    @Component(id = "rowsPerPageSelector", parameters = {"value = rowsPerPage"})
    private Select rowsPerPageSelector;
    
    @OnEvent(component = "rowsPerPageSelector", value="AjaxEvent")
    public void onLogTypeSelected()
    throws IOException
    {
        String data = request.getParameter(AjaxEvent.REQUEST_PARAM_NAME);

        JSONObject json = new JSONObject(data);
        
        int value = Integer.valueOf(NumberUtils.toInt(json.getString("value"), DEFAULT_ROWS));
        
        if (value < MIN_ROWS || value > MAX_ROWS) {
            value = DEFAULT_ROWS;
        }
        
        rowsPerPage = value;
    }
    
    public int getRowsPerPage() {
        return rowsPerPage;
    }
    
    public void setRowsPerPage(int rowsPerPage) {
        this.rowsPerPage = rowsPerPage;
    }
}
