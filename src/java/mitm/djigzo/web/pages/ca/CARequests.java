/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.pages.ca;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import mitm.application.djigzo.admin.FactoryRoles;
import mitm.application.djigzo.ws.CertificateRequestDTO;
import mitm.application.djigzo.ws.CertificateRequestStoreWS;
import mitm.common.ws.WebServiceCheckedException;
import mitm.djigzo.web.grid.CertificateRequestStoreGridDataSource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.tapestry5.Asset;
import org.apache.tapestry5.ComponentResources;
import org.apache.tapestry5.PersistenceConstants;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Path;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.beaneditor.BeanModel;
import org.apache.tapestry5.corelib.components.Grid;
import org.apache.tapestry5.grid.GridDataSource;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.ioc.annotations.Value;
import org.apache.tapestry5.json.JSONObject;
import org.apache.tapestry5.services.BeanModelSource;
import org.apache.tapestry5.services.Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.annotation.Secured;

@Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_PKI_MANAGER})
@IncludeStylesheet("context:styles/pages/ca/caRequests.css")
public class CARequests
{
    private final static Logger logger = LoggerFactory.getLogger(CARequests.class);
    
    @Inject
    private CertificateRequestStoreWS storeWS;
    
    @Inject
    private Request request;
    
    @Inject
    @Value("${certificateRequests.rowsPerPage}")
    private int rowsPerPage;    
    
    /*
     * The certificate request currently being drawn by the grid (is set by the grid component)
     */
    private CertificateRequestDTO certificateRequest;
    
    /*
     * Set of the selected ID's of the CertificateRequest's
     */
    @Persist
    private Set<String> selected;
    
    /*
     * The email address to filter on.
     */
    @Persist
    private String email;
    
    @SuppressWarnings("unused")
    @Component(parameters = {"source = source", "model = model", "row = certificateRequest", "volatile = true",
            "reorder = select,delete, id, email, subject, iteration, info, lastMessage, created, lastUpdated, " +  
            "nextUpdate, certificateHandlerName", "rowsPerPage = prop:rowsPerPage"})
    private Grid grid;

    @Inject
    private BeanModelSource beanModelSource;

    @Inject
    private ComponentResources resources;

    @Inject
    @Path("context:icons/cross.png")
    private Asset deleteAsset;

    /*
     * True if and error has occurred
     */
    @Persist(PersistenceConstants.FLASH)
    private boolean error;

    /*
     * Error message if an error has occurred
     */
    @Persist(PersistenceConstants.FLASH)
    private String errorMessage;
    
    @SetupRender
    @Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_PKI_MANAGER})
    protected void setupRender()
    {
        if (selected == null) {
            selected = Collections.synchronizedSet(new HashSet<String>());
        }
        else {
            selected.clear();
        }
    }
    
    @Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_PKI_MANAGER})
    public void onActivate(String email) {
        this.email = email;
    }
        
    public boolean isFilterDisabled() {
        return StringUtils.isBlank(email);
    }
    
    public void setCertificateRequest(CertificateRequestDTO certificateRequest) {
        this.certificateRequest = certificateRequest;
    }
    
    public CertificateRequestDTO getCertificateRequest() {
        return certificateRequest;
    }
    
    public boolean getSelect() {
        return selected.contains(certificateRequest.getID());
    }
    
    public void setSelect(boolean value) 
    {
        /*
         * Ignored. Checkbox values will be handled using EventCheckbox (see onCheckboxClick)
         */
    }
    
    public GridDataSource getSource() {
        return new CertificateRequestStoreGridDataSource(storeWS, email); 
    }

    public Asset getDeleteAsset() {
        return deleteAsset;
    }
    
    public BeanModel<CertificateRequestDTO> getModel()
    {
        BeanModel<CertificateRequestDTO> model = beanModelSource.createDisplayModel(CertificateRequestDTO.class, 
                resources.getMessages());
        
        model.add("delete", null);
        model.add("select", null);

        /*
         * Disable all sorting
         */
        for (String column : model.getPropertyNames()) {
            model.get(column).sortable(false);
        }
        
        return model;
    }

    @OnEvent(component = "deleteEntry")
    protected void deleteEntry(Long id) 
    {
        try {
            storeWS.deleteRequest(id);
        }
        catch (WebServiceCheckedException e)
        {
            logger.error("Error deleteEntry", e);
            
            error = true;
            errorMessage = ExceptionUtils.getRootCauseMessage(e);
        }
    }

    /*
     * Event handler called when the checkbox is clicked. 
     */
    public void onCheckboxEvent()
    {
        String data = request.getParameter("data");

        if (StringUtils.isEmpty(data)) {
            throw new IllegalArgumentException("data is missing.");
        }
        
        JSONObject json = new JSONObject(data);
                        
        String element = json.getString("element");
        boolean checked = json.getBoolean("checked"); 
        
        if (checked) {
            selected.add(element);
        }
        else {
            selected.remove(element);
        }
    }
    
    @OnEvent(component = "deleteSelected")
    protected void deleteSelected()
    {
        try {
            /*
             * Note: we need to clone the set to make sure that concurrent modifications
             * are possible.
             */
            String[] ids = selected.toArray(new String[]{});
            
            /*
             * We assume that the id's of the checkboxes are thumbprints of certificates
             */
            for (String id : ids) {
                storeWS.deleteRequest(Long.parseLong(id));
            }
        }
        catch(WebServiceCheckedException e)
        {
            logger.error("Error deleteSelected", e);
            
            error = true;
            errorMessage = ExceptionUtils.getRootCauseMessage(e);
        }
    }

    @OnEvent(component = "rescheduleSelected")
    protected void rescheduleSelected()
    {
        try {
            /*
             * Note: we need to clone the set to make sure that concurrent modifications
             * are possible.
             */
            String[] ids = selected.toArray(new String[]{});

            for (String id : ids) {
                storeWS.rescheduleRequest(Long.parseLong(id));
            }
        }
        catch(WebServiceCheckedException e)
        {
            logger.error("Error rescheduleSelected", e);
            
            error = true;
            errorMessage = ExceptionUtils.getRootCauseMessage(e);
        }
    }
    
    public int getRowsPerPage() {
        return rowsPerPage;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }
    
    public boolean isError() {
        return error;
    }
    
    public String getErrorMessage() {
        return StringUtils.isNotBlank(errorMessage) ? errorMessage : "No details.";
    }    
}
