/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.grid;

import java.util.List;
import java.util.regex.Pattern;

import mitm.application.djigzo.ws.PostfixSpoolManagerWS;
import mitm.common.postfix.PostfixQueueItem;
import mitm.common.util.Check;
import mitm.common.ws.WebServiceCheckedException;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.tapestry5.grid.GridDataSource;
import org.apache.tapestry5.grid.SortConstraint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PostfixSpoolGridDataSource implements GridDataSource 
{
	private final static Logger logger = LoggerFactory.getLogger(PostfixSpoolGridDataSource.class);
	
	private List<PostfixQueueItem> queueItems;
	
	/*
	 * we need to keep track of base index because the index in getRowValue is the absolute
	 * index and not relative.
	 */
	private int startIndex;
	
	/*
	 * Web service to talk to the Postfix spool manager
	 */
	private final PostfixSpoolManagerWS spoolManager;
	
    /*
     * The regular expression pattern used to filter on
     */
    private final Pattern searchPattern;
	
	/*
	 * True if an error occured while talking to the spoolManager
	 */
	private boolean failure;
	
    /*
     * The failure message when failure is true
     */
	private String failureMessage;
	
	public PostfixSpoolGridDataSource(PostfixSpoolManagerWS spoolManager, Pattern searchPattern)
	{
		Check.notNull(spoolManager, "spoolManager");
		
		this.spoolManager = spoolManager;
		this.searchPattern = searchPattern;
	}
	
    @Override
	public int getAvailableRows() 
	{
	    int available = 0;
	    
		try {
			available = spoolManager.getQueueLength(searchPattern != null ? searchPattern.pattern() : null);
		} 
		catch (WebServiceCheckedException e) 
		{
		    /*
		     * Can happen when Postfix is down or when a timeout occurs because the spool
		     * contains too many items.
		     */
		    logger.error("Error getting queue length. Is Postfix down?", e);
		    
		    failure = true;
		    failureMessage = ExceptionUtils.getRootCauseMessage(e);
		}
		
		return available;
	}

    @Override
	public Class<PostfixQueueItem> getRowType()
	{
		return PostfixQueueItem.class;
	}

    @Override
	public PostfixQueueItem getRowValue(int index) 
	{
		int rowIndex = index - startIndex;
		
		PostfixQueueItem value = null;
		
		if (queueItems != null && rowIndex < queueItems.size()) {
			value = queueItems.get(rowIndex);
		}
		else {
			logger.warn("Not enough rows.");
			/* 
			 * We do not have enough objects. This can happen when AvailableRows
			 * returned more rows than were actually available when prepared was called. 
			 */ 
			value = null;
		}
		
		return value;
	}

    @Override
	public void prepare(int startIndex, int endIndex, List<SortConstraint> sortConstraints) 
	{
		try {
			queueItems = spoolManager.getQueued(startIndex, endIndex - startIndex + 1,
			        searchPattern != null ? searchPattern.pattern() : null);
		} 
		catch (WebServiceCheckedException e)
		{
            /*
             * Can happen when Postfix is down or when a timeout occurs because the spool
             * contains too many items.
             */
            logger.error("Error getting the spool entries. Is Postfix down?", e);

            failure = true;
            failureMessage = ExceptionUtils.getRootCauseMessage(e);
		} 
		
		this.startIndex = startIndex;
	}

    public boolean isFailure() {
        return failure;
    }

    public String getFailureMessage() {
        return failureMessage;
    }
}
