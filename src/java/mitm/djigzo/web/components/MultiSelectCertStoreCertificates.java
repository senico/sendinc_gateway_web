/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.components;

import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import mitm.application.djigzo.ws.CertificateStore;
import mitm.application.djigzo.ws.CertificateValidatorResult;
import mitm.application.djigzo.ws.KeyAndCertStoreWS;
import mitm.djigzo.web.beans.CertificateType;
import mitm.djigzo.web.beans.CertificateTypeMarker;
import mitm.djigzo.web.beans.X509CertificateBean;
import mitm.djigzo.web.common.CertificateFilterSettings;
import mitm.djigzo.web.common.ValidityCheck;
import mitm.djigzo.web.common.YesNo;
import mitm.djigzo.web.grid.AbstractCertificateGridDataSource;
import mitm.djigzo.web.grid.CertStoreCertificateGridDataSource;
import mitm.djigzo.web.grid.DelegatedStaticCertificateGridDataSource;
import mitm.djigzo.web.services.CertificateStoreMarker;

import org.apache.tapestry5.Block;
import org.apache.tapestry5.Link;
import org.apache.tapestry5.annotations.BeginRender;
import org.apache.tapestry5.annotations.Cached;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.grid.GridDataSource;
import org.apache.tapestry5.internal.services.LinkFactory;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.Request;
import org.apache.tapestry5.services.Response;

@IncludeStylesheet("context:styles/components/multiSelectCertStoreCertificates.css")
public class MultiSelectCertStoreCertificates 
{
    /*
     * CSS classes
     */
    private final static String MSS_HIDDEN_CLASS = "mss-hidden";
    private final static String MSS_AUTOSELECT_CLASS = "mss-autoselect";
    private final static String MSS_INHERITED_CLASS = "mss-inherited";
    
	@Parameter
	private Set<X509CertificateBean> autoSelectCertificates;

	@Parameter
	private Set<X509CertificateBean> inheritedCertificates;

	@Parameter
	private Set<X509CertificateBean> userSelectedCertificates;

    @Parameter(value = "false", defaultPrefix = "literal")
	private boolean useZone;
	
	@Inject
	@CertificateStoreMarker
	private KeyAndCertStoreWS certStore;
	
	@Inject
	private Response response;
	
	@Inject
	private LinkFactory linkFactory;
	
    @Inject
    @Property
    private Block certificateBlock;
            
    @Inject
    private Request request;
    
	@Component(parameters = {"source = source",	"exclude=" + CertificateGrid.DELETE_COLUMN, "rowsPerPage = inherit:rowsPerPage",
	        "pagerPosition = inherit:pagerPosition", "legendBlock = inherit:legendBlock"})
    private MultiSelectCertificateGrid certificateGrid;

	@SuppressWarnings("unused")
	@Component(parameters = {"filterSettings = filterSettings"})
	private CertificateFilter certificateFilter;

	@BeginRender
	public void beginRender() 
	{
		/*
		 * Set the selected checkboxes to match the userSelectedCertificates
		 */
		certificateGrid.clearSelected();
		
		if (getUserSelectedCertificates() != null)
		{
			for (X509CertificateBean certificate : getUserSelectedCertificates()) {
				certificateGrid.getSelected().add(certificate.getThumbprint());
			}
		}
	}
	
	@Cached
	private Set<X509CertificateBean> getAutoSelectCertificates() {
		return autoSelectCertificates;
	}

	@Cached
	private Set<X509CertificateBean> getInheritedCertificates() {
		return inheritedCertificates;
	}

	@Cached
	private Set<X509CertificateBean> getUserSelectedCertificates() {
		return userSelectedCertificates;
	}

	private void addCertificates(Collection<X509CertificateBean> source, 
			Collection<X509CertificateBean> target)
	{
		if (target != null) {
			source.addAll(target);
		}
	}
	
	public GridDataSource getSource()
	{
		AbstractCertificateGridDataSource delegate = new CertStoreCertificateGridDataSource(
				certStore, getFilterSettings());
		
		List<X509CertificateBean> list = new LinkedList<X509CertificateBean>();

		addCertificates(list, getAutoSelectCertificates());
		addCertificates(list, getInheritedCertificates());
		addCertificates(list, getUserSelectedCertificates());
		
		return new DelegatedStaticCertificateGridDataSource(list, delegate);
	}
	
	/*
	 * The user selected filter settings
	 */
	@Persist
	private CertificateFilterSettings filterSettings;
		
	public MultiSelectCertificateGrid getCertificateGrid() {
		return certificateGrid;
	}
	
	public KeyAndCertStoreWS getCertStore() {
		return certStore;
	}

	public void setCertStore(KeyAndCertStoreWS certStore) {
		this.certStore = certStore;
	}

	public CertificateFilterSettings getFilterSettings() 
	{
		if (filterSettings == null) {
			filterSettings = new CertificateFilterSettings();
		}

		return filterSettings;
	}
	
	/*
	 * Returns true if the certificate is either auto selected or inherited
	 */
	private boolean isNonRemovable(X509CertificateBean certificate)
	{
		boolean nonRemovable = getAutoSelectCertificates() != null && 
			getAutoSelectCertificates().contains(certificate);
		
		if (!nonRemovable && getInheritedCertificates() != null) {
			nonRemovable = getInheritedCertificates().contains(certificate);
		}
		
		return nonRemovable;
	}
	
	@OnEvent(CertificateGrid.IS_DISABLED_EVENT)
	protected YesNo isDisabled(X509CertificateBean certificate, String column) 
	{
		YesNo disabled = YesNo.NO;
		
		if (CertificateGrid.SELECT_COLUMN.equals(column)) 
		{
			/*
			 * Disable the checkbox if the certificate is auto select or inherited because the
			 * certificate cannot be 'removed' 
			 */
			if (isNonRemovable(certificate)) {
				disabled = YesNo.YES;
			}
		}
		
		return disabled;
	}
	
	@OnEvent(CertificateGrid.ROW_CLASS_EVENT)
	protected String getRowClass(X509CertificateBean certificate, CertificateValidatorResult validatorResult) 
	{
		/*
		 * We cannot return null because returning null has a special meaning for events so I will
		 * return "" if no class should be specified (the default class will be used instead defined by
		 * the CertificateGrid)
		 */
		String rowClass = "";
		
		if (certificate instanceof CertificateTypeMarker)
		{
			CertificateType certificateType = ((CertificateTypeMarker) certificate).getCertificateType();
			
			if (getAutoSelectCertificates() != null && getAutoSelectCertificates().contains(certificate)) 
			{
				/*
				 * The Certificate is an auto select. If the certificate comes from the dynamic 
				 * certificate store we will make it invisible because it is already shown. This will
				 * have some side-effects (like less rows etc.) but I do not know of a better way.
				 * I have tried to skip certificates in the grid source but that won't work because
				 * you then have to step through all entries which won't work with the grid pagers
				 * because they let the grid source start somewhere in the middle.
				 */
				rowClass = certificateType == CertificateType.GENERAL ? MSS_HIDDEN_CLASS :  MSS_AUTOSELECT_CLASS;
			}
			else if (getInheritedCertificates() != null && getInheritedCertificates().contains(certificate)) 
			{
				/*
				 * See above for explanation
				 */
				rowClass = certificateType == CertificateType.GENERAL ? MSS_HIDDEN_CLASS : MSS_INHERITED_CLASS;
			}
			else if (getUserSelectedCertificates() != null && getUserSelectedCertificates().contains(certificate)) 
			{
				/*
				 * See above for explanation
				 */
				rowClass = certificateType == CertificateType.GENERAL ? MSS_HIDDEN_CLASS : "";
			}
			
			if (!MSS_HIDDEN_CLASS.equals(rowClass))
			{
			    /* 
			     * If the certificate is not hidden and the certificate is not 
			     * valid we will fallback on the CertificateGrid CSS class 
			     * handling
			     */
			    if (!validatorResult.isValid()) {
			        rowClass = "";
			    }
			}
		}
		
		return rowClass;
	}
	
	@OnEvent(CertificateGrid.SUBJECT_CLICKED_EVENT)
	protected void onSubjectClicked(String thumbprint) 
	throws IOException
	{
        Link link = linkFactory.createPageRenderLink("certificate/view", false, CertificateStore.CERTIFICATES,
        		thumbprint, ValidityCheck.ENCRYPTION);

        response.sendRedirect(link);
	}
	
    public Object onSuccess()
    {
        return request.isXHR() ? certificateBlock : null;
    }

    public boolean isUseZone() {
        return useZone;
    }

    public void setUseZone(boolean useZone) {
        this.useZone = useZone;
    }
    
    public Object getZone() {
        return useZone ? "certificateZone" : null;
    }
}
