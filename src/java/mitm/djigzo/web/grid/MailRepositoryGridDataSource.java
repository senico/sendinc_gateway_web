/*
 * Copyright (c) 2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.grid;

import java.util.Collections;
import java.util.List;

import mitm.application.djigzo.DjigzoRuntimeException;
import mitm.application.djigzo.ws.MailRepositoryItemDTO;
import mitm.application.djigzo.ws.MailRepositoryWS;
import mitm.common.mail.repository.MailRepositorySearchField;
import mitm.common.util.Check;
import mitm.common.ws.WebServiceCheckedException;

import org.apache.commons.lang.StringUtils;
import org.apache.tapestry5.grid.GridDataSource;
import org.apache.tapestry5.grid.SortConstraint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MailRepositoryGridDataSource implements GridDataSource 
{
	private final static Logger logger = LoggerFactory.getLogger(MailRepositoryGridDataSource.class);
	
	/*
	 * The Mail Repository items
	 */
	private List<MailRepositoryItemDTO> items;
		
	/*
	 * we need to keep track of base index because the index in getRowValue is the absolute
	 * index and not relative.
	 */
	private int startIndex;
	
	/*
	 * The MailRepository web service 
	 */
	private final MailRepositoryWS mailRepositoryWS;

	/*
	 * The field to search on. Set to null if search is disabled.
	 */
	private final MailRepositorySearchField searchField;
	
	/*
	 * The data to search for.
	 */
	private final String searchKey;
	
	public MailRepositoryGridDataSource(MailRepositoryWS mailRepositoryWS, MailRepositorySearchField searchField,
	        String searchKey)
	{
        Check.notNull(mailRepositoryWS, "mailRepositoryWS");
        
        this.mailRepositoryWS = mailRepositoryWS;
        this.searchField = searchField;
        this.searchKey = searchKey;
    }
	
    @Override
	public int getAvailableRows() 
	{
		try {
            return searchField == null ? mailRepositoryWS.getItemCount() : 
                        mailRepositoryWS.getSearchCount(searchField, getSearchKey());
		} 
		catch (WebServiceCheckedException e) {
			throw new DjigzoRuntimeException(e);
		}
	}

    @Override
	public Class<MailRepositoryItemDTO> getRowType()
	{
		return MailRepositoryItemDTO.class;
	}

    @Override
	public MailRepositoryItemDTO getRowValue(int index) 
	{
		int rowIndex = index - startIndex;
		
		MailRepositoryItemDTO value = null;
		
		if (items != null && rowIndex < items.size()) {
			value = items.get(rowIndex);
		}
		else {
			logger.warn("Not enough rows.");
			/* 
			 * We do not have enough objects. This can happen when AvailableRows
			 * returned more rows than were actually available when prepared was called. 
			 */ 
			value = null;
		}
		
		return value;
	}

    private String getSearchKey() 
    {
        String result = StringUtils.defaultString(searchKey);

        if (result.isEmpty()) {
            /*
             * If search is empty search for all everything
             */
            result = "%%";
        }
        else {
            /*
             * If the filter string does not contain a LIKE special symbol ('%' or '_') the search
             * string will be embedded in %%.
             */
            if (!StringUtils.containsAny(result, "%_")) {
                result = "%" + result + "%";
            }
        }
 
        return result;
    }
	
    @Override
	public void prepare(int startIndex, int endIndex, List<SortConstraint> sortConstraints) 
	{
		try {
		    items = searchField == null ? mailRepositoryWS.getItems(startIndex, endIndex - startIndex + 1) :
		            mailRepositoryWS.searchItems(searchField, getSearchKey(), startIndex, endIndex - startIndex + 1);

            if (items == null) {
                /*
                 * Can happen because of a race condition where getAvailableRows said that
                 * there are rows available but the items were already removed when prepare
                 * was called.
                 */
                items = Collections.emptyList();
            }
		} 
		catch (WebServiceCheckedException e) {
			throw new DjigzoRuntimeException(e);
		} 
		
		this.startIndex = startIndex;
	}
}
