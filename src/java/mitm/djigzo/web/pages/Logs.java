/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.pages;

import mitm.application.djigzo.admin.FactoryRoles;
import mitm.djigzo.web.common.LogType;

import org.apache.tapestry5.Block;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.springframework.security.annotation.Secured;


/**
 * Shows the log of the Mail Processing Agent (MPA).
 * 
 * @author Martijn Brinkers
 *
 */
@IncludeStylesheet("context:styles/pages/logs.css")
@Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_LOG_MANAGER})
public class Logs 
{
    /*
     * CSS classname for the active link
     */
    private final static String ACTIVE_CLASS_NAME = "activeLink";

    /*
     * CSS classname for the inactive link
     */
    private final static String NOT_ACTIVE_CLASS_NAME = "notActiveLink";

    @Inject
    private Block mtaBlock;

    @Inject
    private Block mpaBlock;

    @Persist
    private LogType activeLog;
    
	@SetupRender
	@Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_LOG_MANAGER})
	protected void setupRender() {
		/*
		 * Empty on purpose
		 */
	}
    
    private LogType getActiveLog()
    {
        if (activeLog == null) {
            activeLog = LogType.MTA;
        }
        
        return activeLog;
    }
    
    public Block getActiveBlock()
    {
        Block activeBlock;
        
        switch(getActiveLog())
        {
        case MTA : activeBlock = mtaBlock; break;
        case MPA : activeBlock = mpaBlock; break;
        default:
            throw new IllegalArgumentException("Unknown LogType.");
        }
        
        return activeBlock;
    }
    
    public String getLogName() 
    {
        switch(getActiveLog())
        {
        case MTA : return "Mail transfer agent";
        case MPA : return "Mail processing agent";
        default:
            throw new IllegalArgumentException("Unknown LogType.");
        }
    }
    
    public String getMTAClass() {
        return getActiveLog() == LogType.MTA ? ACTIVE_CLASS_NAME : NOT_ACTIVE_CLASS_NAME;
    }

    public String getMPAClass() {
        return getActiveLog() == LogType.MPA ? ACTIVE_CLASS_NAME : NOT_ACTIVE_CLASS_NAME;
    }
    
    @OnEvent(component = "mtaLog")
    protected void showMTASpool() 
    {
        activeLog = LogType.MTA;
    }

    @OnEvent(component = "mpaLog")
    protected void showMPASpool() 
    {
        activeLog = LogType.MPA;
    }  
}
