/*
 * Copyright (c) 2009-2011, Martijn Brinkers, Djigzo.
 *
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar,
 * wsdl4j-1.6.1.jar (or modified versions of these libraries),
 * containing parts covered by the terms of Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to
 * convey the resulting work.
 */
package mitm.djigzo.web.pages.ca;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import mitm.application.djigzo.admin.FactoryRoles;
import mitm.application.djigzo.ws.CAWS;
import mitm.application.djigzo.ws.CertificateRequestHandlerDTO;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.ws.WebServiceCheckedException;
import mitm.djigzo.web.beans.CASettingsBean;
import mitm.djigzo.web.beans.impl.CASettingsBeanImpl;
import mitm.djigzo.web.common.GenericSelectionModel;
import mitm.djigzo.web.common.SignatureAlgorithm;
import mitm.djigzo.web.entities.GlobalPreferencesManager;

import org.apache.commons.lang.StringUtils;
import org.apache.tapestry5.PersistenceConstants;
import org.apache.tapestry5.SelectModel;
import org.apache.tapestry5.ValidationException;
import org.apache.tapestry5.annotations.Cached;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.corelib.components.Checkbox;
import org.apache.tapestry5.corelib.components.Select;
import org.apache.tapestry5.corelib.components.TextField;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.springframework.security.annotation.Secured;

@Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_PKI_MANAGER})
@IncludeStylesheet("context:styles/pages/ca/caSettings.css")
public class CASettings
{
    @Inject
    private GlobalPreferencesManager globalPreferencesManager;

    @Inject
    private CAWS caWS;

    @SuppressWarnings("unused")
    @Component(id = "commonName", parameters = {"value = cASettings.defaultCommonName", "validate = maxlength=255"})
    private TextField commonNameField;

    @SuppressWarnings("unused")
    @Component(id = "validity", parameters = {"value = cASettings.validity", "validate = required, min=1, max=14600"})
    private TextField validityField;

    @SuppressWarnings("unused")
    @Component(id = "keyLength", parameters = {"value = cASettings.keyLength"})
    private Select keyLengthField;

    @SuppressWarnings("unused")
    @Component(id = "signatureAlgorithm", parameters = {"value = signatureAlgorithm", "blankOption = NEVER", "validate = required"})
    private Select signatureAlgorithmField;

    @SuppressWarnings("unused")
    @Component(id = "caEmail", parameters = {"value = cASettings.caEmail", "validate = maxlength=255, emailmitm=1"})
    private TextField caEmailField;

    @SuppressWarnings("unused")
    @Component(id = "passwordLength", parameters = {"value = cASettings.passwordLength", "validate = required, min=4, max=20"})
    private TextField passwordLengthField;

    @SuppressWarnings("unused")
    @Component(id = "crlDistributionPoint", parameters = {"value = cASettings.crlDistributionPoint", "validate = maxLength=1024"})
    private TextField crlDistributionPointField;

    @SuppressWarnings("unused")
    @Component(id = "addCRLDistributionPoint", parameters = {"value = cASettings.addCRLDistributionPoint"})
    private Checkbox addCRLDistributionPointField;

    @SuppressWarnings("unused")
    @Component(id = "certificateRequestHandler", parameters = {"value = cASettings.certificateRequestHandler",
            "model = certificateRequestHandlerModel", "blankOption = NEVER", "validate = required"})
    private Select certificateRequestHandlerField;

    /*
     * True if changes were applied
     */
    @Persist(PersistenceConstants.FLASH)
    private boolean applied;

    @SetupRender
    @Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_PKI_MANAGER})
    public void setupRender() {
        /*
         * Empty on purpose
         */
    }

    @Cached
    public CASettingsBean getCASettings()
    throws HierarchicalPropertiesException, WebServiceCheckedException
    {
        return new CASettingsBeanImpl(globalPreferencesManager.getProperties());
    }

    public void onSuccess()
    throws WebServiceCheckedException, HierarchicalPropertiesException
    {
        getCASettings().save();

        applied = true;
    }

    @Cached
    public List<String> getCertificateRequestHandlers()
    throws WebServiceCheckedException
    {
        List<String> result = new LinkedList<String>();

        List<CertificateRequestHandlerDTO> handlers = caWS.getCertificateRequestHandlers();

        for (CertificateRequestHandlerDTO handler : handlers)
        {
            if (handler.isEnabled()) {
                result.add(handler.getName());
            }
        }

        return result;
    }

    public SelectModel getCertificateRequestHandlerModel()
    throws WebServiceCheckedException
    {
        return new GenericSelectionModel<String>(new ArrayList<String>(getCertificateRequestHandlers()), null, null);
    }

    /*
     * Event handler called when the cancel button is pressed.
     */
    protected Object onCancel() {
        return CAIssue.class;
    }

    public boolean isApplied() {
        return applied;
    }

    public SignatureAlgorithm getSignatureAlgorithm()
    throws HierarchicalPropertiesException, WebServiceCheckedException
    {
        SignatureAlgorithm algorithm = SignatureAlgorithm.fromName(getCASettings().getSignatureAlgorithm());

        if (algorithm == null)
        {
            /*
             * Name not found to use default
             */
            algorithm = SignatureAlgorithm.SHA256_WITH_RSA;
        }

        return algorithm;
    }

    public void setSignatureAlgorithm(SignatureAlgorithm signatureAlgorithm)
    throws HierarchicalPropertiesException, WebServiceCheckedException
    {
        getCASettings().setSignatureAlgorithm(signatureAlgorithm != null ? signatureAlgorithm.getName() : null);
    }

    public void onValidateFromCrlDistributionPoint(String url)
    throws ValidationException
    {
        if (StringUtils.isNotBlank(url))
        {
            /*
             * Check if the crlDistributionPoint is a valid URL
             */
            try {
                new URL(url);
            }
            catch (MalformedURLException e) {
                throw new ValidationException("CRL distribution point is not a valid URL.");
            }
        }
    }
}
