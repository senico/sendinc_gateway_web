/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.pages.ca.handlers.comodo;

import mitm.application.djigzo.admin.FactoryRoles;
import mitm.application.djigzo.ws.ComodoTierDetailsDTO;
import mitm.application.djigzo.ws.ComodoWS;
import mitm.common.ws.WebServiceCheckedException;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.corelib.components.BeanDisplay;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.springframework.security.annotation.Secured;

@Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_PKI_MANAGER})
@IncludeStylesheet("context:styles/pages/ca/handlers/comodo/tierDetails.css")
public class ComodoTierDetails
{
    @Inject
    private ComodoWS comodoWS;

    /*
     * True if an error occurred 
     */
    private boolean failure;
    
    /*
     * If an error occurred this will be the failure message 
     */
    private String failureMessage;

    /*
     * The detail of the Tier 2 account
     */
    private ComodoTierDetailsDTO details;
    
    @SuppressWarnings("unused")
    @Component(parameters = "object = tierDetails")
    private BeanDisplay beanDisplay;
    
    @SetupRender
    @Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_PKI_MANAGER})
    public void setupRender() {
        loadTierDetails();
    }

    private void loadTierDetails()
    {
        try {
            details = comodoWS.getTierDetails();
        }
        catch (WebServiceCheckedException e)
        {
            failure = true;
            failureMessage = ExceptionUtils.getRootCauseMessage(e);
        }
        
        if (details == null)
        {
            /*
             *  We must return a valid instance so return an empty details object.
             */
            details = new ComodoTierDetailsDTO();
        }
    }
    
    public ComodoTierDetailsDTO getTierDetails() {
        return details;
    }
    
    /*
     * Event handler called when the cancel button is pressed.
     */
    protected Object onCancel() {
        return ComodoSettings.class;
    }
    
    public boolean isFailure() {
        return failure;
    }

    public String getFailureMessage() {
        return failureMessage;
    }
}
