/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.entities;

import java.util.Collection;
import java.util.List;

import mitm.application.djigzo.ws.X509CertificateDTO;
import mitm.common.ws.WebServiceCheckedException;

public interface UserPreferences 
{
    public List<X509CertificateDTO> getCertificates()
    throws WebServiceCheckedException;

    public void setCertificates(Collection<String> thumbprints)
    throws WebServiceCheckedException;    
    
    public List<X509CertificateDTO> getInheritedCertificates()
    throws WebServiceCheckedException;

    public List<X509CertificateDTO> getNamedCertificates(String name)
    throws WebServiceCheckedException;

    public void setNamedCertificates(String name, Collection<String> thumbprints)
    throws WebServiceCheckedException;    
    
    public List<X509CertificateDTO> getInheritedNamedCertificates(String name)
    throws WebServiceCheckedException;
    
    public X509CertificateDTO getKeyAndCertificate()
    throws WebServiceCheckedException;

    public void setKeyAndCertificate(String thumbprint)
    throws WebServiceCheckedException;
    
    public List<X509CertificateDTO> getInheritedKeyAndCertificates()
    throws WebServiceCheckedException;
    
    public SaveableUserProperties getProperties()
    throws WebServiceCheckedException;
}
