/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.beans.impl;

import org.apache.commons.lang.StringUtils;

import mitm.application.djigzo.DLPProperties;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.util.Check;
import mitm.djigzo.web.beans.BooleanProperty;
import mitm.djigzo.web.beans.DLPPropertiesBean;
import mitm.djigzo.web.beans.StringProperty;
import mitm.djigzo.web.entities.SaveableUserProperties;

/**
 * Bean to get and set DLP properties from the DLP properties page.
 * 
 * @author Martijn Brinkers
 *
 */
public class DLPPropertiesBeanImpl implements DLPPropertiesBean
{
	private final BooleanProperty enabled = new BooleanProperty();
    private final StringProperty quarantineURL = new StringProperty();
    private final StringProperty dlpManagers = new StringProperty();
    private final BooleanProperty sendWarningToOriginator = new BooleanProperty();
    private final BooleanProperty sendWarningToDLPManagers = new BooleanProperty();
    private final BooleanProperty sendQuarantineToOriginator = new BooleanProperty();
    private final BooleanProperty sendQuarantineToDLPManagers = new BooleanProperty();
    private final BooleanProperty sendBlockToOriginator = new BooleanProperty();
    private final BooleanProperty sendBlockToDLPManagers = new BooleanProperty();
    private final BooleanProperty sendErrorToOriginator = new BooleanProperty();
    private final BooleanProperty sendErrorToDLPManagers = new BooleanProperty();
    private final BooleanProperty allowDownloadMessage = new BooleanProperty();
    private final BooleanProperty allowReleaseMessage = new BooleanProperty();
    private final BooleanProperty allowReleaseEncryptMessage = new BooleanProperty();
    private final BooleanProperty allowReleaseAsIsMessage = new BooleanProperty();
    private final BooleanProperty allowDeleteMessage = new BooleanProperty();
    private final BooleanProperty quarantineOnError = new BooleanProperty();
    private final BooleanProperty quarantineOnFailedEncryption = new BooleanProperty();
    private final BooleanProperty sendReleaseNotifyToOriginator = new BooleanProperty();
    private final BooleanProperty sendReleaseNotifyToDLPManagers = new BooleanProperty();
    private final BooleanProperty sendDeleteNotifyToOriginator = new BooleanProperty();
    private final BooleanProperty sendDeleteNotifyToDLPManagers = new BooleanProperty();
    private final BooleanProperty sendExpireNotifyToOriginator = new BooleanProperty();
    private final BooleanProperty sendExpireNotifyToDLPManagers = new BooleanProperty();
    
    /*
	 * We need a reference to the SaveableUserProperties to be able to save 
	 * the DLP properties. 
	 */
	private final SaveableUserProperties saveableProperties;
	
	/*
	 * The DLP properties
	 */
	private final DLPProperties dlpProperties;
	
	public DLPPropertiesBeanImpl(SaveableUserProperties saveableProperties) 
	throws HierarchicalPropertiesException 
	{
		Check.notNull(saveableProperties, "saveableProperties");
		
		this.saveableProperties = saveableProperties;
		this.dlpProperties = saveableProperties.getDLPProperties();
		
		initializeProperties();
	}
	
	private boolean isInherited(String property)
	throws HierarchicalPropertiesException 
	{
		return dlpProperties.isInherited(dlpProperties.getFullPropertyName(property));
	}
	
	private void initializeProperties() 
	throws HierarchicalPropertiesException 
	{
	    enabled.setValue(dlpProperties.isEnabled());
	    enabled.setInherit(isInherited(DLPProperties.ENABLED));

        quarantineURL.setValue(dlpProperties.getQuarantineURL());
        quarantineURL.setInherit(isInherited(DLPProperties.QUARANTINE_URL));

        dlpManagers.setValue(StringUtils.join(dlpProperties.getDLPManagers(), ","));
        dlpManagers.setInherit(isInherited(DLPProperties.DLP_MANAGERS));
        
        sendWarningToOriginator.setValue(dlpProperties.isSendWarningToOriginator());
        sendWarningToOriginator.setInherit(isInherited(DLPProperties.SEND_WARNING_TO_ORIGINATOR));

        sendWarningToDLPManagers.setValue(dlpProperties.isSendWarningToDLPManagers());
        sendWarningToDLPManagers.setInherit(isInherited(DLPProperties.SEND_WARNING_TO_DLP_MANAGERS));
        
        sendQuarantineToOriginator.setValue(dlpProperties.isSendQuarantineToOriginator());
        sendQuarantineToOriginator.setInherit(isInherited(DLPProperties.SEND_QUARANTINE_TO_ORIGINATOR));

        sendQuarantineToDLPManagers.setValue(dlpProperties.isSendQuarantineToDLPManagers());
        sendQuarantineToDLPManagers.setInherit(isInherited(DLPProperties.SEND_QUARANTINE_TO_DLP_MANAGERS));
        
        sendBlockToOriginator.setValue(dlpProperties.isSendBlockToOriginator());
        sendBlockToOriginator.setInherit(isInherited(DLPProperties.SEND_BLOCK_TO_ORIGINATOR));

        sendBlockToDLPManagers.setValue(dlpProperties.isSendBlockToDLPManagers());
        sendBlockToDLPManagers.setInherit(isInherited(DLPProperties.SEND_BLOCK_TO_DLP_MANAGERS));

        sendErrorToOriginator.setValue(dlpProperties.isSendErrorToOriginator());
        sendErrorToOriginator.setInherit(isInherited(DLPProperties.SEND_ERROR_TO_ORIGINATOR));

        sendErrorToDLPManagers.setValue(dlpProperties.isSendErrorToDLPManagers());
        sendErrorToDLPManagers.setInherit(isInherited(DLPProperties.SEND_ERROR_TO_DLP_MANAGERS));
        
        allowDownloadMessage.setValue(dlpProperties.isAllowDownloadMessage());
        allowDownloadMessage.setInherit(isInherited(DLPProperties.ALLOW_DOWNLOAD_MESSAGE));
        
        allowReleaseMessage.setValue(dlpProperties.isAllowReleaseMessage());
        allowReleaseMessage.setInherit(isInherited(DLPProperties.ALLOW_RELEASE_MESSAGE));

        allowReleaseEncryptMessage.setValue(dlpProperties.isAllowReleaseEncryptMessage());
        allowReleaseEncryptMessage.setInherit(isInherited(DLPProperties.ALLOW_RELEASE_ENCRYPT_MESSAGE));

        allowReleaseAsIsMessage.setValue(dlpProperties.isAllowReleaseAsIsMessage());
        allowReleaseAsIsMessage.setInherit(isInherited(DLPProperties.ALLOW_RELEASE_AS_IS_MESSAGE));

        allowDeleteMessage.setValue(dlpProperties.isAllowDeleteMessage());
        allowDeleteMessage.setInherit(isInherited(DLPProperties.ALLOW_DELETE_MESSAGE));
        
        quarantineOnError.setValue(dlpProperties.isQuarantineOnError());
        quarantineOnError.setInherit(isInherited(DLPProperties.QUARANTINE_ON_ERROR));

        quarantineOnFailedEncryption.setValue(dlpProperties.isQuarantineOnFailedEncryption());
        quarantineOnFailedEncryption.setInherit(isInherited(DLPProperties.QUARANTINE_ON_FAILED_ENCRYPTION));
        
        sendReleaseNotifyToOriginator.setValue(dlpProperties.isSendReleaseNotifyToOriginator());
        sendReleaseNotifyToOriginator.setInherit(isInherited(DLPProperties.SEND_RELEASE_NOTIFY_TO_ORIGINATOR));

        sendReleaseNotifyToDLPManagers.setValue(dlpProperties.isSendReleaseNotifyToDLPManagers());
        sendReleaseNotifyToDLPManagers.setInherit(isInherited(DLPProperties.SEND_RELEASE_NOTIFY_TO_DLP_MANAGERS));

        sendDeleteNotifyToOriginator.setValue(dlpProperties.isSendDeleteNotifyToOriginator());
        sendDeleteNotifyToOriginator.setInherit(isInherited(DLPProperties.SEND_DELETE_NOTIFY_TO_ORIGINATOR));

        sendDeleteNotifyToDLPManagers.setValue(dlpProperties.isSendDeleteNotifyToDLPManagers());
        sendDeleteNotifyToDLPManagers.setInherit(isInherited(DLPProperties.SEND_DELETE_NOTIFY_TO_DLP_MANAGERS));

        sendExpireNotifyToOriginator.setValue(dlpProperties.isSendExpireNotifyToOriginator());
        sendExpireNotifyToOriginator.setInherit(isInherited(DLPProperties.SEND_EXPIRE_NOTIFY_TO_ORIGINATOR));

        sendExpireNotifyToDLPManagers.setValue(dlpProperties.isSendExpireNotifyToDLPManagers());
        sendExpireNotifyToDLPManagers.setInherit(isInherited(DLPProperties.SEND_EXPIRE_NOTIFY_TO_DLP_MANAGERS));
	}

    @Override
    public BooleanProperty getEnabled() {
        return enabled;
    }

    @Override
    public StringProperty getQuarantineURL() {
        return quarantineURL;
    }
    
    @Override
    public StringProperty getDLPManagers() {
        return dlpManagers;
    }
    
    @Override
    public BooleanProperty getSendWarningToOriginator() {
        return sendWarningToOriginator;
    }

    @Override
    public BooleanProperty getSendWarningToDLPManagers() {
        return sendWarningToDLPManagers;
    }
    
    @Override
    public BooleanProperty getSendQuarantineToOriginator() {
        return sendQuarantineToOriginator;
    }

    @Override
    public BooleanProperty getSendQuarantineToDLPManagers() {
        return sendQuarantineToDLPManagers;
    }
    
    @Override
    public BooleanProperty getSendBlockToOriginator() {
        return sendBlockToOriginator;
    }

    @Override
    public BooleanProperty getSendBlockToDLPManagers() {
        return sendBlockToDLPManagers;
    }
    
    @Override
    public BooleanProperty getSendErrorToOriginator() {
        return sendErrorToOriginator;
    }

    @Override
    public BooleanProperty getSendErrorToDLPManagers() {
        return sendErrorToDLPManagers;
    }


    @Override
    public BooleanProperty getAllowDownloadMessage() {
        return allowDownloadMessage;
    }

    @Override
    public BooleanProperty getAllowReleaseMessage() {
        return allowReleaseMessage;
    }

    @Override
    public BooleanProperty getAllowReleaseEncryptMessage() {
        return allowReleaseEncryptMessage;
    }

    @Override
    public BooleanProperty getAllowReleaseAsIsMessage() {
        return allowReleaseAsIsMessage;
    }
    
    @Override
    public BooleanProperty getAllowDeleteMessage() {
        return allowDeleteMessage;
    }    
    
    @Override
    public BooleanProperty getQuarantineOnError() {
        return quarantineOnError;
    }
    
    @Override
    public BooleanProperty getQuarantineOnFailedEncryption() {
        return quarantineOnFailedEncryption;
    }
    
    @Override
    public BooleanProperty getSendReleaseNotifyToOriginator() {
        return sendReleaseNotifyToOriginator;
    }
    
    @Override
    public BooleanProperty getSendReleaseNotifyToDLPManagers() {
        return sendReleaseNotifyToDLPManagers;
    }    
    
    @Override
    public BooleanProperty getSendDeleteNotifyToOriginator() {
        return sendDeleteNotifyToOriginator;
    }
    
    @Override
    public BooleanProperty getSendDeleteNotifyToDLPManagers() {
        return sendDeleteNotifyToDLPManagers;
    }
    
    @Override
    public BooleanProperty getSendExpireNotifyToOriginator() {
        return sendExpireNotifyToOriginator;
    }
    
    @Override
    public BooleanProperty getSendExpireNotifyToDLPManagers() {
        return sendExpireNotifyToDLPManagers;
    }
    
    @Override
    public void save()
    throws HierarchicalPropertiesException
    {
        dlpProperties.setEnabled(enabled.getUserValue());
        dlpProperties.setQuarantineURL(StringUtils.trim(quarantineURL.getUserValue()));
        dlpProperties.setDLPManagers(StringUtils.split(dlpManagers.getUserValue(), ","));
        dlpProperties.setSendWarningToOriginator(sendWarningToOriginator.getUserValue());
        dlpProperties.setSendWarningToDLPManagers(sendWarningToDLPManagers.getUserValue());
        dlpProperties.setSendQuarantineToOriginator(sendQuarantineToOriginator.getUserValue());
        dlpProperties.setSendQuarantineToDLPManagers(sendQuarantineToDLPManagers.getUserValue());
        dlpProperties.setSendBlockToOriginator(sendBlockToOriginator.getUserValue());
        dlpProperties.setSendBlockToDLPManagers(sendBlockToDLPManagers.getUserValue());
        dlpProperties.setSendErrorToOriginator(sendErrorToOriginator.getUserValue());
        dlpProperties.setSendErrorToDLPManagers(sendErrorToDLPManagers.getUserValue());
        dlpProperties.setAllowDownloadMessage(allowDownloadMessage.getUserValue());
        dlpProperties.setAllowReleaseMessage(allowReleaseMessage.getUserValue());
        dlpProperties.setAllowReleaseEncryptMessage(allowReleaseEncryptMessage.getUserValue());
        dlpProperties.setAllowReleaseAsIsMessage(allowReleaseAsIsMessage.getUserValue());
        dlpProperties.setAllowDeleteMessage(allowDeleteMessage.getUserValue());
        dlpProperties.setQuarantineOnError(quarantineOnError.getUserValue());
        dlpProperties.setQuarantineOnFailedEncryption(quarantineOnFailedEncryption.getUserValue());
        dlpProperties.setSendReleaseNotifyToOriginator(sendReleaseNotifyToOriginator.getUserValue());
        dlpProperties.setSendReleaseNotifyToDLPManagers(sendReleaseNotifyToDLPManagers.getUserValue());
        dlpProperties.setSendDeleteNotifyToOriginator(sendDeleteNotifyToOriginator.getUserValue());
        dlpProperties.setSendDeleteNotifyToDLPManagers(sendDeleteNotifyToDLPManagers.getUserValue());
        dlpProperties.setSendExpireNotifyToOriginator(sendExpireNotifyToOriginator.getUserValue());
        dlpProperties.setSendExpireNotifyToDLPManagers(sendExpireNotifyToDLPManagers.getUserValue());
        
        saveableProperties.save();
    }
}