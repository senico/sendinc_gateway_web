/*
 * Copyright (c) 2009-2011, Martijn Brinkers, Djigzo.
 *
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar,
 * wsdl4j-1.6.1.jar (or modified versions of these libraries),
 * containing parts covered by the terms of Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to
 * convey the resulting work.
 */
package mitm.djigzo.web.pages.ca;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import mitm.application.djigzo.admin.FactoryRoles;
import mitm.application.djigzo.ws.CAWS;
import mitm.application.djigzo.ws.SendCertificatesDTO;
import mitm.application.djigzo.ws.X509CertificateDTO;
import mitm.common.mail.EmailAddressUtils;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.ws.WebServiceCheckedException;
import mitm.djigzo.web.beans.CASettingsBean;
import mitm.djigzo.web.beans.CertificateType;
import mitm.djigzo.web.beans.SMSPropertiesBean;
import mitm.djigzo.web.beans.StringProperty;
import mitm.djigzo.web.beans.UserBean;
import mitm.djigzo.web.beans.X509CertificateBean;
import mitm.djigzo.web.beans.impl.CASettingsBeanImpl;
import mitm.djigzo.web.beans.impl.SMSPropertiesBeanImpl;
import mitm.djigzo.web.beans.impl.UserBeanImpl;
import mitm.djigzo.web.beans.impl.X509CertificateBeanImpl;
import mitm.djigzo.web.common.CertificateFilterBy;
import mitm.djigzo.web.components.EncryptionCertificates;
import mitm.djigzo.web.components.GeneratePasswordEdit;
import mitm.djigzo.web.entities.GlobalPreferencesManager;
import mitm.djigzo.web.entities.User;
import mitm.djigzo.web.entities.UserManager;

import org.apache.commons.lang.StringUtils;
import org.apache.tapestry5.EventConstants;
import org.apache.tapestry5.PersistenceConstants;
import org.apache.tapestry5.annotations.Cached;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.corelib.components.Checkbox;
import org.apache.tapestry5.corelib.components.Form;
import org.apache.tapestry5.corelib.components.TextField;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.ioc.annotations.Value;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.annotation.Secured;

@Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_PKI_MANAGER})
@IncludeStylesheet("context:styles/pages/ca/caSend.css")
public class CASend
{
    private final static Logger logger = LoggerFactory.getLogger(CASend.class);

    /*
     * Email address of the PFX recipient
     */
    @Persist(PersistenceConstants.FLASH)
    private String email;

    /*
     * The password for the generated PFX file
     */
    @Persist(PersistenceConstants.FLASH)
    private String password;

    /*
     * If true certificates with email addresses that do not match recipient are allowed
     * to be sent
     */
    @Persist(PersistenceConstants.FLASH)
    private boolean allowEmailMismatch;

    /*
     * The selected certificates
     */
    @Persist(PersistenceConstants.FLASH)
    private Set<X509CertificateBean> selectedCertificates;

    /*
     * True if certificates were sent
     */
    @Persist(PersistenceConstants.FLASH)
    private boolean sent;

    /*
     * True if and error has occurred
     */
    @Persist(PersistenceConstants.FLASH)
    private boolean error;

    /*
     * Error message if an error has occurred
     */
    @Persist(PersistenceConstants.FLASH)
    private String errorMessage;

    @Inject
    private UserManager userManager;

    @Inject
    private GlobalPreferencesManager globalPreferencesManager;

    @Inject
    private CAWS caWS;

    @Inject
    @Value("${sendCertificate.rowsPerPage}")
    private int rowsPerPage;

    @Component(parameters = {
            "userSelectedCertificates = selectedcertificates", "rowsPerPage = prop:rowsPerPage", "pagerPosition = literal:top",
            "useZone = literal:true"})
    private EncryptionCertificates certificates;

    @Component(id = "email", parameters = {"value = email", "validate = maxlength=255, emailmitm=1, required"})
    private TextField emailField;

    @SuppressWarnings("unused")
    @Component(id = "password", parameters = {"value = password", "validate = minLength=6,maxLength=255, required",
            "passwordLength=prop:passwordLength"})
    private GeneratePasswordEdit passwordField;

    @SuppressWarnings("unused")
    @Component(id = "allowEmailMismatch", parameters = {"value = allowEmailMismatch"})
    private Checkbox allowEmailMismatchField;

    @Component(id = "deliveryForm")
    private Form form;

    @SetupRender
    @Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_PKI_MANAGER})
    protected void setupRender()
    throws HierarchicalPropertiesException, WebServiceCheckedException
    {
    }

    @Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_PKI_MANAGER})
    protected void onActivate(String email)
    throws WebServiceCheckedException, HierarchicalPropertiesException
    {
        UserBean user = getUserBean(email);

        if (user != null)
        {
            this.email = user.getEmail();

            getSelectedCertificates().addAll(user.getAutoSelectEncryptionCertificates());
            getSelectedCertificates().addAll(user.getPreferences().getCertificates());

            /*
             * Set filter to only show entries with private key and matching email
             */
            certificates.getFilterSettings().setAllowMissingKeyAlias(false);
            certificates.getFilterSettings().setFilter(this.email);
            certificates.getFilterSettings().setFilterBy(CertificateFilterBy.EMAIL);
        }
        else {
            certificates.getFilterSettings().setAllowMissingKeyAlias(false);
            certificates.getFilterSettings().setFilter(null);
            certificates.getFilterSettings().setFilterBy(CertificateFilterBy.NO_FILTER);
        }
    }

    private UserBean getUserBean(String email)
    throws WebServiceCheckedException, HierarchicalPropertiesException
    {
        User user = getUser(email);

        if (user == null) {
            return null;
        }

        return new UserBeanImpl(user);
    }

    private User getUser(String email)
    throws WebServiceCheckedException, HierarchicalPropertiesException
    {
        if (StringUtils.isBlank(email)) {
            return null;
        }

        String filtered = EmailAddressUtils.canonicalizeAndValidate(email, true);

        if (filtered == null) {
            return null;
        }

        return userManager.getUser(filtered, false /* null if not exist */);
    }

    private SMSPropertiesBean getSMSProperties(String email)
    throws WebServiceCheckedException, HierarchicalPropertiesException
    {
        User user = getUser(email);

        if (user == null) {
            return null;
        }

        return new SMSPropertiesBeanImpl(user.getPreferences().getProperties());
    }

    public Set<X509CertificateBean> getSelectedCertificates()
    {
        if (selectedCertificates == null) {
            selectedCertificates = new HashSet<X509CertificateBean>();
        }

        return selectedCertificates;
    }

    @Cached
    public CASettingsBean getCASettings()
    throws HierarchicalPropertiesException, WebServiceCheckedException
    {
        return new CASettingsBeanImpl(globalPreferencesManager.getProperties());
    }

    @OnEvent(value = EventConstants.VALIDATE_FORM, component = "deliveryForm")
    public void onValidateFromDeliveryForm()
    throws HierarchicalPropertiesException, WebServiceCheckedException
    {
        /*
         * we have to make sure that the CA email address is set
         */
        if (StringUtils.isBlank(getCASettings().getCAEmail()))
        {
            form.recordError(emailField, "The issued certificate can only be sent when " +
                    "the CA email address is specified. See CA settings.");
        }

        if (certificates.getCertificateGrid().getSelected().size() == 0) {
            form.recordError("There are no certificates selected.");
        }
    }

    @OnEvent(value = EventConstants.PREPARE_FOR_SUBMIT, component = "deliveryForm")
    public void onPrepareForSubmit()
    throws WebServiceCheckedException
    {
        getSelectedCertificates().clear();

        Set<String> thumbprints = certificates.getCertificateGrid().getSelected();

        if (thumbprints != null)
        {
            for (String thumbprint : thumbprints)
            {
                X509CertificateDTO certificate = certificates.getCertStore().getCertificate(thumbprint);

                if (certificate != null) {
                    getSelectedCertificates().add(new X509CertificateBeanImpl(
                            certificate,CertificateType.SELECTED));
                }
            }
        }
    }

    @OnEvent(value = EventConstants.SUCCESS, component = "deliveryForm")
    public void onSuccess()
    {
        try {
            List<String> thumbprints = new ArrayList<String>(certificates.getCertificateGrid().getSelected());

            String sender = getCASettings().getCAEmail();

            String phoneNumber = null;

            SMSPropertiesBean smsProperties = getSMSProperties(email);

            if (smsProperties != null) {
                phoneNumber = smsProperties.getSMSPhoneNumber().getValue();
            }

            SendCertificatesDTO sendParameters = new SendCertificatesDTO(thumbprints, password, sender, email);

            sendParameters.setPhoneNumber(phoneNumber);
            sendParameters.setSendSMS(false);
            sendParameters.setAllowCA(false);
            sendParameters.setAllowEmailMismatch(allowEmailMismatch);

            caWS.sendCertificates(sendParameters);

            setPFXPassword(getUserBean(email), password);

            sent = true;
        }
        catch(Exception e)
        {
            logger.error("Error sending certificates.", e);

            errorMessage = e.getMessage();

            error = true;
        }
    }

    private void setPFXPassword(UserBean userBean, String password)
    throws WebServiceCheckedException, HierarchicalPropertiesException
    {
      throw new WebServiceCheckedException("PFX Password is not supported.");

        /*
         * If the user does not want to store the password we want to reset the password
         * for the user otherwise it's unlcear what the last password was.
         */
        /*if (!storePFXPassword) {
            password = null;
        }


        // We only need to add a user if storePFXPassword is set

        if (userBean == null && storePFXPassword)
        {
            String email = EmailAddressUtils.canonicalizeAndValidate(this.email, true);

            if (email == null) {
                return;
            }

            userManager.addUser(email);

            User user = userManager.getUser(email, false);

            if (user == null) {
                return;
            }

            userBean = new UserBeanImpl(user);
        }


        // Only set the password if there is a user

        if (userBean != null)
        {
            StringProperty pfxPassword = userBean.getPreferences().getProperties().getPFXPassword();

            pfxPassword.setValue(password);

            // We must set inherit false otherwise the value will not be used.

            pfxPassword.setInherit(false);

            userBean.save();
        }*/
    }

    /*
     * Event handler called when the cancel button is pressed.
     */
    protected Object onCancel()
    {
        return CAIssue.class;
    }

    public int getPasswordLength()
    throws HierarchicalPropertiesException, WebServiceCheckedException
    {
        return getCASettings().getPasswordLength();
    }

    public int getRowsPerPage() {
        return rowsPerPage;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isSent() {
        return sent;
    }

    public boolean isError() {
        return error;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public boolean isAllowEmailMismatch() {
        return allowEmailMismatch;
    }

    public void setAllowEmailMismatch(boolean allowEmailMismatch) {
        this.allowEmailMismatch = allowEmailMismatch;
    }
}
