/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 *
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar,
 * wsdl4j-1.6.1.jar (or modified versions of these libraries),
 * containing parts covered by the terms of Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to
 * convey the resulting work.
 */
package mitm.djigzo.web.pages.admin;

import java.util.LinkedList;
import java.util.List;

import mitm.application.djigzo.admin.FactoryRoles;
import mitm.common.ws.WebServiceCheckedException;
import mitm.djigzo.web.common.InjectSelectionModel;
import mitm.djigzo.web.common.security.Admin;
import mitm.djigzo.web.common.security.AdminManager;
import mitm.djigzo.web.common.security.Roles;
import mitm.djigzo.web.pages.Admins;

import org.apache.tapestry5.Asset;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.IncludeJavaScriptLibrary;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.Path;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.beaneditor.Validate;
import org.apache.tapestry5.corelib.components.Form;
import org.apache.tapestry5.corelib.components.Palette;
import org.apache.tapestry5.corelib.components.PasswordField;
import org.apache.tapestry5.corelib.components.TextField;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.springframework.security.annotation.Secured;

@IncludeJavaScriptLibrary("classpath:mitm/djigzo/web/validators/word.js")
@IncludeStylesheet("context:styles/pages/admin/adminAdd.css")
@Secured({FactoryRoles.ROLE_ADMIN})
public class AdminAdd
{
	@Inject
	private AdminManager adminManager;

	@Inject
	private Roles roles;

	@InjectSelectionModel
    private List<String> availableRoles;

    private List<String> selectedRoles;

	@Component
    private Form form;

	@SuppressWarnings("unused")
	@Component(id = "roles", parameters = {"encoder=availableRolesValueEncoder", "selected=selectedRoles",
		"model=availableRolesSelectionModel", "deselect=removeIcon", "select=addIcon"})
	private Palette rolesPalette;

	@Component(id = "username", parameters = {"value = username"})
	private TextField usernameField;

	@Component(id = "password", parameters = {"value = password"})
	private PasswordField passwordField;

	@SuppressWarnings("unused")
	@Component(id = "passwordRepeat", parameters = {"value = passwordRepeat"})
	private PasswordField passwordRepeatField;

	@Inject
    @Path("context:icons/add-list.png")
    private Asset addIcon;

	@Inject
    @Path("context:icons/remove-list.png")
    private Asset removeIcon;

	@Validate("required,maxlength=32,word")
	private String username;

	@Validate("required,minlength=6,maxlength=32")
	private String password;

	@Validate("required,minlength=6,maxlength=32")
	private String passwordRepeat;

	@SetupRender
	@Secured({FactoryRoles.ROLE_ADMIN})
	protected void setupRender() {
		/*
		 * Empty on purpose
		 */
	}

	public Asset getRemoveIcon() {
		return removeIcon;
	}

	public void setRemoveIcon(Asset removeIcon) {
		this.removeIcon = removeIcon;
	}

	public Asset getAddIcon() {
		return addIcon;
	}

	public void setAddIcon(Asset addIcon) {
		this.addIcon = addIcon;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPasswordRepeat() {
		return passwordRepeat;
	}

	public void setPasswordRepeat(String passwordRepeat) {
		this.passwordRepeat = passwordRepeat;
	}

	public List<String> getAvailableRoles() {
		return availableRoles;
	}

	public void setAvailableRoles(List<String> availableRoles) {
		this.availableRoles = availableRoles;
	}

	public List<String> getSelectedRoles() {
		return selectedRoles;
	}

	public void setSelectedRoles(List<String> selectedRoles) {
		this.selectedRoles = selectedRoles;
	}

	/*
	 * GenericValueEncoder requires the roles to be set in onPrepare and not in @SetupRender.
	 * Need to find out why.
	 */
	public void onPrepare()
	throws WebServiceCheckedException
	{
		availableRoles = roles.getAvailableRoles();
		// provision for issue SEN-25
		availableRoles.remove(FactoryRoles.ROLE_DOMAIN_MANAGER);
		availableRoles.remove(FactoryRoles.ROLE_MOBILE_MANAGER);
		availableRoles.remove(FactoryRoles.ROLE_PKI_MANAGER);
		availableRoles.remove(FactoryRoles.ROLE_SMS_MANAGER);
		availableRoles.remove(FactoryRoles.ROLE_USER_MANAGER);
		//

		if (selectedRoles == null)
		{
			selectedRoles = new LinkedList<String>();

			selectedRoles.add(FactoryRoles.ROLE_LOGIN);
		}
	}

	public void onValidateForm()
	throws WebServiceCheckedException
	{
		/*
		 * Check if user does not exist
		 */
		if (adminManager.getAdmin(username) != null)
		{
	        form.recordError(usernameField, "Admin already exists.");

	        return;
		}

		/*
		 * Check if passwords are the same
		 */
		if (password != null && !password.equals(passwordRepeat))
		{
	        form.recordError(passwordField, "Passwords do not match.");

	        return;
		}

		/*
		 * Roles should always contain ROLE_USER
		 */
		if (!selectedRoles.contains(FactoryRoles.ROLE_LOGIN))
		{
	        form.recordError("Every admin should have at least the " +
	        		FactoryRoles.ROLE_LOGIN + " role");

	        return;
		}
	}

	public Object onSuccess()
	throws WebServiceCheckedException
	{
		Admin admin = adminManager.addAdmin(username, false /* not built in */);

		admin.setPassword(password);
		admin.setRoles(selectedRoles);
		admin.setEnabled(true);

		return Admins.class;
	}

    /*
     * Event handler called when the cancel button is pressed.
     */
    public Object onCancel() {
		return Admins.class;
	}
}
