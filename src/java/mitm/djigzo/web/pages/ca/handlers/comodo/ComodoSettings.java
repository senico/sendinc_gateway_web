/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.pages.ca.handlers.comodo;

import mitm.application.djigzo.admin.FactoryRoles;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.ws.WebServiceCheckedException;
import mitm.djigzo.web.beans.ComodoSettingsBean;
import mitm.djigzo.web.beans.impl.ComodoSettingsBeanImpl;
import mitm.djigzo.web.components.NonClearingPassword;
import mitm.djigzo.web.entities.GlobalPreferencesManager;
import mitm.djigzo.web.pages.ca.CAHandlers;

import org.apache.cxf.common.util.StringUtils;
import org.apache.tapestry5.PersistenceConstants;
import org.apache.tapestry5.annotations.Cached;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.corelib.components.Checkbox;
import org.apache.tapestry5.corelib.components.TextField;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.springframework.security.annotation.Secured;

@Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_PKI_MANAGER})
@IncludeStylesheet("context:styles/pages/ca/handlers/comodo/settings.css")
public class ComodoSettings
{
    @Inject
    private GlobalPreferencesManager globalPreferencesManager;

    @SuppressWarnings("unused")
    @Component(id = "loginName", parameters = {"value = comodoSettings.loginName", "validate = required, maxlength=64"})
    private TextField commonNameField;
    
    @SuppressWarnings("unused")
    @Component(id = "loginPassword", parameters = {"value = loginPassword", "validate = required, maxlength=128"})
    private NonClearingPassword loginPasswordField;

    @SuppressWarnings("unused")
    @Component(id = "ap", parameters = {"value = comodoSettings.ap", 
            "validate = required, maxlength=64"})
    private TextField apField;

    @SuppressWarnings("unused")
    @Component(id = "cACertificateID", parameters = {"value = comodoSettings.cACertificateID"})
    private TextField cACertificateIDField;
    
    @SuppressWarnings("unused")
    @Component(id = "autoAuthorize", parameters = {"value = comodoSettings.autoAuthorize"})
    private Checkbox autoAuthorizeCheckbox;
    
    /*
     * True if changes were applied
     */
    @Persist(PersistenceConstants.FLASH)
    private boolean applied;
    
    @SetupRender
    @Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_PKI_MANAGER})
    public void setupRender() {
        /*
         * Empty on purpose
         */
    }

    @Cached
    public ComodoSettingsBean getComodoSettings() 
    throws HierarchicalPropertiesException, WebServiceCheckedException
    {
        return new ComodoSettingsBeanImpl(globalPreferencesManager.getProperties());
    }
   
    public void setLoginPassword(String password)
    throws HierarchicalPropertiesException, WebServiceCheckedException
    {
        if (!NonClearingPassword.DUMMY_PASSWORD.equals(password))
        {
            /*
             * The password has been changed
             */
            getComodoSettings().setLoginPassword(password);
        }
    }
    
    public String getLoginPassword()
    throws HierarchicalPropertiesException, WebServiceCheckedException
    {
        /*
         * return the dummy password if the password has been set
         */
        return StringUtils.isEmpty(getComodoSettings().getLoginPassword()) ? "" : NonClearingPassword.DUMMY_PASSWORD;
    }
    
    public void onSuccess()
    throws WebServiceCheckedException, HierarchicalPropertiesException
    {
        getComodoSettings().save();
        
        applied = true;
    }
 
    /*
     * Event handler called when the cancel button is pressed.
     */
    protected Object onCancel() {
        return CAHandlers.class;
    }

    public boolean isApplied() {
        return applied;
    }           
}
