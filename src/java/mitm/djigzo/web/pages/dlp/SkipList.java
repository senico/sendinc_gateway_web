/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.pages.dlp;

import mitm.application.djigzo.admin.FactoryRoles;
import mitm.application.djigzo.ws.UpdateableWordSkipperWS;
import mitm.common.ws.WebServiceCheckedException;
import mitm.djigzo.web.pages.dlp.patterns.PatternsView;

import org.apache.tapestry5.PersistenceConstants;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.corelib.components.TextArea;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.springframework.security.annotation.Secured;

@Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_DLP_MANAGER})
@IncludeStylesheet("context:styles/pages/dlp/skipList.css")
public class SkipList
{
    @Inject
    private UpdateableWordSkipperWS wordSkipper;

    /*
     * The current skip list content
     */
    private String skipList;    
    
    /*
     * True if changes were applied
     */
    @Persist(PersistenceConstants.FLASH)
    private boolean applied;
    
    /* 
     * We will set an upper limit on the max list size. Jetty for example by default limits the max. form size
     * to 200000. This can be changed by setting the system property org.mortbay.http.HttpRequest.maxFormContentSize. 
     */
    @SuppressWarnings("unused")
    @Component(id = "skipListField", parameters = {"value = prop:skipList", "validate = maxLength=1048576"})
    private TextArea skipListField;
    
    @SetupRender
    @Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_DLP_MANAGER})
    public void setupRender()
    throws WebServiceCheckedException
    {
        skipList = wordSkipper.getSkipList();
    }

    public void onSuccess()
    throws WebServiceCheckedException
    {
        wordSkipper.setSkipList(skipList);
        
        applied = true;
    }
    
    /*
     * Event handler called when the cancel button is pressed.
     */
    public Object onCancel() {
        return PatternsView.class;
    }
    
    public String getSkipList() {
        return skipList;
    }

    public void setSkipList(String skipList) {
        this.skipList = skipList;
    }

    public boolean isApplied() {
        return applied;
    }
}
