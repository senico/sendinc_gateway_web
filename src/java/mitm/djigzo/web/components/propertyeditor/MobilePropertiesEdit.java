/*
 * Copyright (c) 2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.components.propertyeditor;

import mitm.djigzo.web.beans.MobilePropertiesBean;
import mitm.djigzo.web.beans.UserPropertiesBean;

import org.apache.commons.lang.StringUtils;
import org.apache.tapestry5.Block;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.IncludeJavaScriptLibrary;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.json.JSONObject;
import org.apache.tapestry5.services.Request;

@SuppressWarnings("unused")
@IncludeStylesheet("context:styles/components/propertyeditor/userPropertiesEdit.css")
public class MobilePropertiesEdit
{
	@Property
	@Parameter(required = true)
	private MobilePropertiesBean properties;
	
	/*
	 * Block which can be used to add info the relay part
	 */
    @Parameter
    private Block relayBlock;
	
	@Component(id = "blackberryRecipient", parameters = {
			"value = properties.blackberryRecipient.value", 
			"checked = properties.blackberryRecipient.inherit"}
	)
    private BooleanPropertyEdit blackberryRecipient;
	
    @Component(id = "stripUnsupportedFormats", parameters = {
            "value = properties.stripUnsupportedFormats.value", 
            "checked = properties.stripUnsupportedFormats.inherit"}
    )
    private BooleanPropertyEdit stripUnsupportedFormats;

    @Component(id = "relayAllowed", parameters = {
            "value = properties.relayAllowed.value", 
            "checked = properties.relayAllowed.inherit"}
    )
    private BooleanPropertyEdit relayAllowed;
    
    @Component(id = "relayValidityInterval", parameters = {
            "validate = min=0",
            "value = properties.relayValidityInterval.value", 
            "checked = properties.relayValidityInterval.inherit"}
    )
    private LongPropertyEdit relayValidityInterval;

    @Component(id = "relayBounceMode", parameters = {
            "value = properties.relayBounceMode.value", 
            "checked = properties.relayBounceMode.inherit"}
    )
    private RelayBounceModePropertyEdit relayBounceMode;

    public Block getRelayBlock() {
        return relayBlock;
    }
}
