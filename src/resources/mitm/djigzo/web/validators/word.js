Tapestry.Validator.word = function(field, message) 
{
    field.addValidator(function(value)
    {
        if (value == null || value.length == 0) {
            return;
        }

        if (!value.match(/^\w*$/)) 
        {
            throw message;
        }       
    });
};