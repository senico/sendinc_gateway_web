/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.components;

import java.util.LinkedList;
import java.util.List;

import org.apache.tapestry5.Block;
import org.apache.tapestry5.ComponentEventCallback;
import org.apache.tapestry5.ComponentResources;
import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.ioc.annotations.Inject;

public class SingleSelectCertificateGrid extends CertificateGrid
{
    public static final String SELECT_VALUE__EVENT = "singleSelectCertificateGrid-select-value";
	
	/*
	 * The thumbprint of the selected certificate
	 */
	@Persist
    private String selected;

    @Inject
    private ComponentResources resources;
	
    @Inject
    @Property
    private Block defaultNoCertificatesBlock;

    @Parameter
    private Block noCertificatesBlock;
    
    @Inject
    @Property
    private Block defaultLegendBlock;
    
    @Parameter(defaultPrefix="inherit")
    private Block legendBlock;
    
    public Block getNoCertificatesBlock() {
        return noCertificatesBlock != null ? noCertificatesBlock : defaultNoCertificatesBlock;
    }
        
    public Block getLegendBlock() {
        return legendBlock;
    }
    
    public Block defaultLegendBlock() {
        return defaultLegendBlock;
    }
    
    /**
     * Returns the thumbprint of the selected certificate. This is called while rendering the grid to
     * get the value for the radio button.
     */
	public String getSelect()
	{
		final List<String> resultHolder = new LinkedList<String>();
		
		ComponentEventCallback<String> callback = new ComponentEventCallback<String>() 
		{
		    @Override
			public boolean handleResult(String result) 
			{
				resultHolder.add(result);
				
				return true;
			}
		};
		
		resources.triggerEvent(SELECT_VALUE__EVENT, new Object[]{getCertificate()}, callback);
		
		String result = getCertificate().getThumbprint();
		
		if (resultHolder.size() > 0) {
			result = resultHolder.get(0);
		}
		
		return result;
	}
	
	
	/*
	 * The selected certificate (thumbprint) called by the radio group
	 */
	public String getSelected() {
		return selected;
	}

	/*
	 * The selected value will be set by the radio group
	 */
	public void setSelected(String value) {
		selected = value;
	}
}
