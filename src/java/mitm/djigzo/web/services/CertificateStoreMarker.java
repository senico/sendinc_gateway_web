package mitm.djigzo.web.services;

/**
 * Marker annotation used for disambiguating the certificate and root store service.
 * 
 * @author Martijn Brinkers
 *
 */
public @interface CertificateStoreMarker {

}
