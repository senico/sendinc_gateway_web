/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.validators;

import org.apache.tapestry5.Field;
import org.apache.tapestry5.MarkupWriter;
import org.apache.tapestry5.ValidationException;
import org.apache.tapestry5.ioc.MessageFormatter;
import org.apache.tapestry5.services.FormSupport;
import org.apache.tapestry5.validator.AbstractValidator;

/**
 * Tapestry validator for the validation of a word
 * 
 * @author Martijn Brinkers
 *
 */
public class WordValidator extends AbstractValidator<Void, String>
{
	public static final String NAME = "word";
	
    public WordValidator(String messageKey) 
    {
    	super(null, String.class, messageKey);
    }

    public WordValidator() 
    {
    	super(null, String.class, "validate-word");
    }
    
    @Override
    public void validate(Field field, Void constraintValue, MessageFormatter formatter, String value) 
    throws ValidationException
    {
		if (value != null)
		{
			if (!value.matches("\\w*")) {
				throw new ValidationException(formatter.format(field.getLabel()));
			}
		}
    }

    @Override
	public void render(Field field, Void constraintValue, MessageFormatter formatter, MarkupWriter writer,
            FormSupport formSupport)
	{
		formSupport.addValidation(field, NAME, formatter.format(field.getLabel()), null);
	}
}
