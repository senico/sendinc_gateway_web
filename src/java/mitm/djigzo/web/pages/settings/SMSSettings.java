/*
 * Copyright (c) 2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.pages.settings;

import mitm.application.djigzo.admin.FactoryRoles;
import mitm.common.mail.EmailAddressUtils;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.util.DomainUtils;
import mitm.common.util.DomainUtils.DomainType;
import mitm.common.ws.WebServiceCheckedException;
import mitm.djigzo.web.beans.SMSPropertiesBean;
import mitm.djigzo.web.beans.impl.SMSPropertiesBeanImpl;
import mitm.djigzo.web.common.PropertyType;
import mitm.djigzo.web.components.propertyeditor.SMSPropertiesEdit;
import mitm.djigzo.web.entities.Domain;
import mitm.djigzo.web.entities.DomainManager;
import mitm.djigzo.web.entities.GlobalPreferencesManager;
import mitm.djigzo.web.entities.SaveableUserProperties;
import mitm.djigzo.web.entities.User;
import mitm.djigzo.web.entities.UserManager;
import mitm.djigzo.web.pages.GlobalPreferences;
import mitm.djigzo.web.pages.Users;
import mitm.djigzo.web.pages.domain.DomainEdit;
import mitm.djigzo.web.pages.user.UserEdit;

import org.apache.tapestry5.ComponentResources;
import org.apache.tapestry5.PersistenceConstants;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.annotation.Secured;

@Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_SMS_MANAGER})
@IncludeStylesheet("context:styles/pages/settings/settings.css")
public class SMSSettings
{
    private final static Logger logger = LoggerFactory.getLogger(SMSSettings.class);
    
    @Inject
    private UserManager userManager;

    @Inject
    private DomainManager domainManager;
    
    @Inject
    private GlobalPreferencesManager globalPreferencesManager;

    @Inject
    private ComponentResources resources;
    
    @SuppressWarnings("unused")
    @Component(id = "properties", parameters = {"properties = smsProperties"})
    private SMSPropertiesEdit propertiesEdit;
    
    /*
     * True if changes were applied
     */
    @Persist(PersistenceConstants.FLASH)
    private boolean applied;

    /*
     * The bean to get/set the properties
     */
    private SMSPropertiesBean smsProperties;
    
    /*
     * Identifies the property we are editing (for user it should be email, domain a domain and global global)
     */
    private String id;
    
    /*
     * The property type being edited (user, domain etc.)
     */
    private PropertyType propertyType;

    @SetupRender
    @Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_SMS_MANAGER})
    protected void setupRender() {
        /*
         * Empty on purpose
         */
    }
    
    @Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_SMS_MANAGER})
    protected Object onActivate(Object[] context) 
    throws WebServiceCheckedException, HierarchicalPropertiesException 
    {
        if (context.length < 2) {
            return Users.class;
        }

        if (!(context[0] instanceof String)) {
            return Users.class;
        }

        this.id = (String) context[0];

        if (!(context[1] instanceof String)) {
            return Users.class;
        }
        
        this.propertyType = PropertyType.fromString((String) context[1]);

        if (this.propertyType == null) {
            return Users.class;
        }
        
        SaveableUserProperties properties = getProperties();
        
        if (properties == null) {
            return Users.class;
        }
        
        smsProperties = new SMSPropertiesBeanImpl(properties);
        
        return null;
    }

    private SaveableUserProperties getUserProperties()
    throws WebServiceCheckedException
    {
        SaveableUserProperties properties = null;
        
        String email = EmailAddressUtils.canonicalizeAndValidate(id, true);

        if (email != null)
        {
            User user = userManager.getUser(email, false /* null if not exist */);
            
            if (user != null) {
                properties = user.getPreferences().getProperties();
            }
        }
        else {
            logger.warn("email " + id + " is not a valid email address");
        }
        
        return properties;
    }

    private SaveableUserProperties getDomainProperties()
    throws WebServiceCheckedException
    {
        SaveableUserProperties properties = null;
        
        String domainName = DomainUtils.canonicalizeAndValidate(id, DomainType.WILD_CARD);
        
        if (domainName != null)
        {
            Domain domain = domainManager.getDomain(domainName);
            
            if (domain != null) {
                properties = domain.getPreferences().getProperties();
            }
        }
        else {
            logger.warn("domain " + id + " is not a valid domain");
        }
        
        return properties;
    }

    private SaveableUserProperties getGlobalProperties()
    throws WebServiceCheckedException
    {
        return globalPreferencesManager.getPreferences().getProperties();
    }
    
    private SaveableUserProperties getProperties() 
    throws WebServiceCheckedException
    {
        switch(propertyType)
        {
        case USER   : return getUserProperties(); 
        case DOMAIN : return getDomainProperties();
        case GLOBAL : return getGlobalProperties();
        }
        
        throw new IllegalArgumentException("Unknown PropertyType " + propertyType);
    }
    
    public void onSuccess()
    throws HierarchicalPropertiesException 
    {
        assert(smsProperties != null);

        smsProperties.save();
        
        applied = true;
    }

    /*
     * Event handler called when the cancel button is pressed.
     */
    public Object onCancel()
    {
        switch(propertyType)
        {
        case USER   : return resources.createPageLink(UserEdit.class, false, id); 
        case DOMAIN : return resources.createPageLink(DomainEdit.class, false, id); 
        case GLOBAL : return GlobalPreferences.class; 
        }
        
        return null; 
    }
    
    public SMSPropertiesBean getSMSProperties() {
        return smsProperties;
    }
    
    public String getId() {
        return id;
    }

    public PropertyType getPropertyType() {
        return propertyType;
    }
    
    protected Object[] onPassivate() {
        return new Object[]{id, propertyType};
    }
    
    public boolean isUserPropertyType() {
        return propertyType == PropertyType.USER;
    }

    public boolean isDomainPropertyType() {
        return propertyType == PropertyType.DOMAIN;
    }

    public boolean isGlobalPropertyType() {
        return propertyType == PropertyType.GLOBAL;
    }

    public boolean isApplied() {
        return applied;
    }    
}
