/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.beans.impl;

import java.util.LinkedList;
import java.util.List;

import mitm.common.util.Check;
import mitm.djigzo.web.beans.AdminBean;
import mitm.djigzo.web.common.security.Admin;

import org.springframework.security.GrantedAuthority;

public class AdminBeanImpl implements AdminBean 
{
	private final Admin admin;
	
	public AdminBeanImpl(Admin admin)
	{
		Check.notNull(admin, "admin");
		
		this.admin = admin;
	}
	
	@Override
    public List<String> getRoles() 
	{
		List<String> roles = new LinkedList<String>();
		
		GrantedAuthority[] grantedAuthorities = admin.getAuthorities();
		
		for (GrantedAuthority grantedAuthority : grantedAuthorities) {
			roles.add(grantedAuthority.getAuthority());
		}
		
		return roles;
	}

    @Override
	public String getUsername() {
		return admin.getUsername();
	}

    @Override
	public boolean isEnabled() {
		return admin.isEnabled();
	}
	
    @Override
	public boolean isBuiltIn() {
		return admin.isBuiltIn();
	}
}
