/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.common;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.ObjectUtils;
import org.apache.tapestry5.ValueEncoder;
import org.apache.tapestry5.ioc.services.PropertyAccess;
import org.apache.tapestry5.ioc.services.PropertyAdapter;

/**
 * Based on http://wiki.apache.org/tapestry/Tapestry5AnotherSelectWithObjects 
 */
public class GenericValueEncoder<T> implements ValueEncoder<T> 
{
	private PropertyAdapter idFieldAdapter;
	
	private List<T> list;

	public GenericValueEncoder(List<T> list, String idField, PropertyAccess access) 
	{
		if(list == null) list = new ArrayList<T>();
		
		if (idField != null && !idField.equalsIgnoreCase("null"))
		{
			if(list.size() > 0)
			{
				this.idFieldAdapter = access.getAdapter(list.get(0).getClass()).
						getPropertyAdapter(idField);
			}
		}
		this.list = list;
	}

    @Override
	public String toClient(T obj) 
	{
		return idFieldAdapter == null ? ObjectUtils.toString(obj) : 
				ObjectUtils.toString(idFieldAdapter.get(obj)); 
	}

    @Override
	public T toValue(String string) 
	{
		if (idFieldAdapter == null) 
		{
			for (T obj : list) {
				if (ObjectUtils.toString(obj).equals(string)) return obj;
			}
		} 
		else {
			for (T obj : list) {
				if (ObjectUtils.toString(idFieldAdapter.get(obj)).equals(string)) return obj;
			}
		}
		
		return null;
	}
}