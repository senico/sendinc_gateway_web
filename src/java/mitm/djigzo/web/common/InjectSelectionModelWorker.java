/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.common;

import java.lang.reflect.Modifier;

import org.apache.tapestry5.ioc.internal.util.InternalUtils;
import org.apache.tapestry5.ioc.services.PropertyAccess;
import org.apache.tapestry5.ioc.util.BodyBuilder;
import org.apache.tapestry5.model.MutableComponentModel;
import org.apache.tapestry5.services.ClassTransformation;
import org.apache.tapestry5.services.ComponentClassTransformWorker;
import org.apache.tapestry5.services.TransformMethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * ComponentClassTransformWorker implementation that that injects two member variables into the class
 * when the @InjectSelectionModel annotation is found. This is used for the automatic creation of a
 * model and encoder to be used by a Tapestry select component.
 * 
 * @author Martijn Brinkers
 *
 */
public class InjectSelectionModelWorker implements ComponentClassTransformWorker
{
	final private Logger logger = LoggerFactory.getLogger(InjectSelectionModelWorker.class);

	final private PropertyAccess access;

	public InjectSelectionModelWorker(PropertyAccess propertyAccess) {
		access = propertyAccess;
	}

	/**
	 * Transforms the class by adding a getter named get*SelectionModel and get*ValueEncoder where * is based 
	 * on the member that is annotated with @InjectSelectionModel.
     */
    @Override
	public void transform(ClassTransformation transformation, MutableComponentModel componentModel) 
	{
	    /*
	     * Transform all members annotated with @InjectSelectionModel
	     */
		for (String fieldName : transformation.findFieldsWithAnnotation(InjectSelectionModel.class)) 
		{
			InjectSelectionModel annotation = transformation.getFieldAnnotation(fieldName, 
					InjectSelectionModel.class);

			if (logger.isDebugEnabled()){
				logger.debug("Creating selection model getter method for the field " + fieldName);
			}
			
			String accessActualName = transformation.addField(Modifier.PRIVATE,
					"org.apache.tapestry5.ioc.services.PropertyAccess", "_access");
			
			transformation.injectField(accessActualName, access);

			addGetSelectionModelMethod(transformation, fieldName, annotation.labelField(),
					accessActualName);

			if (logger.isDebugEnabled()){
				logger.debug("Creating value encoder getter method for the field " + fieldName);
			}

			addGetValueEncoderMethod(transformation, fieldName, annotation.idField(), accessActualName);
		}
	}

    /*
     * Add a getter named get*SelectionModel where * is based on the member that is
     * annotated with @InjectSelectionModel.
     */
	private void addGetSelectionModelMethod(ClassTransformation transformation, String fieldName, 
			String labelField, String accessName) 
	{

		String methodName = "get" + InternalUtils.capitalize(InternalUtils.stripMemberName(
				fieldName)) + "SelectionModel";

		String modelQualifiedName = (GenericSelectionModel.class).getName();
		
		TransformMethodSignature sig = new TransformMethodSignature(Modifier.PUBLIC, modelQualifiedName,
					methodName, null, null);

		BodyBuilder builder = new BodyBuilder();
		
		builder.begin();
		
		builder.addln("return new " + modelQualifiedName + "(" + fieldName + ", \"" + 
				labelField +"\", " + accessName + ");");
		
		builder.end();

		transformation.addMethod(sig, builder.toString());

	}

    /*
     * Add a getter named get*ValueEncoder where * is based on the member that is
     * annotated with @InjectSelectionModel.
     */
	private void addGetValueEncoderMethod(ClassTransformation transformation, String fieldName, 
			String idField, String accessName) 
	{
		String methodName = "get" + InternalUtils.capitalize(InternalUtils.stripMemberName(
				fieldName)) + "ValueEncoder";

		String encoderQualifiedName = (GenericValueEncoder.class).getName();
		
		TransformMethodSignature sig = new TransformMethodSignature(Modifier.PUBLIC, 
				encoderQualifiedName, methodName, null, null);

		BodyBuilder builder = new BodyBuilder();
		
		builder.begin();
		
		String line = "return new " + encoderQualifiedName + "(" + fieldName + ",\"" + idField + 
				"\", " + accessName + ");";
		
		builder.addln(line);
		builder.end();

		transformation.addMethod(sig, builder.toString());
	}
}
