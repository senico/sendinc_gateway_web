/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.services;

import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.ws.soap.SOAPFaultException;

import mitm.application.djigzo.ws.AdminManagerWS;
import mitm.application.djigzo.ws.AdminWS;
import mitm.application.djigzo.ws.AuthorityManagerWS;
import mitm.application.djigzo.ws.BackupWS;
import mitm.application.djigzo.ws.CACertStoreViewWS;
import mitm.application.djigzo.ws.CAWS;
import mitm.application.djigzo.ws.CTLWS;
import mitm.application.djigzo.ws.CertificateRequestStoreWS;
import mitm.application.djigzo.ws.CertificateValidatorWS;
import mitm.application.djigzo.ws.ClickatellWS;
import mitm.application.djigzo.ws.ComodoWS;
import mitm.application.djigzo.ws.DjigzoWSDefaults;
import mitm.application.djigzo.ws.DomainWS;
import mitm.application.djigzo.ws.DomainsWS;
import mitm.application.djigzo.ws.EventLoggerWS;
import mitm.application.djigzo.ws.FetchmailManagerWS;
import mitm.application.djigzo.ws.GlobalPreferencesManagerWS;
import mitm.application.djigzo.ws.HierarchicalPropertiesWS;
import mitm.application.djigzo.ws.JamesManagerWS;
import mitm.application.djigzo.ws.JamesRepositoryManagerWS;
import mitm.application.djigzo.ws.KeyAndCertStoreWS;
import mitm.application.djigzo.ws.KeyAndCertificateWorkflowWS;
import mitm.application.djigzo.ws.LoggerManagerWS;
import mitm.application.djigzo.ws.LoginWS;
import mitm.application.djigzo.ws.MailRepositoryWS;
import mitm.application.djigzo.ws.MatchFilterRegistryWS;
import mitm.application.djigzo.ws.OTPWS;
import mitm.application.djigzo.ws.PolicyPatternManagerWS;
import mitm.application.djigzo.ws.PolicyPatternNodeWS;
import mitm.application.djigzo.ws.PortalUserWS;
import mitm.application.djigzo.ws.PostfixConfigManagerWS;
import mitm.application.djigzo.ws.PostfixLogManagerWS;
import mitm.application.djigzo.ws.PostfixSpoolManagerWS;
import mitm.application.djigzo.ws.SMSGatewayWS;
import mitm.application.djigzo.ws.SystemManagerWS;
import mitm.application.djigzo.ws.UpdateableWordSkipperWS;
import mitm.application.djigzo.ws.UserPreferencesPolicyPatternManagerWS;
import mitm.application.djigzo.ws.UserPreferencesWS;
import mitm.application.djigzo.ws.UserPreferencesWorkflowWS;
import mitm.application.djigzo.ws.UserWS;
import mitm.application.djigzo.ws.UsersWS;
import mitm.application.djigzo.ws.X509CRLStoreWS;
import mitm.application.djigzo.ws.impl.factory.AdminManagerWSProxyFactory;
import mitm.application.djigzo.ws.impl.factory.AdminWSProxyFactory;
import mitm.application.djigzo.ws.impl.factory.AuthorityManagerWSProxyFactory;
import mitm.application.djigzo.ws.impl.factory.BackupWSProxyFactory;
import mitm.application.djigzo.ws.impl.factory.CACertStoreViewWSProxyFactory;
import mitm.application.djigzo.ws.impl.factory.CAWSProxyFactory;
import mitm.application.djigzo.ws.impl.factory.CTLWSProxyFactory;
import mitm.application.djigzo.ws.impl.factory.CertificateRequestStoreWSProxyFactory;
import mitm.application.djigzo.ws.impl.factory.CertificateValidatorWSProxyFactory;
import mitm.application.djigzo.ws.impl.factory.ClickatellWSProxyFactory;
import mitm.application.djigzo.ws.impl.factory.ComodoWSProxyFactory;
import mitm.application.djigzo.ws.impl.factory.DomainWSProxyFactory;
import mitm.application.djigzo.ws.impl.factory.DomainsWSProxyFactory;
import mitm.application.djigzo.ws.impl.factory.EventLoggerWSProxyFactory;
import mitm.application.djigzo.ws.impl.factory.FetchmailManagerWSProxyFactory;
import mitm.application.djigzo.ws.impl.factory.GlobalPreferencesManagerWSProxyFactory;
import mitm.application.djigzo.ws.impl.factory.HierarchicalPropertiesWSProxyFactory;
import mitm.application.djigzo.ws.impl.factory.JamesManagerWSProxyFactory;
import mitm.application.djigzo.ws.impl.factory.JamesRepositoryManagerWSProxyFactory;
import mitm.application.djigzo.ws.impl.factory.KeyAndCertStoreWSProxyFactory;
import mitm.application.djigzo.ws.impl.factory.KeyAndCertificateWorkflowWSProxyFactory;
import mitm.application.djigzo.ws.impl.factory.KeyAndRootCertStoreWSProxyFactory;
import mitm.application.djigzo.ws.impl.factory.LoggerManagerWSProxyFactory;
import mitm.application.djigzo.ws.impl.factory.LoginWSProxyFactory;
import mitm.application.djigzo.ws.impl.factory.MailRepositoryWSProxyFactory;
import mitm.application.djigzo.ws.impl.factory.MatchFilterRegistryWSProxyFactory;
import mitm.application.djigzo.ws.impl.factory.OTPWSProxyFactory;
import mitm.application.djigzo.ws.impl.factory.PolicyPatternManagerWSProxyFactory;
import mitm.application.djigzo.ws.impl.factory.PolicyPatternNodeWSProxyFactory;
import mitm.application.djigzo.ws.impl.factory.PortalUserWSProxyFactory;
import mitm.application.djigzo.ws.impl.factory.PostfixConfigManagerWSProxyFactory;
import mitm.application.djigzo.ws.impl.factory.PostfixLogManagerWSProxyFactory;
import mitm.application.djigzo.ws.impl.factory.PostfixSpoolManagerWSProxyFactory;
import mitm.application.djigzo.ws.impl.factory.SMSGatewayWSProxyFactory;
import mitm.application.djigzo.ws.impl.factory.SystemManagerWSProxyFactory;
import mitm.application.djigzo.ws.impl.factory.UpdateableWordSkipperWSProxyFactory;
import mitm.application.djigzo.ws.impl.factory.UserPreferencesPolicyPatternManagerWSProxyFactory;
import mitm.application.djigzo.ws.impl.factory.UserPreferencesWSProxyFactory;
import mitm.application.djigzo.ws.impl.factory.UserPreferencesWorkflowWSProxyFactory;
import mitm.application.djigzo.ws.impl.factory.UserWSProxyFactory;
import mitm.application.djigzo.ws.impl.factory.UsersWSProxyFactory;
import mitm.application.djigzo.ws.impl.factory.X509CRLStoreWSProxyFactory;
import mitm.common.security.crypto.RandomGenerator;
import mitm.common.ws.Credential;
import mitm.common.ws.WSProxyFactoryException;
import mitm.djigzo.web.asos.SoapCredential;
import mitm.djigzo.web.common.RedirectRequestExceptionHandler;
import mitm.djigzo.web.common.security.AdminManager;
import mitm.djigzo.web.common.security.AdminManagerImpl;
import mitm.djigzo.web.entities.DomainManager;
import mitm.djigzo.web.entities.GlobalPreferencesManager;
import mitm.djigzo.web.entities.UserManager;
import mitm.djigzo.web.entities.impl.DomainManagerImpl;
import mitm.djigzo.web.entities.impl.GlobalPreferencesManagerImpl;
import mitm.djigzo.web.entities.impl.UserManagerImpl;

import org.apache.tapestry5.internal.services.LinkFactory;
import org.apache.tapestry5.ioc.ScopeConstants;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.ioc.annotations.Marker;
import org.apache.tapestry5.ioc.annotations.Match;
import org.apache.tapestry5.ioc.annotations.Scope;
import org.apache.tapestry5.ioc.annotations.Value;
import org.apache.tapestry5.services.ApplicationStateManager;
import org.apache.tapestry5.services.RequestExceptionHandler;
import org.apache.tapestry5.services.Response;
import org.springframework.security.providers.dao.SaltSource;
import org.springframework.security.providers.encoding.PasswordEncoder;

/**
 * Tapestry module with webservice related services.
 * 
 * @author Martijn Brinkers
 *
 */
public class SoapModule 
{
    public static UsersWSProxyFactory buildUsersWSProxyFactory(
    		@Inject @Value("${djigzo.ws.server.host}") String host,
    		@Inject @Value("${djigzo.ws.server.port}") int port,
    		@Inject @Value("${djigzo.ws.server.protocol}") String protocol) 
    throws MalformedURLException
    {
    	URL wsdlURL = new URL(protocol, host, port, DjigzoWSDefaults.USERS_WSDL);
		
    	return new UsersWSProxyFactory(wsdlURL, DjigzoWSDefaults.NAMESPACE, 
    	        DjigzoWSDefaults.USERS_SERVICE_NAME);
    }

    public static UserWSProxyFactory buildUserWSProxyFactory( 
    		@Inject @Value("${djigzo.ws.server.host}") String host,
    		@Inject @Value("${djigzo.ws.server.port}") int port,
    		@Inject @Value("${djigzo.ws.server.protocol}") String protocol) 
    throws MalformedURLException
    {
    	URL wsdlURL = new URL(protocol, host, port, DjigzoWSDefaults.USER_WSDL);
		
    	return new UserWSProxyFactory(wsdlURL, DjigzoWSDefaults.NAMESPACE, 
    	        DjigzoWSDefaults.USER_SERVICE_NAME);
    }

    public static DomainsWSProxyFactory buildDomainsWSProxyFactory(
    		@Inject @Value("${djigzo.ws.server.host}") String host,
    		@Inject @Value("${djigzo.ws.server.port}") int port,
    		@Inject @Value("${djigzo.ws.server.protocol}") String protocol) 
    throws MalformedURLException
    {
    	URL wsdlURL = new URL(protocol, host, port, DjigzoWSDefaults.DOMAINS_WSDL);
		
    	return new DomainsWSProxyFactory(wsdlURL, DjigzoWSDefaults.NAMESPACE, 
    	        DjigzoWSDefaults.DOMAINS_SERVICE_NAME);
    }

    public static DomainWSProxyFactory buildDomainWSProxyFactory( 
    		@Inject @Value("${djigzo.ws.server.host}") String host,
    		@Inject @Value("${djigzo.ws.server.port}") int port,
    		@Inject @Value("${djigzo.ws.server.protocol}") String protocol) 
    throws MalformedURLException
    {
    	URL wsdlURL = new URL(protocol, host, port, DjigzoWSDefaults.DOMAIN_WSDL);
		
    	return new DomainWSProxyFactory(wsdlURL, DjigzoWSDefaults.NAMESPACE, 
    	        DjigzoWSDefaults.DOMAIN_SERVICE_NAME);
    }
    
    public static UserPreferencesWSProxyFactory buildUserPreferencesWSProxyFactory( 
    		@Inject @Value("${djigzo.ws.server.host}") String host,
    		@Inject @Value("${djigzo.ws.server.port}") int port,
    		@Inject @Value("${djigzo.ws.server.protocol}") String protocol) 
    throws MalformedURLException
    {
    	URL wsdlURL = new URL(protocol, host, port, DjigzoWSDefaults.USER_PREFERENCES_WSDL);
		
    	return new UserPreferencesWSProxyFactory(wsdlURL, DjigzoWSDefaults.NAMESPACE, 
    	        DjigzoWSDefaults.USER_PREFERENCES_SERVICE_NAME);
    }

    public static UserPreferencesWorkflowWSProxyFactory buildUserPreferencesWorkflowWSProxyFactory( 
            @Inject @Value("${djigzo.ws.server.host}") String host,
            @Inject @Value("${djigzo.ws.server.port}") int port,
            @Inject @Value("${djigzo.ws.server.protocol}") String protocol) 
    throws MalformedURLException
    {
        URL wsdlURL = new URL(protocol, host, port, DjigzoWSDefaults.USER_PREFERENCES_WORKFLOW_WSDL);
        
        return new UserPreferencesWorkflowWSProxyFactory(wsdlURL, DjigzoWSDefaults.NAMESPACE, 
                DjigzoWSDefaults.USER_PREFERENCES_WORKFLOW_SERVICE_NAME);
    }
    
    public static GlobalPreferencesManagerWSProxyFactory buildGlobalPreferencesManagerWSProxyFactory( 
    		@Inject @Value("${djigzo.ws.server.host}") String host,
    		@Inject @Value("${djigzo.ws.server.port}") int port,
    		@Inject @Value("${djigzo.ws.server.protocol}") String protocol) 
    throws MalformedURLException
    {
    	URL wsdlURL = new URL(protocol, host, port, DjigzoWSDefaults.GLOBAL_PREFERENCES_MANAGER_WSDL);
		
    	return new GlobalPreferencesManagerWSProxyFactory(wsdlURL, DjigzoWSDefaults.NAMESPACE, 
    	        DjigzoWSDefaults.GLOBAL_PREFERENCES_MANAGER_SERVICE_NAME);
    }
    
    public static HierarchicalPropertiesWSProxyFactory buildHierarchicalPropertiesWSProxyFactory( 
    		@Inject @Value("${djigzo.ws.server.host}") String host,
    		@Inject @Value("${djigzo.ws.server.port}") int port,
    		@Inject @Value("${djigzo.ws.server.protocol}") String protocol) 
    throws MalformedURLException
    {
    	URL wsdlURL = new URL(protocol, host, port, DjigzoWSDefaults.HIERARCHICAL_PROPERTIES_WSDL);
		
    	return new HierarchicalPropertiesWSProxyFactory(wsdlURL, DjigzoWSDefaults.NAMESPACE, 
    	        DjigzoWSDefaults.HIERARCHICAL_PROPERTIES_SERVICE_NAME);
    }
    
    public static KeyAndRootCertStoreWSProxyFactory buildKeyAndRootCertStoreWSProxyFactory( 
    		@Inject @Value("${djigzo.ws.server.host}") String host,
    		@Inject @Value("${djigzo.ws.server.port}") int port,
    		@Inject @Value("${djigzo.ws.server.protocol}") String protocol) 
    throws MalformedURLException
    {
    	URL wsdlURL = new URL(protocol, host, port, DjigzoWSDefaults.KEY_AND_ROOT_CERTSTORE_WSDL);
		
    	return new KeyAndRootCertStoreWSProxyFactory(wsdlURL, DjigzoWSDefaults.NAMESPACE, 
    	        DjigzoWSDefaults.KEY_AND_ROOT_CERTSTORE_SERVICE_NAME);
    }

    public static KeyAndCertStoreWSProxyFactory buildKeyAndCertStoreWSProxyFactory( 
    		@Inject @Value("${djigzo.ws.server.host}") String host,
    		@Inject @Value("${djigzo.ws.server.port}") int port,
    		@Inject @Value("${djigzo.ws.server.protocol}") String protocol) 
    throws MalformedURLException
    {
    	URL wsdlURL = new URL(protocol, host, port, DjigzoWSDefaults.KEY_AND_CERTSTORE_WSDL);
		
    	return new KeyAndCertStoreWSProxyFactory(wsdlURL, DjigzoWSDefaults.NAMESPACE, 
    	        DjigzoWSDefaults.KEY_AND_CERTSTORE_SERVICE_NAME);
    }

    public static X509CRLStoreWSProxyFactory buildCRLStoreProxyFactory( 
    		@Inject @Value("${djigzo.ws.server.host}") String host,
    		@Inject @Value("${djigzo.ws.server.port}") int port,
    		@Inject @Value("${djigzo.ws.server.protocol}") String protocol) 
    throws MalformedURLException
    {
    	URL wsdlURL = new URL(protocol, host, port, DjigzoWSDefaults.CRL_STORE_WSDL);
		
    	return new X509CRLStoreWSProxyFactory(wsdlURL, DjigzoWSDefaults.NAMESPACE, 
    	        DjigzoWSDefaults.CRL_STORE_SERVICE_NAME);
    }
    
    public static CertificateValidatorWSProxyFactory buildCertificateValidatorWSProxyFactory( 
    		@Inject @Value("${djigzo.ws.server.host}") String host,
    		@Inject @Value("${djigzo.ws.server.port}") int port,
    		@Inject @Value("${djigzo.ws.server.protocol}") String protocol) 
    throws MalformedURLException
    {
    	URL wsdlURL = new URL(protocol, host, port, DjigzoWSDefaults.CERTIFICATE_VALIDATOR_WSDL);
		
    	return new CertificateValidatorWSProxyFactory(wsdlURL, DjigzoWSDefaults.NAMESPACE, 
    	        DjigzoWSDefaults.CERTIFICATE_VALIDATOR_SERVICE_NAME);
    }
    
    public static AdminManagerWSProxyFactory buildAdminManagerWSProxyFactory( 
    		@Inject @Value("${djigzo.ws.server.host}") String host,
    		@Inject @Value("${djigzo.ws.server.port}") int port,
    		@Inject @Value("${djigzo.ws.server.protocol}") String protocol) 
    throws MalformedURLException
    {
    	URL wsdlURL = new URL(protocol, host, port, DjigzoWSDefaults.ADMIN_MANAGER_WSDL);
		
    	return new AdminManagerWSProxyFactory(wsdlURL, DjigzoWSDefaults.NAMESPACE, 
    	        DjigzoWSDefaults.ADMIN_MANAGER_SERVICE_NAME);
    }

    public static AdminWSProxyFactory buildAdminWSProxyFactory( 
    		@Inject @Value("${djigzo.ws.server.host}") String host,
    		@Inject @Value("${djigzo.ws.server.port}") int port,
    		@Inject @Value("${djigzo.ws.server.protocol}") String protocol) 
    throws MalformedURLException
    {
    	URL wsdlURL = new URL(protocol, host, port, DjigzoWSDefaults.ADMIN_WSDL);
		
    	return new AdminWSProxyFactory(wsdlURL, DjigzoWSDefaults.NAMESPACE, 
    	        DjigzoWSDefaults.ADMIN_SERVICE_NAME);
    }

    public static AuthorityManagerWSProxyFactory buildAuthorityManagerWSProxyFactory( 
    		@Inject @Value("${djigzo.ws.server.host}") String host,
    		@Inject @Value("${djigzo.ws.server.port}") int port,
    		@Inject @Value("${djigzo.ws.server.protocol}") String protocol) 
    throws MalformedURLException
    {
    	URL wsdlURL = new URL(protocol, host, port, DjigzoWSDefaults.AUTHORITY_MANAGER_WSDL);
		
    	return new AuthorityManagerWSProxyFactory(wsdlURL, DjigzoWSDefaults.NAMESPACE, 
    	        DjigzoWSDefaults.AUTHORITY_MANAGER_SERVICE_NAME);
    }
    
    public static LoginWSProxyFactory buildLoginWSProxyFactory( 
    		@Inject @Value("${djigzo.ws.server.host}") String host,
    		@Inject @Value("${djigzo.ws.server.port}") int port,
    		@Inject @Value("${djigzo.ws.server.protocol}") String protocol) 
    throws MalformedURLException
    {
    	URL wsdlURL = new URL(protocol, host, port, DjigzoWSDefaults.LOGIN_WSDL);
		
    	return new LoginWSProxyFactory(wsdlURL, DjigzoWSDefaults.NAMESPACE, 
    	        DjigzoWSDefaults.LOGIN_SERVICE_NAME);
    }

    public static SMSGatewayWSProxyFactory buildSMSGatewayWSProxyFactory( 
    		@Inject @Value("${djigzo.ws.server.host}") String host,
    		@Inject @Value("${djigzo.ws.server.port}") int port,
    		@Inject @Value("${djigzo.ws.server.protocol}") String protocol) 
    throws MalformedURLException
    {
    	URL wsdlURL = new URL(protocol, host, port, DjigzoWSDefaults.SMS_GATEWAY_WSDL);
		
    	return new SMSGatewayWSProxyFactory(wsdlURL, DjigzoWSDefaults.NAMESPACE, 
    	        DjigzoWSDefaults.SMS_GATEWAY_SERVICE_NAME);
    }

    public static ClickatellWSProxyFactory buildClickatellWSProxyFactory( 
            @Inject @Value("${djigzo.ws.server.host}") String host,
            @Inject @Value("${djigzo.ws.server.port}") int port,
            @Inject @Value("${djigzo.ws.server.protocol}") String protocol) 
    throws MalformedURLException
    {
        URL wsdlURL = new URL(protocol, host, port, DjigzoWSDefaults.CLICKATELL_WSDL);
        
        return new ClickatellWSProxyFactory(wsdlURL, DjigzoWSDefaults.NAMESPACE, 
                DjigzoWSDefaults.CLICKATELL_SERVICE_NAME);
    }
    
    public static KeyAndCertificateWorkflowWSProxyFactory buildKeyAndCertificateWorkflowWSProxyFactory( 
    		@Inject @Value("${djigzo.ws.server.host}") String host,
    		@Inject @Value("${djigzo.ws.server.port}") int port,
    		@Inject @Value("${djigzo.ws.server.protocol}") String protocol) 
    throws MalformedURLException
    {
    	URL wsdlURL = new URL(protocol, host, port, DjigzoWSDefaults.KEY_AND_CERTIFICATE_WORKFLOW_WSDL);
		
    	return new KeyAndCertificateWorkflowWSProxyFactory(wsdlURL, DjigzoWSDefaults.NAMESPACE, 
    	        DjigzoWSDefaults.KEY_AND_CERTIFICATE_WORKFLOW_SERVICE_NAME);
    }

    public static JamesManagerWSProxyFactory buildJamesManagerWSProxyFactory( 
    		@Inject @Value("${djigzo.ws.server.host}") String host,
    		@Inject @Value("${djigzo.ws.server.port}") int port,
    		@Inject @Value("${djigzo.ws.server.protocol}") String protocol) 
    throws MalformedURLException
    {
    	URL wsdlURL = new URL(protocol, host, port, DjigzoWSDefaults.JAMES_MANAGER_WSDL);
		
    	return new JamesManagerWSProxyFactory(wsdlURL, DjigzoWSDefaults.NAMESPACE, 
    	        DjigzoWSDefaults.JAMES_MANAGER_SERVICE_NAME);
    }

    public static JamesRepositoryManagerWSProxyFactory buildJamesRepositoryManagerWSProxyFactory( 
            @Inject @Value("${djigzo.ws.server.host}") String host,
            @Inject @Value("${djigzo.ws.server.port}") int port,
            @Inject @Value("${djigzo.ws.server.protocol}") String protocol) 
    throws MalformedURLException
    {
        URL wsdlURL = new URL(protocol, host, port, DjigzoWSDefaults.JAMES_REPOSITORY_MANAGER_WSDL);

        return new JamesRepositoryManagerWSProxyFactory(wsdlURL, DjigzoWSDefaults.NAMESPACE, 
                DjigzoWSDefaults.JAMES_REPOSITORY_MANAGER_SERVICE_NAME);
    }
    
    public static PostfixSpoolManagerWSProxyFactory buildPostfixSpoolManagerWSProxyFactory( 
    		@Inject @Value("${djigzo.ws.server.host}") String host,
    		@Inject @Value("${djigzo.ws.server.port}") int port,
    		@Inject @Value("${djigzo.ws.server.protocol}") String protocol) 
    throws MalformedURLException
    {
    	URL wsdlURL = new URL(protocol, host, port, DjigzoWSDefaults.POSTFIX_SPOOL_MANAGER_WSDL);
		
    	return new PostfixSpoolManagerWSProxyFactory(wsdlURL, DjigzoWSDefaults.NAMESPACE, 
    	        DjigzoWSDefaults.POSTFIX_SPOOL_MANAGER_SERVICE_NAME);
    }

    public static PostfixLogManagerWSProxyFactory buildPostfixLogManagerWSProxyFactory( 
            @Inject @Value("${djigzo.ws.server.host}") String host,
            @Inject @Value("${djigzo.ws.server.port}") int port,
            @Inject @Value("${djigzo.ws.server.protocol}") String protocol) 
    throws MalformedURLException
    {
        URL wsdlURL = new URL(protocol, host, port, DjigzoWSDefaults.POSTFIX_LOG_MANAGER_WSDL);

        return new PostfixLogManagerWSProxyFactory(wsdlURL, DjigzoWSDefaults.NAMESPACE, 
                DjigzoWSDefaults.POSTFIX_LOG_MANAGER_SERVICE_NAME);
    }

    public static PostfixConfigManagerWSProxyFactory buildPostfixConfigManagerWSProxyFactory( 
            @Inject @Value("${djigzo.ws.server.host}") String host,
            @Inject @Value("${djigzo.ws.server.port}") int port,
            @Inject @Value("${djigzo.ws.server.protocol}") String protocol) 
    throws MalformedURLException
    {
        URL wsdlURL = new URL(protocol, host, port, DjigzoWSDefaults.POSTFIX_CONFIG_MANAGER_WSDL);

        return new PostfixConfigManagerWSProxyFactory(wsdlURL, DjigzoWSDefaults.NAMESPACE, 
                DjigzoWSDefaults.POSTFIX_CONFIG_MANAGER_SERVICE_NAME);
    }
    
    public static LoggerManagerWSProxyFactory buildLoggerManagerWSProxyFactory( 
            @Inject @Value("${djigzo.ws.server.host}") String host,
            @Inject @Value("${djigzo.ws.server.port}") int port,
            @Inject @Value("${djigzo.ws.server.protocol}") String protocol) 
    throws MalformedURLException
    {
        URL wsdlURL = new URL(protocol, host, port, DjigzoWSDefaults.LOGGER_MANAGER_WSDL);

        return new LoggerManagerWSProxyFactory(wsdlURL, DjigzoWSDefaults.NAMESPACE, 
                DjigzoWSDefaults.LOGGER_MANAGER_SERVICE_NAME);
    }

    public static SystemManagerWSProxyFactory buildSystemManagerWSProxyFactory( 
            @Inject @Value("${djigzo.ws.server.host}") String host,
            @Inject @Value("${djigzo.ws.server.port}") int port,
            @Inject @Value("${djigzo.ws.server.protocol}") String protocol) 
    throws MalformedURLException
    {
        URL wsdlURL = new URL(protocol, host, port, DjigzoWSDefaults.SYSTEM_MANAGER_WSDL);

        return new SystemManagerWSProxyFactory(wsdlURL, DjigzoWSDefaults.NAMESPACE, 
                DjigzoWSDefaults.SYSTEM_MANAGER_SERVICE_NAME);
    }

    public static BackupWSProxyFactory buildBackupWSProxyFactory( 
            @Inject @Value("${djigzo.ws.server.host}") String host,
            @Inject @Value("${djigzo.ws.server.port}") int port,
            @Inject @Value("${djigzo.ws.server.protocol}") String protocol) 
    throws MalformedURLException
    {
        URL wsdlURL = new URL(protocol, host, port, DjigzoWSDefaults.BACKUP_WSDL);

        return new BackupWSProxyFactory(wsdlURL, DjigzoWSDefaults.NAMESPACE, 
                DjigzoWSDefaults.BACKUP_SERVICE_NAME);
    }

    public static EventLoggerWSProxyFactory buildEventLoggerWSProxyFactory( 
            @Inject @Value("${djigzo.ws.server.host}") String host,
            @Inject @Value("${djigzo.ws.server.port}") int port,
            @Inject @Value("${djigzo.ws.server.protocol}") String protocol) 
    throws MalformedURLException
    {
        URL wsdlURL = new URL(protocol, host, port, DjigzoWSDefaults.EVENT_LOGGER_WSDL);

        return new EventLoggerWSProxyFactory(wsdlURL, DjigzoWSDefaults.NAMESPACE, 
                DjigzoWSDefaults.EVENT_LOGGER_SERVICE_NAME);
    }

    public static CAWSProxyFactory buildCAWSProxyFactory( 
            @Inject @Value("${djigzo.ws.server.host}") String host,
            @Inject @Value("${djigzo.ws.server.port}") int port,
            @Inject @Value("${djigzo.ws.server.protocol}") String protocol) 
    throws MalformedURLException
    {
        URL wsdlURL = new URL(protocol, host, port, DjigzoWSDefaults.CA_WSDL);

        return new CAWSProxyFactory(wsdlURL, DjigzoWSDefaults.NAMESPACE, 
                DjigzoWSDefaults.CA_SERVICE_NAME);
    }

    public static CACertStoreViewWSProxyFactory buildX509CACertStoreProxyFactory( 
            @Inject @Value("${djigzo.ws.server.host}") String host,
            @Inject @Value("${djigzo.ws.server.port}") int port,
            @Inject @Value("${djigzo.ws.server.protocol}") String protocol) 
    throws MalformedURLException
    {
        URL wsdlURL = new URL(protocol, host, port, DjigzoWSDefaults.CA_CERT_STORE_VIEW_WSDL);

        return new CACertStoreViewWSProxyFactory(wsdlURL, DjigzoWSDefaults.NAMESPACE, 
                DjigzoWSDefaults.CA_CERT_STORE_VIEW_SERVICE_NAME);
    }

    public static CertificateRequestStoreWSProxyFactory buildCertificateRequestStoreProxyFactory( 
            @Inject @Value("${djigzo.ws.server.host}") String host,
            @Inject @Value("${djigzo.ws.server.port}") int port,
            @Inject @Value("${djigzo.ws.server.protocol}") String protocol) 
    throws MalformedURLException
    {
        URL wsdlURL = new URL(protocol, host, port, DjigzoWSDefaults.CERTIFICATE_REQUEST_STORE_WSDL);

        return new CertificateRequestStoreWSProxyFactory(wsdlURL, DjigzoWSDefaults.NAMESPACE, 
                DjigzoWSDefaults.CERTIFICATE_REQUEST_STORE_SERVICE_NAME);
    }    
    
    public static CTLWSProxyFactory buildCTLProxyFactory( 
            @Inject @Value("${djigzo.ws.server.host}") String host,
            @Inject @Value("${djigzo.ws.server.port}") int port,
            @Inject @Value("${djigzo.ws.server.protocol}") String protocol) 
    throws MalformedURLException
    {
        URL wsdlURL = new URL(protocol, host, port, DjigzoWSDefaults.CTL_WSDL);

        return new CTLWSProxyFactory(wsdlURL, DjigzoWSDefaults.NAMESPACE, 
                DjigzoWSDefaults.CTL_SERVICE_NAME);
    }

    public static FetchmailManagerWSProxyFactory buildFetchmailManagerWSProxyFactory( 
            @Inject @Value("${djigzo.ws.server.host}") String host,
            @Inject @Value("${djigzo.ws.server.port}") int port,
            @Inject @Value("${djigzo.ws.server.protocol}") String protocol) 
    throws MalformedURLException
    {
        URL wsdlURL = new URL(protocol, host, port, DjigzoWSDefaults.FETCHMAIL_MANAGER_WSDL);

        return new FetchmailManagerWSProxyFactory(wsdlURL, DjigzoWSDefaults.NAMESPACE, 
                DjigzoWSDefaults.FETCHMAIL_MANAGER_SERVICE_NAME);
    }

    public static PolicyPatternManagerWSProxyFactory buildPolicyPatternManagerWSProxyFactory( 
            @Inject @Value("${djigzo.ws.server.host}") String host,
            @Inject @Value("${djigzo.ws.server.port}") int port,
            @Inject @Value("${djigzo.ws.server.protocol}") String protocol) 
    throws MalformedURLException
    {
        URL wsdlURL = new URL(protocol, host, port, DjigzoWSDefaults.POLICY_PATTERN_MANAGER_WSDL);

        return new PolicyPatternManagerWSProxyFactory(wsdlURL, DjigzoWSDefaults.NAMESPACE, 
                DjigzoWSDefaults.POLICY_PATTERN_MANAGER_SERVICE_NAME);
    }
    
    public static PolicyPatternNodeWSProxyFactory buildPolicyPatternNodeWSProxyFactory( 
            @Inject @Value("${djigzo.ws.server.host}") String host,
            @Inject @Value("${djigzo.ws.server.port}") int port,
            @Inject @Value("${djigzo.ws.server.protocol}") String protocol) 
    throws MalformedURLException
    {
        URL wsdlURL = new URL(protocol, host, port, DjigzoWSDefaults.POLICY_PATTERN_NODE_WSDL);

        return new PolicyPatternNodeWSProxyFactory(wsdlURL, DjigzoWSDefaults.NAMESPACE, 
                DjigzoWSDefaults.POLICY_PATTERN_NODE_SERVICE_NAME);
    }

    public static MatchFilterRegistryWSProxyFactory buildMatchFilterRegistryWSProxyFactory( 
            @Inject @Value("${djigzo.ws.server.host}") String host,
            @Inject @Value("${djigzo.ws.server.port}") int port,
            @Inject @Value("${djigzo.ws.server.protocol}") String protocol) 
    throws MalformedURLException
    {
        URL wsdlURL = new URL(protocol, host, port, DjigzoWSDefaults.MATCH_FILTER_REGISTRY_WSDL);

        return new MatchFilterRegistryWSProxyFactory(wsdlURL, DjigzoWSDefaults.NAMESPACE, 
                DjigzoWSDefaults.MATCH_FILTER_REGISTRY_SERVICE_NAME);
    }
    
    public static UserPreferencesPolicyPatternManagerWSProxyFactory buildUserPreferencesPolicyPatternManagerWSProxyFactory( 
            @Inject @Value("${djigzo.ws.server.host}") String host,
            @Inject @Value("${djigzo.ws.server.port}") int port,
            @Inject @Value("${djigzo.ws.server.protocol}") String protocol) 
    throws MalformedURLException
    {
        URL wsdlURL = new URL(protocol, host, port, DjigzoWSDefaults.USER_PREFERENCES_POLICY_PATTERN_MANAGER_WSDL);

        return new UserPreferencesPolicyPatternManagerWSProxyFactory(wsdlURL, DjigzoWSDefaults.NAMESPACE, 
                DjigzoWSDefaults.USER_PREFERENCES_POLICY_PATTERN_MANAGER_SERVICE_NAME);
    }
    
    public static ComodoWSProxyFactory buildComodoWSProxyFactory( 
            @Inject @Value("${djigzo.ws.server.host}") String host,
            @Inject @Value("${djigzo.ws.server.port}") int port,
            @Inject @Value("${djigzo.ws.server.protocol}") String protocol) 
    throws MalformedURLException
    {
        URL wsdlURL = new URL(protocol, host, port, DjigzoWSDefaults.COMODO_WSDL);

        return new ComodoWSProxyFactory(wsdlURL, DjigzoWSDefaults.NAMESPACE, 
                DjigzoWSDefaults.COMODO_SERVICE_NAME);
    }

    public static UpdateableWordSkipperWSProxyFactory buildUpdateableWordSkipperWSProxyFactory( 
            @Inject @Value("${djigzo.ws.server.host}") String host,
            @Inject @Value("${djigzo.ws.server.port}") int port,
            @Inject @Value("${djigzo.ws.server.protocol}") String protocol) 
    throws MalformedURLException
    {
        URL wsdlURL = new URL(protocol, host, port, DjigzoWSDefaults.WORD_SKIPPER_WSDL);

        return new UpdateableWordSkipperWSProxyFactory(wsdlURL, DjigzoWSDefaults.NAMESPACE, 
                DjigzoWSDefaults.WORD_SKIPPER_SERVICE_NAME);
    }

    public static MailRepositoryWSProxyFactory buildQuarantineMailRepositoryWSProxyFactory( 
        @Inject @Value("${djigzo.ws.server.host}") String host,
        @Inject @Value("${djigzo.ws.server.port}") int port,
        @Inject @Value("${djigzo.ws.server.protocol}") String protocol) 
    throws MalformedURLException
    {
        URL wsdlURL = new URL(protocol, host, port, DjigzoWSDefaults.QUARANTINE_MAIL_REPOSITORY_WSDL);

        return new MailRepositoryWSProxyFactory(wsdlURL, DjigzoWSDefaults.NAMESPACE, 
            DjigzoWSDefaults.QUARANTINE_MAIL_REPOSITORY_SERVICE_NAME);
    }

    public static OTPWSProxyFactory buildOTPWSProxyFactory( 
            @Inject @Value("${djigzo.ws.server.host}") String host,
            @Inject @Value("${djigzo.ws.server.port}") int port,
            @Inject @Value("${djigzo.ws.server.protocol}") String protocol) 
    throws MalformedURLException
    {
        URL wsdlURL = new URL(protocol, host, port, DjigzoWSDefaults.OTP_WSDL);

        return new OTPWSProxyFactory(wsdlURL, DjigzoWSDefaults.NAMESPACE, 
                DjigzoWSDefaults.OTP_SERVICE_NAME);
    }

    public static PortalUserWSProxyFactory buildPortalUserWSProxyFactory( 
            @Inject @Value("${djigzo.ws.server.host}") String host,
            @Inject @Value("${djigzo.ws.server.port}") int port,
            @Inject @Value("${djigzo.ws.server.protocol}") String protocol) 
    throws MalformedURLException
    {
        URL wsdlURL = new URL(protocol, host, port, DjigzoWSDefaults.PORTAL_USER_WSDL);

        return new PortalUserWSProxyFactory(wsdlURL, DjigzoWSDefaults.NAMESPACE, 
                DjigzoWSDefaults.PORTAL_USER_SERVICE_NAME);
    }

    @Scope(ScopeConstants.PERTHREAD)
    public static UsersWS buildUsersWS(UsersWSProxyFactory usersWSProxyFactory, ApplicationStateManager asm)
    throws WSProxyFactoryException
    {
    	SoapCredential authentication = asm.get(SoapCredential.class);
    	
    	return usersWSProxyFactory.createProxy(authentication.getCredential());
    }

    @Scope(ScopeConstants.PERTHREAD)
    public static UserWS buildUserWS(UserWSProxyFactory userWSProxyFactory, ApplicationStateManager asm)
    throws WSProxyFactoryException
    {
    	SoapCredential authentication = asm.get(SoapCredential.class);
    	
    	return userWSProxyFactory.createProxy(authentication.getCredential());
    }

    @Scope(ScopeConstants.PERTHREAD)
    public static DomainsWS buildDomainsWS(DomainsWSProxyFactory domainsWSProxyFactory, ApplicationStateManager asm)
    throws WSProxyFactoryException
    {
    	SoapCredential authentication = asm.get(SoapCredential.class);
    	
    	return domainsWSProxyFactory.createProxy(authentication.getCredential());
    }

    @Scope(ScopeConstants.PERTHREAD)
    public static DomainWS buildDomainWS(DomainWSProxyFactory domainWSProxyFactory, ApplicationStateManager asm)
    throws WSProxyFactoryException
    {
    	SoapCredential authentication = asm.get(SoapCredential.class);
    	
    	return domainWSProxyFactory.createProxy(authentication.getCredential());
    }
    
    @Scope(ScopeConstants.PERTHREAD)
    public static UserPreferencesWS buildUsersPreferencesWS(UserPreferencesWSProxyFactory userPreferencesWSProxyFactory, 
    		ApplicationStateManager asm)
    throws WSProxyFactoryException
    {
    	SoapCredential authentication = asm.get(SoapCredential.class);
    	
    	return userPreferencesWSProxyFactory.createProxy(authentication.getCredential());
    }

    @Scope(ScopeConstants.PERTHREAD)
    public static UserPreferencesWorkflowWS buildUsersPreferencesWorkflowWS(UserPreferencesWorkflowWSProxyFactory userPreferencesWorkflowWSProxyFactory, 
            ApplicationStateManager asm)
    throws WSProxyFactoryException
    {
        SoapCredential authentication = asm.get(SoapCredential.class);
        
        return userPreferencesWorkflowWSProxyFactory.createProxy(authentication.getCredential());
    }
    
    @Scope(ScopeConstants.PERTHREAD)
    public static GlobalPreferencesManagerWS buildGlobalPreferencesManagerWS(GlobalPreferencesManagerWSProxyFactory globalPreferencesManagerWSProxyFactory, 
    		ApplicationStateManager asm)
    throws WSProxyFactoryException
    {
    	SoapCredential authentication = asm.get(SoapCredential.class);
    	
    	return globalPreferencesManagerWSProxyFactory.createProxy(authentication.getCredential());
    }

    @Scope(ScopeConstants.PERTHREAD)
    public static HierarchicalPropertiesWS buildHierarchicalPropertiesWS(HierarchicalPropertiesWSProxyFactory hierarchicalPropertiesWSProxyFactory, 
    		ApplicationStateManager asm)
    throws WSProxyFactoryException
    {
    	SoapCredential authentication = asm.get(SoapCredential.class);
    	
    	return hierarchicalPropertiesWSProxyFactory.createProxy(authentication.getCredential());
    }

    @Scope(ScopeConstants.PERTHREAD)
    @Marker(RootStoreMarker.class)
    public static KeyAndCertStoreWS buildKeyAndRootCertStoreWS(KeyAndRootCertStoreWSProxyFactory keyAndRootCertStoreWSProxyFactory, 
            ApplicationStateManager asm)
    throws WSProxyFactoryException
    {
        SoapCredential authentication = asm.get(SoapCredential.class);
        
        return keyAndRootCertStoreWSProxyFactory.createProxy(authentication.getCredential());
    }

    @Scope(ScopeConstants.PERTHREAD)
    @Marker(CertificateStoreMarker.class)
    public static KeyAndCertStoreWS buildKeyAndCertStoreWS(KeyAndCertStoreWSProxyFactory keyAndCertStoreWSProxyFactory, 
    		ApplicationStateManager asm)
    throws WSProxyFactoryException
    {
    	SoapCredential authentication = asm.get(SoapCredential.class);
    	
    	return keyAndCertStoreWSProxyFactory.createProxy(authentication.getCredential());
    }

    @Scope(ScopeConstants.PERTHREAD)
    public static X509CRLStoreWS buildX509CRLStoreWS(X509CRLStoreWSProxyFactory x509CRLStoreWSProxyFactory, 
    		ApplicationStateManager asm)
    throws WSProxyFactoryException
    {
    	SoapCredential authentication = asm.get(SoapCredential.class);
    	
    	return x509CRLStoreWSProxyFactory.createProxy(authentication.getCredential());
    }
    
    @Scope(ScopeConstants.PERTHREAD)
    public static CertificateValidatorWS buildCertificateValidatorWS(CertificateValidatorWSProxyFactory certificateValidatorWSProxyFactory, 
    		ApplicationStateManager asm)
    throws WSProxyFactoryException
    {
    	SoapCredential authentication = asm.get(SoapCredential.class);
    	
    	return certificateValidatorWSProxyFactory.createProxy(authentication.getCredential());
    }

    @Scope(ScopeConstants.PERTHREAD)
    public static AdminManagerWS buildAdminManagerWS(AdminManagerWSProxyFactory adminManagerWSProxyFactory, 
    		ApplicationStateManager asm)
    throws WSProxyFactoryException
    {
    	SoapCredential authentication = asm.get(SoapCredential.class);
    	
    	return adminManagerWSProxyFactory.createProxy(authentication.getCredential());
    }

    @Scope(ScopeConstants.PERTHREAD)
    public static AdminWS buildAdminWS(AdminWSProxyFactory adminWSProxyFactory, 
    		ApplicationStateManager asm)
    throws WSProxyFactoryException
    {
    	SoapCredential authentication = asm.get(SoapCredential.class);
    	
    	return adminWSProxyFactory.createProxy(authentication.getCredential());
    }

    @Scope(ScopeConstants.PERTHREAD)
    public static AuthorityManagerWS buildAuthorityManagerWS(AuthorityManagerWSProxyFactory authorityManagerWSProxyFactory, 
    		ApplicationStateManager asm)
    throws WSProxyFactoryException
    {
    	SoapCredential authentication = asm.get(SoapCredential.class);
    	
    	return authorityManagerWSProxyFactory.createProxy(authentication.getCredential());
    }
    
    @Scope(ScopeConstants.PERTHREAD)
    public static SMSGatewayWS buildSMSGatewayWS(SMSGatewayWSProxyFactory smsGatewayWSProxyFactory, 
    		ApplicationStateManager asm)
    throws WSProxyFactoryException
    {
    	SoapCredential authentication = asm.get(SoapCredential.class);
    	
    	return smsGatewayWSProxyFactory.createProxy(authentication.getCredential());
    }

    @Scope(ScopeConstants.PERTHREAD)
    public static ClickatellWS buildClickatellWS(ClickatellWSProxyFactory clickatellWSProxyFactory, 
            ApplicationStateManager asm)
    throws WSProxyFactoryException
    {
        SoapCredential authentication = asm.get(SoapCredential.class);
        
        return clickatellWSProxyFactory.createProxy(authentication.getCredential());
    }
    
    @Scope(ScopeConstants.PERTHREAD)
    public static KeyAndCertificateWorkflowWS buildKeyAndCertificateWorkflowWS(
    		KeyAndCertificateWorkflowWSProxyFactory keyAndCertificateWorkflowWS, 
    		ApplicationStateManager asm)
    throws WSProxyFactoryException
    {
    	SoapCredential authentication = asm.get(SoapCredential.class);
    	
    	return keyAndCertificateWorkflowWS.createProxy(authentication.getCredential());
    }

    @Scope(ScopeConstants.PERTHREAD)
    public static JamesManagerWS buildJamesManagerWS(
            JamesManagerWSProxyFactory jamesManagerWSProxyFactory, 
            ApplicationStateManager asm)
    throws WSProxyFactoryException
    {
        SoapCredential authentication = asm.get(SoapCredential.class);
        
        return jamesManagerWSProxyFactory.createProxy(authentication.getCredential());
    }

    @Scope(ScopeConstants.PERTHREAD)
    public static JamesRepositoryManagerWS buildJamesRepositoryManagerWS(
            JamesRepositoryManagerWSProxyFactory jamesRepositoryManagerWSProxyFactory, 
            ApplicationStateManager asm)
    throws WSProxyFactoryException
    {
        SoapCredential authentication = asm.get(SoapCredential.class);
        
        return jamesRepositoryManagerWSProxyFactory.createProxy(authentication.getCredential());
    }

    @Scope(ScopeConstants.PERTHREAD)
    public static PostfixSpoolManagerWS buildPostfixSpoolManagerWS(
            PostfixSpoolManagerWSProxyFactory postfixSpoolManagerWSProxyFactory, 
            ApplicationStateManager asm)
    throws WSProxyFactoryException
    {
        SoapCredential authentication = asm.get(SoapCredential.class);
        
        return postfixSpoolManagerWSProxyFactory.createProxy(authentication.getCredential());
    }

    @Scope(ScopeConstants.PERTHREAD)
    public static PostfixLogManagerWS buildPostfixLogManagerWS(
            PostfixLogManagerWSProxyFactory postfixLogManagerWSProxyFactory, 
            ApplicationStateManager asm)
    throws WSProxyFactoryException
    {
        SoapCredential authentication = asm.get(SoapCredential.class);
        
        return postfixLogManagerWSProxyFactory.createProxy(authentication.getCredential());
    }

    @Scope(ScopeConstants.PERTHREAD)
    public static PostfixConfigManagerWS buildPostfixConfigManagerWS(
            PostfixConfigManagerWSProxyFactory postfixConfigManagerWSProxyFactory, 
            ApplicationStateManager asm)
    throws WSProxyFactoryException
    {
        SoapCredential authentication = asm.get(SoapCredential.class);
        
        return postfixConfigManagerWSProxyFactory.createProxy(authentication.getCredential());
    }
    
    @Scope(ScopeConstants.PERTHREAD)
    public static LoggerManagerWS buildLoggerManagerWS(
            LoggerManagerWSProxyFactory loggerManagerWSProxyFactory, 
            ApplicationStateManager asm)
    throws WSProxyFactoryException
    {
        SoapCredential authentication = asm.get(SoapCredential.class);
        
        return loggerManagerWSProxyFactory.createProxy(authentication.getCredential());
    }

    @Scope(ScopeConstants.PERTHREAD)
    public static SystemManagerWS buildSystemManagerWS(
            SystemManagerWSProxyFactory systemManagerWSProxyFactory, 
            ApplicationStateManager asm)
    throws WSProxyFactoryException
    {
        SoapCredential authentication = asm.get(SoapCredential.class);
        
        return systemManagerWSProxyFactory.createProxy(authentication.getCredential());
    }

    @Scope(ScopeConstants.PERTHREAD)
    public static BackupWS buildBackupWS(
            BackupWSProxyFactory backupWSProxyFactory, 
            ApplicationStateManager asm)
    throws WSProxyFactoryException
    {
        SoapCredential authentication = asm.get(SoapCredential.class);
        
        return backupWSProxyFactory.createProxy(authentication.getCredential());
    }

    @Scope(ScopeConstants.PERTHREAD)
    public static CAWS buildCAWS(
            CAWSProxyFactory caWSProxyFactory, 
            ApplicationStateManager asm)
    throws WSProxyFactoryException
    {
        SoapCredential authentication = asm.get(SoapCredential.class);
        
        return caWSProxyFactory.createProxy(authentication.getCredential());
    }

    @Scope(ScopeConstants.PERTHREAD)
    public static CACertStoreViewWS buildX509CACertStoreWS(
            CACertStoreViewWSProxyFactory caCertStoreViewWSProxyFactory, 
            ApplicationStateManager asm)
    throws WSProxyFactoryException
    {
        SoapCredential authentication = asm.get(SoapCredential.class);
        
        return caCertStoreViewWSProxyFactory.createProxy(authentication.getCredential());
    }

    @Scope(ScopeConstants.PERTHREAD)
    public static CertificateRequestStoreWS buildCertificateRequestStoreWS(
            CertificateRequestStoreWSProxyFactory certificateRequestStoreWSProxyFactory, 
            ApplicationStateManager asm)
    throws WSProxyFactoryException
    {
        SoapCredential authentication = asm.get(SoapCredential.class);
        
        return certificateRequestStoreWSProxyFactory.createProxy(authentication.getCredential());
    }
    
    @Scope(ScopeConstants.PERTHREAD)
    public static CTLWS buildCTLWS(
            CTLWSProxyFactory ctlWSProxyFactory, 
            ApplicationStateManager asm)
    throws WSProxyFactoryException
    {
        SoapCredential authentication = asm.get(SoapCredential.class);
        
        return ctlWSProxyFactory.createProxy(authentication.getCredential());
    }

    @Scope(ScopeConstants.PERTHREAD)
    public static FetchmailManagerWS buildFetchmailManagerWS(
            FetchmailManagerWSProxyFactory fetchmailManagerWSFactory, 
            ApplicationStateManager asm)
    throws WSProxyFactoryException
    {
        SoapCredential authentication = asm.get(SoapCredential.class);
        
        return fetchmailManagerWSFactory.createProxy(authentication.getCredential());
    }

    @Scope(ScopeConstants.PERTHREAD)
    public static PolicyPatternManagerWS buildPolicyPatternManagerWS(
            PolicyPatternManagerWSProxyFactory policyPatternManagerWSFactory, 
            ApplicationStateManager asm)
    throws WSProxyFactoryException
    {
        SoapCredential authentication = asm.get(SoapCredential.class);
        
        return policyPatternManagerWSFactory.createProxy(authentication.getCredential());
    }
    
    @Scope(ScopeConstants.PERTHREAD)
    public static PolicyPatternNodeWS buildPolicyPatternNodeWS(
            PolicyPatternNodeWSProxyFactory policyPatternNodeWSFactory, 
            ApplicationStateManager asm)
    throws WSProxyFactoryException
    {
        SoapCredential authentication = asm.get(SoapCredential.class);
        
        return policyPatternNodeWSFactory.createProxy(authentication.getCredential());
    }

    @Scope(ScopeConstants.PERTHREAD)
    public static MatchFilterRegistryWS buildMatchFilterRegistryWS(
            MatchFilterRegistryWSProxyFactory matchFilterRegistryWSFactory, 
            ApplicationStateManager asm)
    throws WSProxyFactoryException
    {
        SoapCredential authentication = asm.get(SoapCredential.class);
        
        return matchFilterRegistryWSFactory.createProxy(authentication.getCredential());
    }

    @Scope(ScopeConstants.PERTHREAD)
    public static UserPreferencesPolicyPatternManagerWS buildUserPreferencesPolicyPatternManagerWS(
            UserPreferencesPolicyPatternManagerWSProxyFactory userPreferencesPolicyPatternManagerWSFactory, 
            ApplicationStateManager asm)
    throws WSProxyFactoryException
    {
        SoapCredential authentication = asm.get(SoapCredential.class);
        
        return userPreferencesPolicyPatternManagerWSFactory.createProxy(authentication.getCredential());
    }
    
    @Scope(ScopeConstants.PERTHREAD)
    public static ComodoWS buildComodoWS(
            ComodoWSProxyFactory comodoWSFactory, 
            ApplicationStateManager asm)
    throws WSProxyFactoryException
    {
        SoapCredential authentication = asm.get(SoapCredential.class);
        
        return comodoWSFactory.createProxy(authentication.getCredential());
    }

    @Scope(ScopeConstants.PERTHREAD)
    public static UpdateableWordSkipperWS buildUpdateableWordSkipperWS(
            UpdateableWordSkipperWSProxyFactory factory, 
            ApplicationStateManager asm)
    throws WSProxyFactoryException
    {
        SoapCredential authentication = asm.get(SoapCredential.class);
        
        return factory.createProxy(authentication.getCredential());
    }

    @Scope(ScopeConstants.PERTHREAD)
    public static MailRepositoryWS buildQuarantineMailRepositoryWS(
            MailRepositoryWSProxyFactory factory, 
            ApplicationStateManager asm)
    throws WSProxyFactoryException
    {
        SoapCredential authentication = asm.get(SoapCredential.class);
        
        return factory.createProxy(authentication.getCredential());
    }

    @Scope(ScopeConstants.PERTHREAD)
    public static OTPWS buildOTPWS(
            OTPWSProxyFactory factory, 
            ApplicationStateManager asm)
    throws WSProxyFactoryException
    {
        SoapCredential authentication = asm.get(SoapCredential.class);
        
        return factory.createProxy(authentication.getCredential());
    }

    @Scope(ScopeConstants.PERTHREAD)
    public static PortalUserWS buildPortalUserWS(
            PortalUserWSProxyFactory factory, 
            ApplicationStateManager asm)
    throws WSProxyFactoryException
    {
        SoapCredential authentication = asm.get(SoapCredential.class);
        
        return factory.createProxy(authentication.getCredential());
    }

    /*
     * Note: The EventLoggerWS service is special in that it does not use different credentials for each
     * user. This is required because the EventLogger is used to log login events. In the login event
     * the user does not have a session and therefore no login credentials.
     */
    public static EventLoggerWS buildEventLoggerWS(
            EventLoggerWSProxyFactory eventLoggerWSProxyFactory, 
            @Inject @Value("${soap.username}") String soapUsername,
            @Inject @Value("${soap.password}") String soapPassword)
    throws WSProxyFactoryException
    {
        return eventLoggerWSProxyFactory.createProxy(new Credential(soapUsername, soapPassword));
    }
    
    /*
     * Note: The LoginWS SOAP service is special in that it does not use different credentials for each
     * user. I have not found a way yet to inject a per-thread Tapestry service into a Spring bean 
     * (See the spring bean UserDetailsServiceImpl in spring.xml).
     */
    public static LoginWS buildLoginWS(LoginWSProxyFactory loginWSProxyFactory,
    		@Inject @Value("${soap.username}") String soapUsername,
    		@Inject @Value("${soap.password}") String soapPassword)
    throws WSProxyFactoryException
    {
    	return loginWSProxyFactory.createProxy(new Credential(soapUsername, soapPassword));
    }
    
    public UserManager buildUserManager(UsersWS usersWS, UserWS userWS, UserPreferencesWS userPreferencesWS,
			HierarchicalPropertiesWS hierarchicalPropertiesWS) 
    {
    	return new UserManagerImpl(usersWS, userWS, userPreferencesWS, hierarchicalPropertiesWS);
    }

    public DomainManager buildDomainManager(DomainsWS domainsWS, DomainWS domainWS, 
    		UserPreferencesWS userPreferencesWS, HierarchicalPropertiesWS hierarchicalPropertiesWS) 
    {
    	return new DomainManagerImpl(domainsWS, domainWS, userPreferencesWS, 
    			hierarchicalPropertiesWS);
    }

    public AdminManager buildAdminManager(AdminManagerWS adminManagerWS, AdminWS adminWS,
			PasswordEncoder passwordEncoder, SaltSource saltSource, RandomGenerator randomGenerator) 
    {
    	return new AdminManagerImpl(adminManagerWS, adminWS, passwordEncoder, saltSource,
    			randomGenerator);
    }
    
    public GlobalPreferencesManager buildGlobalPreferencesManager(GlobalPreferencesManagerWS globalPreferencesManagerWS, 
    		UserPreferencesWS userPreferencesWS, HierarchicalPropertiesWS hierarchicalPropertiesWS) 
    {
    	return new GlobalPreferencesManagerImpl(globalPreferencesManagerWS, userPreferencesWS,
    			hierarchicalPropertiesWS);
    }
    
    /*
     * RequestExceptionHandler that intercepts SOAP authentication failed and redirects
     * to the provided page.
     */
    private static class SOAPAuthenticationFailedRedirectRequestExceptionHandler extends
    		RedirectRequestExceptionHandler<SOAPFaultException>
    {
    	public SOAPAuthenticationFailedRedirectRequestExceptionHandler(RequestExceptionHandler delegate, 
    			Response response, LinkFactory linkFactory, String redirectToPage, Object... context)
    	{
    		super(delegate, response, linkFactory, redirectToPage, context);
    	}
    	
    	@Override
    	public boolean isMatch(SOAPFaultException exception)
    	{
    		/*
    		 * I do not know of a better way to detect a SOAP authentication failure
    		 * than checking the message for specific content.
    		 */
    		return exception.getMessage().startsWith("Security processing failed"); 
    	}
    }
    
    /*
     * RequestExceptionHandler that intercepts and handles SOAP related exceptions
     */
    @Match(value = {"RequestExceptionHandler"})
    public static RequestExceptionHandler decorateSOAPAuthenticationFailedRequestExceptionHandler(
            final Object delegate, final Response response, 
            final LinkFactory linkFactory,
            @Inject @Value("${soap.authentication-failed-page}") final String redirectToPage)
    {
    	return new SOAPAuthenticationFailedRedirectRequestExceptionHandler(
    			(RequestExceptionHandler)delegate, response, linkFactory, redirectToPage);
    }
}
