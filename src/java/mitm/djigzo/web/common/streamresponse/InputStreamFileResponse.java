/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.common.streamresponse;

import java.io.IOException;
import java.io.InputStream;

import mitm.common.util.Check;

import org.apache.tapestry5.StreamResponse;
import org.apache.tapestry5.services.Response;

/**
 * StreamResponse implementation that returns a given byte array to the user using the provided content-type
 * 
 * @author Martijn Brinkers
 *
 */
public class InputStreamFileResponse implements StreamResponse
{
	private final InputStream input;
	
	private final String contentType;
	
	private final String filename;
	
	public InputStreamFileResponse(InputStream input, String contentType, String filename)
	{
		Check.notNull(input, "input");
		Check.notNull(contentType, "contentType");
		
		this.input = input;
		this.contentType = contentType;
		this.filename = filename;
	}
	
    @Override
	public String getContentType() {
		return contentType;
	}

    @Override
	public InputStream getStream() 
	throws IOException 
	{
		return input;
	}

    @Override
	public void prepareResponse(Response response) 
	{
		if (filename != null) {
			response.setHeader("content-disposition", "attachment; filename=\"" + filename + "\"");
		}
		
		response.setHeader("Expires", "0");
	}
}
