/*
 * Copyright (c) 2009-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.beans;

/**
 * Tapestry uses introspection to detect the class of the property. Unfortunately We cannot use 
 * generics because they are only known at compile time :(. We therefore have to create properties
 * for every type we need to use otherwise default type coercions are non functional.
 * 
 */
public class IntegerProperty 
{
	private Integer value;
	private boolean inherit;
	
	public Integer getValue() {
		return value;
	}
	
	public Integer getUserValue() {
		return inherit ? null : value;
	}
	
	public void setValue(Integer value) {
		this.value = value;
	}
	
	public boolean isInherit() {
		return inherit;
	}
	
	public void setInherit(boolean inherit) {
		this.inherit = inherit;
	}
}
