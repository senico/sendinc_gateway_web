/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.common;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang.ObjectUtils;
import org.apache.tapestry5.OptionGroupModel;
import org.apache.tapestry5.OptionModel;
import org.apache.tapestry5.internal.OptionModelImpl;
import org.apache.tapestry5.ioc.services.PropertyAccess;
import org.apache.tapestry5.ioc.services.PropertyAdapter;
import org.apache.tapestry5.util.AbstractSelectModel;

/**
 * Based on http://wiki.apache.org/tapestry/Tapestry5AnotherSelectWithObjects
 */
public class GenericSelectionModel<T> extends AbstractSelectModel 
{
	private PropertyAdapter labelFieldAdapter = null;

	private Collection<T> collection;

	public GenericSelectionModel(Collection<T> collection, String labelField, PropertyAccess access) 
	{
		if(collection == null) { 
		    collection = new ArrayList<T>();
		}
		
		if(labelField != null && !labelField.equalsIgnoreCase("null"))
		{
			if(collection.size() > 0)
			{
				this.labelFieldAdapter = access.getAdapter(collection.iterator().next().getClass()).
						getPropertyAdapter(labelField);
			}
		}
		this.collection = collection;
	}

    @Override
	public List<OptionGroupModel> getOptionGroups() 
	{
		return null;
	}

    @Override
	public List<OptionModel> getOptions() 
	{
		List<OptionModel> optionModelList = new ArrayList<OptionModel>();
		
		for (T obj : collection) 
		{
			if (labelFieldAdapter == null) {
				optionModelList.add(new OptionModelImpl(ObjectUtils.toString(obj) + "", obj));
			} 
			else {
				optionModelList.add(new OptionModelImpl(ObjectUtils.toString(labelFieldAdapter.get(obj)), obj));
			}
		}
		return optionModelList;
	}
}