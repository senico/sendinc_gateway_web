/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.common;

/**
 * Class for the storage of filter settings as set by the user.
 * 
 * @author Martijn Brinkers
 *
 */
public class CertificateFilterSettings 
{
	/*
	 * Determines to which field the filter applies to
	 */
	private CertificateFilterBy filterby = CertificateFilterBy.NO_FILTER;
	
	/*
	 * Determines whether entries with a missing key alias are allowed
	 */
	private boolean allowMissingKeyAlias = true;
	
	/*
	 * Determines whether expired certificates are allowed
	 */
	private boolean allowExpired = true;
	
	/*
	 * The actual filter value
	 */
	private String filter;
	
	public void setFilterBy(CertificateFilterBy filterby) {
		this.filterby = filterby;
	}
	
	public CertificateFilterBy getFilterBy() {
		return filterby;
	}
	
	public void setFilter(String filter) {
		this.filter = filter;
	}
	
	public String getFilter() {
		return filter;
	}
	
	public void setAllowMissingKeyAlias(boolean allowMissingKeyAlias) {
		this.allowMissingKeyAlias = allowMissingKeyAlias;
	}
	
	public boolean isAllowMissingKeyAlias()	{
		return allowMissingKeyAlias;
	}

	public void setAllowExpired(boolean allowExpired) {
		this.allowExpired = allowExpired;
	}
	
	public boolean isAllowExpired()	{
		return allowExpired;
	}
	
	public boolean isDisabled() {
		return filterby == CertificateFilterBy.NO_FILTER && allowExpired && allowMissingKeyAlias;
	}
	
	/*
	 * Used by a Tapestry template for getting the enum value of this option
	 */
	public CertificateFilterBy getNoFilterEnumValue() {
		return CertificateFilterBy.NO_FILTER;
	}

	/*
	 * Used by a Tapestry template for getting the enum value of this radio button
	 */
	public CertificateFilterBy getEmailFilterEnumValue() {
		return CertificateFilterBy.EMAIL;
	}

	/*
	 * Used by a Tapestry template for getting the enum value of this radio button
	 */
	public CertificateFilterBy getSubjectFilterEnumValue() {
		return CertificateFilterBy.SUBJECT;
	}

	/*
	 * Used by a Tapestry template for getting the enum value of this radio button
	 */
	public CertificateFilterBy getIssuerFilterEnumValue() {
		return CertificateFilterBy.ISSUER;
	}
}
