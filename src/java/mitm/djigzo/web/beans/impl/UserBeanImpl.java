/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.beans.impl;

import java.util.List;
import java.util.Set;

import mitm.application.djigzo.UserLocality;
import mitm.application.djigzo.ws.X509CertificateDTO;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.util.Check;
import mitm.common.ws.WebServiceCheckedException;
import mitm.djigzo.web.beans.BeanUtils;
import mitm.djigzo.web.beans.CertificateType;
import mitm.djigzo.web.beans.UserBean;
import mitm.djigzo.web.beans.UserPreferencesBean;
import mitm.djigzo.web.beans.X509CertificateBean;
import mitm.djigzo.web.entities.User;

public class UserBeanImpl implements UserBean
{
	private final User user;
	
	private UserPreferencesBean userPreferences;
	
	public UserBeanImpl(User user) 
	{
		Check.notNull(user, "user");
		
		this.user = user;
	}

    @Override
	public String getEmail() {
		return user.getEmail();
	}

    @Override
    public UserLocality getUserLocality()
    throws WebServiceCheckedException
    {
        return user.getUserLocality();
    }
	
    @Override
    public Set<X509CertificateBean> getAutoSelectEncryptionCertificates() 
    throws WebServiceCheckedException 
    {
    	Set<X509CertificateBean> certificateBeans = null;
    	
    	List<X509CertificateDTO> certificateDTOs = user.getAutoSelectEncryptionCertificates();

    	certificateBeans = BeanUtils.certificatesToBeans(certificateDTOs, CertificateType.AUTO_SELECT);
    	
    	return certificateBeans;
    }
	
    @Override
    public X509CertificateBean getSigningKeyAndCertificate()
    throws WebServiceCheckedException
    {
    	X509CertificateBean keyAndCertificateBean = null;
    	
    	X509CertificateDTO keyAndCertificateDTO = user.getSigningKeyAndCertificate();
    	
    	if (keyAndCertificateDTO != null) 
    	{
    		keyAndCertificateBean = new X509CertificateBeanImpl(keyAndCertificateDTO, 
    				CertificateType.SELECTED);
    	}
    	
    	return keyAndCertificateBean;
    }
    
    @Override
    public UserPreferencesBean getPreferences() 
    throws WebServiceCheckedException
    {
    	if (userPreferences == null) {
    		userPreferences = new UserPreferencesBeanImp(user.getPreferences());
    	}
    	
    	return userPreferences;
    }

    @Override
    public void save() 
	throws HierarchicalPropertiesException, WebServiceCheckedException 
	{
    	if (userPreferences != null) {
    		userPreferences.save();
    	}
	}
}
