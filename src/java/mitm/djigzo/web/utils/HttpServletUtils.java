/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.utils;

import javax.servlet.http.HttpServletRequest;

import org.apache.tapestry5.services.Response;

public class HttpServletUtils
{
	/**
	 * Sets HTTP headers to disable HTTP caching.
	 * Has been tested with IE and FF. Does not seem to work with Opera (if the user presses the back button the 
	 * page is displayed) because Opera seems to cash aggressively. 
	 * 
	 * See: http://my.opera.com/yngve/blog/2007/02/27/introducing-cache-contexts-or-why-the
	 */
	public static void disableHTTPCaching(Response response)
	{
		response.setHeader("Cache-Control", "private, no-cache, no-store, must-revalidate, max-stale=0"); 
		response.setHeader("Pragma", "no-cache"); 
		response.setHeader("Expires", "-1"); 
	}
	
	public static void invalidateSession(HttpServletRequest request)
	{
        /*
         * This is a dirty workaround for https://issues.apache.org/jira/browse/TAP5-413 when running
         * in Tomcat.
         * 
         * SessionApplicationStatePersistenceStrategy needs to access the session to store the
         * ASOs but this results in the following exception:
         * 
         * java.lang.IllegalStateException: Cannot create a session after the response has been committed
         */
	    ASOUtils.clearASOs(request);
	    
        try {
            request.getSession().invalidate();
        }
        catch(IllegalStateException e) {
            /* thrown when session is already invalidated. ignore */
        }
	}
}
