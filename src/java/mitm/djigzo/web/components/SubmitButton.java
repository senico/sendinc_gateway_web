package mitm.djigzo.web.components;

//Copyright 2007, 2008 The Apache Software Foundation
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import org.apache.tapestry5.*;
import org.apache.tapestry5.annotations.Environmental;
import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.annotations.SupportsInformalParameters;
import org.apache.tapestry5.dom.Element;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.FormSupport;
import org.apache.tapestry5.services.Heartbeat;
import org.apache.tapestry5.services.Request;

/**
 * Almost an exact copy of the Submit component but instead of being rendered as &lt;input&gt; the submit is now rendered as &lt;button&gt;
 */
@SupportsInformalParameters
public class SubmitButton implements ClientElement
{
    /**
     * If true (the default), then any notification sent by the component will be deferred until the end of the form
     * submission (this is usually desirable).
     */
    @Parameter
    private boolean defer = true;

    /**
     * The name of the event that will be triggered if this component is the cause of the form submission. The default
     * is "selected".
     */
    @Parameter(allowNull = false, defaultPrefix = BindingConstants.LITERAL)
    private String event = EventConstants.SELECTED;

    /**
     * If true, then the field will render out with a disabled attribute (to turn off client-side behavior). Further, a
     * disabled field ignores any value in the request when the form is submitted.
     */
    @Parameter("false")
    private boolean disabled;


    @Environmental
    private FormSupport formSupport;

    @Environmental
    private Heartbeat heartbeat;

    @Inject
    private ComponentResources resources;

    @Inject
    private Request request;

    @Inject
    private RenderSupport renderSupport;

    private Element element;

    private String clientId;

    private static class ProcessSubmission implements ComponentAction<SubmitButton>
    {
		private static final long serialVersionUID = 2791951458057389828L;

		private final String elementName;

        public ProcessSubmission(String elementName)
        {
            this.elementName = elementName;
        }

        @Override
        public void execute(SubmitButton component)
        {
            component.processSubmission(elementName);
        }
    }

    void beginRender(MarkupWriter writer)
    {
        clientId = null;

        String name = formSupport.allocateControlName(resources.getId());

        // Save the element, to see if an id is later requested.

        element = writer.element("button", "type", "submit", "name", name);

        if (disabled) writer.attributes("disabled", "disabled");

        formSupport.store(this, new ProcessSubmission(name));

        resources.renderInformalParameters(writer);
    }

    void afterRender(MarkupWriter writer)
    {
        writer.end();
    }

    void processSubmission(String elementName)
    {
        if (disabled) return;

        String value = request.getParameter(elementName);

        if (value == null) return;

        Runnable sendNotification = new Runnable()
        {
            @Override
            public void run()
            {
                resources.triggerEvent(event, null, null);
            }
        };

        // When not deferred, don't wait, fire the event now (actually, at the end of the current
        // heartbeat). This is most likely because the Submit is inside a Loop and some contextual
        // information will change if we defer.

        if (defer) formSupport.defer(sendNotification);
        else heartbeat.defer(sendNotification);
    }

    /**
     * Returns the component's client id. This must be called after the component has rendered. The id is allocated
     * lazily (first time this method is invoked).
     *
     * @return client id for the component
     */
    @Override
    public String getClientId()
    {
        if (clientId == null)
        {
            clientId = renderSupport.allocateClientId(resources);

            element.forceAttributes("id", clientId);
        }

        return clientId;
    }
}