/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.beans.impl;

import java.util.Date;

import mitm.application.djigzo.ws.X509CRLDTO;
import mitm.common.util.Check;
import mitm.djigzo.web.beans.X509CRLBean;

public class X509CRLBeanImpl implements X509CRLBean 
{
	private X509CRLDTO dto; 
	
	public X509CRLBeanImpl(X509CRLDTO dto)
	{
		Check.notNull(dto, "dto");
		
		this.dto = dto;
	}
	
    @Override
	public String getCrlNumber() {
		return dto.getCrlNumber();
	}

    @Override
	public String getDeltaIndicator() {
		return dto.getDeltaIndicator();
	}

    @Override
	public String getIssuer() {
		return dto.getIssuerFriendly();
	}

    @Override
	public Date getNextUpdate() {
		return dto.getNextUpdate();
	}

    @Override
	public Date getThisUpdate() {
		return dto.getThisUpdate();
	}

    @Override
	public String getThumbprint() {
		return dto.getThumbprint();
	}
	
    @Override
	public String getThumbprintSHA1() {
		return dto.getThumbprintSHA1();
	}

    @Override
    public String getSignatureAlgorithm() {
        return dto.getSignatureAlgorithm();
    }
	
    @Override
	public int getVersion() {
		return dto.getVersion();
	}

    @Override
	public boolean isDeltaCRL() {
		return dto.isDeltaCRL();
	}
}
