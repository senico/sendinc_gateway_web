/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.services;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import mitm.common.cache.ContentCache;
import mitm.common.cache.RateCounter;
import mitm.common.locale.CharacterEncoding;
import mitm.common.util.RegExprUtils;
import mitm.common.util.SizeUtils;
import mitm.djigzo.web.bindings.JSEBindingFactory;
import mitm.djigzo.web.bindings.JoinBindingFactory;
import mitm.djigzo.web.common.CertificateRequestHandlerConfigPageRegistry;
import mitm.djigzo.web.common.CertificateRequestHandlerConfigPageRegistryImpl;
import mitm.djigzo.web.common.InjectSelectionModelWorker;
import mitm.djigzo.web.common.security.Roles;
import mitm.djigzo.web.common.security.RolesImpl;
import mitm.djigzo.web.pages.ca.handlers.comodo.ComodoSettings;
import mitm.djigzo.web.render.FragmentMarkupRenderContent;
import mitm.djigzo.web.render.FragmentMarkupRenderer;
import mitm.djigzo.web.render.FragmentMarkupRendererFilter;
import mitm.djigzo.web.render.FragmentMarkupRendererRegistry;
import mitm.djigzo.web.render.SystemRendererTags;
import mitm.djigzo.web.render.TextFragmentMarkupRender;
import mitm.djigzo.web.render.impl.FragmentMarkupRenderContentImpl;
import mitm.djigzo.web.render.impl.InlineAddClassPattern;
import mitm.djigzo.web.render.impl.InlineAddClassPatternFromRegistry;
import mitm.djigzo.web.render.impl.TextFragmentMarkupRenderImpl;
import mitm.djigzo.web.services.security.AuthenticationEventListener;
import mitm.djigzo.web.services.security.BannedAuthenticationProcessingFilter;
import mitm.djigzo.web.validators.DomainValidator;
import mitm.djigzo.web.validators.EmailValidator;
import mitm.djigzo.web.validators.HexValidator;
import mitm.djigzo.web.validators.PhoneNumberValidator;
import mitm.djigzo.web.validators.URIValidator;
import mitm.djigzo.web.validators.WordValidator;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.text.StrBuilder;
import org.apache.commons.lang.time.DateUtils;
import org.apache.tapestry5.MetaDataConstants;
import org.apache.tapestry5.Validator;
import org.apache.tapestry5.ioc.MappedConfiguration;
import org.apache.tapestry5.ioc.OrderedConfiguration;
import org.apache.tapestry5.ioc.ServiceBinder;
import org.apache.tapestry5.ioc.annotations.EagerLoad;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.ioc.annotations.InjectService;
import org.apache.tapestry5.ioc.annotations.SubModule;
import org.apache.tapestry5.ioc.annotations.Value;
import org.apache.tapestry5.ioc.services.PipelineBuilder;
import org.apache.tapestry5.ioc.services.PropertyAccess;
import org.apache.tapestry5.ioc.services.RegistryShutdownHub;
import org.apache.tapestry5.ioc.services.SymbolProvider;
import org.apache.tapestry5.ioc.services.SymbolSource;
import org.apache.tapestry5.services.BindingFactory;
import org.apache.tapestry5.services.BindingSource;
import org.apache.tapestry5.services.ComponentClassTransformWorker;
import org.apache.tapestry5.services.RequestFilter;
import org.apache.tapestry5.services.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.ui.webapp.AuthenticationProcessingFilter;

/**
 * The main Tapestry application module.
 * 
 * @author Martijn Brinkers
 *
 */
@SubModule({SecurityModule.class, SoapModule.class})
public class AppModule
{
    private final static Logger logger = LoggerFactory.getLogger(AppModule.class);
    
    /*
     * The system parameter for DJIGZO home
     */
	private static final String DJIGZO_HOME_PARAMETER = "djigzo-web.home";
	
	/*
	 * The symbol name for the highlight patterns
	 */
	private static final String HIGHLIGHTS_PATTERNS_SYMBOL = "highlights.patterns";
	
    public static void bind(final ServiceBinder binder) 
    {
        binder.bind(Roles.class, RolesImpl.class);
        binder.bind(FragmentMarkupRenderContent.class, FragmentMarkupRenderContentImpl.class);
        binder.bind(TextFragmentMarkupRender.class, TextFragmentMarkupRenderImpl.class);
        binder.bind(Locales.class, LocalesImpl.class);
    }

    public static void contributeFactoryDefaults(MappedConfiguration<String, String> configuration)
    {
        /*
         * djigzo home defaults to current user directory
         */
        configuration.add(DJIGZO_HOME_PARAMETER, System.getProperty("user.dir"));
    	
        configuration.add("djigzo.https.all", "true");
        configuration.add("djigzo.ws.server.host", "127.0.0.1");
        configuration.add("djigzo.ws.server.port", "9000");
        configuration.add("djigzo.ws.server.protocol", "http");
        configuration.add("djigzo.restart.delay", "45");
        configuration.add("protected.system.password", "djigzo");
        configuration.add("soap.username", "admin");
        configuration.add("soap.password", "password");
        configuration.add("soap.authentication-failed-page", "accessdenied");
        configuration.add("access-denied-page", "accessdenied");
        configuration.add("csrf.length", "6");
        configuration.add("csrf.redirectTo", "accessdenied");
        configuration.add("hmac.algorithm", "HmacSHA1");
        configuration.add("hmac.redirectTo", "accessdenied");
        /*
         * When an email is viewed it will not be larger than email.max-size size (prevent DOS)
         */
        configuration.add("email.max-size", "65535");
        configuration.add("email.attachment.max-size", Integer.toString(5 * SizeUtils.MB));
        /*
         * Maximum time an uploaded attachment (by external user) may live
         */
        configuration.add("upload.max-stale-time", Long.toString(DateUtils.MILLIS_PER_MINUTE * 30));
        configuration.add("upload.max-attachments", "3");
        
        /*
         * the maximum number of concurrent sessions for one 'user' (message key etc.)
         */
        configuration.add("web.max-reply-rate", "5");
        /*
         * Time a reply lives in the rate counter
         */
        configuration.add("web.reply-rate-lifetime", Long.toString(DateUtils.MILLIS_PER_MINUTE * 5));

        /*
         * The character encoding to use when replying to the PDF
         */
        configuration.add("web.pdf.reply.charset", CharacterEncoding.UTF_8);
        
        /*
         * parameters for the SSL management page
         */
        configuration.add("ssl.password", "djigzo");
        configuration.add("ssl.file", "./ssl/sslCertificate.p12");
        
        /*
         * If true the admin can edit My Destination
         */
        configuration.add("mta.enableMyDestination", "false");        

        /*
         * If true the Fetchmail pages are enabled
         */
        configuration.add("fetchmail.enabled", "false");                 

        configuration.add("fetchmail.hidePasswords", "true");                 
        configuration.add("saslPasswords.hidePasswords", "true");                 

        
        configuration.add("caBulkRequest.maxLines", "10000");                 
        configuration.add("caBulkRequest.maxValueLength", "256");                 
        
        /*
         * Check whether a certificate is in use (if in use the delete option is disabled). 
         * This is a relatively slow process. Disable if more speed is required. 
         */
        configuration.add("certificates.checkInUse", "true");                 
        
        /*
         * maxBatchSize settings
         */
        configuration.add("certificateRequestsPreview.maxBatchSize", "10");                 
        configuration.add("certificateImport.maxBatchSize", "10");                 
        configuration.add("certificateImportKey.maxBatchSize", "10");                 
        
        /*
         * rowsPerPage settings
         */
        configuration.add("users.rowsPerPage", "25");        
        configuration.add("certificates.rowsPerPage", "25");        
        configuration.add("crls.rowsPerPage", "25");        
        configuration.add("jamesLog.rowsPerPage", "25");        
        configuration.add("postfixLog.rowsPerPage", "25");      
        configuration.add("postfixSpool.rowsPerPage", "25"); 
        configuration.add("sendCertificate.rowsPerPage", "25");                 
        configuration.add("certificateUsage.rowsPerPage", "25");                 
        configuration.add("ctls.rowsPerPage", "25");                 
        configuration.add("fetchmail.rowsPerPage", "25");                 
        configuration.add("saslPasswords.rowsPerPage", "25");                 
        configuration.add("certificateRequests.rowsPerPage", "25");                 
        configuration.add("certificateRequestsPreview.rowsPerPage", "25");                 
        configuration.add("dlp.patterns.rowsPerPage", "25");      
        configuration.add("quarantineManager.rowsPerPage", "25");      
        configuration.add("quarantineView.rowsPerPage", "10");      
        configuration.add("patternsUsage.rowsPerPage", "25");
        
        /*
         * portal settings
         */
        configuration.add("portal.selectpassword.validity", Long.toString(DateUtils.MILLIS_PER_DAY * 30));
        
        /*
         * Brute force filter settings.
         */
        configuration.add("bff.lifetime", "60000" /* failure is 1 min in cache */);                
        configuration.add("bff.maxFailures", "5"  /* 5 failures in cache => ban */);                
        configuration.add("bff.enabled", "true");                
    }
       
    public static void contributeApplicationDefaults(MappedConfiguration<String, String> configuration) 
    {
        /*
         * We want to include a newer version of prototype, scriptaculous etc. because the one that comes with
         * T5 is not compatible with IE8 
         */
        configuration.add("tapestry.scriptaculous.path", "mitm/djigzo/web/prototype");
    }
    
    public static void contributeClasspathAssetAliasManager(MappedConfiguration<String, String> configuration)
    {
        /*
         * We want to include a newer version of prototype, scriptaculous etc. because the one that comes with
         * T5 is not compatible with IE8. We need to add the location here to override the factory settings.
         * Another option would be to set the property in the web.xml or as a command line option but I prefer
         * to do it here to make sure it's not forgotten.
         */
        
        configuration.add("tapx-prototype/1.6.1", "mitm/djigzo/web/prototype");
    }
    
    /**
     * Builds the PropertiesFileSymbolProvider service
     */
    public static PropertiesFileSymbolProvider buildPropertiesFileSymbolProvider()
    {
    	/*
    	 * The location where the properties file will be loaded from can be set with a system
    	 * property. The properties file will be loaded from the conf sub-dir of this dir.
    	 */
        String base = System.getProperty(DJIGZO_HOME_PARAMETER);

        return new PropertiesFileSymbolProvider("djigzo-web.properties", new File(base, "conf"));
    }
    
    public static void contributeSymbolSource(OrderedConfiguration<SymbolProvider> configuration, 
    		@InjectService("PropertiesFileSymbolProvider") SymbolProvider symbolProvider)
    {
        configuration.add("PropertiesFileSymbolProvider", symbolProvider, "after:SystemProperties", 
                "before:ApplicationDefaults");
    }
    
    /*
     * Builds the ContentCache service.  
     */
    public static ContentCache buildContentCache(@Inject @Value("${upload.max-stale-time}") long maxStaleTime, 
    		RegistryShutdownHub registryShutdownHub)
    {
    	ContentCache cache = new ContentCacheServiceImpl(maxStaleTime, registryShutdownHub);
    	
    	cache.start();
    	
    	return cache;
    }

    /*
     * Builds the RateCounter service.  
     */
    public static RateCounter buildRateCounter(RegistryShutdownHub registryShutdownHub)
    {
    	RateCounter rateCounter = new RateCounterServiceImpl(registryShutdownHub);
    	
    	rateCounter.start();
    	
    	return rateCounter;
    }
    
    /*
     * Note: must be eager loaded since it must be "injected" into a spring bean
     */
    @EagerLoad
    public static LoginBanService buildLoginBanService(RateCounter rateCounter,
            AuthenticationEventListener authenticationEventListener,
            AuthenticationProcessingFilter authenticationProcessingFilter, 
            @Inject @Value("${bff.lifetime}") long lifetime, 
            @Inject @Value("${bff.maxFailures}") int maxFailures,
            @Inject @Value("${bff.enabled}") boolean enabled)
    {
        LoginBanService loginBanService = new LoginBanServiceImpl(rateCounter, lifetime, maxFailures, enabled);
        
        if (authenticationEventListener != null) {
            authenticationEventListener.setLoginBanService(loginBanService);
        }

        if (authenticationProcessingFilter instanceof BannedAuthenticationProcessingFilter)
        {
            ((BannedAuthenticationProcessingFilter) authenticationProcessingFilter).setLoginBanService(
                    loginBanService);
        }
        
        return loginBanService;
    }
    
    /*
     * Builds the CertificateRequestHandlerConfigPageRegistry service.
     * 
     * Note: somehow Tapestry only accepts Class and not Class<?>.
     */
    @SuppressWarnings("rawtypes")
    public static CertificateRequestHandlerConfigPageRegistry buildCertificateRequestHandlerConfigPageRegistry(
            Map<String, Class> pages)
    {
        return new CertificateRequestHandlerConfigPageRegistryImpl(pages);
    }
    
    /*
     * Adds pages to the CertificateRequestHandlerConfigPageRegistry 
     * 
     * Note: somehow Tapestry only accepts Class and not Class<?>.
     */
    @SuppressWarnings("rawtypes")
    public static void contributeCertificateRequestHandlerConfigPageRegistry(
            MappedConfiguration<String, Class> pages) 
    {
        pages.add("Comodo", ComodoSettings.class);
    }
    
    /*
     * Register Tapestry field validators
     */
    public static void contributeFieldValidatorSource(MappedConfiguration<String, Validator<?, ?>> configuration) 
    {
    	configuration.add(PhoneNumberValidator.NAME, new PhoneNumberValidator());
    	configuration.add(EmailValidator.NAME, new EmailValidator());
    	configuration.add(DomainValidator.NAME, new DomainValidator());
        configuration.add(WordValidator.NAME, new WordValidator());
        configuration.add(HexValidator.NAME, new HexValidator());
        configuration.add(URIValidator.NAME, new URIValidator());
    }

    /*
     * Register ValidationMessages.properties containing validation messages
     */
	public static void contributeValidationMessagesSource(OrderedConfiguration<String> configuration) {
	    configuration.add("djigzo", "mitm/djigzo/web/validators/ValidationMessages");
	}
	
    /*
     * Register new Tapestry binding factories
     */
	public static void contributeBindingSource(MappedConfiguration<String, BindingFactory> configuration,
			BindingSource bindingSource)
    {
        configuration.add("jse",new JSEBindingFactory(bindingSource));
        configuration.add("join",new JoinBindingFactory(bindingSource));
    }

	/*
	 * Add new ComponentClassTransformWorker's
	 */
    public static void contributeComponentClassTransformWorker(
    		OrderedConfiguration<ComponentClassTransformWorker> configuration, 
    		PropertyAccess propertyAccess) 
    {
    	configuration.add("InjectSelectionModel", new InjectSelectionModelWorker(propertyAccess), 
    			"after:Inject*");
    }
    
    /*
     * Make sure that all pages need to be access via HTTPS
     */
    public void contributeMetaDataLocator(MappedConfiguration<String, String> configuration,
    		@Inject @Value("${djigzo.https.all}") boolean https)
    {
    	if (https) {
    		configuration.add(MetaDataConstants.SECURE_PAGE, Boolean.toString(https));
    	}
    }
    
    /*
     * Add new ComponentClassTransformWorker's
     */
    public static void contributeComponentClassTransformWorker(Response response,
            OrderedConfiguration<ComponentClassTransformWorker> configuration) 
    {
        configuration.add("DisableHttpCacheWorker", new DisableHttpCacheWorker(response));
    }    
    
    /*
     * FragmentMarkupRenderer is a pipline service that can add HTML content to strings used for
     * example by the logging pages to highlight certain details etc. 
     */
    public FragmentMarkupRenderer buildFragmentMarkupRenderer(PipelineBuilder pipelineBuilder, Logger logger, 
            List<FragmentMarkupRendererFilter> configuration, final FragmentMarkupRenderContent contentRenderer)
    {
        FragmentMarkupRenderer terminator = new FragmentMarkupRenderer()
        {
            @Override
			public void renderMarkup(StrBuilder builder, FragmentMarkupRendererRegistry meta) 
			{
				contentRenderer.render(builder);
			}
        };

        return pipelineBuilder.build(logger, FragmentMarkupRenderer.class, FragmentMarkupRendererFilter.class, 
        		configuration, terminator);
    }
    
    /*
     * Adds FragmentMarkupRenderer's to the FragmentMarkupRenderer pipeline (See buildFragmentMarkupRenderer)
     */
    public void contributeFragmentMarkupRenderer(OrderedConfiguration<FragmentMarkupRendererFilter> filters,
            SymbolSource symbolSource)
    {
    	/*
    	 * Add a markup rendered that will highlight the search term
    	 */
        filters.add("HighlightSearch", new InlineAddClassPatternFromRegistry(SystemRendererTags.FIILTER, 
    	        "highlight-filter" /* CSS class name */));

        /*
         * Dynamically register highlight patterns (read from the properties)
         */
        String patternNames; 
            
        try {
            patternNames = symbolSource.valueForSymbol(HIGHLIGHTS_PATTERNS_SYMBOL);
        }
        catch (RuntimeException e) {
            /*
             * Thrown when the symbol cannot be found
             */
            logger.warn("symbol {} not found", HIGHLIGHTS_PATTERNS_SYMBOL);
            
            return;
        }
        
        if (patternNames != null)
        {
            for (String patternName : StringUtils.split(patternNames, ','))
            {
                patternName = StringUtils.trimToEmpty(patternName);
                
                String pattern;

                try {
                    pattern = symbolSource.valueForSymbol(patternName);
                }
                catch (RuntimeException e) {
                    /*
                     * Thrown when the symbol cannot be found
                     */
                    logger.warn("pattern with name {} not found", patternName);
                    
                    continue;
                }
                
                String[] regExp = RegExprUtils.splitRegExp(pattern);
                
                if (regExp == null)
                {
                    logger.warn("pattern {} is not a valid regular expression/value pattern");
                    
                    continue;
                }
                
                filters.add(patternName, new InlineAddClassPattern(Pattern.compile(regExp[0]), 
                        regExp[1] /* CSS class name */));
            }
        }
    }
    
    /*
     * Injects a X-UA-Compatible compatibility header to make IE9 compatible with Prototype used by DJIGZO.
     */
    public static void contributeRequestHandler(OrderedConfiguration<RequestFilter> configuration) {
        configuration.add("IECompatibilityHeaderInjector", new IECompatibilityHeaderInjector());
    }     
}
