/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 *
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar,
 * wsdl4j-1.6.1.jar (or modified versions of these libraries),
 * containing parts covered by the terms of Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to
 * convey the resulting work.
 */
package mitm.djigzo.web.pages.admin;

import java.util.LinkedList;
import java.util.List;

import mitm.application.djigzo.admin.FactoryRoles;
import mitm.common.ws.WebServiceCheckedException;
import mitm.djigzo.web.beans.AdminBean;
import mitm.djigzo.web.beans.impl.AdminBeanImpl;
import mitm.djigzo.web.common.InjectSelectionModel;
import mitm.djigzo.web.common.security.Admin;
import mitm.djigzo.web.common.security.AdminManager;
import mitm.djigzo.web.common.security.Roles;
import mitm.djigzo.web.pages.Admins;

import org.apache.tapestry5.Asset;
import org.apache.tapestry5.PersistenceConstants;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.IncludeJavaScriptLibrary;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.Path;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.beaneditor.Validate;
import org.apache.tapestry5.corelib.components.Checkbox;
import org.apache.tapestry5.corelib.components.Form;
import org.apache.tapestry5.corelib.components.Palette;
import org.apache.tapestry5.corelib.components.PasswordField;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.annotation.Secured;

@IncludeJavaScriptLibrary("classpath:mitm/djigzo/web/validators/word.js")
@IncludeStylesheet("context:styles/pages/admin/adminAdd.css")
@Secured({FactoryRoles.ROLE_ADMIN})
public class AdminEdit
{
	private final static Logger logger = LoggerFactory.getLogger(AdminEdit.class);

	@Inject
	private AdminManager adminManager;

	@Inject
	private Roles roles;

	@InjectSelectionModel
    private List<String> availableRoles;

    private List<String> selectedRoles;

    private AdminBean adminBean;

    /*
     * True if changes were applied
     */
    @Persist(PersistenceConstants.FLASH)
    private boolean applied;

	@Component
    private Form form;

	@SuppressWarnings("unused")
	@Component(id = "roles", parameters = {"encoder=availableRolesValueEncoder", "selected=selectedRoles",
		"model=availableRolesSelectionModel", "deselect=removeIcon", "select=addIcon"})
	private Palette rolesPalette;

	@Component(id = "password", parameters = {"value = password"})
	private PasswordField passwordField;

	@SuppressWarnings("unused")
	@Component(id = "passwordRepeat", parameters = {"value = passwordRepeat"})
	private PasswordField passwordRepeatField;

	@SuppressWarnings("unused")
	@Component(id = "enabled", parameters = {"value = enabled"})
	private Checkbox enabledField;

	@Inject
    @Path("context:icons/add-list.png")
    private Asset addIcon;

	@Inject
    @Path("context:icons/remove-list.png")
    private Asset removeIcon;

	@Validate("minlength=6,maxlength=32")
	private String password;

	@Validate("minlength=6,maxlength=32")
	private String passwordRepeat;

	private boolean enabled;

    public boolean isApplied() {
        return applied;
    }

	@SetupRender
	@Secured({FactoryRoles.ROLE_ADMIN})
	protected void setupRender() {
		/*
		 * Empty on purpose
		 */
	}

	public AdminBean getAdminBean() {
		return adminBean;
	}

	public void setAdminBean(AdminBean adminBean) {
		this.adminBean = adminBean;
	}

	public Asset getRemoveIcon() {
		return removeIcon;
	}

	public void setRemoveIcon(Asset removeIcon) {
		this.removeIcon = removeIcon;
	}

	public Asset getAddIcon() {
		return addIcon;
	}

	public void setAddIcon(Asset addIcon) {
		this.addIcon = addIcon;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPasswordRepeat() {
		return passwordRepeat;
	}

	public void setPasswordRepeat(String passwordRepeat) {
		this.passwordRepeat = passwordRepeat;
	}

	public boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public List<String> getAvailableRoles() {
		return availableRoles;
	}

	public void setAvailableRoles(List<String> availableRoles) {
		this.availableRoles = availableRoles;
	}

	public List<String> getSelectedRoles() {
		return selectedRoles;
	}

	public void setSelectedRoles(List<String> selectedRoles) {
		this.selectedRoles = selectedRoles;
	}

	private AdminBean createAdminBean(String username)
	throws WebServiceCheckedException
	{
		if (username == null) {
			return null;
		}

		Admin admin = adminManager.getAdmin(username);

		if (admin == null) {
			return null;
		}

		return new AdminBeanImpl(admin);
	}

    @Secured({FactoryRoles.ROLE_ADMIN})
	public Object onActivate(String username)
	throws WebServiceCheckedException
	{
		adminBean = createAdminBean(username);

		if (adminBean == null) {
			return Admins.class;
		}

		return null;
	}

	public String onPassivate()
	{
		String username = null;

		if (adminBean != null) {
			username = adminBean.getUsername();
		}

		return username;
	}

	/*
	 * GenericValueEncoder requires the roles to be set in onPrepare and not in @SetupRender.
	 * Need to find out why.
	 */
	public void onPrepare()
	throws WebServiceCheckedException
	{
		availableRoles = roles.getAvailableRoles();
		// provision for issue SEN-25
		availableRoles.remove(FactoryRoles.ROLE_DOMAIN_MANAGER);
		availableRoles.remove(FactoryRoles.ROLE_MOBILE_MANAGER);
		availableRoles.remove(FactoryRoles.ROLE_PKI_MANAGER);
		availableRoles.remove(FactoryRoles.ROLE_SMS_MANAGER);
		availableRoles.remove(FactoryRoles.ROLE_USER_MANAGER);
		//

		if (selectedRoles == null && adminBean != null)
		{
			selectedRoles = new LinkedList<String>();

			selectedRoles.addAll(adminBean.getRoles());
		}

		if (adminBean != null) {
			enabled = adminBean.isEnabled();
		}
	}

	public void onValidateForm()
	throws WebServiceCheckedException
	{
		/*
		 * Check if passwords are the same
		 */
		if (password != null && !password.equals(passwordRepeat))
		{
	        form.recordError(passwordField, "Passwords do not match.");

	        return;
		}

		/*
		 * Roles should always contain ROLE_USER
		 */
		if (!selectedRoles.contains(FactoryRoles.ROLE_LOGIN))
		{
	        form.recordError(passwordField, "Every admin should have at least the " +
	        		FactoryRoles.ROLE_LOGIN + " role");

	        return;
		}
	}

	public void onSuccess()
	throws WebServiceCheckedException
	{
		Admin admin = adminManager.getAdmin(adminBean.getUsername());

		if (admin == null)
		{
			logger.warn("Admin " + adminBean.getUsername() + " not found.");

			return;
		}

		if (password != null) {
			admin.setPassword(password);
		}

		admin.setRoles(selectedRoles);
		admin.setEnabled(enabled);

		applied = true;
	}

    /*
     * Event handler called when the cancel button is pressed.
     */
    public Object onCancel() {
		return Admins.class;
	}
}
