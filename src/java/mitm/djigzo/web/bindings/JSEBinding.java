/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.bindings;

import java.lang.annotation.Annotation;

import mitm.common.util.Check;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.tapestry5.Binding;
import org.apache.tapestry5.internal.bindings.AbstractBinding;

/**
 * Tapestry binding that escapes Javascript entities so quotes will not break the javascript that is
 * 'injected'.
 * 
 * @author Martijn Brinkers
 *
 */
public class JSEBinding extends AbstractBinding
{
	private final Binding delegate;
	
	public JSEBinding(Binding delegate)
	{
		Check.notNull(delegate, "delegate");
		
		this.delegate = delegate;
	}
	
    @Override
    public Object get() 
    {
    	Object value = delegate.get();
    	
    	if (value instanceof String) 
    	{
    		/*
    		 * We will escape all javascript entities. A problem arises when the string to escape
    		 * contains nested quotes because escaping nested quotes require double (or more) escaping
    		 * depending on the level. The level also depends on external information which we do not
    		 * have here (Note: we can create a special JSEBinding that also provides the level). 
    		 * Right now I assume that the string will be embedded inside a double quoted (") string.
    		 * We therefore need to escape " to \\".
    		 */
    		value = StringEscapeUtils.escapeJavaScript((String) value);
    		
    		/*
    		 * Do second level escape of \" to \\"
    		 */
    		value = ((String) value).replaceAll("\\\"", "\\\\\"");
    	}
    	
    	return value;
    }
    
    @Override
    public boolean isInvariant() {
        return delegate.isInvariant();
    }

    @Override
    public Class<?> getBindingType() {
        return delegate.getBindingType();
    }

    @Override
    public <T extends Annotation> T getAnnotation(Class<T> annotationClass) {
        return delegate.getAnnotation(annotationClass);
    }
}
