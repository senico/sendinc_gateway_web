/*
 * Copyright (c) 2009-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.pages.ca;

import mitm.application.djigzo.admin.FactoryRoles;
import mitm.application.djigzo.ws.CAWS;
import mitm.application.djigzo.ws.X509CertificateDTO;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.ws.WebServiceCheckedException;
import mitm.djigzo.web.beans.CertificateType;
import mitm.djigzo.web.beans.X509CertificateBean;
import mitm.djigzo.web.beans.impl.X509CertificateBeanImpl;
import mitm.djigzo.web.components.CACertificateSelect;

import org.apache.commons.lang.StringUtils;
import org.apache.tapestry5.PersistenceConstants;
import org.apache.tapestry5.annotations.Cached;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.annotation.Secured;

@Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_PKI_MANAGER})
@IncludeStylesheet("context:styles/pages/ca/caSelect.css")
public class CASelect
{    
    private final static Logger logger = LoggerFactory.getLogger(CASelect.class);
    
    @Inject
    private CAWS caWS;

    @Component(parameters = {"selectedCertificate = prop:selectedCertificate"})
    private CACertificateSelect certificateSelect;
    
    /*
     * True if changes were applied
     */
    @Persist(PersistenceConstants.FLASH)
    private boolean applied;
    
    @SetupRender
    @Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_PKI_MANAGER})
    protected void setupRender()
    {
        /*
         * Empty on purpose
         */
    }

    public boolean isApplied() {
        return applied;
    }

    @Cached
    public X509CertificateBean getSelectedCertificate()
    throws HierarchicalPropertiesException, WebServiceCheckedException
    {
        X509CertificateBean selectedCertificate = null;
        
        X509CertificateDTO certificateDTO = caWS.getActiveCA();
        
        if (certificateDTO != null)
        {
            selectedCertificate = new X509CertificateBeanImpl(certificateDTO, 
                    CertificateType.SELECTED);
        }
        else {
            logger.warn("CA certificate with not found.");
        }
        
        return selectedCertificate;
    }

    public void onSuccess()
    throws HierarchicalPropertiesException, WebServiceCheckedException
    {
        String thumbprintSelected = certificateSelect.getSelected();
        
        if (StringUtils.isNotBlank(thumbprintSelected))
        {
            caWS.setActiveCA(thumbprintSelected);
            
            applied = true;
        }
    }
    
    /*
     * Event handler called when the cancel button is pressed.
     */
    protected Object onCancel() {
        return CAIssue.class;
    }    
}
