/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.entities.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import mitm.application.djigzo.ws.HierarchicalPropertiesWS;
import mitm.application.djigzo.ws.UserPreferencesDTO;
import mitm.application.djigzo.ws.UserPreferencesWS;
import mitm.application.djigzo.ws.X509CertificateDTO;
import mitm.common.util.Check;
import mitm.common.ws.WebServiceCheckedException;
import mitm.djigzo.web.entities.SaveableUserProperties;
import mitm.djigzo.web.entities.UserPreferences;

/**
 * UserPreferencesImpl is not thread safe.

 * @author Martijn Brinkers
 *
 */
public class UserPreferencesImpl implements UserPreferences
{
	private final UserPreferencesWS userPreferencesWS;
	private final HierarchicalPropertiesWS hierarchicalPropertiesWS; 
	private final UserPreferencesDTO userPreferencesDTO;
	
	public UserPreferencesImpl(UserPreferencesWS userPreferencesWS, 
			HierarchicalPropertiesWS hierarchicalPropertiesWS, UserPreferencesDTO userPreferencesDTO) 
	{
		Check.notNull(userPreferencesWS, "userPreferencesWS");
		Check.notNull(hierarchicalPropertiesWS, "hierarchicalPropertiesWS");
		Check.notNull(userPreferencesDTO, "userPreferencesDTO");

		this.userPreferencesWS = userPreferencesWS;
		this.hierarchicalPropertiesWS = hierarchicalPropertiesWS;
		this.userPreferencesDTO = userPreferencesDTO;
	}
	
    @Override
    public List<X509CertificateDTO> getCertificates()
    throws WebServiceCheckedException
    {
    	return userPreferencesWS.getCertificates(userPreferencesDTO);
    }

    @Override
    public void setCertificates(Collection<String> thumbprints)
    throws WebServiceCheckedException
    {
    	List<String> list = new ArrayList<String>(thumbprints);
    	
    	userPreferencesWS.setCertificates(userPreferencesDTO, list);
    }
    
    @Override
    public List<X509CertificateDTO> getInheritedCertificates()
    throws WebServiceCheckedException
    {
    	return userPreferencesWS.getInheritedCertificates(userPreferencesDTO);
    }
    
    @Override
    public List<X509CertificateDTO> getNamedCertificates(String name)
    throws WebServiceCheckedException
    {
        return userPreferencesWS.getNamedCertificates(userPreferencesDTO, name);
    }

    @Override
    public void setNamedCertificates(String name, Collection<String> thumbprints)
    throws WebServiceCheckedException
    {
        List<String> list = new ArrayList<String>(thumbprints);
        
        userPreferencesWS.setNamedCertificates(userPreferencesDTO, name, list);
    }
    
    @Override
    public List<X509CertificateDTO> getInheritedNamedCertificates(String name)
    throws WebServiceCheckedException
    {
        return userPreferencesWS.getInheritedNamedCertificates(userPreferencesDTO, name);
    }
    
    @Override
    public X509CertificateDTO getKeyAndCertificate()
    throws WebServiceCheckedException
    {
    	return userPreferencesWS.getKeyAndCertificate(userPreferencesDTO);
    }

    @Override
    public void setKeyAndCertificate(String thumbprint)
    throws WebServiceCheckedException
    {
    	userPreferencesWS.setKeyAndCertificate(userPreferencesDTO, thumbprint);
    }
    
    @Override
    public List<X509CertificateDTO> getInheritedKeyAndCertificates()
    throws WebServiceCheckedException
    {
    	return userPreferencesWS.getInheritedKeyAndCertificates(userPreferencesDTO);
    }

    @Override
	public SaveableUserProperties getProperties() 
	throws WebServiceCheckedException 
	{
		return new SaveableUserPropertiesImpl(new CachingSaveableHierarchicalPropertiesImpl(
                hierarchicalPropertiesWS, userPreferencesDTO));
	}	
}
