/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.entities.impl;

import mitm.application.djigzo.UserLocality;
import mitm.application.djigzo.ws.DomainWS;
import mitm.application.djigzo.ws.HierarchicalPropertiesWS;
import mitm.application.djigzo.ws.UserPreferencesDTO;
import mitm.application.djigzo.ws.UserPreferencesWS;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.util.Check;
import mitm.common.ws.WebServiceCheckedException;
import mitm.djigzo.web.entities.Domain;
import mitm.djigzo.web.entities.UserPreferences;

/**
 * DomainImpl is not thread safe.
 * 
 * @author Martijn Brinkers
 *
 */
public class DomainImpl implements Domain
{
	private final DomainWS domainWS;
	private final UserPreferencesWS userPreferencesWS;
	private final HierarchicalPropertiesWS hierarchicalPropertiesWS;
    private final String domain;

    private UserLocality userLocality;
    private boolean inUse;
	
	public DomainImpl(DomainWS domainWS, UserPreferencesWS userPreferencesWS, 
			HierarchicalPropertiesWS hierarchicalPropertiesWS, String domain,
			UserLocality userLocality, boolean inUse)
	{
		Check.notNull(domainWS, "domainWS");
		Check.notNull(userPreferencesWS, "userPreferencesWS");
		Check.notNull(hierarchicalPropertiesWS, "hierarchicalPropertiesWS");
		Check.notNull(domain, "domain");
		
		this.domainWS = domainWS;
		this.userPreferencesWS = userPreferencesWS;
		this.hierarchicalPropertiesWS = hierarchicalPropertiesWS;
		this.domain = domain;
		this.userLocality = userLocality;
		this.inUse = inUse;
	}
	
    @Override
	public String getDomain() {
		return domain;
	}
	
    @Override
    public UserLocality getUserLocality()
    throws WebServiceCheckedException
    {
        try {
            if (userLocality == null) {
                userLocality = getPreferences().getProperties().getUserLocality();
            }
            return userLocality;
        }
        catch(HierarchicalPropertiesException e) {
            throw new WebServiceCheckedException(e);
        }
    }
	
    @Override
	public boolean isInUse() {
	    return inUse;
	}
	
    @Override
    public UserPreferences getPreferences()
    throws WebServiceCheckedException
    {
    	UserPreferencesDTO userPreferencesDTO = domainWS.getDomainPreferences(domain);
    
    	return new UserPreferencesImpl(userPreferencesWS, hierarchicalPropertiesWS, 
    			userPreferencesDTO);
    }
}
