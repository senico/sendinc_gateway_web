/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.pages.crl;

import mitm.application.djigzo.ws.CRLTrustCheckResult;
import mitm.application.djigzo.ws.X509CRLDTO;
import mitm.application.djigzo.ws.X509CRLStoreWS;
import mitm.common.security.asn1.ObjectEncoding;
import mitm.common.util.MiscStringUtils;
import mitm.common.ws.WebServiceCheckedException;
import mitm.djigzo.web.beans.X509CRLBean;
import mitm.djigzo.web.beans.impl.X509CRLBeanImpl;
import mitm.djigzo.web.common.CRLDownloadConst;
import mitm.djigzo.web.common.ContentTypes;
import mitm.djigzo.web.common.streamresponse.ByteArrayFileResponse;

import org.apache.tapestry5.ComponentResources;
import org.apache.tapestry5.StreamResponse;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.beaneditor.BeanModel;
import org.apache.tapestry5.corelib.components.BeanDisplay;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.BeanModelSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@IncludeStylesheet("context:styles/pages/crl/crlView.css")
public class CRLView 
{
	private final static Logger logger = LoggerFactory.getLogger(CRLView.class);
	
	/*
	 * The CRL being viewed
	 */
	private X509CRLBean crl;

	@Inject
	@Property
	private X509CRLStoreWS clrStoreWS;
	
    @Inject
    private BeanModelSource modelSource;
	
    @Inject
    private ComponentResources resources;

    @Inject
    private X509CRLStoreWS crlStore;
    
	@SuppressWarnings("unused")
	@Component(parameters = "model=prop:model")
	private BeanDisplay crlDisplay;
    
	public void onActivate(String thumbprint)
	throws WebServiceCheckedException 
	{
		if (thumbprint != null)
		{
			X509CRLDTO crlDTO = clrStoreWS.getCRL(thumbprint);
			
			if (crlDTO != null) {
				crl = new X509CRLBeanImpl(crlDTO);
			}
		}
	}
	
	public String onPassivate()
	{
		String thumbprint = null;
		
		if (crl != null) {
			thumbprint = crl.getThumbprint();
		}
		
		return thumbprint;
	}
	
    @OnEvent(component="downloadCRL")
    protected Object downloadCRL() 
    throws WebServiceCheckedException 
    {
    	StreamResponse response = null;
    	
    	if (crl == null) 
    	{
    		logger.warn("CRL is null.");
    		/*
    		 * Can happen when the CRL is deleted
    		 */
    		return null;
    	}
    	
    	ObjectEncoding crlEncoding = CRLDownloadConst.DEFAUL_ENCODING;
    	
		byte[] encodedCRL = crlStore.getEncodedCRL(crl.getThumbprint(),	crlEncoding);

		if (encodedCRL == null) 
		{
    		logger.warn("encoded CRL is null.");
    		
			/*
			 * The CRL was not found
			 */
			return null;
		}
		
		String filename = CRLDownloadConst.getFilename(true /* single CRL */, crlEncoding);
		
		response = new ByteArrayFileResponse(encodedCRL, ContentTypes.X_DOWNLOAD, filename);
		
    	return response;
    }

    @OnEvent(component="downloadCRLEntries")
    protected Object downloadCRLEntries() 
    throws WebServiceCheckedException 
    {
        StreamResponse response = null;
        
        if (crl == null) 
        {
            logger.warn("CRL is null.");
            /*
             * Can happen when the CRL is deleted
             */
            return null;
        }
        
        String crlEntries = crlStore.getCRLEntries(crl.getThumbprint());

        if (crlEntries == null)
        {
            logger.warn("CRL was not found.");
            
            return null;
        }
        
        String filename = "crl-entries-" + crl.getThumbprintSHA1() + ".txt";
        
        response = new ByteArrayFileResponse(MiscStringUtils.toAsciiBytes(crlEntries), 
                ContentTypes.X_DOWNLOAD, filename);
        
        return response;
    }
    
	public X509CRLBean getCRL() {
		return crl;
	}
	
	public BeanModel<X509CRLBeanImpl> getModel()
	{
		BeanModel<X509CRLBeanImpl> model = modelSource.createDisplayModel(X509CRLBeanImpl.class, 
				resources.getMessages());
		
		model.add("info", null);
		
		return model;
	}
	
	public String getTrustInfo() 
	throws WebServiceCheckedException 
	{
		CRLTrustCheckResult trustResult = crlStore.checkTrust(crl.getThumbprint());
		
		return trustResult.getFailureMessage();
	}
}
