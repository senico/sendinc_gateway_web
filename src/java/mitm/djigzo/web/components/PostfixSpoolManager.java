/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.components;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import mitm.application.djigzo.ws.PostfixSpoolManagerWS;
import mitm.common.postfix.PostfixQueueItem;
import mitm.common.ws.WebServiceCheckedException;
import mitm.djigzo.web.grid.PostfixSpoolGridDataSource;

import org.apache.commons.lang.StringUtils;
import org.apache.tapestry5.Asset;
import org.apache.tapestry5.ComponentResources;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Path;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.beaneditor.BeanModel;
import org.apache.tapestry5.corelib.components.Grid;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.ioc.annotations.Value;
import org.apache.tapestry5.json.JSONObject;
import org.apache.tapestry5.services.BeanModelSource;
import org.apache.tapestry5.services.Request;

@IncludeStylesheet("context:styles/components/postfixSpoolManager.css")
public class PostfixSpoolManager 
{
    public static final String QUEUE_ID_CLICKED_EVENT = "postfixSpoolManager-queueID-clicked";
	
	@Inject
	private PostfixSpoolManagerWS spoolManager;
	
	@Inject
    private BeanModelSource beanModelSource;

    @Inject
    private ComponentResources resources;

    @Inject
    private Request request;
    
    @Inject
    @Value("${postfixSpool.rowsPerPage}")
    private int defaultRowsPerPage; 
    
    @Persist
    private Integer rowsPerPage;    
    
	/*
	 * Set of the selected queue IDs
	 */
	@Persist
	private Set<String> selected;
    
    /*
     * The queue item currently being rendered
     */
    private PostfixQueueItem queueItem;
    
    /*
     * Cache dataSource because we need it to check for failures
     */
    private PostfixSpoolGridDataSource dataSource;
    
	@SuppressWarnings("unused")
	@Component(id = "postfixSpoolGrid", parameters = {"source=gridDataSource", "volatile=true", 
			"model=model", "row=queueItem", "reorder=delete,select,queueID,queueStatus," +
					"messageSize,arrivalTime,sender,recipients,failure", "rowsPerPage=prop:rowsPerPage"})
    private Grid grid;
	
    @Component(id = "filter")
    private RegExprFilter filter;
	
    @SuppressWarnings("unused")
	@Inject
    @Path("context:icons/cross.png")
    @Property
    private Asset deleteAsset;

    @SetupRender
    public void setupRender() 
    {
        if (selected == null) {
            selected = Collections.synchronizedSet(new HashSet<String>());
        }
        else {
            selected.clear();
        }
    }
    
    public PostfixSpoolGridDataSource getGridDataSource()
    {
        dataSource = new PostfixSpoolGridDataSource(spoolManager, filter.getPattern(false));
        
        return dataSource;
	}

    public BeanModel<PostfixQueueItem> getModel() 
    {
        BeanModel<PostfixQueueItem> model = beanModelSource.createDisplayModel(PostfixQueueItem.class, 
                resources.getMessages());
        
        model.add("recipients", null);
        model.add("delete", null);
        model.add("select", null);
        
        /*
         * Disable all sorting
         */
        for (String column : model.getPropertyNames()) {
        	model.get(column).sortable(false);
        }
        
        return model;
    }

    public boolean isFailure() {
        return dataSource != null && dataSource.isFailure();
    }

    public String getFailureMessage() {
        return dataSource != null ? dataSource.getFailureMessage() : null;
    }
    
	public PostfixQueueItem getQueueItem() {
		return queueItem;
	}

	public void setQueueItem(PostfixQueueItem queueItem) {
		this.queueItem = queueItem;
	}
	
	public boolean getSelect() {
		return selected.contains(queueItem.getQueueID());
	}
	
	public void setSelect(boolean value) 
	{
		/*
		 * Ignored. Checkbox values will be handled using EventCheckbox (see onEventActionLink)
		 */
	}
	
    /*
     * Ajax event handler called when the checkbox is clicked. 
     */
    public void onCheckboxEvent()
    {
    	String data = request.getParameter("data");

    	if (StringUtils.isEmpty(data)) {
    		throw new IllegalArgumentException("data is missing.");
    	}
    	
    	JSONObject json = new JSONObject(data);
    	    	    	
    	String element = json.getString("element");
    	boolean checked = json.getBoolean("checked"); 
    	
    	if (checked) {
    		selected.add(element);
    	}
    	else {
    		selected.remove(element);
    	}
    }
    
    @OnEvent(component="deleteMessage")
    protected void deleteMessage(String queueID) 
    throws WebServiceCheckedException 
    {
    	spoolManager.delete(queueID);
    }
    
    @OnEvent(component="deleteSelected")
    protected void deleteSelected() 
    throws WebServiceCheckedException 
    {
        /*
         * Note: we need to clone the set to make sure that concurrent modifications
         * are possible.
         */
        String[] ids = selected.toArray(new String[]{});
        
        for (String queueID : ids) {
            spoolManager.delete(queueID);
        }
    }

    @OnEvent(component="holdSelected")
    protected void holdSelected() 
    throws WebServiceCheckedException 
    {
        /*
         * Note: we need to clone the set to make sure that concurrent modifications
         * are possible.
         */
        String[] ids = selected.toArray(new String[]{});

        for (String queueID : ids) {
            spoolManager.hold(queueID);
        }
    }

    @OnEvent(component="releaseSelected")
    protected void releaseSelected() 
    throws WebServiceCheckedException 
    {
        /*
         * Note: we need to clone the set to make sure that concurrent modifications
         * are possible.
         */
        String[] ids = selected.toArray(new String[]{});

        for (String queueID : ids) {
		    	spoolManager.release(queueID);
        }
    }

    @OnEvent(component="requeueSelected")
    protected void requeueSelected() 
    throws WebServiceCheckedException 
    {
        /*
         * Note: we need to clone the set to make sure that concurrent modifications
         * are possible.
         */
        String[] ids = selected.toArray(new String[]{});

        for (String queueID : ids) {
            spoolManager.requeue(queueID);
        }
    }

    @OnEvent(component="flush")
    protected void flush() 
    throws WebServiceCheckedException 
    {
    	spoolManager.flush();
    }
    
    @OnEvent(component="queueIDLink")
    protected void queueIDClicked(String queueID) 
    {
		resources.triggerEvent(QUEUE_ID_CLICKED_EVENT, new Object[]{queueID}, null);
    }

    public int getRowsPerPage()
    {
        return rowsPerPage != null ? rowsPerPage : defaultRowsPerPage;
    }
    
    public void setRowsPerPage(int rowsPerPage) {
        this.rowsPerPage = rowsPerPage;
    }
}
