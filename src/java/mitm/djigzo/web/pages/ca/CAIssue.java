/*
 * Copyright (c) 2009-2012, Martijn Brinkers, Djigzo.
 *
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar,
 * wsdl4j-1.6.1.jar (or modified versions of these libraries),
 * containing parts covered by the terms of Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to
 * convey the resulting work.
 */
package mitm.djigzo.web.pages.ca;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import mitm.application.djigzo.admin.FactoryRoles;
import mitm.application.djigzo.ws.CAWS;
import mitm.application.djigzo.ws.CertificateRequestHandlerDTO;
import mitm.application.djigzo.ws.RequestParametersDTO;
import mitm.application.djigzo.ws.SendCertificatesDTO;
import mitm.application.djigzo.ws.X500PrincipalDTO;
import mitm.application.djigzo.ws.X509CertificateDTO;
import mitm.common.mail.EmailAddressUtils;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.security.ca.handlers.BuiltInCertificateRequestHandler;
import mitm.common.ws.WebServiceCheckedException;
import mitm.djigzo.web.beans.CASettingsBean;
import mitm.djigzo.web.beans.SMSPropertiesBean;
import mitm.djigzo.web.beans.StringProperty;
import mitm.djigzo.web.beans.UserBean;
import mitm.djigzo.web.beans.impl.CASettingsBeanImpl;
import mitm.djigzo.web.beans.impl.SMSPropertiesBeanImpl;
import mitm.djigzo.web.beans.impl.UserBeanImpl;
import mitm.djigzo.web.common.CertificateSelectionType;
import mitm.djigzo.web.common.GenericSelectionModel;
import mitm.djigzo.web.common.SignatureAlgorithm;
import mitm.djigzo.web.components.GeneratePasswordEdit;
import mitm.djigzo.web.components.X500PrincipalEdit;
import mitm.djigzo.web.entities.GlobalPreferencesManager;
import mitm.djigzo.web.entities.User;
import mitm.djigzo.web.entities.UserManager;
import mitm.djigzo.web.pages.Users;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.tapestry5.PersistenceConstants;
import org.apache.tapestry5.SelectModel;
import org.apache.tapestry5.annotations.Cached;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.Mixins;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.corelib.components.Checkbox;
import org.apache.tapestry5.corelib.components.Form;
import org.apache.tapestry5.corelib.components.Select;
import org.apache.tapestry5.corelib.components.TextField;
import org.apache.tapestry5.internal.services.LinkFactory;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.json.JSONObject;
import org.apache.tapestry5.services.Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.annotation.Secured;

@Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_PKI_MANAGER})
@IncludeStylesheet("context:styles/pages/ca/caIssue.css")
public class CAIssue
{
    private final static Logger logger = LoggerFactory.getLogger(CAIssue.class);

    @Inject
    private CAWS caWS;

    @Inject
    private GlobalPreferencesManager globalPreferencesManager;

    @Inject
    private UserManager userManager;

    @Inject
    private LinkFactory linkFactory;

    @Inject
    private Request request;

    /*
     * Validity in days of the certificate
     */
    @Persist(PersistenceConstants.FLASH)
    private Integer validity;

    /*
     * The length of the generated key
     */
    @Persist(PersistenceConstants.FLASH)
    private Integer keyLength;

    /*
     * Algorithm for signing the certificate
     */
    @Persist(PersistenceConstants.FLASH)
    private SignatureAlgorithm signatureAlgorithm;

    /*
     * If true the PFX file will be sent by email to the recipient
     */
    @Persist
    private Boolean sendByEmail;

    /*
     * If true, the crlDistributionPoint will be added
     */
    @Persist(PersistenceConstants.FLASH)
    private Boolean addCRLDistributionPoint;

    /*
     * The CRL distribution point for the intermediate certificate
     */
    @Persist(PersistenceConstants.FLASH)
    private String crlDistributionPoint;

    /*
     * The selected Certificate Request Handler
     */
    @Persist(PersistenceConstants.FLASH)
    private String certificateRequestHandler;

    /*
     * If true a user object will be added when a certificate is ussued
     */
    @Persist(PersistenceConstants.FLASH)
    private Boolean addUser;

    /*
     * The password for the generated PFX file
     */
    @Persist(PersistenceConstants.FLASH)
    private String password;

    /*
     * Is used to know where to goto when closed is pressed
     */
    private CertificateSelectionType certificateSelectionType;

    @Component(id = "subjectPrincipal", parameters = {"emailRequired=true", "emailReadOnly=prop:emailReadOnly"})
    private X500PrincipalEdit subjectPrincipal;

    @SuppressWarnings("unused")
    @Component(id = "validity", parameters = {"value = validity", "validate = required, min=1, max=14600"})
    private TextField validityField;

    @SuppressWarnings("unused")
    @Component(id = "keyLength", parameters = {"value = keyLength"})
    private Select keyLengthField;

    @Component(id = "sendByEmail", parameters = {"value = sendByEmail", "invert = true",
            "idsToDisable = literal:passwordEdit sendBySMS storePFXPassword"})
    @Mixins({"DisableOnCheck"})
    private Checkbox sendByEmailField;

    @Component(id = "password", parameters = {"value = password", "validate = minLength=6,maxLength=255",
            "passwordLength=prop:passwordLength"})
    private GeneratePasswordEdit passwordField;

    @SuppressWarnings("unused")
    @Component(id = "signatureAlgorithm", parameters = {"value = signatureAlgorithm",
            "blankOption = NEVER", "validate = required"})
    private Select signatureAlgorithmField;

    @Component(id = "crlDistributionPoint", parameters = {"value = crlDistributionPoint", "validate = maxLength=1024"})
    private TextField crlDistributionPointField;

    @SuppressWarnings("unused")
    @Component(id = "addCRLDistributionPoint", parameters = {"value = addCRLDistributionPoint"})
    private Checkbox addCRLDistributionPointField;

    @SuppressWarnings("unused")
    @Component(id = "certificateRequestHandler", parameters = {"value = certificateRequestHandler",
            "model = certificateRequestHandlerModel", "blankOption = NEVER", "validate = required"})
    private Select certificateRequestHandlerField;

    @SuppressWarnings("unused")
    @Component(id = "addUser", parameters = {"value = addUser"})
    private Checkbox addUserField;

    @Component
    private Form form;

    @Persist
    private boolean advanced;

    /*
     * True if certificate was issued
     */
    @Persist(PersistenceConstants.FLASH)
    private boolean certificateIssued;

    /*
     * True if certificate was not directly issued but is pending. This happens when the certificate issuer does
     * not directly issue the certificate
     */
    @Persist(PersistenceConstants.FLASH)
    private boolean certificatePending;

    /*
     * If the certificate was not directly issued and is pending the certificate cannot be sent by email. If the
     * user selected this option, the user should be warned.
     */
    @Persist(PersistenceConstants.FLASH)
    private boolean sentByEmailFailed;

    /*
     * True if and error has occurred
     */
    @Persist(PersistenceConstants.FLASH)
    private boolean error;

    /*
     * Error message if an error has occurred
     */
    @Persist(PersistenceConstants.FLASH)
    private String errorMessage;

    @SetupRender
    @Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_PKI_MANAGER})
    public void setupRender()
    throws HierarchicalPropertiesException, WebServiceCheckedException
    {
        subjectPrincipal.setCommonName(getCASettings().getDefaultCommonName());

        loadDefaults();
    }

    private void loadDefaults()
    throws HierarchicalPropertiesException, WebServiceCheckedException
    {
        if (validity == null) {
            validity = getCASettings().getValidity();
        }

        if (keyLength == null) {
            keyLength = getCASettings().getKeyLength();
        }

        if (addCRLDistributionPoint == null) {
            addCRLDistributionPoint = getCASettings().isAddCRLDistributionPoint();
        }

        if (crlDistributionPoint == null) {
            crlDistributionPoint = getCASettings().getCRLDistributionPoint();
        }

        if (signatureAlgorithm == null) {
            signatureAlgorithm = SignatureAlgorithm.fromName(getCASettings().getSignatureAlgorithm());
        }

        if (signatureAlgorithm == null) {
            /*
             * signatureAlgorithm not found to use default
             */
            signatureAlgorithm = SignatureAlgorithm.SHA1_WITH_RSA;
        }

        /*
         * Show advanced when a CRL dist point is added
         */
        if (StringUtils.isNotEmpty(crlDistributionPoint) && addCRLDistributionPoint) {
            advanced = true;
        }

        if (certificateRequestHandler == null) {
            certificateRequestHandler = getCASettings().getCertificateRequestHandler();
        }

        if (StringUtils.isEmpty(certificateRequestHandler)) {
            /*
             * Use default Certificate Request Handler
             */
            certificateRequestHandler = BuiltInCertificateRequestHandler.NAME;
        }

        if (sendByEmail == null) {
            sendByEmail = false;
        }

        if (addUser == null) {
            addUser = true;
        }
    }

    @Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_PKI_MANAGER})
    public void onActivate(String email)
    {
        this.subjectPrincipal.setEmail(email);
    }

    @Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_PKI_MANAGER})
    public void onActivate(String email, CertificateSelectionType certificateSelectionType)
    {
        this.subjectPrincipal.setEmail(email);
        this.certificateSelectionType = certificateSelectionType;
    }

    public List<Object> onPassivate()
    {
        List<Object> result = new LinkedList<Object>();

        if (subjectPrincipal.getEmail() != null) {
            result.add(subjectPrincipal.getEmail());
        }

        if (certificateSelectionType != null) {
            result.add(certificateSelectionType);
        }

        return result;
    }

    public SelectModel getCertificateRequestHandlerModel()
    throws WebServiceCheckedException
    {
        return new GenericSelectionModel<String>(new ArrayList<String>(getCertificateRequestHandlers()), null, null);
    }

    public boolean isEmailReadOnly() {
        return certificateSelectionType != null && StringUtils.isNotBlank(subjectPrincipal.getEmail());
    }

    @Cached
    public CASettingsBean getCASettings()
    throws HierarchicalPropertiesException, WebServiceCheckedException
    {
        return new CASettingsBeanImpl(globalPreferencesManager.getProperties());
    }

    @Cached
    public boolean isActiveCA()
    {
        boolean active = false;

        try {
            active = caWS.getActiveCA() != null;
        }
        catch (WebServiceCheckedException e) {
            logger.error("Error getting activeCA", e);
        }

        return active;
    }

    @Cached
    public List<String> getCertificateRequestHandlers()
    throws WebServiceCheckedException
    {
        List<String> result = new LinkedList<String>();

        List<CertificateRequestHandlerDTO> handlers = caWS.getCertificateRequestHandlers();

        for (CertificateRequestHandlerDTO handler : handlers)
        {
            if (handler.isEnabled()) {
                result.add(handler.getName());
            }
        }

        return result;
    }

    public int getPasswordLength()
    throws HierarchicalPropertiesException, WebServiceCheckedException
    {
        return getCASettings().getPasswordLength();
    }

    private X500PrincipalDTO createX500Principal(X500PrincipalEdit principalEdit)
    {
        X500PrincipalDTO principal = new X500PrincipalDTO();

        if (StringUtils.isNotEmpty(principalEdit.getEmail())) {
            principal.setEmail(Arrays.asList(principalEdit.getEmail()));
        }

        if (StringUtils.isNotEmpty(principalEdit.getOrganisation())) {
            principal.setOrganisation(Arrays.asList(principalEdit.getOrganisation()));
        }

        if (StringUtils.isNotEmpty(principalEdit.getCommonName())) {
            principal.setCommonName(Arrays.asList(principalEdit.getCommonName()));
        }

        if (StringUtils.isNotEmpty(principalEdit.getFirstName())) {
            principal.setGivenName(Arrays.asList(principalEdit.getFirstName()));
        }

        if (StringUtils.isNotEmpty(principalEdit.getLastName())) {
            principal.setSurname(Arrays.asList(principalEdit.getLastName()));
        }

        return principal;
    }

    private void issueCertificate()
    throws WebServiceCheckedException, HierarchicalPropertiesException
    {
        loadDefaults();

        X500PrincipalDTO subject = createX500Principal(subjectPrincipal);

        RequestParametersDTO requestParameters = new RequestParametersDTO(subject,
                subjectPrincipal.getEmail(),
                validity,
                keyLength,
                signatureAlgorithm.getName(),
                addCRLDistributionPoint ? crlDistributionPoint : null,
                certificateRequestHandler);

        X509CertificateDTO keyAndCertificate = caWS.requestCertificate(requestParameters,
                addUser, false /* add cert even if available */);

        if (keyAndCertificate != null)
        {
            certificateIssued = true;

            if (sendByEmail)
            {
                List<String> thumbprints = Collections.singletonList(keyAndCertificate.getThumbprint());
                String sender = getCASettings().getCAEmail();
                String recipient = subjectPrincipal.getEmail();

                String phoneNumber = null;

                SMSPropertiesBean smsProperties = getSMSProperties();

                if (smsProperties != null) {
                    phoneNumber = smsProperties.getSMSPhoneNumber().getValue();
                }

                SendCertificatesDTO sendParameters = new SendCertificatesDTO(thumbprints, password, sender, recipient);

                sendParameters.setPhoneNumber(phoneNumber);

                caWS.sendCertificates(sendParameters);

                setPFXPassword(password);
            }
        }
        else {
            /*
             * This can happen when the certificate issuer cannot immediately issue the certificate.
             */
            certificatePending = true;

            if (sendByEmail) {
                /*
                 * Because the certificate was not ready it has not been sent by email. We must warn the user.
                 */
                sentByEmailFailed = true;
            }
        }

        /*
         * Because the certificate has been issued we need to reset the password to make sure the user does
         * not click the apply button multiple times.
         */
        subjectPrincipal.setEmail("");
    }

    private void setPFXPassword(String password)
    throws WebServiceCheckedException, HierarchicalPropertiesException
    {
      throw new WebServiceCheckedException("PFX Password is not supported.");

        /*UserBean userBean = getUserBean();


        // If the user does not want to store the password we want to reset the password
        // for the user otherwise it's unlcear what the last password was.

        if (!storePFXPassword) {
            password = null;
        }


        // We only need to add a user if storePFXPassword is set

        if (userBean == null && storePFXPassword)
        {
            String email = EmailAddressUtils.canonicalizeAndValidate(
                    subjectPrincipal.getEmail(), true);

            if (email == null) {
                return;
            }

            userManager.addUser(email);

            User user = userManager.getUser(email, false);

            if (user == null) {
                return;
            }

            userBean = new UserBeanImpl(user);
        }


        // Only set the password if there is a user

        if (userBean != null)
        {
            StringProperty pfxPassword = userBean.getPreferences().getProperties().getPFXPassword();

            pfxPassword.setValue(password);

            // We must set inherit false otherwise the value will not be used.

            pfxPassword.setInherit(false);

            userBean.save();
        }*/
    }

    @Cached
    private User getUser()
    throws WebServiceCheckedException
    {
        String email = EmailAddressUtils.canonicalizeAndValidate(
                subjectPrincipal.getEmail(), true);

        if (email == null) {
            return null;
        }

        return userManager.getUser(email, false /* null if not exist */);
    }


    @Cached
    private UserBean getUserBean()
    throws WebServiceCheckedException
    {
        User user = getUser();

        if (user == null) {
            return null;
        }

        return new UserBeanImpl(user);
    }

    @Cached
    private SMSPropertiesBean getSMSProperties()
    throws WebServiceCheckedException, HierarchicalPropertiesException
    {
        User user = getUser();

        if (user == null) {
            return null;
        }

        return new SMSPropertiesBeanImpl(user.getPreferences().getProperties());
    }

    public void onValidateForm()
    throws HierarchicalPropertiesException, WebServiceCheckedException
    {
        if (StringUtils.isNotBlank(crlDistributionPoint) && addCRLDistributionPoint)
        {
            /*
             * Check if the crlDistributionPoint is a valid URL
             */
            try {
                new URL(crlDistributionPoint);
            }
            catch (MalformedURLException e)
            {
                form.recordError(crlDistributionPointField, "CRL distribution point is not a valid URL.");

                return;
            }
        }

        /*
         * If send by email is checked we have to make sure that the CA email
         * address is set
         */
        if (sendByEmail)
        {
            if (StringUtils.isBlank(getCASettings().getCAEmail()))
            {
                form.recordError(sendByEmailField, "The issued certificate can only be sent when " +
                        "the CA email address is specified. See CA settings.");
            }

            if (StringUtils.isBlank(password)) {
                form.recordError(passwordField.getPasswordEdit(), "Password must be set.");
            }
        }
    }

    public void onSuccess()
    {
        try {
            issueCertificate();
        }
        catch(Exception e)
        {
            logger.error("Error issuing certificate.", e);

            errorMessage = ExceptionUtils.getRootCauseMessage(e);

            error = true;
        }
    }

    public boolean isCertificateIssued() {
        return certificateIssued;
    }

    public boolean isCertificatePending() {
        return certificatePending;
    }

    public boolean isSentByEmailFailed() {
        return sentByEmailFailed;
    }

    public int getvalidity() {
        return validity;
    }

    public void setvalidity(int validity) {
        this.validity = validity;
    }

    public int getkeyLength() {
        return keyLength;
    }

    public void setKeyLength(int keyLength) {
        this.keyLength = keyLength;
    }

    public boolean isError() {
        return error;
    }

    public String getErrorMessage() {
        return StringUtils.isNotBlank(errorMessage) ? errorMessage : "No details.";
    }

    /*
     * Event handler called when the cancel button is pressed.
     */
    protected Object onCancel()
    {
        if (certificateSelectionType == null || StringUtils.isBlank(subjectPrincipal.getEmail())) {
            return Users.class;
        }

        String page;

        switch(certificateSelectionType)
        {
        case ENCRYPTION : page = "user/encryption"; break;
        case SIGNING    : page = "user/signing"; break;
        default:
            return CASettings.class;
        }

        return linkFactory.createPageRenderLink(page, false, subjectPrincipal.getEmail());
    }

    public boolean isShowCloseButton() {
        return certificateSelectionType != null;
    }

    public boolean isSendByEmail() {
        return sendByEmail;
    }

    public void setSendByEmail(boolean sendByEmail) {
        this.sendByEmail = sendByEmail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isAddCRLDistributionPoint() {
        return addCRLDistributionPoint;
    }

    public void setAddCRLDistributionPoint(boolean addCRLDistributionPoint) {
        this.addCRLDistributionPoint = addCRLDistributionPoint;
    }

    public SignatureAlgorithm getSignatureAlgorithm() {
        return signatureAlgorithm;
    }

    public void setSignatureAlgorithm(SignatureAlgorithm signatureAlgorithm) {
        this.signatureAlgorithm = signatureAlgorithm;
    }

    public boolean isAdvanced() {
        return advanced;
    }

    public void setAdvanced(boolean advanced) {
        this.advanced = advanced;
    }

    public String getCrlDistributionPoint() {
        return crlDistributionPoint;
    }

    public void setCrlDistributionPoint(String crlDistributionPoint) {
        this.crlDistributionPoint = crlDistributionPoint;
    }

    public void setCertificateRequestHandler(String certificateRequestHandler) {
        this.certificateRequestHandler = certificateRequestHandler;
    }

    public String getCertificateRequestHandler() {
        return certificateRequestHandler;
    }

    /*
     * Ajax event handler called when the advanced settings checkbox is clicked. We want to store
     * the selected value without requiring a submit
     */
    @OnEvent(value="checkboxEvent", component="advanced")
    public void onCheckboxEvent()
    {
        String data = request.getParameter("data");

        if (StringUtils.isEmpty(data)) {
            throw new IllegalArgumentException("data is missing.");
        }

        JSONObject json = new JSONObject(data);

        boolean checked = json.getBoolean("checked");

        advanced = checked;
    }

    public boolean isAddUser() {
        return addUser;
    }

    public void setAddUser(boolean addUser) {
        this.addUser = addUser;
    }
}
