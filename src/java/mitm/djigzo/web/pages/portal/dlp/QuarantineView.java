/*
 * Copyright (c) 2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.pages.portal.dlp;

import javax.servlet.http.HttpServletRequest;

import mitm.application.djigzo.ws.BinaryDTO;
import mitm.application.djigzo.ws.EventLoggerWS;
import mitm.application.djigzo.ws.MailRepositoryItemDTO;
import mitm.application.djigzo.ws.MailRepositoryWS;
import mitm.application.djigzo.ws.PolicyViolationDTO;
import mitm.application.djigzo.ws.ReleaseProcessor;
import mitm.common.mail.EmailAddressUtils;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.ws.WebServiceCheckedException;
import mitm.djigzo.web.beans.DLPPropertiesBean;
import mitm.djigzo.web.beans.impl.DLPPropertiesBeanImpl;
import mitm.djigzo.web.common.ContentTypes;
import mitm.djigzo.web.common.streamresponse.InputStreamFileResponse;
import mitm.djigzo.web.entities.User;
import mitm.djigzo.web.entities.UserManager;
import mitm.djigzo.web.services.DisableHttpCache;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.tapestry5.Asset;
import org.apache.tapestry5.ComponentResources;
import org.apache.tapestry5.PersistenceConstants;
import org.apache.tapestry5.StreamResponse;
import org.apache.tapestry5.annotations.BeginRender;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.Path;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.beaneditor.BeanModel;
import org.apache.tapestry5.corelib.components.Grid;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.ioc.annotations.Value;
import org.apache.tapestry5.services.BeanModelSource;
import org.apache.tapestry5.services.Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@IncludeStylesheet("context:styles/pages/portal/dlp/quarantineView.css")
public class QuarantineView
{
    private final static Logger logger = LoggerFactory.getLogger(QuarantineView.class);
    
    /*
     * the available submit buttons
     */
    private static enum SubmitButton {
        DOWNLOAD,
        RELEASE,
        RELEASE_ENCRYPT,
        RELEASE_AS_IS,
        DELETE
    }
    
    @Inject
    private Request request;

    @Inject
    private MailRepositoryWS mailRepository;
    
    @Inject
    private UserManager userManager;
    
    @Inject
    private BeanModelSource beanModelSource;

    @Inject
    private ComponentResources resources;
    
    @Inject
    private EventLoggerWS eventLogger;
    
    @Inject
    private HttpServletRequest httpRequest;
    
    @Inject
    @Value("${quarantineView.rowsPerPage}")
    private int rowsPerPage;    
    
    @SuppressWarnings("unused")
    @Inject
    @Path("context:images/logo.png")
    @Property
    private Asset logoAsset;
    
    /*
     * True if and error has occurred
     */
    @Persist(PersistenceConstants.FLASH)
    private boolean error;

    /*
     * Error message if an error has occurred
     */
    @Persist(PersistenceConstants.FLASH)
    private String errorMessage;
    
    /*
     * The MailRepositoryItem that's being viewed.
     */
    private MailRepositoryItemDTO mailRepositoryItem;
    
    /*
     * The policyViolation that's being drawn in the grid.
     */
    private PolicyViolationDTO policyViolation;
        
    /*
     * The submit button that was selected
     */
    private SubmitButton submitButton;
    
    /*
     * If true, download is allowed
     */
    private boolean allowDownload;

    /*
     * If true, release is allowed
     */
    private boolean allowRelease;

    /*
     * If true, release encrypt is allowed
     */
    private boolean allowReleaseEncrypt;

    /*
     * If true, release as-is is allowed
     */
    private boolean allowReleaseAsIs;

    /*
     * If true, delete is allowed
     */
    private boolean allowDelete;
    
    @SuppressWarnings("unused")
    @Component(parameters = {"source = mailRepositoryItem.policyViolations", "model = model", "row = policyViolation", 
            "volatile = true", "rowsPerPage = prop:rowsPerPage"})
    private Grid policyViolationsGrid;
    
    @BeginRender
    @DisableHttpCache
    public void beginRender() {
        /*
         * empty on purpose. We need beginRender to set @DisableHttpCache
         */
    }
    
    private Class<?> getInvalidIdPage() {
        return QuarantineInvalidRequest.class;
    }
        
    public Class<?> init(String id)
    throws WebServiceCheckedException, HierarchicalPropertiesException
    {
        if (StringUtils.isEmpty(id))
        {
            logger.warn("id is empty");
            
            return getInvalidIdPage();
        }
        
        mailRepositoryItem = mailRepository.getItem(id);
        
        if (mailRepositoryItem == null)
        {
            logger.warn("Mail repository item was not found.");

            return getInvalidIdPage();
        }
        
        String originator = EmailAddressUtils.canonicalizeAndValidate(mailRepositoryItem.getOriginator(), 
                false /* return invalid@invalid.tld if invalid */);

        User user = userManager.getUser(originator, true /* dummy if user not exist */);
        
        DLPPropertiesBean dlpProperties = new DLPPropertiesBeanImpl(user.getPreferences().getProperties());

        allowDownload = dlpProperties.getAllowDownloadMessage().getValue();
        allowRelease = dlpProperties.getAllowReleaseMessage().getValue();
        allowReleaseEncrypt = dlpProperties.getAllowReleaseEncryptMessage().getValue();
        allowReleaseAsIs = dlpProperties.getAllowReleaseAsIsMessage().getValue();
        allowDelete = dlpProperties.getAllowDeleteMessage().getValue();
        
        return null;
    }
    
    public Class<?> onActivate()
    throws WebServiceCheckedException, HierarchicalPropertiesException
    {
        /*
         * If onActivate(id) was already called skip
         */
        if (mailRepositoryItem != null) {
            return null;
        }
        
        return init(request.getParameter("id"));
    }

    public Class<?> onActivate(String id)
    throws WebServiceCheckedException, HierarchicalPropertiesException
    {
        return init(id);
    }

    public String onPassivate() {
        return mailRepositoryItem != null ? mailRepositoryItem.getId() : null;
    }
    
    public BeanModel<PolicyViolationDTO> getModel() 
    {
        BeanModel<PolicyViolationDTO> model = beanModelSource.createDisplayModel(PolicyViolationDTO.class, 
                resources.getMessages());
        
        /*
         * Disable all sorting
         */
        for (String column : model.getPropertyNames()) {
            model.get(column).sortable(false);
        }
        
        return model;
    }
    
    protected void onSelectedFromDownload() {
        submitButton = SubmitButton.DOWNLOAD;
    }

    protected void onSelectedFromRelease() {
        submitButton = SubmitButton.RELEASE;
    }

    protected void onSelectedFromReleaseEncrypt() {
        submitButton = SubmitButton.RELEASE_ENCRYPT;
    }

    protected void onSelectedFromReleaseAsIs() {
        submitButton = SubmitButton.RELEASE_AS_IS;
    }

    protected void onSelectedFromDelete() {
        submitButton = SubmitButton.DELETE;
    }

    public Object onSuccess()
    {
        if (submitButton == null) {
            return null;
        }

        String id = mailRepositoryItem.getId();
        
        switch(submitButton)
        {
        case DOWNLOAD        : return download(id);
        case RELEASE         : return release(id);
        case RELEASE_ENCRYPT : return releaseEncrypt(id);
        case RELEASE_AS_IS   : return releaseAsIs(id);
        case DELETE          : return delete(id);
        }
        
        return null;
    }
    
    private StreamResponse download(String id)
    {
        logEvent("Downloading message", id);        

        if (!allowDownload)
        {
            error = true;
            errorMessage = "Downloading an email is not allowed.";
            
            return null;
        }
        
        StreamResponse result = null;
        
        try {
            BinaryDTO backup = mailRepository.getMimeMessage(id);

            result = new InputStreamFileResponse(backup.getDataHandler().getInputStream(), 
                    ContentTypes.X_DOWNLOAD, id + ".eml");
        }
        catch (Exception e)
        {
            logger.error("Error downloading email", e);
            
            error = true;
            errorMessage = ExceptionUtils.getRootCauseMessage(e);
        }
        
        return result;
    }

    private Class<?> release(String id)
    {
        logEvent("Releasing message", id);        
        
        if (!allowRelease)
        {
            error = true;
            errorMessage = "Releasing an email is not allowed.";
            
            return null;
        }
                
        try {
            mailRepository.releaseMessage(id, ReleaseProcessor.DEFAULT);
            
            return QuarantineReleased.class;
        }
        catch (Exception e)
        {
            logger.error("Error releasing email", e);
            
            error = true;
            errorMessage = ExceptionUtils.getRootCauseMessage(e);
        }
        
        return null;
    }

    private Class<?> releaseEncrypt(String id)
    {
        logEvent("Releasing message encrypted", id);        

        if (!allowReleaseEncrypt)
        {
            error = true;
            errorMessage = "Releasing an email in encrypt mode is not allowed.";
            
            return null;
        }
                
        try {
            mailRepository.releaseMessage(id, ReleaseProcessor.ENCRYPT);
            
            return QuarantineReleased.class;
        }
        catch (Exception e)
        {
            logger.error("Error releasing email", e);
            
            error = true;
            errorMessage = ExceptionUtils.getRootCauseMessage(e);
        }
        
        return null;
    }

    private Class<?> releaseAsIs(String id)
    {
        logEvent("Releasing message as-is", id);        

        if (!allowReleaseAsIs)
        {
            error = true;
            errorMessage = "Releasing an email in as-is mode is not allowed.";
            
            return null;
        }
                
        try {
            mailRepository.releaseMessage(id, ReleaseProcessor.AS_IS);
            
            return QuarantineReleased.class;
        }
        catch (Exception e)
        {
            logger.error("Error releasing email", e);
            
            error = true;
            errorMessage = ExceptionUtils.getRootCauseMessage(e);
        }
        
        return null;
    }
    
    private Class<?> delete(String id)
    {
        logEvent("Deleting message", id);        
        
        if (!allowDelete)
        {
            error = true;
            errorMessage = "Deleting an email is not allowed.";
            
            return null;
        }
        
        try {
            mailRepository.deleteItem(id);
            
            return QuarantineDeleted.class;
        }
        catch (Exception e)
        {
            logger.error("Error deleting email", e);
            
            error = true;
            errorMessage = ExceptionUtils.getRootCauseMessage(e);
        }
        
        return null;
    }

    private void logEvent(String message, String id)
    {
        try {
            eventLogger.info("DLP", message + ", Quarantine id: " + id + ", IP address: " + 
                    httpRequest.getRemoteAddr());
        } 
        catch (Exception e) {
            logger.error("Error logging message");
        }
    }
    
    public String getRecipients() {
        /*
         * The recipients are stored as list so they must be converted to a string
         */
        return StringUtils.join(mailRepositoryItem.getRecipients(), ", ");
    }
    
    public String getPolicyViolations()
    {
        /*
         * The PolicyViolations is stored as list so they must be converted to a string
         */
        return StringUtils.join(mailRepositoryItem.getPolicyViolations(), ", ");
    }

    public MailRepositoryItemDTO getMailRepositoryItem() {
        return mailRepositoryItem;
    }

    public int getRowsPerPage() {
        return rowsPerPage;
    }

    public PolicyViolationDTO getPolicyViolation() {
        return policyViolation;
    }

    public void setPolicyViolation(PolicyViolationDTO policyViolation) {
        this.policyViolation = policyViolation;
    }

    public boolean isError() {
        return error;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public boolean isNotAllowDownload() {
        return !allowDownload;
    }

    public boolean isNotAllowRelease() {
        return !allowRelease;
    }

    public boolean isNotAllowReleaseEncrypt() {
        return !allowReleaseEncrypt;
    }

    public boolean isNotAllowReleaseAsIs() {
        return !allowReleaseAsIs;
    }

    public boolean isNotAllowDelete() {
        return !allowDelete;
    }
}
