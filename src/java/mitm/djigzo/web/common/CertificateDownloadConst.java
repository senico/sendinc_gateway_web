/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.common;

import mitm.common.security.asn1.ObjectEncoding;

/**
 * Helper class for certificate downloads.
 * 
 * @author Martijn Brinkers
 *
 */
public class CertificateDownloadConst 
{
	/*
	 * Filename to be used when a single certificate is downloaded 
	 */
	private final static String CERTIFICATE_FILENAME  = "certificate";

	/*
	 * Filename to be used when multiple certificates are downloaded 
	 */
	private final static String CERTIFICATES_FILENAME  = "certificates";
		
	/**
	 * Encoding used to encode (to byte array) the Certificate with. Windows 'prefers' DER encoding
	 * whereas OpenSSL 'prefers' PEM encoding. Windows however only supports files with multiple 
	 * certificates when it's DER encoded.
	 */
	public final static ObjectEncoding DEFAUL_ENCODING = ObjectEncoding.DER;

	private static String getExtension(boolean singleCertificate, ObjectEncoding encoding)
	{
		switch(encoding)
		{
		case DER : return singleCertificate ? ".cer" : ".p7b";
		case PEM : return singleCertificate ? ".cer" : ".pem";
		default:
			return ".cer";
		}
	}
	
	/**
	 * Returns the filename for the downloaded certificate
	 */
	public static String getFilename(boolean singleCertificate, ObjectEncoding encoding)
	{
		return (singleCertificate ? CERTIFICATE_FILENAME : CERTIFICATES_FILENAME) + 
				getExtension(singleCertificate, encoding);
	}	
}
