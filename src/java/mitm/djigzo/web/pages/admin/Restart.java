/*
 * Copyright (c) 2009-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.pages.admin;

import mitm.application.djigzo.admin.FactoryRoles;
import mitm.application.djigzo.ws.SystemManagerWS;
import mitm.common.ws.WebServiceCheckedException;
import mitm.djigzo.web.pages.Admins;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.tapestry5.PersistenceConstants;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.ioc.annotations.Value;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.annotation.Secured;

@IncludeStylesheet("context:styles/pages/admin/restart.css")
@Secured({FactoryRoles.ROLE_ADMIN})
public class Restart
{
    private final static Logger logger = LoggerFactory.getLogger(Restart.class);
    
    @Inject
    private SystemManagerWS systemManager;

    /*
     * True if restart failed
     */
    @Persist(PersistenceConstants.FLASH)
    private boolean restartFailure;
    
    /*
     * The failure message when restart failed
     */    
    @Persist(PersistenceConstants.FLASH)
    private String failureMessage;

    @Inject
    @Value("${djigzo.restart.delay}")
    private int delay;
    
    @SetupRender
    @Secured({FactoryRoles.ROLE_ADMIN})
    protected void setupRender()
    {
        /*
         * Empty on purpose
         */
    }
    
    public int getDelay() {
        return delay;
    }
    
    public boolean isRestartFailure() {
        return restartFailure;
    }

    public String getFailureMessage() {
        return failureMessage;
    }    
    
    public Object onSuccess()
    {
        Object result = null;
        
        try {
            systemManager.restart();
            
            result = Restarting.class;
        }
        catch(WebServiceCheckedException e)
        {
            logger.error("Error restarting", e);
            
            restartFailure = true;
            failureMessage = ExceptionUtils.getRootCauseMessage(e);
        }
        
        return result;
    }
    
    /*
     * Event handler called when the cancel button is pressed.
     */
    public Object onCancel() {
        return Admins.class;
    }
}
