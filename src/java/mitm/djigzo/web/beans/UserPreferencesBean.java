/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.beans;

import java.util.Set;

import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.ws.WebServiceCheckedException;

/**
 * Represents the user preferences for a user
 * 
 * @author Martijn Brinkers
 *
 */
public interface UserPreferencesBean 
{
	/**
	 * Returns the manually selected certificates for this user
	 */
    public Set<X509CertificateBean> getCertificates()
    throws WebServiceCheckedException; 

	/**
	 * Sets the manually selected certificates for this user
	 */
    public void setCertificates(Set<String> thumbprints);

    /**
     * Returns the inherited certificates for this user
     */
    public Set<X509CertificateBean> getInheritedCertificates()
    throws WebServiceCheckedException; 

    /**
     * Returns the named certificates for this user
     */
    public Set<X509CertificateBean> getNamedCertificates(String name)
    throws WebServiceCheckedException; 

    /**
     * Sets the named certificates for this user
     */
    public void setNamedCertificates(String name, Set<String> thumbprints);
    
    /**
     * Returns the inherited named certificates for this user
     */
    public Set<X509CertificateBean> getInheritedNamedCertificates(String name)
    throws WebServiceCheckedException; 
    
    /**
     * Returns the signing certificate for this user
     */
    public X509CertificateBean getKeyAndCertificate()
    throws WebServiceCheckedException;
    
    /**
     * Sets the signing certificate for this user
     */
    public void setKeyAndCertificate(String thumbprint);
        
    /**
     * Returns the properties of this user
     */
    public UserPropertiesBean getProperties()
    throws WebServiceCheckedException, HierarchicalPropertiesException;
    
    /**
     * Saves (makes persistent) this users user preferences 
     */
	public void save()
	throws WebServiceCheckedException, HierarchicalPropertiesException;
}
