function afterSelectTemplate(data)
{
    var json = data.evalJSON(true)

    // redirect to the same page but now with a different context
    window.location = json.result.redirectURL
}
