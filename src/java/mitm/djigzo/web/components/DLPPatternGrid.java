/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.components;

import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import mitm.application.djigzo.admin.FactoryRoles;
import mitm.application.djigzo.ws.PolicyPatternDTO;
import mitm.application.djigzo.ws.PolicyPatternNodeDTO;
import mitm.common.dlp.PolicyViolationPriority;
import mitm.common.ws.WebServiceCheckedException;
import mitm.djigzo.web.common.YesNo;
import mitm.djigzo.web.pages.dlp.patterns.PatternsEdit;
import mitm.djigzo.web.pages.dlp.patterns.PatternsGroup;
import mitm.djigzo.web.pages.dlp.patterns.PatternsRename;
import mitm.djigzo.web.pages.dlp.patterns.PatternsUsage;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.text.StrBuilder;
import org.apache.tapestry5.Asset;
import org.apache.tapestry5.BindingConstants;
import org.apache.tapestry5.ComponentEventCallback;
import org.apache.tapestry5.ComponentResources;
import org.apache.tapestry5.Link;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.IncludeJavaScriptLibrary;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.annotations.Path;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.beaneditor.BeanModel;
import org.apache.tapestry5.corelib.components.Grid;
import org.apache.tapestry5.grid.GridDataSource;
import org.apache.tapestry5.internal.TapestryInternalUtils;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.json.JSONObject;
import org.apache.tapestry5.services.BeanModelSource;
import org.apache.tapestry5.services.Request;
import org.springframework.security.annotation.Secured;

/**
 * Grid for showing DLP pattern's.
 * 
 * @author Martijn Brinkers
 *
 */
@IncludeStylesheet("context:styles/components/dlpPatternGrid.css")
@IncludeJavaScriptLibrary("dlpPatternGrid.js")
public class DLPPatternGrid
{
    /*
     * All events
     */
    public static final String DELETE_PATTERN_EVENT = "dlppatterngrid-delete-pattern";
    public static final String IS_DISABLED_EVENT = "dlppatterngrid-disabled";
    public static final String IS_SELECTED_EVENT = "dlppatterngrid-selected";
    public static final String IS_IN_USE_EVENT = "dlppatterngrid-inuse";
    
    @Inject
    private Request request;
    
    @Parameter(required = true)
    private GridDataSource source;

    @Parameter(required = false, defaultPrefix = BindingConstants.LITERAL)
    private boolean addNameLink;

    @Parameter(required = false, defaultPrefix = BindingConstants.LITERAL)
    private String deleteTitle = "Delete pattern";

    @Parameter(required = false, defaultPrefix = BindingConstants.LITERAL)
    private boolean removeDelete;
    
    /*
     * The PolicyPatternNode currently being drawn by the grid (is set by the grid component)
     */
    private PolicyPatternNodeDTO policyPatternNode;
    
    /*
     * Set of the selected PolicyPatternNode (names of the PolicyPatternNode's).
     */
    @Persist
    private Set<String> selected;
    
    @SuppressWarnings("unused")
    @Component(parameters = {"source = source", "model = model", "row = policyPatternNode", "volatile = true", 
            "reorder = prop:reorder", "rowsPerPage = inherit:rowsPerPage"})
    private Grid patternGrid;
    
    @Inject
    private BeanModelSource beanModelSource;

    @Inject
    private ComponentResources resources;

    @Inject
    @Path("context:icons/cross.png")
    private Asset deleteAsset;

    @Inject
    @Path("context:icons/group.png")
    private Asset groupAsset;
    
    @Inject
    private Messages messages;
    
    @SetupRender
    public void setupGrid() 
    {
        if (selected == null) {
            selected = Collections.synchronizedSet(new HashSet<String>());
        }
        else {
            selected.clear();
        }
    }
    
    public void setPolicyPatternNode(PolicyPatternNodeDTO policyPatternNode) {
        this.policyPatternNode = policyPatternNode;
    }
    
    public PolicyPatternNodeDTO getPolicyPatternNode() {
        return policyPatternNode;
    }

    public PolicyPatternDTO getPolicyPattern() {
        return policyPatternNode.getPolicyPattern();
    }
    
    public String getFriendlyPriority()
    {
        PolicyViolationPriority priority = getPolicyPattern().getPriority();

        /*
         * Use some Tapestry internal function to get a friendly name for the enum
         */
        return priority != null ? TapestryInternalUtils.getLabelForEnum(messages, 
                getPolicyPattern().getPriority()) : ""; 
    }
    
    public Set<String> getSelected() {
        return selected;
    }

    public boolean isSelect()
    {
        boolean value = selected.contains(policyPatternNode.getName());
        
        if (!value)
        {
            final List<YesNo> resultHolder = new LinkedList<YesNo>();
            
            ComponentEventCallback<YesNo> callback = new ComponentEventCallback<YesNo>() 
            {
                @Override
                public boolean handleResult(YesNo result) 
                {
                    resultHolder.add(result);
                    
                    return true;
                }
            };
            
            resources.triggerEvent(IS_SELECTED_EVENT, new Object[]{policyPatternNode}, callback);
            
            if (resultHolder.size() > 0) {
                value = resultHolder.get(0) == YesNo.YES;
            }
        }
        
        return value;
    }
    
    public void setSelect(boolean value) 
    {
        /*
         * Ignored. Checkbox values will be handled using EventCheckbox (see onCheckboxClick)
         */
    }
    
    public GridDataSource getSource() {
        return source;
    }

    public Asset getDeleteAsset() {
        return deleteAsset;
    }

    public Asset getGroupAsset() {
        return groupAsset;
    }
    
    public boolean isGroup() {
        return policyPatternNode.getPolicyPattern() == null;
    }
        
    public BeanModel<PolicyPatternNodeDTO> getModel()
    {
        BeanModel<PolicyPatternNodeDTO> model = beanModelSource.createDisplayModel(PolicyPatternNodeDTO.class, 
                resources.getMessages());
        
        model.exclude("inherited", "hasChilds");
        if (!removeDelete) {
            model.add("delete", null);
        }
        model.add("select", null);
        model.add("regexp", null);
        model.add("description", null);
        model.add("matchFilter", null);
        model.add("threshold", null);
        model.add("priority", null);

        /*
         * Disable all sorting
         */
        for (String column : model.getPropertyNames()) {
            model.get(column).sortable(false);
        }
        
        return model;
    }

    public String getReorder()
    {
        StrBuilder sb = new StrBuilder();
        
        sb.append("select,");

        if (!removeDelete) {
            sb.append("delete,");
        }
        sb.append("name,regexp,description,threshold,priority,matchFilter");
        
        return sb.toString();
    }
    
    
    public boolean isDisabled()
    {
        boolean disabled = false;
        
        final List<YesNo> resultHolder = new LinkedList<YesNo>();
        
        ComponentEventCallback<YesNo> callback = new ComponentEventCallback<YesNo>() 
        {
            @Override
            public boolean handleResult(YesNo result) 
            {
                resultHolder.add(result);
                
                return true;
            }
        };
        
        resources.triggerEvent(IS_DISABLED_EVENT, new Object[]{policyPatternNode}, callback);
        
        if (resultHolder.size() > 0) {
            disabled = resultHolder.get(0) == YesNo.YES;
        }

        return disabled;
    }
    
    public boolean isInUse()
    {
        boolean inUse = false;
        
        final List<YesNo> resultHolder = new LinkedList<YesNo>();

        ComponentEventCallback<YesNo> callback = new ComponentEventCallback<YesNo>() 
        {
            @Override
            public boolean handleResult(YesNo result) 
            {
                resultHolder.add(result);

                return true;
            }
        };

        resources.triggerEvent(IS_IN_USE_EVENT, new Object[]{policyPatternNode}, callback);

        if (resultHolder.size() > 0) {
            inUse = resultHolder.get(0) == YesNo.YES;
        }
        
        return inUse;
    }
    
    @OnEvent(component = "deletePattern")
    @Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_DLP_MANAGER})
    protected void deletePattern(String name) 
    throws WebServiceCheckedException 
    {
        resources.triggerEvent(DELETE_PATTERN_EVENT, new Object[]{name}, null);
    }

    /*
     * Event handler called when the checkbox is clicked. 
     */
    public void onCheckboxEvent()
    {
        String data = request.getParameter("data");

        if (StringUtils.isEmpty(data)) {
            throw new IllegalArgumentException("data is missing.");
        }
        
        JSONObject json = new JSONObject(data);
                        
        String element = json.getString("element");
        boolean checked = json.getBoolean("checked"); 
        
        if (checked) {
            selected.add(element);
        }
        else {
            selected.remove(element);
        }
    }

    public Link getPatternEditLink() {
        return resources.createPageLink(PatternsEdit.class, false, policyPatternNode.getName());
    }

    public Link getPatternRenameLink() {
        return resources.createPageLink(PatternsRename.class, false, policyPatternNode.getName());
    }

    public Link getGroupEditLink() {
        return resources.createPageLink(PatternsGroup.class, false, policyPatternNode.getName());
    }

    public Link getGroupRenameLink() {
        return resources.createPageLink(PatternsRename.class, false, policyPatternNode.getName());
    }

    public Link getUsageLink() {
        return resources.createPageLink(PatternsUsage.class, false, policyPatternNode.getName());
    }
    
    public boolean isAddNameLink() {
        return addNameLink;
    }

    public String getDeleteTitle() {
        return deleteTitle;
    }
}
