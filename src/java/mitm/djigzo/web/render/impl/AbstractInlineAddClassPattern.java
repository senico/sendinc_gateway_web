/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.render.impl;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import mitm.common.util.Check;
import mitm.djigzo.web.render.FragmentMarkupRenderer;
import mitm.djigzo.web.render.FragmentMarkupRendererFilter;
import mitm.djigzo.web.render.FragmentMarkupRendererRegistry;

import org.apache.commons.lang.text.StrBuilder;

/**
 * A FragmentMarkupRendererFilter implementation that adds a span with a class to a string when the
 * pattern matches.
 * 
 * 
 * @author Martijn Brinkers
 *
 */
public abstract class AbstractInlineAddClassPattern implements FragmentMarkupRendererFilter 
{
	private final String className;
	
	public AbstractInlineAddClassPattern(String className)
	{
		Check.notNull(className, "className");
		
		this.className = className;
	}
	
    @Override
	public void renderMarkup(StrBuilder builder, FragmentMarkupRendererRegistry registry, 
			FragmentMarkupRenderer renderer) 
	{
		applyPattern(builder);
		
		renderer.renderMarkup(builder, registry);
	}

    /**
     * Returns the Pattern that matches the input
     */
	protected abstract Pattern getPattern();
	
	private void applyPattern(StrBuilder builder)
	{
		Pattern pattern = getPattern();
		
		if (pattern != null)
		{
			Matcher matcher = pattern.matcher(builder.toString());
			
			StrBuilder copy = null;
			
			/*
			 * Because we will modify the StrBuilder (replacing matches etc.) we need
			 * to keep track of changes with respect to indexes. If there is a match
			 * we will create a copy of the line and do the replacing on that line. We
			 * however need to correct the indexes because we are adding or removing 
			 * characters to the copy.  
			 */
			int indexCorrection = 0;
			
			while (matcher.find())
			{
				if (copy == null) {
					copy = new StrBuilder(builder.toString());
				}
				
				String replaceWith = "<span class=\"" + className + "\">" + 
						matcher.group() + "</span>";
				
				copy.replace(matcher.start() + indexCorrection, matcher.end() + indexCorrection, 
						replaceWith);
				
				indexCorrection = indexCorrection + replaceWith.length() - 
						matcher.group().length(); 
			}
			
			if (copy != null) 
			{
				/*
				 * Content has changed so replace it
				 */
				builder.clear();
				builder.append(copy.toString());
			}
		}
	}
}
