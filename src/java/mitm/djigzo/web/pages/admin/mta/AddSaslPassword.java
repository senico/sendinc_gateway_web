/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.pages.admin.mta;

import java.util.LinkedList;
import java.util.List;

import mitm.application.djigzo.admin.FactoryRoles;
import mitm.application.djigzo.ws.PostfixConfigManagerWS;
import mitm.common.postfix.SaslPassword;
import mitm.common.util.DomainUtils;
import mitm.common.util.DomainUtils.DomainType;
import mitm.common.ws.WebServiceCheckedException;

import org.apache.cxf.common.util.StringUtils;
import org.apache.tapestry5.annotations.Cached;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.corelib.components.Checkbox;
import org.apache.tapestry5.corelib.components.Form;
import org.apache.tapestry5.corelib.components.PasswordField;
import org.apache.tapestry5.corelib.components.TextField;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.springframework.security.annotation.Secured;

@IncludeStylesheet("context:styles/pages/admin/mta/addSaslPassword.css")
@Secured({FactoryRoles.ROLE_ADMIN})
public class AddSaslPassword
{
    @Inject
    private PostfixConfigManagerWS postfixConfigManager;

    @Component
    private Form form;
    
    @Component(id = "server", parameters = {"value = password.server", "validate=required,maxlength=255"})
    private TextField serverField;

    @SuppressWarnings("unused")
    @Component(id = "port", parameters = {"value = password.port", "validate=min=1,max=65535"})
    private TextField portField;

    @SuppressWarnings("unused")
    @Component(id = "mxLookup", parameters = {"value = password.mxLookup"})
    private Checkbox mxLookupField;
    
    @SuppressWarnings("unused")
    @Component(id = "username", parameters = {"value = password.username", "validate=required,maxlength=255"})
    private TextField usernameField;

    @SuppressWarnings("unused")
    @Component(id = "password", parameters = {"value = password.password", "validate=required,maxlength=255"})
    private PasswordField passwordField;
    
    @SetupRender
    @Secured({FactoryRoles.ROLE_ADMIN})
    protected void setupRender() {
        /*
         * Empty on purpose
         */
    }

    @Cached
    public SaslPassword getPassword() {
        return new SaslPassword();
    }
    
    @Cached
    private List<SaslPassword> getPasswords()
    throws WebServiceCheckedException
    {
        List<SaslPassword> passwords = postfixConfigManager.getSaslPasswords();
        
        if (passwords == null) {
            passwords = new LinkedList<SaslPassword>();
        }
        
        return passwords;
    }
    
    public void onValidateForm() 
    throws WebServiceCheckedException 
    {
        String filteredServer = DomainUtils.validate(getPassword().getServer(), DomainType.FRAGMENT);
        
        if (StringUtils.isEmpty(filteredServer))
        {
            form.recordError(serverField, "The server is not valid.");
            return;
        }
        
        getPassword().setServer(filteredServer);
        
        if (getPasswords().contains(getPassword())) {
            form.recordError("The server, port combination is already in the list.");
        }
    }
    
    protected Object onSuccess()
    throws WebServiceCheckedException
    {
        List<SaslPassword> passwords = getPasswords();

        passwords.add(getPassword());
        
        postfixConfigManager.setSaslPasswords(passwords);
        
        return SaslPasswords.class;
    }    

    /*
     * Event handler called when the cancel button is pressed.
     */
    public Object onCancel() {
        return SaslPasswords.class;
    }
}
