/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.beans.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import mitm.application.djigzo.ws.X509CertificateDTO;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.util.Check;
import mitm.common.ws.WebServiceCheckedException;
import mitm.djigzo.web.beans.BeanUtils;
import mitm.djigzo.web.beans.CertificateType;
import mitm.djigzo.web.beans.UserPreferencesBean;
import mitm.djigzo.web.beans.UserPropertiesBean;
import mitm.djigzo.web.beans.X509CertificateBean;
import mitm.djigzo.web.entities.UserPreferences;

import org.apache.commons.lang.StringUtils;

public class UserPreferencesBeanImp implements UserPreferencesBean
{
	private final UserPreferences userPreferences;
	
	private UserPropertiesBean userProperties;
	
	/*
	 * Thumbprints of the certificates that were manually selected
	 */
	private Set<String> selectedCertificateThumbprints;
	
	/*
	 * Map that map's a name to a Set of thumbprints 
	 */
	private Map<String, Set<String>> namedThumbprints = new HashMap<String, Set<String>>();
	
	/*
	 * Thumbprint if the signing certificate
	 */
	private String keyAndCertificate;
	
	public UserPreferencesBeanImp(UserPreferences userPreferences) 
	{
		Check.notNull(userPreferences, "userPreferences");
		
		this.userPreferences = userPreferences;
	}
	
    @Override
    public Set<X509CertificateBean> getCertificates()
    throws WebServiceCheckedException 
    {
    	return BeanUtils.certificatesToBeans(userPreferences.getCertificates(), CertificateType.SELECTED);
    }

    @Override
    public Set<X509CertificateBean> getInheritedCertificates() 
    throws WebServiceCheckedException 
    {
        return BeanUtils.certificatesToBeans(userPreferences.getInheritedCertificates(), CertificateType.INHERITED);
    }
    
    @Override
    public void setCertificates(Set<String> thumbprints) {
    	this.selectedCertificateThumbprints = thumbprints;
    }

    @Override
    public Set<X509CertificateBean> getNamedCertificates(String name) 
    throws WebServiceCheckedException 
    {
        return BeanUtils.certificatesToBeans(userPreferences.getNamedCertificates(name), CertificateType.SELECTED);
    }

    @Override
    public Set<X509CertificateBean> getInheritedNamedCertificates(String name) 
    throws WebServiceCheckedException 
    {
        return BeanUtils.certificatesToBeans(userPreferences.getInheritedNamedCertificates(name), CertificateType.INHERITED);
    }
    
    @Override
    public void setNamedCertificates(String name, Set<String> thumbprints) {
        this.namedThumbprints.put(name, thumbprints);
    }
    
    @Override
    public X509CertificateBean getKeyAndCertificate()
    throws WebServiceCheckedException
    {
    	X509CertificateDTO keyAndCertificateDTO = userPreferences.getKeyAndCertificate();
    	
    	X509CertificateBean result = null;
    	
    	if (keyAndCertificateDTO != null) {
    		result = new X509CertificateBeanImpl(keyAndCertificateDTO, CertificateType.SELECTED);
    	}
    	
    	return result;
    }
    
    @Override
    public void setKeyAndCertificate(String thumbprint) 
    {
    	/*
    	 * We need to be able to detect whether the thumbprint is set to a null value
    	 */
    	if (thumbprint == null) {
    		thumbprint = StringUtils.EMPTY;
    	}
    	
    	this.keyAndCertificate = thumbprint;
    }
        
    @Override
    public UserPropertiesBean getProperties() 
    throws HierarchicalPropertiesException, WebServiceCheckedException 
    {
    	if (userProperties == null) {
    		userProperties = new UserPropertiesBeanImpl(userPreferences.getProperties());
    	}
    	return userProperties; 
    }
    
    @Override
	public void save()
	throws WebServiceCheckedException, HierarchicalPropertiesException
	{
		if (userProperties != null) {
			userProperties.save();
		}
		
		if (selectedCertificateThumbprints != null) {
			userPreferences.setCertificates(selectedCertificateThumbprints);
		}
		
		for (Map.Entry<String, Set<String>> entry : namedThumbprints.entrySet())
		{
		    userPreferences.setNamedCertificates(entry.getKey(), entry.getValue());
		}
		
		if (keyAndCertificate != null) 
		{
			/*
			 * If keyAndCertificate is the special 'null' value set the keyAndCertificate to null
			 */
			userPreferences.setKeyAndCertificate(keyAndCertificate.equals(StringUtils.EMPTY) ? null : 
				keyAndCertificate);
		}
	}
}
