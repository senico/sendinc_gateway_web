package mitm.djigzo.web.mixins;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.tapestry5.BindingConstants;
import org.apache.tapestry5.ClientElement;
import org.apache.tapestry5.RenderSupport;
import org.apache.tapestry5.annotations.AfterRender;
import org.apache.tapestry5.annotations.IncludeJavaScriptLibrary;
import org.apache.tapestry5.annotations.InjectContainer;
import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.ioc.annotations.Inject;

/**
 * A simple mixin for attaching a javascript confirmation box to the onclick
 * event of any component that implements ClientElement.
 * 
 * @author <a href="mailto:chris@thegodcode.net">Chris Lewis</a> Apr 18, 2008
 * 
 * Downloaded from: http://wiki.apache.org/tapestry/Tapestry5AndJavaScriptExplained
 * 
 * Modifications by Martijn Brinkers
 * - support Tapestry 5.0.13.
 * - added escapeJavaScript
 * 
 */
@IncludeJavaScriptLibrary("confirm.js")
public class Confirm 
{
	@Parameter(value = "Are you sure?", defaultPrefix = BindingConstants.LITERAL)
	private String message;

	@Inject
	private RenderSupport renderSupport;

	@InjectContainer
	private ClientElement element;

	@AfterRender
	public void afterRender() 
	{
		renderSupport.addScript(String.format("new Confirm('%s', '%s');",
				StringEscapeUtils.escapeJavaScript(element.getClientId()), 
				StringEscapeUtils.escapeJavaScript(this.message)));
	}
}