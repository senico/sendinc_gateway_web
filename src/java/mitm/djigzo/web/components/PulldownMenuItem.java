/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.components;

import org.apache.tapestry5.Block;
import org.apache.tapestry5.ComponentResources;
import org.apache.tapestry5.MarkupWriter;
import org.apache.tapestry5.annotations.AfterRenderTemplate;
import org.apache.tapestry5.annotations.BeforeRenderTemplate;
import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.annotations.SupportsInformalParameters;
import org.apache.tapestry5.ioc.annotations.Inject;

@SupportsInformalParameters
public class PulldownMenuItem 
{
	@Inject
	private ComponentResources componentResources;
	
	@Parameter(required=true, defaultPrefix = "literal")
	private int itemCount;

	@Parameter(required=false, defaultPrefix = "literal")
	private String itemClass;
	
	// TODO refactor so the blocks can be specified as a comma separated list
	private int currentItem = 0;

	private Block block;
	
	public Block getBlock() {
		return block;
	}
	
	@BeforeRenderTemplate
	void beforeRenderTemplate(MarkupWriter writer) 
	{
		if (itemClass != null) {
			writer.element("li", "class", itemClass);
		}
		else {
			writer.element("li");
		}

		componentResources.renderInformalParameters(writer);
		
		block = componentResources.getContainerResources().getBlockParameter(
				Integer.toString(currentItem));
		
		if (block == null) {
			throw new IllegalArgumentException("Block with name " + currentItem + " cannot be found");
		}
		
		currentItem++;
	}

	@AfterRenderTemplate
	boolean afterRenderTemplate(MarkupWriter writer) 
	{
		writer.end();
		
		return !(currentItem < itemCount);
	}
}
