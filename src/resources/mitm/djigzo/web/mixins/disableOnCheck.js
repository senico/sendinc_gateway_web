var DisableOnCheck = Class.create();

DisableOnCheck.prototype = 
{
        initialize: function(element, idsToDisable, invert) 
        {
                this.idsToDisable = idsToDisable;
                this.invert = invert;
                
                Event.observe($(element), 'click', this.onCheckEvent.bindAsEventListener(this));
                
        	    this.synchronizeWithCheckbox($(element));
        },

        onCheckEvent: function(e) 
        {
        	this.synchronizeWithCheckbox(e.element());
        },
        
        synchronizeWithCheckbox: function(element) 
        {
			var checked = Boolean(element.checked);

            var inverted = this.invert.strip() == "true";
            
            if ((checked && !inverted) || (!checked && inverted)) {
                $w(this.idsToDisable).each(Field.disable);
            }
            else {
                $w(this.idsToDisable).each(Field.enable);
            }
        }
}