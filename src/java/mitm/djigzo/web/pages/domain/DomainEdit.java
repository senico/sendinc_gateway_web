/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.pages.domain;

import mitm.application.djigzo.admin.FactoryRoles;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.util.DomainUtils;
import mitm.common.util.DomainUtils.DomainType;
import mitm.common.ws.WebServiceCheckedException;
import mitm.djigzo.web.beans.DomainBean;
import mitm.djigzo.web.beans.impl.DomainBeanImpl;
import mitm.djigzo.web.components.propertyeditor.UserPropertiesEdit;
import mitm.djigzo.web.entities.Domain;
import mitm.djigzo.web.entities.DomainManager;
import mitm.djigzo.web.pages.Domains;

import org.apache.tapestry5.PersistenceConstants;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.annotation.Secured;

@Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_DOMAIN_MANAGER})
@IncludeStylesheet("context:styles/pages/domain/domainEdit.css")
public class DomainEdit
{
    private final static Logger logger = LoggerFactory.getLogger(DomainEdit.class);
	
	@Inject
	private DomainManager domainManager;

	@Property
	private DomainBean domainBean;

    /*
     * True if changes were applied
     */
    @Persist(PersistenceConstants.FLASH)
    private boolean applied;
	
	@SuppressWarnings("unused")
	@Component(parameters = { "userProperties = domainBean.preferences.properties" })
	private UserPropertiesEdit userPropertiesEdit;
		
    public boolean isApplied() {
        return applied;
    }
	
	@SetupRender
	@Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_DOMAIN_MANAGER})
	protected void setupRender() {
		/*
		 * Empty on purpose
		 */
	}
	
	private DomainBean createDomainBean(String domainName) 
	throws WebServiceCheckedException
	{
		String filteredDomain = DomainUtils.canonicalizeAndValidate(domainName, DomainType.WILD_CARD);
		
		if (filteredDomain == null) 
		{
			logger.warn("Domain " + domainName + " is not valid." );
			
			return null;
		}
		
		Domain domain = domainManager.getDomain(filteredDomain);
		
		if (domain == null) {
			return null;			
		}
		
		return new DomainBeanImpl(domain);
	}
	
	public void setDomain(String domain) 
	throws WebServiceCheckedException 
	{
		domainBean = createDomainBean(domain);
	}
	
    @Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_DOMAIN_MANAGER})
	public Object onActivate(String domainName)
	throws WebServiceCheckedException 
	{
		domainBean = createDomainBean(domainName);
		
		if (domainBean == null)	{
			return Domains.class;
		}
		
		return null;
	}

	public String onPassivate() 
	{
		String domain = null;
		
		if (domainBean != null) {
			domain = domainBean.getDomain();
		}
		
		return domain;
	}
	
	public void onSuccess()
	throws HierarchicalPropertiesException, WebServiceCheckedException 
	{
		domainBean.save();
		
		applied = true;
	}

    /*
     * Event handler called when the cancel button is pressed.
     */
	protected Object onCancel() {
		return Domains.class;
	}
}
