/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.pages.dlp.patterns;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import mitm.application.djigzo.admin.FactoryRoles;
import mitm.application.djigzo.ws.MatchFilterDTO;
import mitm.application.djigzo.ws.MatchFilterRegistryWS;
import mitm.application.djigzo.ws.PolicyPatternDTO;
import mitm.application.djigzo.ws.PolicyPatternManagerWS;
import mitm.application.djigzo.ws.PolicyPatternNodeDTO;
import mitm.application.djigzo.ws.PolicyPatternNodeWS;
import mitm.common.dlp.PolicyViolationPriority;
import mitm.common.ws.WebServiceCheckedException;
import mitm.djigzo.web.common.GenericSelectionModel;
import mitm.djigzo.web.mixins.AjaxEvent;

import org.apache.commons.lang.StringUtils;
import org.apache.tapestry5.PersistenceConstants;
import org.apache.tapestry5.SelectModel;
import org.apache.tapestry5.ValidationException;
import org.apache.tapestry5.annotations.Cached;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.corelib.components.Select;
import org.apache.tapestry5.corelib.components.TextArea;
import org.apache.tapestry5.corelib.components.TextField;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.json.JSONObject;
import org.apache.tapestry5.services.Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.annotation.Secured;

@Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_DLP_MANAGER})
@IncludeStylesheet("context:styles/pages/dlp/patterns/edit.css")
public class PatternsEdit
{
    private final static Logger logger = LoggerFactory.getLogger(PatternsEdit.class);
    
    @Inject
    private MatchFilterRegistryWS matchFilterRegistryWS;
    
    @Inject
    private PolicyPatternManagerWS policyPatternManagerWS;

    @Inject
    private PolicyPatternNodeWS policyPatternNodeWS;

    @Inject
    private Request request;
    
    /*
     * The name of this pattern
     */
    private String name;

    /*
     * The reg exp pattern of this pattern 
     */
    private String regExp;
    
    /*
     * The name of the filter that will be used to filter any matching content.
     */
    private String matchFilter;
    
    /*
     * Explains what this pattern policy does.
     */
    private String description;

    /*
     * If the pattern matches the content more than the threshold, the policy is violated.
     */
    private Integer threshold;
    
    /*
     * The priority associated with the policy.
     */
    private PolicyViolationPriority priority;
    
    /*
     * Will be set if a PolicyPatternNode is being edited
     */
    private PolicyPatternNodeDTO policyPatternNode;
    
    /*
     * True if and error has occurred
     */
    @Persist(PersistenceConstants.FLASH)
    private boolean error;

    /*
     * Error message if an error has occurred
     */
    @Persist(PersistenceConstants.FLASH)
    private String errorMessage;
    
    @SuppressWarnings("unused")
    @Component(id = "name", parameters = {"value = name", "validate = required, regexp"})
    private TextField nameField;

    @SuppressWarnings("unused")
    @Component(id = "regExp", parameters = {"value = regExp", "validate = required, maxLength=4096"})
    private TextArea regExpField;

    @SuppressWarnings("unused")
    @Component(id = "description", parameters = {"value = description"})
    private TextField descriptionField;

    @SuppressWarnings("unused")
    @Component(id = "threshold", parameters = {"value = threshold", "validate = required, min=1"})
    private TextField thresholdField;

    @SuppressWarnings("unused")
    @Component(id = "priority", parameters = {"value = priority", "validate = required"})
    private Select priorityField;
    
    @SuppressWarnings("unused")
    @Component(id = "matchFilter", parameters = {"value = matchFilter", "model = matchFilterModel"})
    private Select certificateRequestHandlerField;
    
    @SetupRender
    @Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_DLP_MANAGER})
    public void setupRender()
    {
        if (threshold == null) {
            threshold = 1;
        }
    }
    
    @Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_DLP_MANAGER})
    public Object onActivate(String name)
    throws WebServiceCheckedException 
    {
        policyPatternNode = policyPatternManagerWS.getPattern(name);
        
        if (policyPatternNode == null) {
            return PatternsView.class;
        }
     
        this.name = policyPatternNode.getName();
        this.regExp = policyPatternNode.getPolicyPattern().getRegExp();
        this.description = policyPatternNode.getPolicyPattern().getDescription();
        this.threshold = policyPatternNode.getPolicyPattern().getThreshold();
        this.matchFilter = policyPatternNode.getPolicyPattern().getMatchFilter();
        this.priority = policyPatternNode.getPolicyPattern().getPriority();
        
        return null;
    }
    
    public String onPassivate() {
        return policyPatternNode != null ? policyPatternNode.getName() : null;
    }
    
    @Cached
    private List<String> getMatchFilterNames()
    throws WebServiceCheckedException
    {
        List<MatchFilterDTO> filters = matchFilterRegistryWS.getMatchFilters();
        
        List<String> names = null;
        
        if (filters != null)
        {
            names = new ArrayList<String>(filters.size());
            
            for (MatchFilterDTO filter : filters)
            {
                if (filter == null) {
                    continue;
                }
                
                names.add(filter.getName());
            }
        }
        else {
            names = Collections.emptyList();
        }
        
        return names;
    }
    
    /*
     * Returns the description of the filter that's initially selected when a pattern is edited.
     */
    public String getFilterDescription()
    throws WebServiceCheckedException
    {
        String description = null;
        
        if (policyPatternNode != null)
        {
            PolicyPatternDTO policyPattern = policyPatternNode.getPolicyPattern();
            
            if (policyPattern != null && policyPattern.getMatchFilter() != null)
            {
                MatchFilterDTO filter = matchFilterRegistryWS.getMatchFilter(policyPattern.getMatchFilter());
                
                if (filter != null) {
                    description = filter.getDescription();
                }
            }
        }
        
        return description;
    }
    
    public SelectModel getMatchFilterModel()
    throws WebServiceCheckedException
    {
        return new GenericSelectionModel<String>(new ArrayList<String>(getMatchFilterNames()), null, null);
    }

    /*
     * AJAX handler called when a new MatchFilter is selected. The purpose of this is to return the 
     * description of the selected MatchFilter.
     */
    @OnEvent(component = "matchFilter", value="AjaxEvent")
    public JSONObject onFilterSelected()
    throws IOException, WebServiceCheckedException
    {
        String data = request.getParameter(AjaxEvent.REQUEST_PARAM_NAME);

        JSONObject json = new JSONObject(data);
        
        String name = json.getString("value");

        MatchFilterDTO filter = matchFilterRegistryWS.getMatchFilter(name);

        JSONObject result = new JSONObject();
     
        result.put("description", filter != null ? filter.getDescription() : "");
        
        return result;
    }
    
    public void onValidateFromName(String name)
    throws ValidationException, WebServiceCheckedException
    {
        if (name != null && !isEditing())
        {
            if (policyPatternManagerWS.getPattern(StringUtils.trim(name)) != null) {
                throw new ValidationException("A pattern with this name already exists.");
            }
        }
    }
    
    public void onValidateFromRegExp(String regExp)
    throws ValidationException
    {
        if (regExp != null)
        {
            try {
                Pattern.compile(regExp);
            }
            catch(PatternSyntaxException e) {
                throw new ValidationException("The regular expression value is not a valid regular expression");
            }
        }
    }
    
    public Object onSuccess()
    {
        Object result = null;

        try {
            String name = StringUtils.trim(this.name); 
            
            PolicyPatternNodeDTO node = !isEditing() ? policyPatternManagerWS.createPattern(name) : policyPatternNode;
            
            if (node != null)
            {
                PolicyPatternDTO pattern = new PolicyPatternDTO();
                
                pattern.setName(name);
                pattern.setDescription(StringUtils.trim(description));
                pattern.setMatchFilter(matchFilter);
                pattern.setRegExp(regExp);
                pattern.setThreshold(threshold);
                pattern.setPriority(priority);
                
                policyPatternNodeWS.setPattern(node.getName(), pattern);
                
                result = PatternsView.class;
            }
        }
        catch(WebServiceCheckedException e)
        {
            logger.error("Error during submit.", e);
            
            errorMessage = e.getMessage();
            error = true;
        }

        return result;
    }
    
    /*
     * Event handler called when the cancel button is pressed.
     */
    protected Object onCancel() {
        return PatternsView.class;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRegExp() {
        return regExp;
    }

    public void setRegExp(String regExp) {
        this.regExp = regExp;
    }

    public String getMatchFilter() {
        return matchFilter;
    }

    public void setMatchFilter(String matchFilter) {
        this.matchFilter = matchFilter;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getThreshold() {
        return threshold;
    }

    public void setThreshold(int threshold) {
        this.threshold = threshold;
    }    

    public PolicyViolationPriority getPriority() {
        return priority;
    }

    public void setPriority(PolicyViolationPriority priority) {
        this.priority = priority;
    }
    
    public boolean isEditing() {
        return policyPatternNode != null;
    }

    public boolean isError() {
        return error;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}
