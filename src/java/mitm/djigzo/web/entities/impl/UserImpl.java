/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.entities.impl;

import java.util.List;

import mitm.application.djigzo.UserLocality;
import mitm.application.djigzo.ws.HierarchicalPropertiesWS;
import mitm.application.djigzo.ws.UserPreferencesDTO;
import mitm.application.djigzo.ws.UserPreferencesWS;
import mitm.application.djigzo.ws.UserWS;
import mitm.application.djigzo.ws.X509CertificateDTO;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.util.Check;
import mitm.common.ws.WebServiceCheckedException;
import mitm.djigzo.web.entities.User;
import mitm.djigzo.web.entities.UserPreferences;

/**
 * UserImpl is not thread safe.
 * 
 * @author Martijn Brinkers
 *
 */
public class UserImpl implements User
{
	private final UserWS userWS;
	private final UserPreferencesWS userPreferencesWS;
	private final HierarchicalPropertiesWS hierarchicalPropertiesWS;
    private final String email;
    private UserLocality userLocality;
	
	public UserImpl(UserWS userWS, UserPreferencesWS userPreferencesWS, 
			HierarchicalPropertiesWS hierarchicalPropertiesWS, String email,
			UserLocality userLocality)
	{
		Check.notNull(userWS, "userWS");
		Check.notNull(userPreferencesWS, "userPreferencesWS");
		Check.notNull(hierarchicalPropertiesWS, "hierarchicalPropertiesWS");
        Check.notNull(email, "email");
		
		this.userWS = userWS;
		this.userPreferencesWS = userPreferencesWS;
		this.hierarchicalPropertiesWS = hierarchicalPropertiesWS;
		this.email = email;
		this.userLocality = userLocality;
	}
	
    @Override
	public String getEmail() {
		return email;
	}
	
    @Override
    public UserLocality getUserLocality()
    throws WebServiceCheckedException
    {
        try {
            if (userLocality == null) {
                userLocality = getPreferences().getProperties().getUserLocality();
            }
            return userLocality;
        }
        catch(HierarchicalPropertiesException e) {
            throw new WebServiceCheckedException(e);
        }
    }
	
    @Override
    public List<X509CertificateDTO> getAutoSelectEncryptionCertificates()
    throws WebServiceCheckedException
    {
    	return userWS.getAutoSelectEncryptionCertificates(email);
    }
    
    @Override
    public X509CertificateDTO getSigningKeyAndCertificate()
    throws WebServiceCheckedException
    {
    	return userWS.getSigningKeyAndCertificate(email);
    }

    @Override
    public void setSigningKeyAndCertificate(String thumbprint)
    throws WebServiceCheckedException
    {
    	userWS.setSigningKeyAndCertificate(email, thumbprint);
    }
    
    @Override
    public UserPreferences getPreferences()
    throws WebServiceCheckedException
    {
    	UserPreferencesDTO userPreferencesDTO = userWS.getUserPreferences(email);
    
    	return new UserPreferencesImpl(userPreferencesWS, hierarchicalPropertiesWS, 
    			userPreferencesDTO);
    }
}
