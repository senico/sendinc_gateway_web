/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.pages;

import java.io.IOException;

import mitm.application.djigzo.admin.FactoryRoles;
import mitm.application.djigzo.james.JamesRepository;
import mitm.application.djigzo.ws.JamesRepositoryManagerWS;
import mitm.common.ws.WebServiceCheckedException;
import mitm.djigzo.web.common.MailAgent;
import mitm.djigzo.web.common.QueueType;
import mitm.djigzo.web.components.JamesRepositoryManager;
import mitm.djigzo.web.components.PostfixSpoolManager;
import mitm.djigzo.web.components.QuarantineManager;
import mitm.djigzo.web.pages.dlp.quarantine.QuarantineViewMIME;

import org.apache.tapestry5.Block;
import org.apache.tapestry5.ComponentResources;
import org.apache.tapestry5.Link;
import org.apache.tapestry5.PersistenceConstants;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.internal.services.LinkFactory;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.annotation.Secured;

@IncludeStylesheet("context:styles/pages/queues.css")
@Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_QUEUE_MANAGER})
public class Queues
{
	private final static Logger logger = LoggerFactory.getLogger(Queues.class);
	
    /*
     * CSS classname for the active link
     */
    private final static String ACTIVE_CLASS_NAME = "activeLink";

    /*
     * CSS classname for the inactive link
     */
    private final static String NOT_ACTIVE_CLASS_NAME = "notActiveLink";
    
	@Inject
	private Response response;
	
	@Inject
	private LinkFactory linkFactory;
	
    @Inject
    private ComponentResources resources;
	
    @Inject
    private Block mtaBlock;

    @Inject
    private Block mpaBlock;

    @Inject
    private Block quarantineBlock;
    
	@Inject
	private JamesRepositoryManagerWS jamesRepositoryManagerWS;
    
    @Persist
    private JamesRepository activeRepository;
    
    @Persist
    private MailAgent activeAgent;
    
    /*
     * True if an error has occurred
     */
    @Persist(PersistenceConstants.FLASH)
    private boolean error;

    /*
     * Error message if an error has occurred
     */
    @Persist(PersistenceConstants.FLASH)
    private String errorMessage;
    
	@Component(id = "jamesRepositoryManager", parameters = {"showMoveSelected = prop:showMoveSelected", 
			"showRespoolSelected = prop:showRespoolSelected"})
    private JamesRepositoryManager jamesRepositoryManager;
    
	@SetupRender
	@Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_QUEUE_MANAGER})
	protected void setupRender() {
		/*
		 * Empty on purpose
		 */
	}
	
	public JamesRepository getActiveRepository()
	{
	    if (activeRepository == null) {
	        activeRepository = JamesRepository.OUTGOING;
	    }
	    
	    return activeRepository;
	}
	
	private QueueType getActiveQueue()
	{
	    MailAgent mailAgent = getActiveAgent();
	    
	    switch(mailAgent)
	    {
	    case MTA        : return QueueType.MTA;
	    case QUARANTINE : return QueueType.QUARANTINE;
	    case MPA        :
            switch(getActiveRepository())
            {
            case OUTGOING : return QueueType.MPA_OUTGOING;
            case ERROR    : return QueueType.MPA_ERROR;
            case SPOOL    : return QueueType.MPA_SPOOL;
            case RESPOOL  : return QueueType.MPA_RESPOOL;
            default:
                throw new IllegalArgumentException("Unknown JamesRepository.");
            }
	    }
	    
        throw new IllegalArgumentException("Unknown MailAgent: " + mailAgent);
	}
	
	private MailAgent getActiveAgent()
	{
	    if (activeAgent == null) {
	        activeAgent = MailAgent.MTA;
	    }
	    
	    return activeAgent;
	}

	public Block getActiveBlock()
    {
        Block activeBlock;
        
        switch(getActiveAgent())
        {
        case MTA        : activeBlock = mtaBlock; break;
        case MPA        : activeBlock = mpaBlock; break;
        case QUARANTINE : activeBlock = quarantineBlock; break;
        default:
            throw new IllegalArgumentException("Unknown Agent.");
        }
        
        return activeBlock;
    }
	
	public String getQueueName()
	{
	    switch(getActiveQueue())
	    {
        case MTA          : return "Mail transfer agent";
        case MPA_OUTGOING : return "Mail processing agent outgoing";
        case MPA_ERROR    : return "Mail processing agent error";
        case MPA_SPOOL    : return "Mail processing agent spool";
        case MPA_RESPOOL  : return "Mail processing agent respool";
        case QUARANTINE   : return "DLP Quarantine";
        default:
            throw new IllegalArgumentException("Unknown ActiveItem.");
	    }
	}
	
    public String getMTAClass() {
        return getActiveQueue() == QueueType.MTA ? ACTIVE_CLASS_NAME : NOT_ACTIVE_CLASS_NAME;
    }

    public String getMPAOutgoingClass() {
        return getActiveQueue() == QueueType.MPA_OUTGOING ? ACTIVE_CLASS_NAME : NOT_ACTIVE_CLASS_NAME;
    }

    public String getMPAErrorClass() {
        return getActiveQueue() == QueueType.MPA_ERROR ? ACTIVE_CLASS_NAME : NOT_ACTIVE_CLASS_NAME;
    }

    public String getMPASpoolClass() {
        return getActiveQueue() == QueueType.MPA_SPOOL ? ACTIVE_CLASS_NAME : NOT_ACTIVE_CLASS_NAME;
    }

    public String getMPAReSpoolClass() {
        return getActiveQueue() == QueueType.MPA_RESPOOL ? ACTIVE_CLASS_NAME : NOT_ACTIVE_CLASS_NAME;
    }

    public String getQuarantineClass() {
        return getActiveQueue() == QueueType.QUARANTINE ? ACTIVE_CLASS_NAME : NOT_ACTIVE_CLASS_NAME;
    }
    
    public boolean isShowMoveSelected()
    {
    	return getActiveQueue() == QueueType.MPA_ERROR; 
    }

    public boolean isShowRespoolSelected()
    {
    	return getActiveQueue() == QueueType.MPA_RESPOOL; 
    }

    /*
     * Called when event handler throws an unhandled exception
     */
    protected Object onException(Throwable cause)
    {
        error = true;
        errorMessage = cause.getMessage();

        return this;
    }
    
	@OnEvent(PostfixSpoolManager.QUEUE_ID_CLICKED_EVENT)
	protected void onQueueIDClicked(String queueID) 
	throws IOException
	{
        Link link = linkFactory.createPageRenderLink("mta/message", false, queueID);

        response.sendRedirect(link);
	}

    @OnEvent(JamesRepositoryManager.MAIL_NAME_CLICKED_EVENT)
    protected void onMailNameClicked(String name)
    throws IOException
    {
        Link link = linkFactory.createPageRenderLink("mpa/message", false, getActiveRepository(), name);

        response.sendRedirect(link);
    }

    @OnEvent(JamesRepositoryManager.MOVE_SELECTED_CLICKED_EVENT)
    protected void onMoveSelectedClicked()
    {
        JamesRepository sourceRepository = jamesRepositoryManager.getRepository();
        JamesRepository destinationRepository = JamesRepository.RESPOOL;
        
        /*
         * Note: we need to clone the set to make sure that concurrent modifications
         * are possible.
         */
        String[] selected = jamesRepositoryManager.getSelected().toArray(new String[]{});
        
		for (String mailName : selected)
    	{
    		try {
    			/*
    			 * Move the mail item from the current repository to the respool repository
    			 */    			
				jamesRepositoryManagerWS.moveMail(sourceRepository, destinationRepository, 
						mailName, null);
			} 
    		catch (WebServiceCheckedException e) 
    		{
    			logger.error("Error moving mail " + mailName + " from repository " + 
    					sourceRepository + " to " + destinationRepository, e);
			}
    	}
    }

    @OnEvent(JamesRepositoryManager.RESPOOL_SELECTED_CLICKED_EVENT)
    protected void onRespoolSelectedClicked()
    {
        JamesRepository sourceRepository = jamesRepositoryManager.getRepository();

        /*
         * Note: we need to clone the set to make sure that concurrent modifications
         * are possible.
         */
        String[] selected = jamesRepositoryManager.getSelected().toArray(new String[]{});

		for (String mailName : selected)
    	{
    		try {
				jamesRepositoryManagerWS.respoolMail(sourceRepository, mailName, "root",
				        true /* remove existing Mail attributes before respooling */);
			} 
    		catch (WebServiceCheckedException e) 
    		{
    			logger.error("Error respooling mail " + mailName + " from repository " + 
    					sourceRepository, e);
			}
    	}
    }

    @OnEvent(QuarantineManager.VIEW_MIME_CLICKED_EVENT)
    protected Link onViewMIME(String id)
    {
        return resources.createPageLink(QuarantineViewMIME.class, false, id);
    }
    
    @OnEvent(component = "mtaSpool")
    protected void showMTASpool()
    {
        activeAgent = MailAgent.MTA;
    }

    @OnEvent(component = "mpaSpoolOutgoing")
    protected void showMPASpoolOutgoing() 
    {
        activeAgent = MailAgent.MPA;
        activeRepository = JamesRepository.OUTGOING;
    }
	
    @OnEvent(component = "mpaSpoolError")
    protected void showMPASpoolError() 
    {
        activeAgent = MailAgent.MPA;
        activeRepository = JamesRepository.ERROR;
    }

    @OnEvent(component = "mpaSpool")
    protected void showMPASpool() 
    {
        activeAgent = MailAgent.MPA;
        activeRepository = JamesRepository.SPOOL;
    }

    @OnEvent(component = "mpaRespool")
    protected void showMPARespool() 
    {
        activeAgent = MailAgent.MPA;
        activeRepository = JamesRepository.RESPOOL;
    }

    @OnEvent(component = "quarantine")
    protected void showQuarantine() 
    {
        activeAgent = MailAgent.QUARANTINE;
    }

    public boolean isError() {
        return error;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}
