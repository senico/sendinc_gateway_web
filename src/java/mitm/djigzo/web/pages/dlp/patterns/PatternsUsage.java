/*
 * Copyright (c) 2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.pages.dlp.patterns;

import java.util.LinkedList;
import java.util.List;

import mitm.application.djigzo.admin.FactoryRoles;
import mitm.application.djigzo.ws.PolicyPatternManagerWS;
import mitm.common.ws.WebServiceCheckedException;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.tapestry5.ComponentResources;
import org.apache.tapestry5.PersistenceConstants;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.beaneditor.BeanModel;
import org.apache.tapestry5.corelib.components.Grid;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.ioc.annotations.Value;
import org.apache.tapestry5.services.BeanModelSource;
import org.springframework.security.annotation.Secured;

@Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_DLP_MANAGER})
@IncludeStylesheet("context:styles/pages/dlp/patterns/usage.css")
public class PatternsUsage
{
    @Inject
    private PolicyPatternManagerWS policyPatternManagerWS;

    @Inject
    private BeanModelSource beanModelSource;

    @Inject
    private ComponentResources resources;
    
    @Inject
    @Value("${patternsUsage.rowsPerPage}")
    private int rowsPerPage;    

    /*
     * True if and error has occurred
     */
    @Persist(PersistenceConstants.FLASH)
    private boolean error;

    /*
     * Error message if an error has occurred
     */
    @Persist(PersistenceConstants.FLASH)
    private String errorMessage;
    
    /*
     * The name of the pattern for which the usage is being shown
     */
    private String patternName;
    
    /*
     * The Usage being drawn by the grid
     */
    private Usage usage;
    
    public static class Usage
    {        
        final String type;
        final String category;
        final String name;

        Usage(String type, String category, String name)
        {
            this.type = type;
            this.category = category;
            this.name = name;
           
        }

        public String getType() {
            return type;
        }

        public String getCategory() {
            return category;
        }

        public String getName() {
            return name;
        }        
    }
    
    @SuppressWarnings("unused")
    @Component(parameters = {"source = source", "model = model", "volatile = true", "row = usage", 
            "rowsPerPage = prop:rowsPerPage", "reorder = type,category,name"})
    private Grid grid;
    
    @SetupRender
    @Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_DLP_MANAGER})
    public void setupRender() {
        /*
         * Empty on purpose
         */
    }
    
    @Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_DLP_MANAGER})
    public void onActivate(String patternName) {
        this.patternName = patternName;
    }
    
    public List<Object> onPassivate()
    {
        List<Object> context = new LinkedList<Object>();
        
        if (patternName != null) {
            context.add(patternName);
        }
        
        return context;
    }
    
    public BeanModel<Usage> getModel()
    {
        BeanModel<Usage> model = beanModelSource.createDisplayModel(Usage.class, 
                resources.getMessages());
        
        /*
         * Disable all sorting
         */
        for (String column : model.getPropertyNames()) {
            model.get(column).sortable(false);
        }
        
        return model;
    }
    
    public List<Usage> getSource()
    {
        List<Usage> usages = new LinkedList<PatternsUsage.Usage>();
        
        try {
            List<String> infos = policyPatternManagerWS.getInUseInfo(patternName);
            
            if (CollectionUtils.isNotEmpty(infos))
            {
                for (String info : infos)
                {
                    String[] data = StringUtils.split(info, ',');
                    
                    if (ArrayUtils.getLength(data) == 3) {
                        usages.add(new Usage(data[0], data[1], data[2]));
                    }
                }
            }
        } 
        catch (WebServiceCheckedException e)
        {
            errorMessage = e.getMessage();
            error = true;
        }
        
        return usages;
    }
    
    public Usage getUsage() {
        return usage;
    }
    
    public void setUsage(Usage usage) {
        this.usage = usage;
    }
    
    public boolean isError() {
        return error;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public int getRowsPerPage() {
        return rowsPerPage;
    }

    public String getPatternName() {
        return patternName;
    }
}
