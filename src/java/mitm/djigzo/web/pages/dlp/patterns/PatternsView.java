/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.pages.dlp.patterns;

import java.util.Arrays;
import java.util.List;

import mitm.application.djigzo.admin.FactoryRoles;
import mitm.application.djigzo.ws.BinaryDTO;
import mitm.application.djigzo.ws.PolicyPatternManagerWS;
import mitm.application.djigzo.ws.PolicyPatternNodeDTO;
import mitm.common.ws.WebServiceCheckedException;
import mitm.djigzo.web.common.ContentTypes;
import mitm.djigzo.web.common.YesNo;
import mitm.djigzo.web.common.streamresponse.InputStreamFileResponse;
import mitm.djigzo.web.components.DLPPatternGrid;
import mitm.djigzo.web.grid.PolicyPatternNodeGridDataSource;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.tapestry5.Asset;
import org.apache.tapestry5.PersistenceConstants;
import org.apache.tapestry5.StreamResponse;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Path;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.grid.GridDataSource;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.ioc.annotations.Value;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.annotation.Secured;

@Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_DLP_MANAGER})
@IncludeStylesheet("context:styles/pages/dlp/patterns/view.css")
public class PatternsView
{
    private final static Logger logger = LoggerFactory.getLogger(PatternsView.class);
    
    @Inject
    private PolicyPatternManagerWS policyPatternManagerWS;
    
    @Inject
    @Value("${dlp.patterns.rowsPerPage}")
    private int rowsPerPage;    
    
    @Inject
    @Path("context:icons/information.png")
    private Asset informationAsset;
    
    /*
     * True if and error has occurred
     */
    @Persist(PersistenceConstants.FLASH)
    private boolean error;

    /*
     * Error message if an error has occurred
     */
    @Persist(PersistenceConstants.FLASH)
    private String errorMessage;
    
    /*
     * Detailed error message if an error has occurred
     */
    @Persist(PersistenceConstants.FLASH)
    private String errorDetails;
    
    @Component(parameters = {"source = source", "rowsPerPage = prop:rowsPerPage", "addNameLink=true"})
    private DLPPatternGrid patternGrid; 
    
    public GridDataSource getSource() {
        return new PolicyPatternNodeGridDataSource(policyPatternManagerWS);
    }
    
    /*
     * Event handler for delete event from the DLPPatternGrid
     */
    @OnEvent(component = "patternGrid", value = DLPPatternGrid.DELETE_PATTERN_EVENT)
    @Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_DLP_MANAGER})
    protected void deletePattern(String name) 
    {
        try {
            policyPatternManagerWS.deletePattern(name);
        }
        catch(WebServiceCheckedException e)
        {
            logger.error("Error deleting pattern: " + name, e);
            /*
             * assume the exception is thrown because the pattern is still in use 
             */
            errorMessage = "Pattern could not be deleted. The pattern is still in use.";
            errorDetails = e.getMessage();
            error = true;
        }
    }

    @OnEvent(component = "deleteSelected")
    @Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_DLP_MANAGER})
    protected void deleteSelected()
    {
        /*
         * Note: we need to clone the set to make sure that concurrent modifications
         * are possible.
         */
        String[] selected = patternGrid.getSelected().toArray(new String[]{});
        
        for (String name : selected)
        {
            try {
                policyPatternManagerWS.deletePattern(name);
            }
            catch(WebServiceCheckedException e)
            {
                logger.error("Error deleting pattern: " + name, e);
                /*
                 * assume the exception is thrown because the pattern is still in use 
                 */
                errorMessage = "Not all patterns could be deleted. Some patterns are still in use.";
                errorDetails = e.getMessage();
                error = true;
            }
        }
    }

    @OnEvent(component = "exportSelected")
    @Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_DLP_MANAGER})
    protected StreamResponse exportSelected()
    {
        StreamResponse result = null;
        
        /*
         * Note: we need to clone the set to make sure that concurrent modifications
         * are possible.
         */
        List<String> selected = Arrays.asList(patternGrid.getSelected().toArray(new String[]{}));
        
        if (selected.size() > 0)
        {
            try {
                /*
                 * We assume that the id's of the checkboxes are names of patterns
                 */
                BinaryDTO xml = policyPatternManagerWS.exportToXML(selected);
    
                result = new InputStreamFileResponse(xml.getDataHandler().getInputStream(), 
                        ContentTypes.X_DOWNLOAD, "patterns.xml");
    
                /*
                 * We need to clear the selected. This is not visually reflected (we cannot refresh the page
                 * and download the xml afaik) so the check boxes are unchecked using Javascript.
                 */
                patternGrid.getSelected().clear();
            }
            catch (Exception e)
            {
                logger.error("Error exporting patterns", e);
    
                error = true;
                errorMessage = ExceptionUtils.getRootCauseMessage(e);
            }
        }
        
        return result;
    }

    @OnEvent(component = "patternGrid", value = DLPPatternGrid.IS_IN_USE_EVENT)
    @Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_DLP_MANAGER})
    public YesNo isInUse(PolicyPatternNodeDTO policyPatternNode)
    {
        YesNo inUse = YesNo.NO;
        
        try {
            inUse = policyPatternManagerWS.isInUse(policyPatternNode.getName()) ? YesNo.YES : YesNo.NO; 
        } 
        catch (WebServiceCheckedException e) {
            logger.error("Error calling isInUse", e);
        }
        
        return inUse;
    }
    
    public int getRowsPerPage() {
        return rowsPerPage;
    }

    public boolean isError() {
        return error;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public String getErrorDetails() {
        return errorDetails;
    }

    public Asset getInformationAsset() {
        return informationAsset;
    }
}
