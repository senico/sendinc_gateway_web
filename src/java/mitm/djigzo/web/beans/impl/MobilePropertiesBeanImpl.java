/*
 * Copyright (c) 2011, Martijn Brinkers, Djigzo.
 *
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar,
 * wsdl4j-1.6.1.jar (or modified versions of these libraries),
 * containing parts covered by the terms of Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to
 * convey the resulting work.
 */
package mitm.djigzo.web.beans.impl;


import mitm.application.djigzo.relay.RelayBounceMode;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.util.Check;
import mitm.djigzo.web.beans.MobilePropertiesBean;
import mitm.djigzo.web.beans.BooleanProperty;
import mitm.djigzo.web.beans.EnumProperty;
import mitm.djigzo.web.beans.LongProperty;
import mitm.djigzo.web.beans.StringProperty;
import mitm.djigzo.web.entities.SaveableUserProperties;


/**
 *
 * @author jon
 */
public class MobilePropertiesBeanImpl implements MobilePropertiesBean
{
    private final BooleanProperty blackberryRecipient = new BooleanProperty();
    private final BooleanProperty stripUnsupportedFormats = new BooleanProperty();
    private final BooleanProperty relayAllowed = new BooleanProperty();
    private final StringProperty relayEmail = new StringProperty();
    private final LongProperty relayValidityInterval = new LongProperty();
    private final EnumProperty<RelayBounceMode> relayBounceMode = new EnumProperty<RelayBounceMode>();

	private final SaveableUserProperties userProperties;

	public MobilePropertiesBeanImpl(SaveableUserProperties userProperties)
	throws HierarchicalPropertiesException
	{
		Check.notNull(userProperties, "userProperties");

		this.userProperties = userProperties;

		loadProperties();
	}

	private void loadProperties()
	throws HierarchicalPropertiesException
	{
	}

    @Override
	public BooleanProperty getBlackberryRecipient() {
		return blackberryRecipient;
	}

    @Override
    public BooleanProperty getStripUnsupportedFormats() {
        return stripUnsupportedFormats;
    }

    @Override
    public BooleanProperty getRelayAllowed() {
        return relayAllowed;
    }

    @Override
    public StringProperty getRelayEmail() {
        return relayEmail;
    }

    @Override
    public LongProperty getRelayValidityInterval() {
        return relayValidityInterval;
    }

    @Override
    public EnumProperty<RelayBounceMode> getRelayBounceMode() {
        return relayBounceMode;
    }

    @Override
    public void save()
	throws HierarchicalPropertiesException
	{
		userProperties.save();
	}
}
