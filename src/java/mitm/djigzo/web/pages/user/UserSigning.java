/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.pages.user;

import mitm.application.djigzo.admin.FactoryRoles;
import mitm.common.mail.EmailAddressUtils;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.ws.WebServiceCheckedException;
import mitm.djigzo.web.beans.UserBean;
import mitm.djigzo.web.beans.impl.UserBeanImpl;
import mitm.djigzo.web.common.CertificateFilterBy;
import mitm.djigzo.web.common.CertificateFilterSettings;
import mitm.djigzo.web.components.SigningCertificate;
import mitm.djigzo.web.entities.User;
import mitm.djigzo.web.entities.UserManager;
import mitm.djigzo.web.pages.Users;

import org.apache.tapestry5.PersistenceConstants;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.internal.services.LinkFactory;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.springframework.security.annotation.Secured;

@Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_USER_MANAGER})
public class UserSigning 
{
	@Inject
	private UserManager userManager;

	@Inject
	private LinkFactory linkFactory;
	
	@Property
	private UserBean userBean;

    /*
     * True if changes were applied
     */
    @Persist(PersistenceConstants.FLASH)
    private boolean applied;
	
	@Component(parameters = {"selectedCertificate = userBean.signingKeyAndCertificate", "legendBlock = block:legendBlock"})
	private SigningCertificate signingCertificate;

    public boolean isApplied() {
        return applied;
    }
	
	@SetupRender
	@Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_USER_MANAGER})
	protected void setupRender() {
		/*
		 * Empty on purpose
		 */
	}
	
	private UserBean createUserBean(String email) 
	throws WebServiceCheckedException, HierarchicalPropertiesException
	{
		String filteredEmail = EmailAddressUtils.canonicalizeAndValidate(email, true);
		
		if (filteredEmail == null) {
			return null;
		}
		
		User user = userManager.getUser(filteredEmail, false /* null if not exist */);
		
		if (user == null) {
			return null;			
		}
		
		return new UserBeanImpl(user);
	}
	
    @Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_USER_MANAGER})
	public Object onActivate(String email)
	throws WebServiceCheckedException, HierarchicalPropertiesException 
	{
		return onActivate(email, false);
	}

    @Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_USER_MANAGER})
	public Object onActivate(String email, boolean setFilter)
	throws WebServiceCheckedException, HierarchicalPropertiesException 
	{
		userBean = createUserBean(email);
		
		if (userBean == null) {
			return Users.class;
		}
		
		/*
		 * Set the filter to match the email address of the user
		 */
		CertificateFilterSettings filterSettings = signingCertificate.getFilterSettings();

		if (setFilter)
		{
			filterSettings.setFilterBy(CertificateFilterBy.EMAIL);
			filterSettings.setFilter(userBean.getEmail());
		}
		
		return null;
	}
	
	public String onPassivate() 
	{
		String email = null;
		
		if (userBean != null) {
			email = userBean.getEmail();
		}
		
		return email;
	}
	
	@OnEvent(SigningCertificate.APPLY_EVENT)
	public void onApply()
	throws HierarchicalPropertiesException, WebServiceCheckedException
	{
		userBean.getPreferences().setKeyAndCertificate(signingCertificate.
				getCertificateGrid().getSelected());
		
		userBean.save();
		
		applied = true;
	}
	
    /*
     * Event handler called when the cancel button is pressed.
     */
	protected Object onCancel()
	{
		if (userBean == null) {
			return Users.class;
		}
		
        return linkFactory.createPageRenderLink("user/edit", false, userBean.getEmail());
	}
}
