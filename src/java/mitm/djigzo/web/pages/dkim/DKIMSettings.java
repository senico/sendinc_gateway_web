/*
 * Copyright (c) 2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.pages.dkim;

import java.io.IOException;
import java.io.StringReader;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;

import mitm.application.djigzo.admin.FactoryRoles;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.ws.WebServiceCheckedException;
import mitm.djigzo.web.beans.DKIMPropertiesBean;
import mitm.djigzo.web.beans.impl.DKIMPropertiesBeanImpl;
import mitm.djigzo.web.entities.GlobalPreferencesManager;
import mitm.djigzo.web.pages.GlobalPreferences;

import org.apache.commons.lang.StringUtils;
import org.apache.tapestry5.PersistenceConstants;
import org.apache.tapestry5.annotations.Cached;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.corelib.components.Form;
import org.apache.tapestry5.corelib.components.TextArea;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.bouncycastle.openssl.PEMReader;
import org.springframework.security.annotation.Secured;

@Secured({FactoryRoles.ROLE_ADMIN})
@IncludeStylesheet("context:styles/pages/dkim/settings.css")
public class DKIMSettings
{
    @Inject
    private GlobalPreferencesManager globalPreferencesManager;
    
    /*
     * True if changes were applied
     */
    @Persist(PersistenceConstants.FLASH)
    private boolean applied;
    
    /*
     * The PEM encoded keypair
     */
    private String keyPair; 
    
    @Component(id = "form")
    private Form form;
    
    @Component(id = "keyPair", parameters = {"value = keyPair", "validate = required, maxLength=8192"})
    private TextArea keyPairField;
    
    @SetupRender
    @Secured({FactoryRoles.ROLE_ADMIN})
    public void setupRender()
    throws HierarchicalPropertiesException, WebServiceCheckedException
    {
        keyPair = getProperties().getKeyPair().getUserValue();
    }

    @Cached
    private DKIMPropertiesBean getProperties()
    throws HierarchicalPropertiesException, WebServiceCheckedException
    {
        return new DKIMPropertiesBeanImpl(globalPreferencesManager.
                getPreferences().getProperties());
    }
    
    private void logKeyPairError(String errorMessage) {
        form.recordError(keyPairField, errorMessage);
    }
    
    protected void onValidateForm()
    throws HierarchicalPropertiesException, WebServiceCheckedException
    {
        keyPair = StringUtils.trimToNull(keyPair);
        
        if (keyPair != null)
        {
            /*
             * Check if the keyPair is really a PEM encoded keypair 
             */
            PEMReader pem = new PEMReader(new StringReader(keyPair));
            
            Object o = null;
            
            try {
                o = pem.readObject();
            } 
            catch (IOException e) {
                logKeyPairError("The input is not valid PEM encoded");
            }
            
            if (o != null)
            { 
                if (!(o instanceof KeyPair))
                {
                    String clazz = o.getClass().toString();
                    
                    if (o instanceof PublicKey) {
                        clazz = "public key";
                    }
                    else if (o instanceof PrivateKey) {
                        clazz = "private key";
                    } 
                    
                    logKeyPairError("The input is not a valid key pair but is a " + clazz);
                }
                else {
                    KeyPair keyPair = (KeyPair) o;
                    
                    if (keyPair.getPrivate() == null) {
                        logKeyPairError("The private key is missing");                    
                    }
                    else if (keyPair.getPublic() == null) {
                        logKeyPairError("The public key is missing");                    
                    }
                }
            }    
            else {
                logKeyPairError("The input does not contain a valid key pair");                    
            }
        }
        else {
            logKeyPairError("The input does not contain a valid key pair");                    
        }
    }
    
    public void onSuccess()
    throws HierarchicalPropertiesException, WebServiceCheckedException
    {
        DKIMPropertiesBean properties = getProperties();
        
        properties.getKeyPair().setValue(keyPair);
        properties.getKeyPair().setInherit(false);
        
        properties.save();
        
        applied = true;
    }

    /*
     * Event handler called when the cancel button is pressed.
     */
    protected Object onCancel() {
        return GlobalPreferences.class;
    }
    
    public boolean isApplied() {
        return applied;
    }

    public String getKeyPair() {
        return keyPair;
    }

    public void setKeyPair(String keyPair) {
        this.keyPair = keyPair;
    }
}
