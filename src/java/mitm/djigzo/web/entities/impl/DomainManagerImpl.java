/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.entities.impl;

import java.util.LinkedList;
import java.util.List;

import mitm.application.djigzo.ws.DomainDTO;
import mitm.application.djigzo.ws.DomainWS;
import mitm.application.djigzo.ws.DomainsWS;
import mitm.application.djigzo.ws.HierarchicalPropertiesWS;
import mitm.application.djigzo.ws.UserPreferencesWS;
import mitm.common.hibernate.SortDirection;
import mitm.common.util.Check;
import mitm.common.ws.WebServiceCheckedException;
import mitm.djigzo.web.entities.Domain;
import mitm.djigzo.web.entities.DomainManager;

public class DomainManagerImpl implements DomainManager
{
	private final DomainsWS domainsWS;
	private final DomainWS domainWS;
	private final UserPreferencesWS userPreferencesWS;
	private final HierarchicalPropertiesWS hierarchicalPropertiesWS;
	
	public DomainManagerImpl(DomainsWS domainsWS, DomainWS domainWS, UserPreferencesWS userPreferencesWS,
			HierarchicalPropertiesWS hierarchicalPropertiesWS)
	{
		Check.notNull(domainsWS, "domainsWS");
		Check.notNull(domainWS, "domainWS");
		Check.notNull(userPreferencesWS, "userPreferencesWS");
		Check.notNull(hierarchicalPropertiesWS, "hierarchicalPropertiesWS");
		
		this.domainsWS = domainsWS;
		this.domainWS = domainWS;
		this.userPreferencesWS = userPreferencesWS;
		this.hierarchicalPropertiesWS = hierarchicalPropertiesWS;
	}

    @Override
	public boolean addDomain(String domain) 
	throws WebServiceCheckedException 
	{
		boolean added = false;
		
		if (!isDomain(domain)) 
		{
			domainsWS.addDomain(domain);
			
			added = true;
		}
		
		return added;
	}

    @Override
	public boolean deleteDomain(String domain) 
	throws WebServiceCheckedException 
	{
		boolean deleted = false;
		
		if (domainsWS.isDomain(domain))
		{
			domainsWS.deleteDomain(domain);
			
			deleted = true;
		}
		
		return deleted;
	}

    @Override
	public Domain getDomain(String domain) 
	throws WebServiceCheckedException 
	{
		Domain result = null;
		
		if (domainsWS.isDomain(domain)) {
			result = new DomainImpl(domainWS, userPreferencesWS, hierarchicalPropertiesWS, 
					domain, null /* we do not yet now the locality */, false /* not in use */);
		}
		
		return result;
	}

    @Override
	public int getDomainCount() 
	throws WebServiceCheckedException 
	{
		return domainsWS.getDomainCount();
	}

    @Override
	public List<Domain> getDomains(Integer firstResult, Integer maxResults, SortDirection sortDirection)
	throws WebServiceCheckedException 
	{
		List<Domain> domains = new LinkedList<Domain>();
		
		List<DomainDTO> domainsDTO = domainsWS.getDomains(firstResult, maxResults, sortDirection);
		
		/*
		 * The webservices return null when the collection returns no items.
		 */
		if (domainsDTO != null)
		{
			for (DomainDTO domain : domainsDTO)
			{
				domains.add(new DomainImpl(domainWS, userPreferencesWS, hierarchicalPropertiesWS, 
						domain.getDomain(), domain.getUserLocality(), domain.isInUse()));
			}
		}
		
		return domains;
	}

    @Override
	public boolean isDomain(String domain) 
	throws WebServiceCheckedException 
	{
		return domainsWS.isDomain(domain);
	}
	
    @Override
    public boolean isInUse(String domain)
    throws WebServiceCheckedException
	{
		return domainsWS.isDomainInUse(domain);
	}
}
