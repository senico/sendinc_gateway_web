/*
 * Copyright (c) 2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.common.security;

import java.util.concurrent.atomic.AtomicReference;

import mitm.application.djigzo.admin.FactoryRoles;
import mitm.application.djigzo.ws.LoginWS;
import mitm.application.djigzo.ws.PortalUserDTO;
import mitm.common.mail.EmailAddressUtils;
import mitm.common.ws.WebServiceCheckedException;
import mitm.djigzo.web.utils.PasswordCodec;
import mitm.djigzo.web.utils.PasswordCodec.PasswordAndSalt;

import org.apache.commons.lang.StringUtils;
import org.springframework.dao.DataAccessException;
import org.springframework.security.BadCredentialsException;
import org.springframework.security.GrantedAuthority;
import org.springframework.security.GrantedAuthorityImpl;
import org.springframework.security.userdetails.UserDetails;
import org.springframework.security.userdetails.UsernameNotFoundException;

public class PortalUserDetailsServiceExtImpl implements PortalUserDetailsServiceExt 
{
    /*
     * Webservice for getting the login credentials
     */
    private final AtomicReference<LoginWS> loginWSRef = new AtomicReference<LoginWS>();

    /*
     * service for getting extracting the salt from the password
     */
    private final AtomicReference<PasswordCodec> passwordCodecRef = new AtomicReference<PasswordCodec>();

    @Override
    public UserDetails loadUserByUsername(String email)
    throws UsernameNotFoundException, DataAccessException
    {
        try {
            String validatedEmail = EmailAddressUtils.canonicalizeAndValidate(email, true);

            if (validatedEmail == null) {
                throw new UsernameNotFoundException("User with username " + email + " is not valid");
            }

            PortalUserDTO portalUser = getLoginWS().getPortalUser(validatedEmail);
            
            if (portalUser == null) {
                throw new UsernameNotFoundException("User with username " + email + " not found");
            }
            
            String encodedPassword = portalUser.getPassword(); 
            
            if (StringUtils.isEmpty(encodedPassword)) {
                throw new UsernameNotFoundException("There is no password for user " + validatedEmail);
            }
            
            PasswordAndSalt passwordAndSalt = getPasswordCodec().getPasswordAndSalt(encodedPassword);
            
            GrantedAuthority[] authorites = new GrantedAuthority[]{
                    new GrantedAuthorityImpl(FactoryRoles.ROLE_PORTAL_LOGIN), 
                    new GrantedAuthorityImpl(FactoryRoles.ROLE_ANONYMOUS)};
            
            return new PortalUserDetails(validatedEmail, passwordAndSalt.getPassword(), passwordAndSalt.getSalt(), 
                    authorites, portalUser.isEnabled());
        } 
        catch (WebServiceCheckedException e) {
            throw new BadCredentialsException("Error loading user by username", e);
        } 
    }

    @Override
    public void setLoginWS(LoginWS loginWS) {
        this.loginWSRef.set(loginWS);
    }

    private LoginWS getLoginWS()
    {
        LoginWS loginWS = loginWSRef.get();
        
        if (loginWS == null) {
            throw new BadCredentialsException("LoginWS is not set");
        }
        
        return loginWS;
    }
    
    @Override
    public void setPasswordCodec(PasswordCodec passwordCodec) {
        this.passwordCodecRef.set(passwordCodec);
    }

    private PasswordCodec getPasswordCodec()
    {
        PasswordCodec passwordCodec = passwordCodecRef.get();
        
        if (passwordCodec == null) {
            throw new BadCredentialsException("passwordCodec is not set");
        }
        
        return passwordCodec;
    }
}
