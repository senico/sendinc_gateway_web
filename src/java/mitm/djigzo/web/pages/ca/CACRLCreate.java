/*
 * Copyright (c) 2009-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.pages.ca;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.regex.Pattern;

import mitm.application.djigzo.admin.FactoryRoles;
import mitm.application.djigzo.ws.CAWS;
import mitm.application.djigzo.ws.CRLCreateParametersDTO;
import mitm.application.djigzo.ws.CertificateStore;
import mitm.application.djigzo.ws.KeyAndCertStoreWS;
import mitm.application.djigzo.ws.X509CRLDTO;
import mitm.application.djigzo.ws.X509CertificateDTO;
import mitm.common.ws.WebServiceCheckedException;
import mitm.djigzo.web.beans.X509CertificateBean;
import mitm.djigzo.web.beans.impl.X509CertificateBeanImpl;
import mitm.djigzo.web.common.CACRLCreateSubmitButton;
import mitm.djigzo.web.common.GenericSelectionModel;
import mitm.djigzo.web.common.SignatureAlgorithm;
import mitm.djigzo.web.common.ValidityCheck;
import mitm.djigzo.web.services.CertificateStoreMarker;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.tapestry5.Block;
import org.apache.tapestry5.ComponentResources;
import org.apache.tapestry5.Link;
import org.apache.tapestry5.PersistenceConstants;
import org.apache.tapestry5.SelectModel;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.corelib.components.Checkbox;
import org.apache.tapestry5.corelib.components.Form;
import org.apache.tapestry5.corelib.components.Select;
import org.apache.tapestry5.corelib.components.TextField;
import org.apache.tapestry5.internal.services.LinkFactory;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.Request;
import org.apache.tapestry5.services.Response;
import org.springframework.security.annotation.Secured;

@Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_PKI_MANAGER})
@IncludeStylesheet("context:styles/pages/ca/caCRLCreate.css")
public class CACRLCreate
{
    /*
     * Pattern for checking a serial number (in hex format)
     */
    private final static Pattern SERIAL_NUMBER_PATTERN = Pattern.compile("\\s*[0-9a-fA-F \\-]+\\s*");
    
    /*
     * Pattern used to remove all non hex characters
     */
    private final static Pattern REMOVE_NON_HEX = Pattern.compile("[^0-9a-fA-F]*");
    
    @Inject
    @Property
    @CertificateStoreMarker
    private KeyAndCertStoreWS keyAndCertStoreWS;

    @Inject
    private CAWS caWS;
    
    @Inject
    private Request request;
    
    @Inject
    private ComponentResources resources;
    
    @Inject
    private Response response;
    
    @Inject
    private LinkFactory linkFactory;
    
    @Component(id = "serialNumbersForm")
    private Form serialNumbersForm;
    
    @SuppressWarnings("unused")
    @Component(id = "serialNumbers", parameters = {"model = serialNumbersModel", "value = prop:selectedSerialNumber", "blankOption = NEVER"})
    private Select myNetworksField;
    
    @Component(id = "newSerialNumber", parameters = {"value = newSerialNumber", "validate = required, maxlength = 1024"})
    private TextField newSerialNumberField;

    @SuppressWarnings("unused")
    @Component(id = "nextUpdate", parameters = {"value = nextUpdateDays", "validate = required, min = 1"})
    private TextField nextUpdateField;
    
    @SuppressWarnings("unused")
    @Component(id = "updateExistingCRL", parameters = {"value = updateExistingCRL"})
    private Checkbox updateExistingCRLField;
        
    @SuppressWarnings("unused")
    @Component(id = "signatureAlgorithm", parameters = {"value = signatureAlgorithm", "blankOption = NEVER", "validate = required"})
    private Select signatureAlgorithmField;
    
    @Inject
    @Property
    private Block serialNumbersBlock;
    
    /*
     * True if CRL was created
     */
    @Persist(PersistenceConstants.FLASH)
    private boolean created;

    /*
     * Newly created CRL
     */
    @Persist(PersistenceConstants.FLASH)
    private X509CRLDTO newCRL;
    
    /*
     * True if CRL creation failed
     */
    @Persist(PersistenceConstants.FLASH)
    private boolean failed;

    /*
     * Error message if CRL creation failed
     */
    @Persist(PersistenceConstants.FLASH)
    private String failedMessage;
    
    /*
     * All the added serial numbers
     */
    @Persist
    private Set<String> serialNumbers;

    /*
     * The number of days until next update
     */
    @Persist
    private int nextUpdateDays;
    
    /*
     * Algorithm for signing the CRL
     */
    @Persist
    private SignatureAlgorithm signatureAlgorithm;
    
    /*
     * The currently selected serial number
     */
    private String selectedSerialNumber;
    
    /*
     * New serial number to add
     */
    private String newSerialNumber;
    
    /*
     * If true and an existing CRL is present the new entries will be added to the existing CRL
     */
    @Persist
    private boolean updateExistingCRL;
    
    /*
     * The thumbprint of the CA that will sign the CRL
     */
    @Persist
    private String caThumbprint;
    
    /*
     * True when a reload need to keep the main config. We need this when Javascript is disabled because
     * then a full page reload is done when adding/removing entries.
     */
    private boolean keepConfig;
    
    /*
     * Used to keep track which submit button was pressed
     */
    private CACRLCreateSubmitButton submitButton;
    
    @Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_PKI_MANAGER})
    protected void onActivate(String caThumbprint, boolean keepConfig)
    {
        this.caThumbprint = caThumbprint;
        this.keepConfig = keepConfig;
    }
    
    @SetupRender
    @Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_PKI_MANAGER})
    public void setupRender()
    {
        if (!keepConfig || serialNumbers == null)
        {
            serialNumbers = new LinkedHashSet<String>();
            
            nextUpdateDays = 365;
            
            signatureAlgorithm = SignatureAlgorithm.SHA1_WITH_RSA;
            
            updateExistingCRL = true;
        }
    }
    
    public SelectModel getSerialNumbersModel() {
        return new GenericSelectionModel<String>(serialNumbers, null, null);
    }
    
    public X509CertificateBean getCertificate()
    throws WebServiceCheckedException
    {
        X509CertificateBean result = null;
        
        if (StringUtils.isNotEmpty(caThumbprint))
        {
            X509CertificateDTO certificateDTO = keyAndCertStoreWS.getCertificate(caThumbprint);
            
            if (certificateDTO != null) {
                result = new X509CertificateBeanImpl(certificateDTO);
            }
        }
        
        return result;
    }
    
    @OnEvent(component="subjectLink")
    @Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_PKI_MANAGER})
    protected void onSubjectClicked(String thumbprint) 
    throws IOException
    {
        if (StringUtils.isNotBlank(thumbprint))
        {
            Link link = linkFactory.createPageRenderLink("certificate/view", false, CertificateStore.CERTIFICATES,
                    thumbprint, ValidityCheck.TRUST);
    
            response.sendRedirect(link);
        }
    }
    
    /*
     * Returns a link which will be used to reload the page without clearing the config. This is used when the 
     * user disabled Javascript.
     */
    private Link getKeepConfigReloadLink() {
        return resources.createPageLink(CACRLCreate.class, false, caThumbprint, true);
    }
    
    private boolean isValidSerialNumber(String serial)
    {
        if (serial == null) {
            return false;
        }
        
        return SERIAL_NUMBER_PATTERN.matcher(serial).matches();
    }
    
    private String normalizeSerial(String serial) {
        return REMOVE_NON_HEX.matcher(newSerialNumber).replaceAll("").toUpperCase();
    }
    
    private Object validateAddSerialNumber()
    {
        if (StringUtils.isEmpty(newSerialNumber))
        {
            serialNumbersForm.recordError(newSerialNumberField, "Serial number of the revoked certificate must be specified.");
            
            return request.isXHR() ? serialNumbersBlock : getKeepConfigReloadLink();
        }
        else if (!isValidSerialNumber(newSerialNumber))
        {
            serialNumbersForm.recordError(newSerialNumberField, "The entered number is not a valid serial number.");
            
            return request.isXHR() ? serialNumbersBlock : getKeepConfigReloadLink();
        }
        else if (serialNumbers.contains(normalizeSerial(newSerialNumber)))
        {
            serialNumbersForm.recordError(newSerialNumberField, "Serial number of the revoked certificate is already in the list.");

            return request.isXHR() ? serialNumbersBlock : getKeepConfigReloadLink();
        }
        
        return null;
    }

    private Object addSerialNumber()
    {
        serialNumbers.add(normalizeSerial(newSerialNumber));
        
        return request.isXHR() ? serialNumbersBlock : getKeepConfigReloadLink();
    }

    private Object removeSerialNumber()
    {
        serialNumbers.remove(selectedSerialNumber);
        
        return request.isXHR() ? serialNumbersBlock : getKeepConfigReloadLink();
    }
 
    public Object onValidateForm()
    {
        if (submitButton == null) {
            return null;
        }
        
        switch(submitButton)
        {
        case ADD_SERIAL_NUMBER : return validateAddSerialNumber();
        }
        
        return null;
    }

    public Object onSuccess()
    {
        if (submitButton == null) {
            return null;
        }
        
        switch(submitButton)
        {
        case ADD_SERIAL_NUMBER    : return addSerialNumber();
        case REMOVE_SERIAL_NUMBER : return removeSerialNumber();
        case CREATE_CRL           : return createCRL();
        }
        
        return null;
    }

    private Object createCRL()
    {
        Date nextUpdate = DateUtils.addDays(new Date(), nextUpdateDays);

        try {
            CRLCreateParametersDTO dto = new CRLCreateParametersDTO(caThumbprint, new ArrayList<String>(serialNumbers), 
                    nextUpdate, updateExistingCRL, signatureAlgorithm.getName());
            
            newCRL = caWS.createCRL(dto);

            created = true;
        }
        catch (WebServiceCheckedException e)
        {
            failed = true;
            
            failedMessage = e.getMessage();
        }
        
        return getKeepConfigReloadLink();
    }
    
    public void onSelectedFromCreate() {
        submitButton = CACRLCreateSubmitButton.CREATE_CRL;
    }

    public void onSelectedFromAddSerialNumber() {
        submitButton = CACRLCreateSubmitButton.ADD_SERIAL_NUMBER;
    }

    public void onSelectedFromRemoveSerialNumber() {
        submitButton = CACRLCreateSubmitButton.REMOVE_SERIAL_NUMBER;
    }
    
    public boolean isCreated() {
        return created;
    }

    public String getSelectedSerialNumber() {
        return selectedSerialNumber;
    }

    public void setSelectedSerialNumber(String selectedSerialNumber) {
        this.selectedSerialNumber = selectedSerialNumber;
    }

    public String getNewSerialNumber() {
        return newSerialNumber;
    }

    public void setNewSerialNumber(String newSerialNumber) {
        this.newSerialNumber = newSerialNumber;
    }
    
    /*
     * Event handler called when the cancel button is pressed.
     */
    protected Object onCancel() {
        return CAIssue.class;
    }

    public boolean isUpdateExistingCRL() {
        return updateExistingCRL;
    }

    public void setUpdateExistingCRL(boolean updateExistingCRL) {
        this.updateExistingCRL = updateExistingCRL;
    }

    public boolean isFailed() {
        return failed;
    }

    public String getFailedMessage() {
        return failedMessage;
    }

    public X509CRLDTO getNewCRL() {
        return newCRL;
    }

    public int getNextUpdateDays() {
        return nextUpdateDays;
    }

    public void setNextUpdateDays(int nextUpdateDays) {
        this.nextUpdateDays = nextUpdateDays;
    }

    public SignatureAlgorithm getSignatureAlgorithm() {
        return signatureAlgorithm;
    }

    public void setSignatureAlgorithm(SignatureAlgorithm signatureAlgorithm) {
        this.signatureAlgorithm = signatureAlgorithm;
    }
}
