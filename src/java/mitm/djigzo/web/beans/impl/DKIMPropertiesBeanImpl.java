/*
 * Copyright (c) 2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.beans.impl;

import mitm.application.djigzo.DKIMProperties;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.util.Check;
import mitm.djigzo.web.beans.DKIMPropertiesBean;
import mitm.djigzo.web.beans.StringProperty;
import mitm.djigzo.web.entities.SaveableUserProperties;

/**
 * Bean to get and set DKIM properties from the DKIM properties page.
 * 
 * @author Martijn Brinkers
 *
 */
public class DKIMPropertiesBeanImpl implements DKIMPropertiesBean
{
    private final StringProperty keyPair = new StringProperty();
    
    /*
	 * We need a reference to the SaveableUserProperties to be able to save 
	 * the properties. 
	 */
	private final SaveableUserProperties saveableProperties;
	
	/*
	 * The DKIM properties
	 */
	private final DKIMProperties dkimProperties;
	
	public DKIMPropertiesBeanImpl(SaveableUserProperties saveableProperties) 
	throws HierarchicalPropertiesException 
	{
		Check.notNull(saveableProperties, "saveableProperties");
		
		this.saveableProperties = saveableProperties;
		this.dkimProperties = saveableProperties.getDKIMProperties();
		
		initializeProperties();
	}
	
	private boolean isInherited(String property)
	throws HierarchicalPropertiesException 
	{
		return dkimProperties.isInherited(dkimProperties.getFullPropertyName(property));
	}
	
	private void initializeProperties() 
	throws HierarchicalPropertiesException 
	{
	    keyPair.setValue(dkimProperties.getKeyPair());
	    keyPair.setInherit(isInherited(DKIMProperties.KEY_PAIR));
	}

    @Override
    public StringProperty getKeyPair() {
        return keyPair;
    }
    
    @Override
    public void save()
    throws HierarchicalPropertiesException
    {
        dkimProperties.setKeyPair(keyPair.getUserValue());
        
        saveableProperties.save();
    }
}