/*
 * Copyright (c) 2011-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.djigzo.web.services.security;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mitm.common.mail.EmailAddressUtils;
import mitm.common.util.Check;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.security.Authentication;
import org.springframework.security.AuthenticationException;
import org.springframework.security.ui.AbstractProcessingFilter;
import org.springframework.security.ui.savedrequest.SavedRequest;

/**
 * AuthenticationProcessingFilter extension that clears the SavedRequest from the session if the
 * login username (which is assumed to be an email) is different from the email parameter stored
 * in the SavedRequest. After successful authentication we will remove the saved request. This is 
 * required to support the change of the Locale from the login page. The locale is stored in a (Tapestry)
 * cookie.  If the saved request is not removed, the request after authentication will use the "old"  
 * Locale cookie and therefore the first request will use the old Locale instead of the new. I have not 
 * yet found a way to replace the locale cookie in the saved request with the new locale cookie.
 * 
 * Note: not sure what the impact might be of removing the saved request for post requests or 
 * for requests that require additional headers. 
 * 
 * @author Martijn Brinkers
 *
 */
public class ClearSavedRequestAuthenticationProcessingFilter extends BannedAuthenticationProcessingFilter
{
    /*
     * The request param which identifies the logged in user
     */
    private final String usernameRequestParameter;
    
    public ClearSavedRequestAuthenticationProcessingFilter(String usernameRequestParameter)
    {
        Check.notNull(usernameRequestParameter, "usernameRequestParameter");
        
        this.usernameRequestParameter = usernameRequestParameter;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request)
    throws AuthenticationException
    {
        Authentication authentication = super.attemptAuthentication(request);
                
        HttpSession session = request.getSession(false);
        
        if (session != null)
        {
            SavedRequest savedRequest = (SavedRequest) session.getAttribute(AbstractProcessingFilter.
                    SPRING_SECURITY_SAVED_REQUEST_KEY);
            
            if (savedRequest != null)
            {
                String[] savedEmails = savedRequest.getParameterValues(usernameRequestParameter);
                
                if (!ArrayUtils.isEmpty(savedEmails))
                {
                    String email = EmailAddressUtils.canonicalize(obtainUsername(request));
                    
                    for (String savedEmail : savedEmails)
                    {
                        savedEmail = EmailAddressUtils.canonicalize(savedEmail);
                        
                        if (!StringUtils.equals(email, savedEmail))
                        {
                            session.removeAttribute(AbstractProcessingFilter.SPRING_SECURITY_SAVED_REQUEST_KEY);
                            
                            break;
                        }
                    }
                }
            }
        }

        return authentication;
    }
    
    @Override
    protected void onSuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response, 
            Authentication authResult)
    throws IOException
    {
        super.onSuccessfulAuthentication(request, response, authResult);
        
        HttpSession session = request.getSession(false);
        
        /*
         * After successful authentication we will remove the saved request. This is required to support
         * the change of the Locale from the login page. The locale is stored in a (Tapestry) cookie.  If 
         * the saved request is not removed, the request after authentication will use the "old" Locale 
         * cookie and therefore the first request will use the old Locale instead of the new. I have not 
         * yet found a way to replace the locale cookie in the saved request with the new locale cookie.
         * 
         * Note: not sure what the impact might be of removing the saved request for post requests or 
         * for requests that require additional headers. 
         */
        if (session != null) {
            session.removeAttribute(AbstractProcessingFilter.SPRING_SECURITY_SAVED_REQUEST_KEY);
        }
    }
}
