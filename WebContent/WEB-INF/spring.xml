<?xml version="1.0" encoding="UTF-8"?>
<!--
    Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.

    This file is part of Djigzo email encryption.

    Djigzo is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License 
    version 3, 19 November 2007 as published by the Free Software 
    Foundation.

    Djigzo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public 
    License along with Djigzo. If not, see <http://www.gnu.org/licenses/>

    Additional permission under GNU AGPL version 3 section 7

    If you modify this Program, or any covered work, by linking or 
    combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
    wsdl4j-1.6.1.jar (or modified versions of these libraries), 
    containing parts covered by the terms of Common Development and 
    Distribution License (CDDL), Common Public License (CPL) the 
    licensors of this Program grant you additional permission to 
    convey the resulting work.
-->
<beans xmlns="http://www.springframework.org/schema/beans"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns:security="http://www.springframework.org/schema/security"
	xsi:schemaLocation=
	    "http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-2.5.xsd
	    http://www.springframework.org/schema/security http://www.springframework.org/schema/security/spring-security-2.0.xsd">
	    
    <bean id="authenticationEntryPoint" 
        class="org.springframework.security.ui.webapp.AuthenticationProcessingFilterEntryPoint">
                   
        <!-- The default login URL -->   
        <property name="loginFormUrl" value="/login"/>
    </bean>	    
	    
    <bean id="bannedAuthenticationProcessingFilter" class="mitm.djigzo.web.services.security.BannedAuthenticationProcessingFilter">
        <security:custom-filter position="AUTHENTICATION_PROCESSING_FILTER" />
        
        <property name="authenticationFailureUrl" value="/login/failed" />
        <property name="filterProcessesUrl" value="/check" />
        <property name="defaultTargetUrl" value="/globalpreferences"/>
        <property name="alwaysUseDefaultTargetUrl" value="true"/>        
        <property name="authenticationManager" ref="authenticationManager" />        
        <property name="bannedUrl" value="/statics/forbidden.html"/>
    </bean>
        
    <security:authentication-manager alias="authenticationManager" />	    
	    
	<security:http auto-config="false" entry-point-ref="authenticationEntryPoint">
	
        <!-- login page should always be accessible  -->
        <security:intercept-url pattern="/login/**" filters="none"/>  

	    <!-- 
	    	Access to assets should always be accessible because it is getting loaded by the login page. If access
            is blocked some strange visual effects can occur after login because the default.css is not loaded
	    -->
        <security:intercept-url pattern="/assets/**" filters="none"/>
        
        <!--
            icons, images, scripts and styles should always be accessible
        -->  
        <security:intercept-url pattern="/favicon.ico" filters="none"/>  
        <security:intercept-url pattern="/icons/**" filters="none"/>  
        <security:intercept-url pattern="/images/**" filters="none"/>  
        <security:intercept-url pattern="/scripts/**" filters="none"/>
        <security:intercept-url pattern="/styles/**" filters="none"/>
        <security:intercept-url pattern="/statics/**" filters="none"/>
         
        <!--
            Rest is protected
        -->  
    	<security:intercept-url pattern="/**" access="ROLE_LOGIN" />

  	</security:http>

    <!--
        import the authenticator spring config. Which config file is imported depends on 
        the system property djigzo.authenticator.config
    -->
    <import resource="${djigzo-web.spring.authenticator.config}"/>
  
	<bean id="passwordEncoder" class="org.springframework.security.providers.encoding.ShaPasswordEncoder"/>
	
	<bean id="saltSource" class="org.springframework.security.providers.dao.salt.ReflectionSaltSource">
		<property name="userPropertyToUse" value="salt"/>
	</bean>

	<bean id="userDetailsService" class="mitm.djigzo.web.common.security.AdminUserDetailsServiceImpl">
		<constructor-arg ref="passwordEncoder"/>
		<constructor-arg ref="saltSource"/>
	</bean>

    <bean id="authenticationEventListener" class="mitm.djigzo.web.services.security.AuthenticationEventListener">
        <constructor-arg value="Admin Login"/>
    </bean>
	
</beans>
