/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.asos;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.KeyGenerator;
import javax.crypto.Mac;
import javax.crypto.SecretKey;

import mitm.common.util.Base32;
import mitm.common.util.Check;
import mitm.common.util.MiscStringUtils;

import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.ioc.annotations.Value;

/**
 * Application state object which will hold the randomly generated key used for the 
 * calculation of a HMAC checksum. Because this HMAC is an application state object the
 * generated random key is valid until the user session ends.
 * 
 * Note: This won't protect you against a 'reply attack' within one user session.
 * 
 * @author Martijn Brinkers
 *
 */
public class HMAC 
{
	private final String algorithm;
	private final SecretKey key;
	
	public HMAC(@Inject @Value("${hmac.algorithm}") String algorithm) 
	throws NoSuchAlgorithmException
	{
		Check.notNull(algorithm, "algorithm");
		
		this.algorithm = algorithm;
		
		KeyGenerator kg = KeyGenerator.getInstance(algorithm);
		
        key = kg.generateKey();
	}
	
	public SecretKey getKey() {
		return key;
	}
	
	public String calculateHMAC(String input) 
	throws NoSuchAlgorithmException, InvalidKeyException
	{
		Mac mac = Mac.getInstance(algorithm);
		
        mac.init(key);
        
        byte[] hmac = mac.doFinal(MiscStringUtils.toAsciiBytes(input));
        
        return Base32.encode(hmac);
    }
}
