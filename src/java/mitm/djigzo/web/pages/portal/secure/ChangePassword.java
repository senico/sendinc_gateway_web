/*
 * Copyright (c) 2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.pages.portal.secure;

import mitm.application.djigzo.ws.LoginWS;
import mitm.application.djigzo.ws.PortalUserDTO;
import mitm.application.djigzo.ws.PortalUserWS;
import mitm.djigzo.web.components.PortalLayout;
import mitm.djigzo.web.utils.PasswordCodec;

import org.apache.commons.lang.StringUtils;
import org.apache.tapestry5.PersistenceConstants;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.InjectComponent;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.corelib.components.Form;
import org.apache.tapestry5.corelib.components.PasswordField;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.BadCredentialsException;
import org.springframework.security.userdetails.UsernameNotFoundException;

@IncludeStylesheet("context:styles/pages/portal/secure/changePassword.css")
public class ChangePassword
{
    private final static Logger logger = LoggerFactory.getLogger(ChangePassword.class);
    
    @Inject
    private PasswordCodec passwordCodec;

    @Inject
    private LoginWS loginWS;

    @Inject
    private PortalUserWS portalUserWS;

    @Inject
    private Messages messages;
    
    @InjectComponent
    private PortalLayout portalLayout;

    /*
     * The user needs to enter his/her current password when changing the password
     */
    private String currentPassword;
    
    /*
     * The new password for the user
     */
    private String newPassword;

    /*
     * Again the new password (should be the same as newPassword) 
     */
    private String repeatNewPassword;

    /*
     * True if the password was changed
     */
    @Persist(PersistenceConstants.FLASH)
    private boolean passwordChanged;
    
    /*
     * True if and error has occurred
     */
    @Persist(PersistenceConstants.FLASH)
    private boolean error;

    /*
     * Error message if an error has occurred
     */
    @Persist(PersistenceConstants.FLASH)
    private String errorMessage;
    
    @Component(id = "currentPassword", parameters = {"value = currentPassword", "validate=required"})
    private PasswordField currentPasswordField;

    @Component(id = "newPassword", parameters = {"value = newPassword", "validate=required"})
    private PasswordField newPasswordField;

    @SuppressWarnings("unused")
    @Component(id = "repeatNewPassword", parameters = {"value = repeatNewPassword", "validate=required"})
    private PasswordField repeatNewPasswordField;

    @Component
    private Form form;
    
    public void onValidateForm() 
    {
        try {
            PortalUserDTO portalUser = loginWS.getPortalUser(portalLayout.getLoggedInUser());

            if (portalUser == null) {
                throw new UsernameNotFoundException("User was not found.");
            }
            
            String currentEncodedPassword = portalUser.getPassword();
            
            if (StringUtils.isEmpty(currentEncodedPassword)) {
                throw new BadCredentialsException("Current password is not set.");
            }

            if (!passwordCodec.isPasswordValid(currentEncodedPassword, currentPassword))
            {
                form.recordError(currentPasswordField, messages.get("password-not-correct"));
                
                return;
            }

            if (StringUtils.isEmpty(newPassword)) {
                throw new BadCredentialsException("New password is not set.");
            }
                
            if (!StringUtils.equals(newPassword, repeatNewPassword))
            {
                form.recordError(newPasswordField, messages.get("passwords-not-equal"));
                
                return;
            }
        }
        catch (Exception e)
        {
            form.recordError("Error validating form: " + e.getMessage());
            
            logger.error("Error validating form", e);
        }
    }
    
    public void onSuccess()
    {
        try {
            portalUserWS.setEncodedPassword(portalLayout.getLoggedInUser(), 
                    passwordCodec.encodePassword(newPassword));
            
            passwordChanged = true;
        }
        catch (Exception e)
        {
            error = true;
            errorMessage = e.getMessage();
            
            logger.error("Error setting password", e);
        }        
    }
    
    public String getCurrentPassword() {
        return currentPassword;
    }

    public void setCurrentPassword(String currentPassword) {
        this.currentPassword = currentPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getRepeatNewPassword() {
        return repeatNewPassword;
    }

    public void setRepeatNewPassword(String repeatNewPassword) {
        this.repeatNewPassword = repeatNewPassword;
    }

    public boolean isPasswordChanged() {
        return passwordChanged;
    }

    public boolean isError() {
        return error;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}
