/*
 * Copyright (c) 2009-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.pages.ctl;

import mitm.application.djigzo.admin.FactoryRoles;
import mitm.application.djigzo.ws.CTLEntryDTO;
import mitm.application.djigzo.ws.CTLWS;
import mitm.common.security.ctl.CTLEntryStatus;
import mitm.common.security.ctl.CTLManager;
import mitm.common.ws.WebServiceCheckedException;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.tapestry5.PersistenceConstants;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.beaneditor.Validate;
import org.apache.tapestry5.corelib.components.Checkbox;
import org.apache.tapestry5.corelib.components.Form;
import org.apache.tapestry5.corelib.components.Select;
import org.apache.tapestry5.corelib.components.TextField;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.annotation.Secured;

@IncludeStylesheet("context:styles/pages/ctl/ctlAdd.css")
@Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_PKI_MANAGER})
public class CTLAdd
{
    private final static Logger logger = LoggerFactory.getLogger(CTLAdd.class);
    
    private final static String CTL_NAME = CTLManager.DEFAULT_CTL;
    
    @Inject
    private CTLWS ctlWS;

    @Validate("required,hex=64")
    private String thumbprint;
    
    @Validate("required")
    private CTLEntryStatus status;
    
    private boolean allowExpired;

    /*
     * True if an error occurred while adding the CTL entry 
     */
    @Persist(PersistenceConstants.FLASH)
    private boolean failure;
    
    /*
     * If an error occurred this will be the failure message 
     */
    @Persist(PersistenceConstants.FLASH)
    private String failureMessage;
    
    @Component
    private Form form;
    
    @Component(id = "thumbprint")
    private TextField thumbprintEdit;
    
    @SuppressWarnings("unused")
    @Component(id = "status", parameters = {"blankOption=NEVER"})
    private Select statusSelect;
    
    @SuppressWarnings("unused")
    @Component(id = "allowExpired")
    private Checkbox allowExpiredCheckbox;
    
    @SetupRender
    @Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_PKI_MANAGER})
    protected void setupRender() {
        /*
         * Empty on purpose
         */
    }
 
    @Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_PKI_MANAGER})
    public void onActivate(String thumbprint) {
        this.thumbprint = thumbprint; 
    }
    
    public String getThumbprint() {
        return thumbprint;
    }

    public void setThumbprint(String thumbprint) {
        this.thumbprint = thumbprint;
    }

    public CTLEntryStatus getStatus() {
        return status;
    }

    public void setStatus(CTLEntryStatus status) {
        this.status = status;
    }

    public boolean isAllowExpired() {
        return allowExpired;
    }

    public void setAllowExpired(boolean allowExpired) {
        this.allowExpired = allowExpired;
    }    

    public boolean isFailure() {
        return failure;
    }

    public String getFailureMessage() {
        return failureMessage;
    }
    
    public void onValidateFromThumbprint(String thumbprint)
    throws WebServiceCheckedException
    {
        if (StringUtils.isNotBlank(thumbprint))
        {
            if (ctlWS.getEntry(CTL_NAME, thumbprint) != null) {
                form.recordError(thumbprintEdit, "An entry with the given thumbprint already exists.");
            }
        }
    }
    
    public Object onSuccess()
    {
        CTLEntryDTO entry = new CTLEntryDTO(thumbprint, status);
        
        entry.setAllowExpired(allowExpired);
        
        try {
            ctlWS.addEntry(CTL_NAME, entry);
            
            return CTLList.class;
        }
        catch (WebServiceCheckedException e)
        {
            logger.error("Error adding CTL entry.", e);
            
            failure = true;
            failureMessage = ExceptionUtils.getRootCauseMessage(e);       
        }
        
        return null;
    }
    
    /*
     * Event handler called when the cancel button is pressed.
     */
    protected Object onCancel() {
        return CTLList.class;
    }
}
