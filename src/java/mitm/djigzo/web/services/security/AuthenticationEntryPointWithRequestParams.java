/*
 * Copyright (c) 2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar, 
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar, 
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar, 
 * spice-xmlpolicy-1.0.jar, saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Eclipse Public License, 
 * tyrex license, freemarker license, dom4j license, mx4j license,
 * Spice Software License, Common Development and Distribution License
 * (CDDL), Common Public License (CPL) the licensors of this Program grant 
 * you additional permission to convey the resulting work.
 */
package mitm.djigzo.web.services.security;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mitm.common.util.Check;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.security.AuthenticationException;
import org.springframework.security.ui.webapp.AuthenticationProcessingFilterEntryPoint;

/**
 * Extension of AuthenticationProcessingFilterEntryPoint which allows some request parameters to 'survive'
 * the redirect to the login page.
 * 
 * @author Martijn Brinkers
 *
 */
public class AuthenticationEntryPointWithRequestParams extends AuthenticationProcessingFilterEntryPoint
{
    /*
     * The request parameters to add to the redirect url
     */
    private final List<String> requestParameters;

    /*
     * Name of the parameter which will be used to determine which login page should be opened
     */
    private final String loginPageParameter;
    
    /*
     * Maps action to loginURLs
     */
    private final Map<String, String> loginURLs;

    public AuthenticationEntryPointWithRequestParams(List<String> requestParameters, String loginPageParameter, 
            Map<String, String> loginURLs)
    {
        Check.notNull(requestParameters, "requestParameters");
        Check.notNull(loginPageParameter, "loginPageParameter");
        Check.notNull(loginURLs, "loginURLs");
        
        this.requestParameters = Collections.synchronizedList(new ArrayList<String>(requestParameters));
        this.loginPageParameter = loginPageParameter;
        this.loginURLs = Collections.synchronizedMap(new HashMap<String, String>(loginURLs));
    }
    
    private String encode(String value)
    {
        try {
            return URLEncoder.encode(value, "UTF-8");
        } 
        catch (UnsupportedEncodingException e) {
            // ignore. should never happen
        }
        
        return value;
    }
    
    @Override
    protected String determineUrlToUseForThisRequest(HttpServletRequest request, HttpServletResponse response, 
            AuthenticationException exception)
    {
        /*
         * get the "action" parameter from the request and lookup the URL to use from the loginURLs map. If 
         * there is no mapping, fallback to the default URL.
         */
        String url = loginURLs.get(StringUtils.lowerCase(request.getParameter(loginPageParameter)));
        
        return url != null ? url : super.determineUrlToUseForThisRequest(request, response, exception);
    }
    
    @Override
    protected String buildRedirectUrlToLoginPage(HttpServletRequest request, HttpServletResponse response,
            AuthenticationException authException)
    {
        String url = StringUtils.trim(super.buildRedirectUrlToLoginPage(request, response, authException));
        
        if (CollectionUtils.isNotEmpty(requestParameters))
        {
            boolean addParamSeparator = false;
            boolean addAmp = true;
            
            if (!url.contains("?"))
            {
                addParamSeparator = true;
                addAmp = false;
            }

            for (String requestParameter : requestParameters)
            {
                String value = request.getParameter(requestParameter);
                
                if (value != null)
                {
                    if (addParamSeparator)
                    {
                        url = url + "?";
                        addParamSeparator = false;
                    }
                    
                    if (addAmp) {
                        url = url + "&";
                    }
                    
                    addAmp = true;
                    
                    url = url + encode(requestParameter) + "=" + encode(value);
                }
            }
        }
        
        return url;
    }
}
