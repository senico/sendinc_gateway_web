/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.pages.admin;

import java.io.IOException;
import java.security.NoSuchProviderException;

import mitm.application.djigzo.admin.FactoryRoles;
import mitm.application.djigzo.ws.SystemManagerWS;
import mitm.common.util.MiscStringUtils;
import mitm.common.util.SizeUtils;
import mitm.common.ws.WebServiceCheckedException;
import mitm.djigzo.web.pages.Admins;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.io.IOUtils;
import org.apache.tapestry5.PersistenceConstants;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.corelib.components.Form;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.upload.components.Upload;
import org.apache.tapestry5.upload.services.UploadEvents;
import org.apache.tapestry5.upload.services.UploadedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.annotation.Secured;

@IncludeStylesheet("context:styles/pages/admin/jcePolicyManager.css")
@Secured({FactoryRoles.ROLE_ADMIN})
public class JCEPolicyManager
{
	private final static Logger logger = LoggerFactory.getLogger(JCEPolicyManager.class);
	
	/*
	 * Maximum size of the file to upload (sanity check)
	 */
	private static final int MAX_UPLOAD_SIZE = 1 * SizeUtils.MB;
	
	@Inject
	private SystemManagerWS systemManager;

	/*
	 * True when uploading a file resulted in an error (mostly when file size exceeds maximum)
	 */
    @Persist(PersistenceConstants.FLASH)
	private boolean uploadError;
	
	/*
	 * The error message accompanying the uploadError property.
	 */
    @Persist(PersistenceConstants.FLASH)
	private String uploadErrorMessage;

	/*
	 * True if the jce policy files are successfully installed
	 */
    @Persist(PersistenceConstants.FLASH)
	private boolean successfullyInstalled;
    
	/*
	 * 'Handle' to the file that's uploaded
	 */
	private UploadedFile file;

	@SuppressWarnings("unused")
	@Component(id = "policyUpload", parameters = {"value=file", "validate=required"})
	private Upload upload;
	
	/*
	 * I disabled clientValidation because it kept on popping up a warning when I opened the file
	 * browser
	 */
	@Component(id = "form", parameters = {"clientValidation=false"})
	private Form form;
	
	@SetupRender
	@Secured({FactoryRoles.ROLE_ADMIN})
	protected void setupRender()
	{
		/*
		 * Empty on purpose
		 */
	}
	
    /*
     * Event handler that gets called when the uploaded file exceeds the maximum size
     */
    @OnEvent(UploadEvents.UPLOAD_EXCEPTION)
    protected Object onUploadException(FileUploadException uploadException) 
    {
    	logger.error("Error uploading file", uploadException);
    	
    	uploadError = true;
    	uploadErrorMessage = uploadException.getMessage();
    	
    	return JCEPolicyManager.class;
    }
	
    public void onValidateFromPolicyUpload(UploadedFile file)
    throws NoSuchProviderException 
    {
    	/*
    	 * Sanity check
    	 */
    	if (file.getSize() > MAX_UPLOAD_SIZE)
    	{
    		form.recordError("The uploaded file exceeds the maximum size of " + MAX_UPLOAD_SIZE);
    		
    		return;
    	}
    }
	
	public void onSuccess()
	{
		byte[] b64Encoded;
		
		try {
			b64Encoded = Base64.encodeBase64(IOUtils.toByteArray(file.getStream()));
			
			systemManager.installJCEPolicy(MiscStringUtils.toAsciiString(b64Encoded));
			
			successfullyInstalled = true;
		} 
		catch (IOException e) 
		{
			logger.error("Error uploading JCE policy file.");
			
	    	uploadError = true;
	    	
	    	uploadErrorMessage = e.getMessage();
		} 
		catch (WebServiceCheckedException e)
		{
			logger.error("Error uploading JCE policy file.");
			
	    	uploadError = true;
	    	
	    	uploadErrorMessage = e.getMessage();
		}
	}
	
	public boolean isUnlimitedStrength()
	throws WebServiceCheckedException
	{
		/*
		 * Check the local and remote policy
		 */
		return systemManager.isUnlimitedStrength() && mitm.common.security.JCEPolicyManager.isUnlimitedStrength();
	}
	
    /*
     * Event handler called when the cancel button is pressed.
     */
    public Object onCancel() {
		return Admins.class;
	}

	public UploadedFile getFile() {
		return file;
	}

	public void setFile(UploadedFile file) {
		this.file = file;
	}

	public boolean isUploadError() {
		return uploadError;
	}

	public void setUploadError(boolean uploadError) {
		this.uploadError = uploadError;
	}

	public String getUploadErrorMessage() {
		return uploadErrorMessage;
	}

	public void setUploadErrorMessage(String uploadErrorMessage) {
		this.uploadErrorMessage = uploadErrorMessage;
	}

	public boolean isSuccessfullyInstalled() {
		return successfullyInstalled;
	}

	public void setSuccessfullyInstalled(boolean successfullyInstalled) {
		this.successfullyInstalled = successfullyInstalled;
	}
}
