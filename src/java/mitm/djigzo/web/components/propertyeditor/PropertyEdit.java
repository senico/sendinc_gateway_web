/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.components.propertyeditor;

import java.util.Locale;

import mitm.djigzo.web.common.StaticValidationConstraintGenerator;
import mitm.djigzo.web.utils.ValidatorUtils;

import org.apache.tapestry5.BindingConstants;
import org.apache.tapestry5.ComponentResources;
import org.apache.tapestry5.Field;
import org.apache.tapestry5.FieldValidator;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.Mixins;
import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.annotations.SupportsInformalParameters;
import org.apache.tapestry5.corelib.components.Checkbox;
import org.apache.tapestry5.internal.services.FieldValidatorDefaultSourceImpl;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.FieldValidatorDefaultSource;
import org.apache.tapestry5.services.FieldValidatorSource;
import org.apache.tapestry5.services.ValidationConstraintGenerator;

/**
 * @author Martijn Brinkers
 *
 */
@SupportsInformalParameters
public class PropertyEdit
{	
	@Inject 
	private Locale locale;
	
    @Inject
    private ComponentResources resources;

    @Inject
    private FieldValidatorSource fieldValidatorSource;
    
	@Parameter(defaultPrefix = BindingConstants.VALIDATE)
    private FieldValidator<?> fieldValidator;
    
    @Parameter(defaultPrefix = BindingConstants.LITERAL)
    private String validate;
    
	@Parameter(required = true)
	private boolean checked;

	@Parameter(required = true)
	private Field field;
	
	@Parameter("defaultIdsToDisable")
	private String idsToDisable;

	@SuppressWarnings("unused")
	@Component(parameters = {"idsToDisable = prop:idsToDisable"})
    @Mixins({"DisableOnCheck"})
	private Checkbox checkbox;
	
    public String getIdsToDisable() {
    	return idsToDisable;
    }

	public String getDefaultIdsToDisable() {
		return field.getClientId();
	}
	
	public void setChecked(boolean checked) {
		this.checked = checked;
	}
	
	public boolean getChecked() {
		return checked;
	}

	public FieldValidator<?> getFieldValidator()
	{
		if (fieldValidator != null) {
			return fieldValidator;
		}
		
		if (validate == null) {
			return ValidatorUtils.NOOP_VALIDATOR;
		}

		ValidationConstraintGenerator constraintGenerator = new StaticValidationConstraintGenerator(validate);
		
		FieldValidatorDefaultSource fieldValidatorDefaultSource = new FieldValidatorDefaultSourceImpl(constraintGenerator, 
						fieldValidatorSource);
		
		return fieldValidatorDefaultSource.createDefaultValidator(field, resources.getId(), 
				resources.getContainerMessages(), locale, null, null);
	}	
}
