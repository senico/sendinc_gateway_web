var EnableOnChange = Class.create();

EnableOnChange.prototype = 
{
        initialize: function(element, idsToEnable) 
        {
                this.idsToEnable = idsToEnable;
                
                Event.observe($(element), 'change', this.onChangeEvent.bindAsEventListener(this));
                Event.observe($(element), 'keypress', this.onChangeEvent.bindAsEventListener(this));
                
                // initially disabled
        	    $w(this.idsToEnable).each(Field.disable);
        },
        onChangeEvent: function(e) 
        {
            $w(this.idsToEnable).each(Field.enable);
        }
}