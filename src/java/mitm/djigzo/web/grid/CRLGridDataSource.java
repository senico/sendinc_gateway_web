/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.grid;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import mitm.application.djigzo.DjigzoRuntimeException;
import mitm.application.djigzo.ws.X509CRLDTO;
import mitm.application.djigzo.ws.X509CRLStoreWS;
import mitm.common.util.Check;
import mitm.common.ws.WebServiceCheckedException;
import mitm.djigzo.web.beans.X509CRLBean;
import mitm.djigzo.web.beans.impl.X509CRLBeanImpl;

import org.apache.tapestry5.grid.GridDataSource;
import org.apache.tapestry5.grid.SortConstraint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CRLGridDataSource implements GridDataSource 
{
	private final static Logger logger = LoggerFactory.getLogger(CRLGridDataSource.class);
	
	private List<X509CRLBean> crlBeans;
	
	/*
	 * we need to keep track of base index because the index in getRowValue is the absolute
	 * index and not relative.
	 */
	private int startIndex;
	
	private final X509CRLStoreWS crlStore;
	
	public CRLGridDataSource(X509CRLStoreWS crlStore)
	{
		Check.notNull(crlStore, "crlStore");
		
		this.crlStore = crlStore;
	}
	
    @Override
	public int getAvailableRows() 
	{
		try {
			return (int) crlStore.size();
		} 
		catch (WebServiceCheckedException e) {
			throw new DjigzoRuntimeException(e);
		}
	}

    @Override
	public Class<X509CRLBean> getRowType()
	{
		return X509CRLBean.class;
	}

    @Override
	public X509CRLBean getRowValue(int index) 
	{
		int rowIndex = index - startIndex;
		
		X509CRLBean value = null;
		
		if (crlBeans != null && rowIndex < crlBeans.size()) {
			value = crlBeans.get(rowIndex);
		}
		else {
			logger.warn("Not enough rows.");
			/* 
			 * We do not have enough Bean objects. This can happen when AvailableRows
			 * returned more rows than were actually available when prepared was called. 
			 */ 
			value = null;
		}
		
		return value;
	}

    @Override
	public void prepare(int startIndex, int endIndex, List<SortConstraint> sortConstraints) 
	{
		try {
			List<X509CRLDTO> crlDTOs = crlStore.getCRLs(startIndex, endIndex - startIndex + 1);

            if (crlDTOs == null) {
                /*
                 * Can happen because of a race condition where getAvailableRows said that
                 * there are rows available but the items were already removed when prepare
                 * was called.
                 */
                crlDTOs = Collections.emptyList();
            }
			
			crlBeans = new ArrayList<X509CRLBean>(crlDTOs.size());
			
			for (X509CRLDTO dto : crlDTOs) {
				crlBeans.add(new X509CRLBeanImpl(dto));
			}
		} 
		catch (WebServiceCheckedException e) {
			throw new DjigzoRuntimeException(e);
		} 
		
		this.startIndex = startIndex;
	}
}
