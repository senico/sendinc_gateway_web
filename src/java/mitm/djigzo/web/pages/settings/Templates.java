/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 *
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar,
 * wsdl4j-1.6.1.jar (or modified versions of these libraries),
 * containing parts covered by the terms of Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to
 * convey the resulting work.
 */
package mitm.djigzo.web.pages.settings;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import mitm.application.djigzo.ResolvableHierarchicalProperties;
import mitm.application.djigzo.TemplateProperties;
import mitm.application.djigzo.admin.FactoryRoles;
import mitm.common.mail.EmailAddressUtils;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.util.DomainUtils;
import mitm.common.util.DomainUtils.DomainType;
import mitm.common.ws.WebServiceCheckedException;
import mitm.djigzo.web.common.GenericSelectionModel;
import mitm.djigzo.web.common.PropertyType;
import mitm.djigzo.web.common.TemplateType;
import mitm.djigzo.web.entities.Domain;
import mitm.djigzo.web.entities.DomainManager;
import mitm.djigzo.web.entities.GlobalPreferencesManager;
import mitm.djigzo.web.entities.SaveableUserProperties;
import mitm.djigzo.web.entities.User;
import mitm.djigzo.web.entities.UserManager;
import mitm.djigzo.web.mixins.AjaxEvent;
import mitm.djigzo.web.pages.GlobalPreferences;
import mitm.djigzo.web.pages.Users;
import mitm.djigzo.web.pages.domain.DomainEdit;
import mitm.djigzo.web.pages.user.UserEdit;

import org.apache.tapestry5.ComponentResources;
import org.apache.tapestry5.Link;
import org.apache.tapestry5.PersistenceConstants;
import org.apache.tapestry5.SelectModel;
import org.apache.tapestry5.annotations.BeginRender;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.IncludeJavaScriptLibrary;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.Mixins;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.beaneditor.Validate;
import org.apache.tapestry5.corelib.components.Checkbox;
import org.apache.tapestry5.corelib.components.Form;
import org.apache.tapestry5.corelib.components.TextArea;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.json.JSONObject;
import org.apache.tapestry5.services.Request;
import org.apache.tapestry5.services.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.annotation.Secured;

/**
 * Page for editing the templates.
 *
 * @author Martijn Brinkers
 *
 */
@IncludeJavaScriptLibrary("templates.js")
@IncludeStylesheet("context:styles/pages/settings/templates.css")
@Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_TEMPLATE_MANAGER})
public class Templates
{
	private final static Logger logger = LoggerFactory.getLogger(Templates.class);

	@Inject
	private UserManager userManager;

	@Inject
	private DomainManager domainManager;

	@Inject
	private GlobalPreferencesManager globalPreferencesManager;

    @Inject
    private ComponentResources resources;

    @Inject
    private Request request;

	@Inject
	private Response response;

    /*
     * True if changes were applied
     */
    @Persist(PersistenceConstants.FLASH)
    private boolean applied;

    /*
     * The current template content
     */
	private String templateContent;

	/*
	 * True if the template is inherited
	 */
	private boolean inherited;

	/*
	 * The template type being edited
	 */
	@Persist
	private TemplateType templateType;

	/*
	 * THe identifies the property we are editing (for user it should be email, domain a domain and global global)
	 */
	private String id;

	/*
	 * The property type being edited (user, domain etc.)
	 */
	private PropertyType propertyType;

	/*
	 * We will set an upper limit on the max template size. Jetty for example by default limits the max. form size
	 * to 200000. This can be changed by setting the system property org.mortbay.http.HttpRequest.maxFormContentSize.
	 */
	@Component(id = "templateField", parameters = {"value = prop:templateContent", "validate = maxLength=1048576"})
	private TextArea templateField;

	@SuppressWarnings("unused")
	@Component(parameters = {"idsToDisable = literal:templateField", "value = prop:inherited"})
    @Mixins("DisableOnCheck")
	private Checkbox checkbox;

	@Component
	private Form form;

	@SetupRender
	@Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_TEMPLATE_MANAGER})
	protected void setupRender() {
		/*
		 * Empty on purpose
		 */
	}

	@BeginRender
	protected void beginRender()
	throws IOException
	{
		/*
		 * If the template was not loaded return to the Users page
		 */
		if (templateContent == null)
		{
	        Link link = resources.createPageLink(Users.class, false);

	        response.sendRedirect(link);
		}
	}

	private boolean acceptTemplate(TemplateType type)
	{
        return PropertyType.USER == propertyType && type.isUserTemplate() ||
                PropertyType.DOMAIN == propertyType && type.isDomainTemplate() ||
                PropertyType.GLOBAL == propertyType && type.isGlobalTemplate();
	}

    public SelectModel getTemplateModel()
    throws WebServiceCheckedException
    {
        List<String> templates = new LinkedList<String>();

        for (TemplateType type : TemplateType.values())
        {
            /*
             * Only add the template if it is used for the current type (user, domain or global).
             */
            if (acceptTemplate(type)) {
                templates.add(type.getFriendlyName());
            }
        }

        return new GenericSelectionModel<String>(templates, null, null);
    }

    public boolean isApplied() {
        return applied;
    }

	public String getTemplateContent() {
		return templateContent;
	}

	public void setTemplateContent(String templateContent) {
		this.templateContent = templateContent;
	}

	public boolean isInherited()
	{
		return inherited;
	}

	public void setInherited(boolean inherited) {
		this.inherited = inherited;
	}

	private SaveableUserProperties getUserProperties()
	throws WebServiceCheckedException
	{
		SaveableUserProperties properties = null;

		String email = EmailAddressUtils.canonicalizeAndValidate(id, true);

		if (email != null)
		{
			User user = userManager.getUser(email, false /* null if not exist */);

			if (user != null) {
				properties = user.getPreferences().getProperties();
			}
		}
		else {
			logger.warn("email " + id + " is not a valid email address");
		}

		return properties;
	}

	private SaveableUserProperties getDomainProperties()
	throws WebServiceCheckedException
	{
		SaveableUserProperties properties = null;

		String domainName = DomainUtils.canonicalizeAndValidate(id, DomainType.WILD_CARD);

		if (domainName != null)
		{
			Domain domain = domainManager.getDomain(domainName);

			if (domain != null) {
				properties = domain.getPreferences().getProperties();
			}
		}
		else {
			logger.warn("domain " + id + " is not a valid domain");
		}

		return properties;
	}

	private SaveableUserProperties getGlobalProperties()
	throws WebServiceCheckedException
	{
		return globalPreferencesManager.getPreferences().getProperties();
	}

	private SaveableUserProperties getProperties()
	throws WebServiceCheckedException
	{
		switch(propertyType)
		{
		case USER   : return getUserProperties();
		case DOMAIN : return getDomainProperties();
		case GLOBAL : return getGlobalProperties();
		}

		throw new IllegalArgumentException("Unknown PropertyType " + propertyType);
	}

	private boolean isInherited(ResolvableHierarchicalProperties properties, String propertyName)
	throws HierarchicalPropertiesException
	{
		return properties.isInherited(properties.getFullPropertyName(propertyName));
	}

	private void loadTemplate()
	throws WebServiceCheckedException, HierarchicalPropertiesException
	{
		SaveableUserProperties properties = getProperties();

		if (properties != null)
		{
			TemplateProperties tpp = properties.getTemplateProperties();

			switch(templateType)
			{
			  case ENCRYPTION_FAILED_NOTIFICATION :
			    templateContent = tpp.getEncryptionFailedNotificationTemplate();
			    inherited = isInherited(tpp, TemplateProperties.ENCRYPTION_FAILED_NOTIFICATION);
			    break;
			  case ENCRYPTION_NOTIFICATION :
			    templateContent = tpp.getEncryptionNotificationTemplate();
			    inherited = isInherited(tpp, TemplateProperties.ENCRYPTION_NOTIFICATION);
			    break;
        case DLP_WARNING :
          templateContent = tpp.getDLPWarningTemplate();
          inherited = isInherited(tpp, TemplateProperties.DLP_WARNING);
          break;
        case DLP_QUARANTINE :
          templateContent = tpp.getDLPQuarantineTemplate();
          inherited = isInherited(tpp, TemplateProperties.DLP_QUARANTINE);
          break;
        case DLP_BLOCK :
          templateContent = tpp.getDLPBlockTemplate();
          inherited = isInherited(tpp, TemplateProperties.DLP_BLOCK);
          break;
        case DLP_ERROR :
          templateContent = tpp.getDLPErrorTemplate();
          inherited = isInherited(tpp, TemplateProperties.DLP_ERROR);
          break;
        case DLP_RELEASE_NOTIFICATION :
          templateContent = tpp.getDLPReleaseNotificationTemplate();
          inherited = isInherited(tpp, TemplateProperties.DLP_RELEASE_NOTIFICATION);
          break;
        case DLP_DELETE_NOTIFICATION :
          templateContent = tpp.getDLPDeleteNotificationTemplate();
          inherited = isInherited(tpp, TemplateProperties.DLP_DELETE_NOTIFICATION);
          break;
        case DLP_EXPIRE_NOTIFICATION :
          templateContent = tpp.getDLPExpireNotificationTemplate();
          inherited = isInherited(tpp, TemplateProperties.DLP_EXPIRE_NOTIFICATION);
          break;

  			default:
  				throw new IllegalArgumentException("Unknown TemplateType " + templateType);
			}
		}
	}

	private void saveTemplate()
	throws WebServiceCheckedException, HierarchicalPropertiesException
	{
		SaveableUserProperties properties = getProperties();

		if (properties != null)
		{
			TemplateProperties tpp = properties.getTemplateProperties();

			String newTemplate = !inherited ? templateContent : null;

			switch(templateType)
			{
			  case ENCRYPTION_FAILED_NOTIFICATION : tpp.setEncryptionFailedNotificationTemplate(newTemplate); break;
        case ENCRYPTION_NOTIFICATION        : tpp.setEncryptionNotificationTemplate(newTemplate); break;
        case DLP_WARNING                    : tpp.setDLPWarningTemplate(newTemplate); break;
        case DLP_QUARANTINE                 : tpp.setDLPQuarantineTemplate(newTemplate); break;
        case DLP_BLOCK                      : tpp.setDLPBlockTemplate(newTemplate); break;
        case DLP_ERROR                      : tpp.setDLPErrorTemplate(newTemplate); break;
        case DLP_RELEASE_NOTIFICATION       : tpp.setDLPReleaseNotificationTemplate(newTemplate); break;
        case DLP_DELETE_NOTIFICATION        : tpp.setDLPDeleteNotificationTemplate(newTemplate); break;
        case DLP_EXPIRE_NOTIFICATION        : tpp.setDLPExpireNotificationTemplate(newTemplate); break;
			default:
				throw new IllegalArgumentException("Unknown TemplateType " + templateType);
			}

			properties.save();
		}
	}

    @Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_TEMPLATE_MANAGER})
	protected Object onActivate(Object[] context)
	throws WebServiceCheckedException, HierarchicalPropertiesException
	{
	    if (context.length < 2) {
            return Users.class;
	    }

	    if (!(context[0] instanceof String)) {
            return Users.class;
	    }

	    this.id = (String) context[0];

        if (!(context[1] instanceof String)) {
            return Users.class;
        }

		this.propertyType = PropertyType.fromString((String) context[1]);

        if (this.propertyType == null) {
            return Users.class;
        }

		if (context.length >= 3 && context[2] instanceof String) {
            this.templateType = TemplateType.fromString((String) context[2]);
		}

		if (this.templateType == null) {
		    this.templateType = TemplateType.ENCRYPTION_FAILED_NOTIFICATION;
		}

		/*
		 * It can happen that templateType has been cached when the global settings have been previously opened. We
		 * need to make sure that the cached templateType is valid for the propertyType (user, domain or global)
		 */
		if (!acceptTemplate(this.templateType)) {
            this.templateType = TemplateType.ENCRYPTION_FAILED_NOTIFICATION;
		}

		templateContent = null;

		loadTemplate();

		if (templateContent == null) {
		    templateContent = "";
		}

		return null;
	}

	private Object[] getContext(TemplateType templateType)
	{
        List<Object> context = new ArrayList<Object>(3);

        if (id != null) {
            context.add(id);
        }

        if (propertyType != null) {
            context.add(propertyType);
        }

        if (templateType != null) {
            context.add(templateType);
        }

        return context.toArray();
	}

	protected Object[] onPassivate() {
		return getContext(templateType);
	}

	public void onValidateForm()
	throws WebServiceCheckedException
	{
		if (!inherited && (templateContent == null || templateContent.trim().length() == 0))
		{
			form.recordError(templateField, "Template cannot be empty.");
		}
	}

    @OnEvent(component = "templateSelected", value="AjaxEvent")
    public Link onTemplateSelected()
    throws IOException
    {
        String data = request.getParameter(AjaxEvent.REQUEST_PARAM_NAME);

        JSONObject json = new JSONObject(data);

        TemplateType type = TemplateType.fromFriendlyName(json.getString("value"));

        Link link = resources.createPageLink(Templates.class, false, getContext(type));

        return link;
    }

	public void onSuccess()
	throws WebServiceCheckedException, HierarchicalPropertiesException
	{
		saveTemplate();

		applied = true;
	}

    @Validate("required")
    public String getTemplate() {
        return templateType.getFriendlyName();
    }

    public void setTemplate(String friendlyName) {
        this.templateType = TemplateType.fromFriendlyName(friendlyName);
    }

	public TemplateType getTemplateType() {
		return templateType;
	}

    public void setTemplateType(TemplateType templateType) {
        this.templateType = templateType;
    }

	public String getId() {
		return id;
	}

	public PropertyType getPropertyType() {
		return propertyType;
	}

    /*
     * Event handler called when the cancel button is pressed.
     */
    public Object onCancel()
	{
    	Link link;

		switch(propertyType)
		{
		case USER   : link = resources.createPageLink(UserEdit.class, false, id); break;
		case DOMAIN : link = resources.createPageLink(DomainEdit.class, false, id); break;
		case GLOBAL : link = resources.createPageLink(GlobalPreferences.class, false); break;
		default:
			throw new IllegalArgumentException("Unknown PropertyType " + propertyType);
		}

		return link;
	}

    public boolean isUserPropertyType() {
        return propertyType == PropertyType.USER;
    }

    public boolean isDomainPropertyType() {
        return propertyType == PropertyType.DOMAIN;
    }

    public boolean isGlobalPropertyType() {
        return propertyType == PropertyType.GLOBAL;
    }
}
