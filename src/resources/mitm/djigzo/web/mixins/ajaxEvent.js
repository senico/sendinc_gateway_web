var AjaxEvent = Class.create();

AjaxEvent.prototype = 
{
    initialize: function(input)
    {
		this.params = input.evalJSON(true);
		
		this.events = $w(this.params.event);

		for (i = 0; i < this.events.length; i++) {
	        Event.observe($(this.params.clientid), this.events[i], this.onEvent.bindAsEventListener(this));
		}
    },
    onEvent: function(e)
    {
    	// need to store the event type otherwise IE will fail
    	var eventType = e.type;

    	var json = Object.toJSON({params: this.params, value: $(this.params.clientid).value, eventType: this.eventType});
    	
    	if (this.params.beforeEvent != null)
        {
            var stop = eval(this.params.beforeEvent + "('" + json + "')");
            
            if (stop) {
                return;
            }
        }
        new Ajax.Request(this.params.uri,
        {
            method: 'post',
            parameters: {data: json},
            onFailure: function(t)
            {
            	Tapestry.ajaxError('Error communication with the server.');
            	
            	if (Tapestry.DEBUG_ENABLED) {
            		alert(t.responseText);
            	}
            },
            onException: function(t, exception)
            {
            	Tapestry.ajaxError('Error communication with the server.');

            	if (Tapestry.DEBUG_ENABLED) {
            		alert(exception);
            	}
            },
            onSuccess: function(t)
            {
            	if (this.params.afterEvent != null)
            	{
                	var json = Object.toJSON({params: this.params, value: $(this.params.clientid).value, 
                		eventType: this.eventType, result: t.responseText.evalJSON(true)});

                	eval(this.params.afterEvent + "('" + json + "')");
				}
            
            	if (this.params.zone != null) {
            		Tapestry.activateZone(this.params.zone, this.params.uri);
            	}
            }
            .bind(this)
        });
    }
}