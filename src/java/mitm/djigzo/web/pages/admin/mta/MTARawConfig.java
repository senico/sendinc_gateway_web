/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.pages.admin.mta;

import mitm.application.djigzo.admin.FactoryRoles;
import mitm.application.djigzo.ws.PostfixConfigManagerWS;
import mitm.common.ws.WebServiceCheckedException;

import org.apache.tapestry5.PersistenceConstants;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.corelib.components.TextArea;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.annotation.Secured;

@IncludeStylesheet("context:styles/pages/admin/mta/mtaRawConfig.css")
@Secured({FactoryRoles.ROLE_ADMIN})
public class MTARawConfig
{
	private final static Logger logger = LoggerFactory.getLogger(MTARawConfig.class);
	
	@Inject
	private PostfixConfigManagerWS postfixConfigManagerWS;

	@SuppressWarnings("unused")
	@Component(id = "mainConfig", parameters = {"value = mainConfig", "validate = required"})
	private TextArea mainConfigField;
	
	@Persist(PersistenceConstants.FLASH)
	private boolean applied;

	@Persist(PersistenceConstants.FLASH)
	private boolean applyError;

	private String mainConfig;
	
	@SetupRender
	@Secured({FactoryRoles.ROLE_ADMIN})
	protected void setupRender() 
	throws WebServiceCheckedException
	{
		mainConfig = postfixConfigManagerWS.getMainConfig();
	}
	
	public String getMainConfig() {
		return mainConfig;
	}
	
	public void setMainConfig(String mainConfig) {
		this.mainConfig = mainConfig;
	}
	
	public void onSuccess()
	{
		try {
			postfixConfigManagerWS.setMainConfig(mainConfig);
			
			postfixConfigManagerWS.reload();
			
			applied = true;
		}
		catch (WebServiceCheckedException e)
		{
			logger.error("Error applying config.", e);
			
			applyError = true;
		}
	}
	
    /*
     * Event handler called when the cancel button is pressed.
     */
    public Object onCancel() {
		return MTAConfig.class;
	}

	public boolean isApplied() {
		return applied;
	}

	public boolean isApplyError() {
		return applyError;
	}
}
