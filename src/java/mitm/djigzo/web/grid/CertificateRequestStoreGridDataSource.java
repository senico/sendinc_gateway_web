/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.grid;

import java.util.Collections;
import java.util.List;

import mitm.application.djigzo.DjigzoRuntimeException;
import mitm.application.djigzo.ws.CertificateRequestDTO;
import mitm.application.djigzo.ws.CertificateRequestStoreWS;
import mitm.common.security.ca.Match;
import mitm.common.util.Check;
import mitm.common.ws.WebServiceCheckedException;

import org.apache.commons.lang.StringUtils;
import org.apache.tapestry5.grid.GridDataSource;
import org.apache.tapestry5.grid.SortConstraint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CertificateRequestStoreGridDataSource implements GridDataSource 
{
	private final static Logger logger = LoggerFactory.getLogger(CertificateRequestStoreGridDataSource.class);
	
	/*
	 * The certificate request store entries
	 */
	private List<CertificateRequestDTO> requests;
	
	/*
	 * If email is specified, only entries with the given email will be returned 
	 */
	private final String email;
	
	/*
	 * we need to keep track of base index because the index in getRowValue is the absolute
	 * index and not relative.
	 */
	private int startIndex;
	
	private final CertificateRequestStoreWS storeWS;

	public CertificateRequestStoreGridDataSource(CertificateRequestStoreWS storeWS) {
	    this(storeWS, null);
	}

    public CertificateRequestStoreGridDataSource(CertificateRequestStoreWS storeWS, String email)
    {
        Check.notNull(storeWS, "storeWS");
        
        this.storeWS = storeWS;
        this.email = email;
    }
	
    @Override
	public int getAvailableRows() 
	{
		try {
            return StringUtils.isEmpty(email) ? storeWS.getSize() : storeWS.getSizeByEmail(email, Match.LIKE);
		} 
		catch (WebServiceCheckedException e) {
			throw new DjigzoRuntimeException(e);
		}
	}

    @Override
	public Class<CertificateRequestDTO> getRowType()
	{
		return CertificateRequestDTO.class;
	}

    @Override
	public CertificateRequestDTO getRowValue(int index) 
	{
		int rowIndex = index - startIndex;
		
		CertificateRequestDTO value = null;
		
		if (requests != null && rowIndex < requests.size()) {
			value = requests.get(rowIndex);
		}
		else {
			logger.warn("Not enough rows.");
			/* 
			 * We do not have enough objects. This can happen when AvailableRows
			 * returned more rows than were actually available when prepared was called. 
			 */ 
			value = null;
		}
		
		return value;
	}

    @Override
	public void prepare(int startIndex, int endIndex, List<SortConstraint> sortConstraints) 
	{
		try {
		    requests = StringUtils.isEmpty(email) ? storeWS.getAllRequests(startIndex, endIndex - startIndex + 1) :
		                storeWS.getRequestsByEmail(email, Match.LIKE, startIndex, endIndex - startIndex + 1);

            if (requests == null) {
                /*
                 * Can happen because of a race condition where getAvailableRows said that
                 * there are rows available but the items were already removed when prepare
                 * was called.
                 */
                requests = Collections.emptyList();
            }
		} 
		catch (WebServiceCheckedException e) {
			throw new DjigzoRuntimeException(e);
		} 
		
		this.startIndex = startIndex;
	}
}
