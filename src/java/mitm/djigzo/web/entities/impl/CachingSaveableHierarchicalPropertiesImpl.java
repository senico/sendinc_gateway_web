/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.entities.impl;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import mitm.application.djigzo.ws.HierarchicalPropertiesWS;
import mitm.application.djigzo.ws.PropertyDTO;
import mitm.application.djigzo.ws.PropertySelectorDTO;
import mitm.application.djigzo.ws.UserPreferencesDTO;
import mitm.application.djigzo.ws.impl.SaveableHierarchicalPropertiesImpl;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.ws.WebServiceCheckedException;

/**
 * This is an extension of SaveableHierarchicalPropertiesImpl that caches calls to property set to make sure
 * all setters are called withing one transactions (when save is called). This is needed to support transactions
 * when setting properties using a soap interface. CachingSaveableHierarchicalPropertiesImpl also allows certain
 * properties to be preloaded for optimization purposes (loading a large number of properties using multiple soap
 * calls is slow)
 * 
 * Note: CachingSaveableHierarchicalPropertiesImpl does not completely follow the HierarchicalProperties
 * contract. Properties that are deleted return null values even though the source may have an
 * inherited value different from null. A property that has been set is returned. If a property
 * is set to null a get will also return null even when the source may have an inherited value
 * different from null. Also isInherit returns false when a value different from null has been
 * set. If no value has been set the source value is retrieved.
 * 
 * @author Martijn Brinkers
 *
 */
public class CachingSaveableHierarchicalPropertiesImpl extends SaveableHierarchicalPropertiesImpl 
    implements CachingSaveableHierarchicalProperties
{	
    private final static Logger logger = LoggerFactory.getLogger(CachingSaveableHierarchicalPropertiesImpl.class);
    
    /*
     * All the pre-loaded properties
     */
    private Map<String, PropertyDTO> preloaded;
    
	public CachingSaveableHierarchicalPropertiesImpl(HierarchicalPropertiesWS hierarchicalPropertiesWS,
			UserPreferencesDTO userPreferencesDTO)
	{
	    super(hierarchicalPropertiesWS, userPreferencesDTO);
	}
	
	@Override
    public String getProperty(String propertyName, boolean decrypt) 
	throws HierarchicalPropertiesException
	{
	    if (deleteAll || toDelete.contains(propertyName)) {
	        return null;
	    }
	    
	    PropertyDTO prop = toSet.get(propertyName);

	    if (prop != null) {
	        return prop.getValue();
	    }

	    if (preloaded != null)
	    {
            prop = preloaded.get(propertyName);
            
            if (prop != null) {
                return prop.getValue();
            }
            else {
                logger.debug("Property {} is not preloaded", propertyName);
            }
	    }
	    
	    return super.getProperty(propertyName, decrypt);
	}
        
    @Override
    public boolean isInherited(String propertyName) 
	throws HierarchicalPropertiesException
	{
        if (deleteAll || toDelete.contains(propertyName)) {
            return true;
        }

        PropertyDTO prop = toSet.get(propertyName);

        if (prop != null && prop.getValue() != null) {
            return false;
        }

        if (preloaded != null)
        {
            prop = preloaded.get(propertyName);
            
            if (prop != null) {
                return prop.isInherited();
            }
            else {
                logger.debug("Property {} is not preloaded", propertyName);
            }
        }
        
        return super.isInherited(propertyName);
	}
    
    @Override
    public Set<String> getProperyNames(boolean recursive) 
    throws HierarchicalPropertiesException
	{
        if (deleteAll) {
            return Collections.emptySet();
        }

        /*
         * CXF does not yet support a Set as return type so we need to convert it from
         * List to Set. 
         * 
         * See: https://issues.apache.org/jira/browse/CXF-1558?page=com.atlassian.jira.plugin.system.issuetabpanels:all-tabpanel
         */
        Set<String> all = super.getProperyNames(recursive);

        all.addAll(toSet.keySet());
        all.removeAll(toDelete);

        return all;
	}

    @Override
    public void preload(PropertySelectorDTO... propertySelectors)
    throws HierarchicalPropertiesException
    {
        try {
            List<PropertyDTO> properties = hierarchicalPropertiesWS.getProperties(userPreferencesDTO, 
                    Arrays.asList(propertySelectors));
            
            preloaded = new HashMap<String, PropertyDTO>();
            
            if (properties != null)
            {
                for (PropertyDTO property : properties) {
                    preloaded.put(property.getPropertyName(), property);
                }
            }
        } 
        catch (WebServiceCheckedException e) {
            throw new HierarchicalPropertiesException(e);
        }
    }    
}
