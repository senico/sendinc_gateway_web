/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.pages.sms;

import mitm.application.djigzo.admin.FactoryRoles;
import mitm.application.djigzo.ws.ClickatellWS;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.ws.WebServiceCheckedException;
import mitm.djigzo.web.beans.ClickatellParametersBean;
import mitm.djigzo.web.beans.impl.ClickatellParametersBeanImpl;
import mitm.djigzo.web.components.NonClearingPassword;
import mitm.djigzo.web.entities.GlobalPreferencesManager;
import mitm.djigzo.web.pages.SMS;
import mitm.djigzo.web.services.DisableHttpCache;

import org.apache.cxf.common.util.StringUtils;
import org.apache.tapestry5.Block;
import org.apache.tapestry5.PersistenceConstants;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.corelib.components.TextField;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.annotation.Secured;

@IncludeStylesheet("context:styles/pages/sms/clickatell.css")
@Secured({FactoryRoles.ROLE_ADMIN})
public class Clickatell
{
	private final static Logger logger = LoggerFactory.getLogger(Clickatell.class);
	
	@Inject
	private GlobalPreferencesManager globalPreferencesManager;

    @Inject
    private ClickatellWS clickatellWS;
	
	@SuppressWarnings("unused")
	@Component(id = "apiID", parameters = {"value = clickatellParameters.apiID", "validate=required"})
	private TextField apiIDField;

	@SuppressWarnings("unused")
	@Component(id = "user", parameters = {"value = clickatellParameters.user", "validate=required"})
	private TextField userField;

	@SuppressWarnings("unused")
	@Component(id = "password", parameters = {"value = password", "validate=required"})
	private NonClearingPassword passwordField;

	@SuppressWarnings("unused")
	@Component(id = "from", parameters = {"value = clickatellParameters.from"})
	private TextField fromField;
	
	@Inject
	@Property
	private Block balanceBlock;

	@Inject
	@Property
	private Block errorBlock;
	
	@Persist(PersistenceConstants.FLASH)
	private String clickatellErrorMessage;

	@Persist(PersistenceConstants.FLASH)
	private boolean clickatellError;
	
	@Persist
	private String balance;
	
    /*
     * True if changes were applied
     */
    @Persist(PersistenceConstants.FLASH)
    private boolean applied;
	
	private ClickatellParametersBean clickatellParameters;
	
    public boolean isApplied() {
        return applied;
    }
	
	@SetupRender
    @DisableHttpCache /* disable cache for security purposes */
	@Secured({FactoryRoles.ROLE_ADMIN})
	protected void setupRender() {
		/*
		 * Empty on purpose
		 */
	}
	
    public void setPassword(String password)
    throws HierarchicalPropertiesException, WebServiceCheckedException
    {
        if (!NonClearingPassword.DUMMY_PASSWORD.equals(password))
        {
            /*
             * The password has been changed
             */
            getClickatellParameters().setPassword(password);
        }
    }
    
    public String getPassword()
    throws HierarchicalPropertiesException, WebServiceCheckedException
    {
        /*
         * return the dummy password if the password has been set
         */
        return StringUtils.isEmpty(getClickatellParameters().getPassword()) ? "" : NonClearingPassword.DUMMY_PASSWORD;
    }
	
	public ClickatellParametersBean getClickatellParameters() 
	throws HierarchicalPropertiesException, WebServiceCheckedException
	{
		if (clickatellParameters == null) 
		{
			clickatellParameters = new ClickatellParametersBeanImpl(
					globalPreferencesManager.getProperties());
		}
		
		return clickatellParameters;
	}

	protected Block onActionFromUpdateBalance()
	throws WebServiceCheckedException, HierarchicalPropertiesException 
	{
		updateBalance();
		
		return clickatellError ? errorBlock : balanceBlock;
	}
	
	public void onSuccess()
	throws WebServiceCheckedException, HierarchicalPropertiesException 
	{
		getClickatellParameters().save();
		
		applied = true;
	}
	
	private void updateBalance() 
	throws HierarchicalPropertiesException
	{
	    try {
	        float value = clickatellWS.getBalance(getClickatellParameters().getAPIID(), getClickatellParameters().getUser(), 
	                getClickatellParameters().getPassword());
	        
	        balance = Float.toString(value);
	    } 
	    catch (WebServiceCheckedException e) 
	    {
	        logger.error("Error getting balance.", e);

	        clickatellError = true;
	        clickatellErrorMessage = e.getMessage();

	        balance = "";
	    }
	}
	
    /*
     * Event handler called when the cancel button is pressed.
     */
    public Object onCancel() {
		return SMS.class;
	}

    public String getClickatellErrorMessage() {
		return clickatellErrorMessage;
	}

	public boolean isClickatellError() {
		return clickatellError;
	}

	public String getBalance() {
		return balance;
	}
}
