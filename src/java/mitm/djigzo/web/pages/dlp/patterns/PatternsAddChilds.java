/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.pages.dlp.patterns;

import java.util.List;

import mitm.application.djigzo.admin.FactoryRoles;
import mitm.application.djigzo.ws.PolicyPatternManagerWS;
import mitm.application.djigzo.ws.PolicyPatternNodeDTO;
import mitm.application.djigzo.ws.PolicyPatternNodeWS;
import mitm.common.ws.WebServiceCheckedException;
import mitm.djigzo.web.common.YesNo;
import mitm.djigzo.web.components.DLPPatternGrid;
import mitm.djigzo.web.grid.PolicyPatternNodeGridDataSource;

import org.apache.commons.lang.StringUtils;
import org.apache.tapestry5.ComponentResources;
import org.apache.tapestry5.PersistenceConstants;
import org.apache.tapestry5.annotations.Cached;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.ioc.annotations.Value;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.annotation.Secured;

@Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_DLP_MANAGER})
@IncludeStylesheet("context:styles/pages/dlp/patterns/addChilds.css")
public class PatternsAddChilds
{
    private final static Logger logger = LoggerFactory.getLogger(PatternsAddChilds.class);
    
    @Inject
    @Value("${dlp.patterns.rowsPerPage}")
    private int rowsPerPage;    

    @Inject
    private PolicyPatternManagerWS policyPatternManagerWS;

    @Inject
    private PolicyPatternNodeWS policyPatternNodeWS;
    
    @Inject
    private ComponentResources resources;
    
    /*
     * The name of this group
     */
    private String name;

    /*
     * The policyPatternNode to which childs will be added
     */
    private PolicyPatternNodeDTO policyPatternNode;

    /*
     * True if an error has occurred
     */
    @Persist(PersistenceConstants.FLASH)
    private boolean error;

    /*
     * Error message if an error has occurred
     */
    @Persist(PersistenceConstants.FLASH)
    private String errorMessage;

    @Component(parameters = {"source = source", "rowsPerPage = prop:rowsPerPage", "removeDelete=true"})
    private DLPPatternGrid patternGrid; 
    
    @SetupRender
    @Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_DLP_MANAGER})
    public void setupRender() {
        /*
         * Empty on purpose
         */
    }
    
    @Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_DLP_MANAGER})
    public Object onActivate(String name)
    throws WebServiceCheckedException 
    {
        policyPatternNode = policyPatternManagerWS.getPattern(name);
        
        if (policyPatternNode == null) {
            return PatternsView.class;
        }
     
        this.name = policyPatternNode.getName();
        
        return null;
    }
    
    public String onPassivate() {
        return policyPatternNode != null ? policyPatternNode.getName() : null;
    }
    
    @Cached
    public Object getSource()
    throws WebServiceCheckedException
    {
        return new PolicyPatternNodeGridDataSource(policyPatternManagerWS);
    }

    @Cached
    private List<PolicyPatternNodeDTO> getSelectedNodes()
    throws WebServiceCheckedException
    {
        return policyPatternNode != null ? policyPatternNodeWS.getChilds(
                policyPatternNode.getName()) : null;
    }

    private boolean isSelected(PolicyPatternNodeDTO patternNode)
    throws WebServiceCheckedException
    {
        boolean selected = false;
        
        List<PolicyPatternNodeDTO> selectedNodes = getSelectedNodes(); 
        
        if (selectedNodes != null && patternNode != null) {
            selected = selectedNodes.contains(patternNode);
        }
        
        return selected;
    }
    
    /*
     * Event handler for isDisabled event from the DLPPatternGrid
     */
    @OnEvent(component = "patternGrid", value = DLPPatternGrid.IS_DISABLED_EVENT)
    @Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_DLP_MANAGER})
    protected YesNo isDisabledEvent(PolicyPatternNodeDTO patternNode)
    throws WebServiceCheckedException 
    {
        if (patternNode != null && StringUtils.equals(name, patternNode.getName()))
        {
            /*
             * Prevent the user from selecting this group as a child.
             */
            return YesNo.YES;
        }
        
        return isSelected(patternNode) ? YesNo.YES : YesNo.NO;
    }

    /*
     * Event handler for isSelected event from the DLPPatternGrid
     */
    @OnEvent(component = "patternGrid", value = DLPPatternGrid.IS_SELECTED_EVENT)
    @Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_DLP_MANAGER})
    protected YesNo isSelectedEvent(PolicyPatternNodeDTO patternNode) 
    throws WebServiceCheckedException 
    {
        return isSelected(patternNode) ? YesNo.YES : YesNo.NO;
    }
    
    public Object onSuccess()
    {
        Object result = null;

        try {
            /*
             * Note: we need to clone the set to make sure that concurrent modifications
             * are possible.
             */
            String[] selected = patternGrid.getSelected().toArray(new String[]{});
            
            for (String childName : selected) {
                policyPatternNodeWS.addChild(name, childName);
            }
            
            result = resources.createPageLink(PatternsGroup.class, false, name);
        }
        catch(WebServiceCheckedException e)
        {
            logger.error("Error during submit.", e);
            
            errorMessage = e.getMessage();
            error = true;
        }

        return result;
    }
        
    /*
     * Event handler called when the cancel button is pressed.
     */
    protected Object onCancel() {
        return resources.createPageLink(PatternsGroup.class, false, name);
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isError() {
        return error;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public int getRowsPerPage() {
        return rowsPerPage;
    }
}
