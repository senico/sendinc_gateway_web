package mitm.djigzo.web.pages.portal.secure;

import java.io.IOException;
import java.io.InputStream;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

import org.apache.tapestry5.Asset;
import org.apache.tapestry5.annotations.Cached;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.Path;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.ApplicationGlobals;

@IncludeStylesheet("context:styles/pages/portal/secure/about.css")
public class About
{
    @Inject
    private ApplicationGlobals applicationGlobals;
    
    @SuppressWarnings("unused")
    @Inject
    @Path("context:images/logo.png")
    @Property
    private Asset logoAsset;
    
    @Cached
    private Attributes getAttributes()
    throws IOException
    {
        /*
         * Try to read Implementation-Version from the MANIFEST. This only works when deplayed from 
         * a WAR file.
         */
        InputStream input = applicationGlobals.getServletContext().
            getResourceAsStream("/META-INF/MANIFEST.MF");
        
        
        Attributes attributes = null;
        
        if (input != null)
        {
            Manifest manifest = new Manifest(input);
            
            attributes = manifest.getMainAttributes();
        }
        
        return attributes;
    }
    
    @Cached
    public String getVersion()
    throws IOException
    {
        String version = null;
        
        Attributes attributes = getAttributes();
        
        if (attributes != null) {
            version = attributes.getValue("Implementation-Version");
        }
                
        return version;
    }

    @Cached
    public String getBuilt()
    throws IOException
    {
        String built = null;
        
        Attributes attributes = getAttributes();
        
        if (attributes != null) {
            built = attributes.getValue("Built");
        }
                
        return built;
    }
}
