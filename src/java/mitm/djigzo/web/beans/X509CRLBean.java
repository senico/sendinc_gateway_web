/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.beans;

import java.util.Date;

/**
 * Represents a X509CRL
 * 
 * @author Martijn Brinkers
 *
 */
public interface X509CRLBean 
{
	/**
	 * The issuer (friendly name) of this CRL
	 */
	public String getIssuer();

	/**
	 * The next update (when the new CRL will be available) of this CRL
	 */
	public Date getNextUpdate();

	/**
	 * The date this CRL was issued
	 */
	public Date getThisUpdate();

	/**
	 * The version of the CRL (mostly 1 or 2)
	 */
	public int getVersion();

	/**
	 * The CRL number in hex format
	 */
	public String getCrlNumber();

	/**
	 * The delta CRL indicator in hex format
	 */
	public String getDeltaIndicator();

	/**
	 * True if this CRL is a delta CRL
	 */
	public boolean isDeltaCRL();

	/**
	 * The thumbprint (currently SHA512) of this CRL in hex format
	 */
	public String getThumbprint();
	
	/**
	 * The SHA1 hash of this CRL in hex format
	 */
	public String getThumbprintSHA1();
	
	/**
	 * The algorithm used to sign the CRL with
	 */
    public String getSignatureAlgorithm();	
}
