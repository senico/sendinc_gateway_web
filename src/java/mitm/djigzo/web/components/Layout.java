/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.components;

import mitm.common.security.JCEPolicyManager;
import mitm.djigzo.web.DjigzoWebConstants;
import mitm.djigzo.web.services.LogoutService;

import org.apache.commons.lang.StringUtils;
import org.apache.tapestry5.Block;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.annotations.SupportsInformalParameters;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.springframework.security.Authentication;
import org.springframework.security.context.SecurityContextHolder;

@SupportsInformalParameters
@IncludeStylesheet({"context:styles/style.css", "context:styles/components/layout.css"})
public class Layout
{
    @Inject 
    private LogoutService logoutService;

	@Parameter
	private String title = DjigzoWebConstants.TITLE;

	@Parameter
	private Block left;

	@Component
	private Left leftRegularContent;

	/**
	 * This method check if the left parameter has been set by the user
	 * if not the regular content is shown otherwise the content of this parameter
	 *
	 * @return the component we want to display
	 */
	public Object getLeftContent() {
		return left == null ?  leftRegularContent : left;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	/*
	 * Returns true if the JCE unlimited strength policy file is installed
	 */
	public boolean isUnlimitedStrength() {
		return JCEPolicyManager.isUnlimitedStrength();
	}
	
	void onActionFromLogout()
    {
		logoutService.logout();
    }
	
	public String getAdmin() 
	{
        String name = null;

        Authentication currentUser = SecurityContextHolder.getContext().getAuthentication();
        
        if (currentUser != null) {
            name = currentUser.getName();
        }
        
        return StringUtils.defaultString(name);
	}
}
