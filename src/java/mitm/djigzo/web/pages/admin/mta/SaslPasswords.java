/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.pages.admin.mta;

import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import mitm.application.djigzo.admin.FactoryRoles;
import mitm.application.djigzo.ws.PostfixConfigManagerWS;
import mitm.common.postfix.SaslPassword;
import mitm.common.ws.WebServiceCheckedException;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang.text.StrBuilder;
import org.apache.tapestry5.Asset;
import org.apache.tapestry5.ComponentResources;
import org.apache.tapestry5.PersistenceConstants;
import org.apache.tapestry5.annotations.Cached;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Path;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.beaneditor.BeanModel;
import org.apache.tapestry5.corelib.components.Grid;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.ioc.annotations.Value;
import org.apache.tapestry5.json.JSONObject;
import org.apache.tapestry5.services.BeanModelSource;
import org.apache.tapestry5.services.Request;
import org.springframework.security.annotation.Secured;

@IncludeStylesheet("context:styles/pages/admin/mta/saslPasswords.css")
@Secured({FactoryRoles.ROLE_ADMIN})
public class SaslPasswords
{
    @Inject
    private PostfixConfigManagerWS postfixConfigManager;
    
    @Inject
    private BeanModelSource beanModelSource;

    @Inject
    private ComponentResources resources;
    
    @Inject
    private Request request;
    
    @Inject
    @Value("${saslPasswords.rowsPerPage}")
    private int rowsPerPage;    
        
    @Inject
    @Value("${saslPasswords.hidePasswords}")
    private boolean hidePasswords;    
    
    @Inject
    @Path("context:icons/cross.png")
    private Asset deleteAsset;
    
    @Persist(PersistenceConstants.FLASH)
    private boolean applied;

    @Persist(PersistenceConstants.FLASH)
    private boolean applyError;
    
    /*
     * The failure message when backup
     */    
    @Persist(PersistenceConstants.FLASH)
    private String failureMessage;
    
    /*
     * The current rendering row
     */
    private SaslPassword row;
    
    /*
     * Set of the selected SaslPassword's
     */
    @Persist
    private Set<String> selected;
    
    @SuppressWarnings("unused")
    @Component(parameters = {"source = passwords", "model = model", "volatile = true", "row = row", 
            "reorder = select,delete", "rowsPerPage = prop:rowsPerPage"})
    private Grid grid;
    
    @SetupRender
    @Secured({FactoryRoles.ROLE_ADMIN})
    protected void setupRender() {
        /*
         * Empty on purpose
         */
    }

    @SetupRender
    public void setupGrid() 
    {
        if (selected == null) {
            selected = Collections.synchronizedSet(new HashSet<String>());
        }
        else {
            selected.clear();
        }
    }
    
    public BeanModel<SaslPassword> getModel()
    {
        BeanModel<SaslPassword> model = beanModelSource.createDisplayModel(SaslPassword.class, 
                resources.getMessages());
        
        model.add("delete", null).sortable(false);
        model.add("select", null).sortable(false);
        
        return model;
    }
    
    @Cached
    public List<SaslPassword> getPasswords()
    throws WebServiceCheckedException
    {
        return postfixConfigManager.getSaslPasswords();
    }
    
    public void onSuccess()
    {
        try {
            postfixConfigManager.reload();
            
            applied = true;
        }
        catch (WebServiceCheckedException e)
        {
            applyError = true;
            failureMessage = ExceptionUtils.getRootCauseMessage(e);
        }
    }
    
    /*
     * Event handler called when the cancel button is pressed.
     */
    public Object onCancel() {
        return MTAConfig.class;
    }
    
    public String getPassword() {
        return hidePasswords ? "***" : row.getPassword();
    }
    
    /*
     * A entry in the grid gets an id based on server and port
     */
    public String getID(SaslPassword password)
    {
        return new StrBuilder()
            .append(password.getServer())
            .append(":")
            .append(password.getPort())
            .toString();
    }

    /*
     * Returns the ID of the current rendering grid element
     */
    public String getCurrentID() {
        return getID(row);
    }
    
    @OnEvent(component = "deleteEntry")
    protected void deleteEntry(String id) 
    throws WebServiceCheckedException 
    {
        List<SaslPassword> passwords = getPasswords();

        for (SaslPassword password : passwords)
        {
            if (StringUtils.equals(id, getID(password)))
            {
                passwords.remove(password);

                postfixConfigManager.setSaslPasswords(passwords);
                
                break;
            }
        }
    }
    
    /*
     * Event handler called when the checkbox is clicked. 
     */
    public void onCheckboxEvent()
    {
        String data = request.getParameter("data");

        if (StringUtils.isEmpty(data)) {
            throw new IllegalArgumentException("data is missing.");
        }
        
        JSONObject json = new JSONObject(data);
                        
        String element = json.getString("element");
        boolean checked = json.getBoolean("checked"); 
        
        if (checked) {
            selected.add(element);
        }
        else {
            selected.remove(element);
        }
    }
    
    @OnEvent(component = "deleteSelected")
    protected void deleteSelected()
    throws WebServiceCheckedException 
    {
        if (selected != null)
        {
            List<SaslPassword> passwords = getPasswords();

            Iterator<SaslPassword> it = passwords.iterator();
            
            boolean removed = false;
            
            while (it.hasNext())
            {
                SaslPassword password = it.next();
                
                String id = getID(password);
                
                if (selected.contains(id))
                {
                    it.remove();
                    
                    selected.remove(id);
                 
                    removed = true; 
                    
                    if (selected.size() == 0) {
                        break;
                    }
                }
            }
            
            if (removed) {
                postfixConfigManager.setSaslPasswords(passwords);
            }
        }
    }
    
    public boolean getSelect() {
        return selected.contains(getCurrentID());
    }
    
    public void setSelect(boolean value) 
    {
        /*
         * Ignored. Checkbox values will be handled using EventCheckbox (see onCheckboxClick)
         */
    }
    
    public int getRowsPerPage() {
        return rowsPerPage;
    }
    
    public boolean isApplied() {
        return applied;
    }

    public boolean isApplyError() {
        return applyError;
    }

    public SaslPassword getRow() {
        return row;
    }

    public void setRow(SaslPassword row) {
        this.row = row;
    }
    
    public Asset getDeleteAsset() {
        return deleteAsset;
    }

    public String getFailureMessage() {
        return StringUtils.isNotBlank(failureMessage) ? failureMessage : "No details.";
    }
}
