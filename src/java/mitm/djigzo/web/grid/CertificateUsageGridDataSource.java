/*
 * Copyright (c) 2009-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.grid;

import java.util.LinkedList;
import java.util.List;

import mitm.application.djigzo.DjigzoRuntimeException;
import mitm.application.djigzo.ws.UserPreferencesDTO;
import mitm.application.djigzo.ws.UserPreferencesWorkflowWS;
import mitm.common.util.Check;
import mitm.common.ws.WebServiceCheckedException;
import mitm.djigzo.web.beans.CertificateUsageBean;
import mitm.djigzo.web.beans.CertificateUsageBean.Usage;
import mitm.djigzo.web.beans.impl.CertificateUsageBeanImpl;

import org.apache.tapestry5.grid.GridDataSource;
import org.apache.tapestry5.grid.SortConstraint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CertificateUsageGridDataSource implements GridDataSource 
{
	private final static Logger logger = LoggerFactory.getLogger(CertificateUsageGridDataSource.class);
	
	private List<CertificateUsageBean> usages;
	
	/*
	 * we need to keep track of base index because the index in getRowValue is the absolute
	 * index and not relative.
	 */
	private int startIndex;
	
	private final UserPreferencesWorkflowWS userPreferencesWorkflowWS;
	
	private final String thumbprint;
	
	public CertificateUsageGridDataSource(UserPreferencesWorkflowWS userPreferencesWorkflowWS, String thumbprint)
	{
        Check.notNull(userPreferencesWorkflowWS, "userPreferencesWorkflowWS");
        Check.notNull(thumbprint, "thumbprint");
		
		this.userPreferencesWorkflowWS = userPreferencesWorkflowWS;
		this.thumbprint = thumbprint;
	}

    @Override
    public int getAvailableRows() 
    {
        try {
            return (int) (userPreferencesWorkflowWS.getReferencingFromCertificatesCount(thumbprint) + 
                userPreferencesWorkflowWS.getReferencingFromKeyAndCertificateCount(thumbprint) + 
                userPreferencesWorkflowWS.getReferencingFromNamedCertificatesCount(thumbprint));
        }
        catch (WebServiceCheckedException e) {
            throw new DjigzoRuntimeException(e);
        }
    }

    @Override
    public CertificateUsageBean getRowValue(int index) 
    {
        int rowIndex = index - startIndex;
        
        CertificateUsageBean value = null;
        
        if (usages != null && rowIndex < usages.size()) {
            value = usages.get(rowIndex);
        }
        else {
            logger.warn("Not enough rows.");
            /* 
             * We do not have enough Bean objects. This can happen when AvailableRows
             * returned more rows than were actually available when prepared was called. 
             */ 
            value = null;
        }
        
        return value;
    }

    private void addToUsages(List<UserPreferencesDTO> found, CertificateUsageBean.Usage usage)
    {
        if (found == null) {
            return;
        }
        
        for (UserPreferencesDTO userPreference : found) {
            usages.add(new CertificateUsageBeanImpl(userPreference, usage));
        }
    }
    
    @Override
    public void prepare(int startIndex, int endIndex, List<SortConstraint> sortConstraints) 
    {
        try {
            /*
             * We need to save the startIndex because we need in to get the correct index of 
             * certificateBeans in getRowValue (the index value is absolute but we need to 
             * make it relative)
             */
            this.startIndex = startIndex;

            usages = new LinkedList<CertificateUsageBean>();
            
            List<UserPreferencesDTO> found = userPreferencesWorkflowWS.getReferencingFromCertificates(thumbprint, 
                    startIndex, endIndex - startIndex + 1);

            addToUsages(found, Usage.ENCRYPTION);
            
            /*
             * calculate how many we have to retrieve from KeyAndCertificates
             */            
            int left = endIndex - startIndex + 1 - usages.size();
    
            if (left > 0)
            {
                startIndex = startIndex - (int) userPreferencesWorkflowWS.getReferencingFromCertificatesCount(thumbprint);
                
                if (startIndex < 0) {
                    startIndex = 0;
                }
                
                found = userPreferencesWorkflowWS.getReferencingFromKeyAndCertificate(thumbprint, startIndex, left);
                
                addToUsages(found, Usage.SIGNING);
            }
            
            /*
             * calculate how many we have to retrieve from NamedCertificates
             */            
            left = endIndex - startIndex + 1 - usages.size();
    
            if (left > 0)
            {
                startIndex = startIndex - (int) userPreferencesWorkflowWS.getReferencingFromKeyAndCertificateCount(thumbprint);
                
                if (startIndex < 0) {
                    startIndex = 0;
                }
                
                found = userPreferencesWorkflowWS.getReferencingFromNamedCertificates(thumbprint, startIndex, left);
                
                addToUsages(found, Usage.NAMED);
            }            
        }
        catch(WebServiceCheckedException e) {
            throw new DjigzoRuntimeException(e);
        }
    }

    @Override
    public Class<?> getRowType() {
        return CertificateUsageBean.class;
    }
}
