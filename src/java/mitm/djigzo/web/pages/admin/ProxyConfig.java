/*
 * Copyright (c) 2009-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.pages.admin;

import mitm.application.djigzo.admin.FactoryRoles;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.util.DomainUtils;
import mitm.common.util.DomainUtils.DomainType;
import mitm.common.ws.WebServiceCheckedException;
import mitm.djigzo.web.beans.ProxySettingsBean;
import mitm.djigzo.web.beans.impl.ProxySettingsBeanImpl;
import mitm.djigzo.web.components.NonClearingPassword;
import mitm.djigzo.web.entities.GlobalPreferencesManager;
import mitm.djigzo.web.pages.Admins;
import mitm.djigzo.web.services.DisableHttpCache;

import org.apache.cxf.common.util.StringUtils;
import org.apache.tapestry5.PersistenceConstants;
import org.apache.tapestry5.annotations.Cached;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.corelib.components.Checkbox;
import org.apache.tapestry5.corelib.components.Form;
import org.apache.tapestry5.corelib.components.TextField;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.springframework.security.annotation.Secured;

@IncludeStylesheet("context:styles/pages/admin/proxyConfig.css")
@Secured({FactoryRoles.ROLE_ADMIN})
public class ProxyConfig
{
    @Inject
    private GlobalPreferencesManager globalPreferencesManager;

    /*
     * true if the settings were applied
     */
    @Persist(PersistenceConstants.FLASH)
    private boolean applied;
    
    @SuppressWarnings("unused")
    @Component(id = "username", parameters = {"value = proxySettings.username"})
    private TextField usernameField;

    @SuppressWarnings("unused")
    @Component(id = "password", parameters = {"value = password"})
    private NonClearingPassword passwordField;

    @SuppressWarnings("unused")
    @Component(id = "domain", parameters = {"value = proxySettings.domain"})
    private TextField domainField;

    @Component(id = "host", parameters = {"value = proxySettings.host", "validate = required"})
    private TextField hostField;

    @SuppressWarnings("unused")
    @Component(id = "port", parameters = {"value = proxySettings.port", "validate = min=1, max=65535, required"})
    private TextField portField;

    @SuppressWarnings("unused")
    @Component(id = "proxyEnabled", parameters = {"value = proxySettings.enabled"})
    private Checkbox proxyEnabledField;
    
    @Component(id = "form")
    private Form form;
    
    @SetupRender
    @DisableHttpCache /* disable cache for security purposes */
    @Secured({FactoryRoles.ROLE_ADMIN})
    protected void setupRender() {
        /*
         * Empty on purpose
         */
    }
    
    @Cached
    public ProxySettingsBean getProxySettings()
    throws HierarchicalPropertiesException, WebServiceCheckedException
    {
        return new ProxySettingsBeanImpl(globalPreferencesManager.getProperties());
    }

    public void setPassword(String password)
    throws HierarchicalPropertiesException, WebServiceCheckedException
    {
        if (!NonClearingPassword.DUMMY_PASSWORD.equals(password))
        {
            /*
             * The password has been changed
             */
            getProxySettings().setPassword(password);
        }
    }
    
    public String getPassword()
    throws HierarchicalPropertiesException, WebServiceCheckedException
    {
        /*
         * return the dummy password if the password has been set
         */
        return StringUtils.isEmpty(getProxySettings().getPassword()) ? "" : NonClearingPassword.DUMMY_PASSWORD;
    }
    
    public void onValidateFromHost(String host)
    {
        if (DomainUtils.validate(host, DomainType.FRAGMENT) == null) {
            form.recordError(hostField, "The proxy host is not a valid hostname");
        }
    }
    
    // XXX use dummy password. MOet host een check hebben of het een valide host is?
    
    public void onSuccess()
    throws HierarchicalPropertiesException, WebServiceCheckedException
    {
        getProxySettings().save();
        
        applied = true;
    }
    
    public boolean isApplied() {
        return applied;
    }
    
    /*
     * Event handler called when the cancel button is pressed.
     */
    public Object onCancel() {
        return Admins.class;
    }    
}
