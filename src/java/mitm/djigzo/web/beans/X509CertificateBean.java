/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.beans;

import java.math.BigInteger;
import java.util.Date;

/**
 * Represents a X509Certificate
 * 
 * @author Martijn Brinkers
 *
 */
public interface X509CertificateBean 
{
	/**
	 * Email addresses (comma separated) of this certificate
	 */
    public String getEmail();

    /**
     * Subject of the certificate (friendly name)
     */
    public String getSubject();

    /**
     * The certificate is not valid before this date
     */
    public Date getNotBefore();

    /**
     * The certificate is not valid after this date
     */
    public Date getNotAfter();
    
    /**
     * True if the certificate is expired (ie the current date is before notBefore or after notAfter)
     */
    public boolean isExpired();
        
    /**
     * The key usage of this certificate (comma separated)
     */
    public String getKeyUsage();

    /**
     * The extended key usage of this certificate (comma separated)
     */
    public String getExtendedKeyUsage();
    
    /**
     * The issuer of this certificate (friendly name)
     */
    public String getIssuer();

    /**
     * The serial number (in hex format) of this certificate
     */
    public String getSerialNumberHex();

    /**
     * The SHA1 hash (in hex format) of this certificate
     */
    public String getThumbprintSHA1();

    /**
     * True if the CA flag of the certificate is true
     */
    public boolean isCA();
    
    /**
     * Returns the path length constraint (only valid when isCA is true) 
     */
    public BigInteger getPathLengthConstraint();
    
    /**
     * The subject key identifier (in hex format) of this certificate
     */
    public String getSubjectKeyIdentifier();
    
    /**
     * The thumbprint (in hex format) of this certificate (currently SHA512)
     */
    public String getThumbprint();

    /**
     * The signature algorithm used to sign the certificate
     */
    public String getSignatureAlgorithm();
    
    /**
     * The length (in bits of the public key)
     */
    public int getPublicKeyLength();

    /**
     * The algorithm name of the public key (this is a friendly name just for display purposes)
     */
    public String getPublicKeyAlgorithm();
    
    /**
     * Comma separated URI distribution points
     */
    public String getURIDistributionPointNames();
    
    /**
     * The key alias of the associated key. Null if there is no associated key
     */
    public String getKeyAlias();   
    
	/**
	 * True if this certificate has an associated private key
	 */
	public boolean isPrivateKeyAvailable();
	
	/**
	 * True if the private key is accessible
	 */
	public boolean isPrivateKeyAccessible();
	
	/**
	 * True if this certificate is inherited
	 */
	public boolean isInherited();
}
