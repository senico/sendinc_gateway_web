/*
 * Copyright (c) 2009-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.pages.admin.backup;

import java.io.IOException;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.util.ByteArrayDataSource;

import mitm.application.djigzo.admin.FactoryRoles;
import mitm.application.djigzo.ws.BackupWS;
import mitm.application.djigzo.ws.BinaryDTO;
import mitm.common.util.Check;
import mitm.common.ws.WebServiceCheckedException;
import mitm.djigzo.web.common.BackupType;
import mitm.djigzo.web.common.ContentTypes;
import mitm.djigzo.web.common.streamresponse.InputStreamFileResponse;
import mitm.djigzo.web.pages.admin.Restarting;

import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.tapestry5.PersistenceConstants;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.corelib.components.Form;
import org.apache.tapestry5.corelib.components.PasswordField;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.upload.components.Upload;
import org.apache.tapestry5.upload.services.UploadEvents;
import org.apache.tapestry5.upload.services.UploadedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.annotation.Secured;

@IncludeStylesheet("context:styles/pages/admin/backup/backupManager.css")
@Secured({FactoryRoles.ROLE_ADMIN})
public class BackupManager
{
    private final static Logger logger = LoggerFactory.getLogger(BackupManager.class);
    
    @Inject
    private BackupWS backupWS;
    
    private String password;
    
    @Component
    private Form form;
    
    @SuppressWarnings("unused")
    @Component(id = "password", parameters = {"value = password"})
    private PasswordField passwordField;
    
    @Component(id = "upload", parameters = {"value = file"})
    private Upload upload;
    
    /*
     * True if backup was successful
     */
    @Persist(PersistenceConstants.FLASH)
    private boolean backupSuccess;

    /*
     * True if backup could not be made
     */
    @Persist(PersistenceConstants.FLASH)
    private boolean backupFailure;

    /*
     * True if restore could not be made
     */
    @Persist(PersistenceConstants.FLASH)
    private boolean restoreFailure;
    
    /*
     * The failure message when backup
     */    
    @Persist(PersistenceConstants.FLASH)
    private String failureMessage;

    /*
     * The backup identifier
     */    
    @Persist(PersistenceConstants.FLASH)
    private String backupIdentifier;
    
    /*
     * 'Handle' to the file that's uploaded
     */
    private UploadedFile file;
    
    /*
     * True when createBackup button is pressed
     */
    private boolean createBackup;

    /*
     * True when restoreBackup button is pressed
     */
    private boolean restoreBackup;
    
    /*
     * determines where the backup will be stored.
     */
    @Persist
    private BackupType backupType;
    
    @SetupRender
    @Secured({FactoryRoles.ROLE_ADMIN})
    protected void setupRender() {
        /*
         * Empty on purpose
         */
        if (backupType == null) {
            backupType = BackupType.LOCAL;
        }
    }
    
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    public boolean isBackupSuccess() {
        return backupSuccess;
    }

    public boolean isBackupFailure() {
        return backupFailure;
    }

    public boolean isRestoreFailure() {
        return restoreFailure;
    }
    
    public String getFailureMessage() {
        return StringUtils.isNotBlank(failureMessage) ? failureMessage : "No details.";
    }

    public String getBackupIdentifier() {
        return StringUtils.defaultString(backupIdentifier);
    }
    
    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public BackupType getBackupType() {
        return backupType;
    }
    
    public void setbackupType(BackupType backupType) {
        this.backupType = backupType;
    }
    
    public BackupType getLocal() {
        return BackupType.LOCAL;
    }

    public BackupType getRemote() {
        return BackupType.REMOTE;
    }
    
    /*
     * Event handler that gets called when the uploaded file exceeds the maximum size
     */
    @OnEvent(UploadEvents.UPLOAD_EXCEPTION)
    protected Object onUploadException(FileUploadException uploadException) 
    {
        restoreBackup = true;
        failureMessage = uploadException.getMessage();
        
        return this;
    }
    
    public void onValidateForm() 
    {
        if (restoreBackup)
        {
            if (file == null) {
                form.recordError(upload, "Restore file is missing.");
            }
        }
    }
    
    private void restoreFailure(Throwable e)
    {
        logger.error("Error restoring backup.", e);
        
        restoreFailure = true;
        
        failureMessage = ExceptionUtils.getRootCauseMessage(e);
    }
    
    private Object restore()
    {        
        Check.notNull(file, "file");
        
        Object result = null;
        
        restoreFailure = false;

        try {
            DataSource source = new ByteArrayDataSource(file.getStream(), file.getContentType());
            DataHandler dataHandler = new DataHandler(source);
            
            BinaryDTO backup = new BinaryDTO(dataHandler);
            
            backupWS.restore(backup, password);
            
            result = Restarting.class;
        }
        catch (IOException e) {
            restoreFailure(e);
        }
        catch (WebServiceCheckedException e) {
            restoreFailure(e);
        }
        
        return result;
    }

    private void backupFailure(Throwable e)
    {        
        logger.error("Error creating backup.", e);
        
        backupFailure = true;
        
        failureMessage = ExceptionUtils.getRootCauseMessage(e);
    }
    
    private void backup()
    {        
        backupSuccess = false;
        backupFailure = false;

        try {
            backupIdentifier = backupWS.backup(password);
            
            backupSuccess = true;
        }
        catch (WebServiceCheckedException e) {
            backupFailure(e);
        }
    }

    private Object backupDirect()
    {        
        /*
         * Because the backup will be downloaded we cannot report any success
         * so we set all properties to false
         */
        backupSuccess = false;
        backupFailure = false;

        Object result = null;
        
        try {
            BinaryDTO backup = backupWS.backupDirect(password);

            result = new InputStreamFileResponse(backup.getDataHandler().getInputStream(), 
                    ContentTypes.X_DOWNLOAD, backup.getFilename());
        }
        catch (WebServiceCheckedException e) {
            backupFailure(e);
        }
        catch (IOException e) {
            backupFailure(e);
        }
        
        return result;
    }
    
    public Object onSuccess()
    {
        Object result = null;
        
        if (createBackup)
        {
            if (backupType == BackupType.REMOTE) {
                backup();
            }
            else {
                result = backupDirect();
            }
        }
        else if (restoreBackup) {
            result = restore();
        }
        
        return result;
    }
    
    public void onSelectedFromCreateBackup() {
        createBackup = true;
    }
    
    public void onSelectedFromRestoreBackup() {
        restoreBackup = true;
    }
}
