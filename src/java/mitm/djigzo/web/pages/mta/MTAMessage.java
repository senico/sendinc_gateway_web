/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.pages.mta;

import mitm.application.djigzo.admin.FactoryRoles;
import mitm.application.djigzo.ws.PostfixSpoolManagerWS;
import mitm.common.util.MiscStringUtils;
import mitm.common.ws.WebServiceCheckedException;
import mitm.djigzo.web.pages.Queues;

import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.ioc.annotations.Value;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.annotation.Secured;

@IncludeStylesheet("context:styles/pages/mta/mtaMessage.css")
@Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_QUEUE_MANAGER})
public class MTAMessage 
{
	private static final Logger logger = LoggerFactory.getLogger(MTAMessage.class);
	
	@Inject @Value("${email.max-size}")
	private int maxMessageLength;
	
	private String queueID;
	
	@Inject
	private PostfixSpoolManagerWS spoolManager;
	
	@SetupRender
	@Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_QUEUE_MANAGER})
	protected void setupRender() {
		/*
		 * Empty on purpose
		 */
	}
	
    @Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_QUEUE_MANAGER})
	public void onActivate(String queueID) {
		this.queueID = queueID;
	}

	public String onPassivate() {
		return queueID;
	}
	
	public String getMessage() 
	{
		String message = null;
		
		if (queueID != null) 
		{
			try {
			    byte[] encodedMessage = spoolManager.getMessage(queueID, maxMessageLength);
			    
                message = MiscStringUtils.toUTF8String(encodedMessage);
			} 
			catch (WebServiceCheckedException e) {
				/*
				 * Can happen if the message no longer exists
				 */
				logger.error("Error getting message. Cause: " + e.getMessage());
			}
		}
		
		if (message == null) {
			message = "Message not found."; 
		}
		
		return message;
	}

    /*
     * Event handler called when the cancel button is pressed.
     */
    protected Object onCancel() {
        return Queues.class;
    }

	public String getQueueID() {
		return queueID;
	}
}
