/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.components;

import mitm.common.util.MiscStringUtils;

import org.apache.tapestry5.MarkupWriter;
import org.apache.tapestry5.annotations.BeginRender;
import org.apache.tapestry5.annotations.Parameter;

/**
 * Component that trims the string when the string is larger than the given size. If the string
 * is trimmed it appends three ellipses (...) to indicate that the string was trimmed.
 * 
 * 
 * @author Martijn Brinkers
 *
 */
public class RestrictLength 
{
    @Parameter(required = true, defaultPrefix = "literal")
    private String value;

    @Parameter(required = true, defaultPrefix = "literal")
    private Integer maxLength;
    
    @BeginRender
    void beginRender(MarkupWriter writer)
    {
    	writer.write(MiscStringUtils.restrictLength(value, maxLength, true /* add dots */));
    }
}
