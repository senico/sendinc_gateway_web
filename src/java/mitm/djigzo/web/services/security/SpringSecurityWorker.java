/*
 * Copyright 2007 Ivan Dubrov
 * Copyright 2007, 2008 Robin Helgelin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package mitm.djigzo.web.services.security;

import java.lang.reflect.Modifier;
import java.util.LinkedList;
import java.util.List;

import org.apache.tapestry5.model.MutableComponentModel;
import org.apache.tapestry5.services.ClassTransformation;
import org.apache.tapestry5.services.ComponentClassTransformWorker;
import org.apache.tapestry5.services.TransformMethodSignature;
import org.springframework.security.ConfigAttribute;
import org.springframework.security.ConfigAttributeDefinition;
import org.springframework.security.SecurityConfig;
import org.springframework.security.annotation.Secured;

/**
 * @author Ivan Dubrov
 * 
 * Martijn Brinkers
 * 	- 15/07/08 
 * 		- Updated to make it compatible with Spring security and Tapestry 5.0.13
 *  	- Renamed to SpringSecurityWorker
 *  	- Secure page removed because it was not working when a page already had a beginRender method
 */
public class SpringSecurityWorker implements ComponentClassTransformWorker 
{
    private SecurityChecker securityChecker;

    public SpringSecurityWorker(final SecurityChecker securityChecker) {
        this.securityChecker = securityChecker;
    }

    @Override
    public final void transform(ClassTransformation transformation, MutableComponentModel model) 
    {
        /*
         * Secure methods
         */ 
        for (TransformMethodSignature method : transformation.findMethodsWithAnnotation(Secured.class)) 
        {
            transformMethod(transformation, method);
        }
    }

    private void transformMethod(ClassTransformation transformation, TransformMethodSignature method) 
    {
        /* 
         * inject Security checker
         */
        final String interField = transformation.addInjectedField(SecurityChecker.class, "_$checker", securityChecker);
        
        /*
         * Interceptor status token
         */ 
        final String statusToken = transformation.addField(Modifier.PRIVATE,
                "org.springframework.security.intercept.InterceptorStatusToken", "_$token");

        /* 
         * Attribute definition
         */
        final Secured annotation = transformation.getMethodAnnotation(method, Secured.class);
        final String configField = createConfigAttributeDefinitionField(transformation, annotation);

        /*
         * Prefix and extend method
         */ 
        transformation.prefixMethod(method, statusToken + " = " + interField + ".checkBefore(" + configField + ");");
        transformation.extendExistingMethod(method, interField + ".checkAfter(" + statusToken + ", null);");
    }

    private String createConfigAttributeDefinitionField(ClassTransformation transformation, Secured annotation) 
    {
    	List<ConfigAttribute> configAttributes = new LinkedList<ConfigAttribute>();

    	for (String auth : annotation.value()) 
        {
    		configAttributes.add(new SecurityConfig(auth));
        }
        
        ConfigAttributeDefinition configAttributeDefinition = new ConfigAttributeDefinition(configAttributes);
        
        return transformation.addInjectedField(ConfigAttributeDefinition.class,
                "_$configAttributeDefinition", configAttributeDefinition);
    }
}