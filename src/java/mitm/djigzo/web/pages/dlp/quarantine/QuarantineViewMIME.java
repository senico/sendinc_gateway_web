/*
 * Copyright (c) 2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.pages.dlp.quarantine;

import java.io.InputStream;

import mitm.application.djigzo.admin.FactoryRoles;
import mitm.application.djigzo.ws.BinaryDTO;
import mitm.application.djigzo.ws.MailRepositoryWS;
import mitm.common.locale.CharacterEncoding;
import mitm.common.util.SizeLimitedInputStream;
import mitm.djigzo.web.pages.Queues;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.tapestry5.PersistenceConstants;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.ioc.annotations.Value;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.annotation.Secured;

@IncludeStylesheet("context:styles/pages/dlp/quarantine/viewMIME.css")
@Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_DLP_MANAGER, FactoryRoles.ROLE_QUARANTINE_MANAGER})
public class QuarantineViewMIME
{
    private final static Logger logger = LoggerFactory.getLogger(QuarantineViewMIME.class);
    
    @Inject @Value("${email.max-size}")
    private int maxMessageLength;
    
    @Inject
    private MailRepositoryWS mailRepository;
    
    /*
     * True if and error has occurred
     */
    @Persist(PersistenceConstants.FLASH)
    private boolean error;

    /*
     * Error message if an error has occurred
     */
    @Persist(PersistenceConstants.FLASH)
    private String errorMessage;
    
    /*
     * The Id of the mail from which the MIME message will be shown
     */
    private String mailId;
    
    @SetupRender
    @Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_DLP_MANAGER, FactoryRoles.ROLE_QUARANTINE_MANAGER})
    protected void setupRender()
    {
        /*
         * Empty on purpose
         */
    }
    
    @Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_DLP_MANAGER, FactoryRoles.ROLE_QUARANTINE_MANAGER})
    public void onActivate(String mailId) {
        this.mailId = mailId;
    }
    
    public String getMIME()
    {
        String mime = null;
        
        try {
            BinaryDTO backup = mailRepository.getMimeMessage(mailId);

            /*
             * Wrap in a SizeLimitedInputStream so we can limit the nr of bytes read.
             */
            InputStream mimeInput = new SizeLimitedInputStream(backup.getDataHandler().getInputStream(), 
                    maxMessageLength, false);
            
            try {
                mime = IOUtils.toString(mimeInput, CharacterEncoding.US_ASCII);
            }
            finally {
                IOUtils.closeQuietly(mimeInput);
            }
        }
        catch (Exception e)
        {
            logger.error("Error getting MIME content", e);
            
            error = true;
            errorMessage = ExceptionUtils.getRootCauseMessage(e);
        }
        
        return mime;
    }

    /*
     * Event handler called when the cancel button is pressed.
     */
    protected Object onCancel() {
        return Queues.class;
    }
    
    public String getMailId()
    {
        return mailId;
    }
    
    public boolean isError() {
        return error;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}
