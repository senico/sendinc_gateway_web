/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.common.security;

import java.util.LinkedList;
import java.util.List;

import mitm.application.djigzo.ws.AdminDTO;
import mitm.application.djigzo.ws.AdminManagerWS;
import mitm.application.djigzo.ws.AdminWS;
import mitm.common.hibernate.SortDirection;
import mitm.common.security.crypto.RandomGenerator;
import mitm.common.util.Check;
import mitm.common.ws.WebServiceCheckedException;

import org.springframework.security.providers.dao.SaltSource;
import org.springframework.security.providers.encoding.PasswordEncoder;

public class AdminManagerImpl implements AdminManager 
{
	private final AdminManagerWS adminManagerWS;
	private final AdminWS adminWS;
	private final PasswordEncoder passwordEncoder;
	private final SaltSource saltSource;
	private final RandomGenerator randomGenerator;
	
	public AdminManagerImpl(AdminManagerWS adminManagerWS, AdminWS adminWS,
			PasswordEncoder passwordEncoder, SaltSource saltSource,
			RandomGenerator randomGenerator)
	{
		Check.notNull(adminManagerWS, "adminManagerWS");
		Check.notNull(adminWS, "adminWS");
		Check.notNull(passwordEncoder, "passwordEncoder");
		Check.notNull(saltSource, "saltSource");
		Check.notNull(randomGenerator, "randomGenerator");
		
		this.adminManagerWS = adminManagerWS;
		this.adminWS = adminWS;
		this.passwordEncoder = passwordEncoder;
		this.saltSource = saltSource;
		this.randomGenerator = randomGenerator;
	}
	
    @Override
	public List<Admin> getAdmins(Integer firstResult, Integer maxResults, SortDirection sortDirection) 
	throws WebServiceCheckedException
	{
		List<Admin> admins = new LinkedList<Admin>();
		
		List<AdminDTO> adminDTOs = adminManagerWS.getAdmins(firstResult, maxResults, sortDirection);
		
		for (AdminDTO adminDTO : adminDTOs)	
		{
			admins.add(createAdmin(adminDTO));
		}
		
		return admins;
	}

    @Override
	public Admin addAdmin(String username, boolean builtIn) 
	throws WebServiceCheckedException
	{
		AdminDTO adminDTO = adminManagerWS.addAdmin(username, builtIn);
		
		return createAdmin(adminDTO);
	}

    @Override
	public Admin getAdmin(String username) 
	throws WebServiceCheckedException
	{
		Admin admin = null;
		
		AdminDTO adminDTO = adminManagerWS.getAdmin(username);
		
		if (adminDTO != null) {
			admin = createAdmin(adminDTO);
		}
		
		return admin;
	}

    @Override
	public boolean deleteAdmin(String username) 
	throws WebServiceCheckedException
	{
		return adminManagerWS.deleteAdmin(username);
	}
	
    @Override
	public void deleteAll() 
	throws WebServiceCheckedException
	{
		adminManagerWS.deleteAll();
	}
	
    @Override
	public int getAdminCount() 
	throws WebServiceCheckedException
	{
		return adminManagerWS.getAdminCount();
	}
	
	private Admin createAdmin(AdminDTO adminDTO)
	{
		return new AdminImpl(adminDTO, passwordEncoder, saltSource, randomGenerator, adminWS);
	}
}
