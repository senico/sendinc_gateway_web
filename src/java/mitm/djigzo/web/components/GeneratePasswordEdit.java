/*
 * Copyright (c) 2009-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.components;

import java.util.Locale;

import mitm.common.security.crypto.RandomGenerator;
import mitm.common.util.Base32;
import mitm.djigzo.web.common.StaticValidationConstraintGenerator;
import mitm.djigzo.web.utils.ValidatorUtils;

import org.apache.tapestry5.Asset;
import org.apache.tapestry5.ComponentResources;
import org.apache.tapestry5.FieldValidator;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.IncludeJavaScriptLibrary;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.Mixins;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.annotations.Path;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SupportsInformalParameters;
import org.apache.tapestry5.corelib.components.Any;
import org.apache.tapestry5.corelib.components.TextField;
import org.apache.tapestry5.internal.services.FieldValidatorDefaultSourceImpl;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.json.JSONObject;
import org.apache.tapestry5.services.FieldValidatorDefaultSource;
import org.apache.tapestry5.services.FieldValidatorSource;
import org.apache.tapestry5.services.ValidationConstraintGenerator;

/**
 * 
 * @author Martijn Brinkers
 */
@SupportsInformalParameters
@IncludeJavaScriptLibrary("generatePasswordEdit.js")
@IncludeStylesheet("context:styles/components/generatePasswordEdit.css")
public class GeneratePasswordEdit
{
    @Inject 
    private Locale locale;
    
    @Inject
    private ComponentResources resources;

    @Inject
    private FieldValidatorSource fieldValidatorSource;
    
    @Parameter(required = true)
    private String value;

    @Parameter
    private String subLabel;

    @Parameter(defaultPrefix="literal")
    private String validate;
    
    /*
     * The number of random bytes to use for generation of secret
     */
    @Parameter("16")
    private int passwordLength;
    
    @Component(id = "passwordEdit", parameters = {"validate = prop:passwordValidator"})
    private TextField passwordEdit;
    
    @SuppressWarnings("unused")
    @Component(id = "generatePassword", parameters = {"eventName=click", "beforeEvent=beforeSecretKeyGenerated", 
            "afterEvent=afterSecretKeyGenerated", "context=prop:passwordContext"})
    @Mixins("AjaxEvent")
    private Any generatePassword;
    
    @Inject
    private RandomGenerator randomGenerator;
    
    @Inject
    @Path("context:icons/cog.png")
    @Property
    @SuppressWarnings("unused")
    private Asset iconAsset;
    
    public String getPasswordContext()
    {
        /*
         * Script context will be the field clientID so we can set the value
         */
        return passwordEdit.getClientId();
    }
    
    public TextField getField() {
        return passwordEdit;
    }
    
    public void setValue(String value) {
        this.value = value;
    }
    
    public String getValue() {
        return value;
    }

    public FieldValidator<?> getPasswordValidator()
    {
        if (validate == null) {
            return ValidatorUtils.NOOP_VALIDATOR;
        }
        
        ValidationConstraintGenerator constraintGenerator = new StaticValidationConstraintGenerator(validate);
        
        FieldValidatorDefaultSource fieldValidatorDefaultSource = new FieldValidatorDefaultSourceImpl(
                constraintGenerator, fieldValidatorSource);
        
        return fieldValidatorDefaultSource.createDefaultValidator(passwordEdit, resources.getId(), 
                resources.getContainerMessages(), locale, null, null);
    }
    
    @OnEvent(component = "generatePassword", value = "click")
    public JSONObject onClickGeneratePassword()
    {
        byte[] random = randomGenerator.generateRandom(passwordLength);
        
        String key = Base32.encode(random);
        
        JSONObject result = new JSONObject();
        
        result.append("key", key);
        
        return result;
    }

    public String getSubLabel() {
        return subLabel;
    }

    public void setSubLabel(String subLabel) {
        this.subLabel = subLabel;
    }

    public TextField getPasswordEdit() {
        return passwordEdit;
    }

    public String getValidate() {
        return validate;
    }

    public void setValidate(String validate) {
        this.validate = validate;
    }
}
