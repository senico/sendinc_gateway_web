/*
 * Copyright (c) 2011-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.components;

import mitm.djigzo.web.DjigzoWebConstants;
import mitm.djigzo.web.pages.portal.secure.Login;
import mitm.djigzo.web.services.LogoutService;

import org.apache.commons.lang.StringUtils;
import org.apache.tapestry5.annotations.Cached;
import org.apache.tapestry5.annotations.IncludeJavaScriptLibrary;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.annotations.SupportsInformalParameters;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.springframework.security.Authentication;
import org.springframework.security.context.SecurityContextHolder;

@SupportsInformalParameters
@IncludeJavaScriptLibrary("portalLayout.js")
@IncludeStylesheet({"context:styles/style.css", "context:styles/components/portalLayout.css"})
public class PortalLayout
{
    @Inject 
    private LogoutService logoutService;

    @Parameter
    private String title = DjigzoWebConstants.TITLE;

	Object onActionFromLogout()
	{
		logoutService.logout();
		
		/*
		 * We need to redirect to the Login page directly. If not the current page will be opened again which is not
		 * allowed because we just signed out. The page will then be redirected to the Login page but the request
		 * will be saved by Spring as a saved request. If the user then logs in again, all of a sudden the user will
		 * be redirected to the previous page with (for example to the OTP page with filled in PasswordID). By 
		 * redirecting explicitly to the Login page, this won't happen.
		 */
		return Login.class;
    }
	
	@Cached
	public String getLoggedInUser() 
	{
        String name = null;

        Authentication currentUser = SecurityContextHolder.getContext().getAuthentication();
        
        if (currentUser != null) {
        	name = currentUser.getName();
        }
        
        return StringUtils.defaultString(name);
	}
	
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
