/*
 * Copyright (c) 2009-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.validators;

import mitm.common.util.HexUtils;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.lang.StringUtils;
import org.apache.tapestry5.Field;
import org.apache.tapestry5.MarkupWriter;
import org.apache.tapestry5.ValidationException;
import org.apache.tapestry5.ioc.MessageFormatter;
import org.apache.tapestry5.services.FormSupport;
import org.apache.tapestry5.validator.AbstractValidator;

/**
 * Validator that checks whether the input is in HEX format. If an (optional) integer parameter is given
 * an extra check is done to make sure that the hex decoded byte array length is equal to the 
 * parameter.
 * 
 * @author Martijn Brinkers
 *
 */
public class HexValidator extends AbstractValidator<Integer, String>
{
    public static final String NAME = "hex";
    
    public HexValidator()
    {
        super(Integer.class, String.class, "validate-hex");
    }

    @Override
    public void validate(Field field, Integer constraintValue, MessageFormatter formatter, String value)
    throws ValidationException
    {
        if (StringUtils.isNotEmpty(value))
        {
            if (!HexUtils.isHex(value)) {
                throw new ValidationException(buildMessage(formatter, field, constraintValue));
            }

            if (constraintValue != null && constraintValue > 0)
            {
                try {
                    byte[] decoded = HexUtils.hexDecode(value);
                    
                    if (decoded == null || decoded.length != constraintValue) {
                        throw new ValidationException(buildMessage(formatter, field, constraintValue));
                    }
                }
                catch (DecoderException e) {
                    throw new ValidationException(buildMessage(formatter, field, constraintValue));
                }
            }
        }
    }

    private String buildMessage(MessageFormatter formatter, Field field, Integer constraintValue)
    {
        return formatter.format(field.getLabel(), constraintValue);
    }

    @Override
    public void render(Field field, Integer constraintValue, MessageFormatter formatter, MarkupWriter writer,
                       FormSupport formSupport)
    {
        /*
         * Empty on purpose because we do not have a Javascript validator
         */
    }
}
