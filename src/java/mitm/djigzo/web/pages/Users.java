/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.pages;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import mitm.application.djigzo.UserLocality;
import mitm.application.djigzo.admin.FactoryRoles;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.ws.WebServiceCheckedException;
import mitm.djigzo.web.beans.UserBean;
import mitm.djigzo.web.entities.UserManager;
import mitm.djigzo.web.grid.UserGridDataSource;

import org.apache.commons.lang.StringUtils;
import org.apache.tapestry5.Asset;
import org.apache.tapestry5.ComponentResources;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Path;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.beaneditor.BeanModel;
import org.apache.tapestry5.corelib.components.Grid;
import org.apache.tapestry5.grid.ColumnSort;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.ioc.annotations.Value;
import org.apache.tapestry5.json.JSONObject;
import org.apache.tapestry5.services.BeanModelSource;
import org.apache.tapestry5.services.Request;
import org.springframework.security.annotation.Secured;

@IncludeStylesheet("context:styles/pages/users.css")
public class Users 
{
	private final static String EMAIL_COLUMN = "email";
	
	@Inject
	private UserManager userManager;
	
	/*
	 * The user currently being drawn by the grid (is set by the grid component)
	 */
	private UserBean user;

	/*
	 * True if the user is an internal user. This value will be reset when a 
	 * new user is set.
	 */
	private Boolean internalUser;
	
	@Persist
	private String filter;
	
	/*
	 * Set of the selected users (email addresses)
	 */
	@Persist
	private Set<String> selected;
	
    @Inject
    @Value("${users.rowsPerPage}")
    private int defaultRowsPerPage;	
	
    @Persist
    private Integer rowsPerPage;    
    
	@Component(parameters = {"source = gridDataSource", "model = model", "row = user", 
			"reorder = select,delete,editCertificate,email", "volatile = true", "rowsPerPage = prop:rowsPerPage"})
    private Grid grid;

	@Inject
    private BeanModelSource beanModelSource;

    @Inject
    private ComponentResources resources;

    @Inject
    private Request request;
    
    @SuppressWarnings("unused")
	@Inject
    @Path("context:icons/cross.png")
    @Property
    private Asset deleteAsset;

    @SuppressWarnings("unused")
    @Inject
    @Path("context:icons/user-key.png")
    @Property
    private Asset editCertificateAsset;
	
    @SuppressWarnings("unused")
	@Inject
    @Path("context:icons/internal.png")
    @Property
    private Asset internalUserAsset;
	
    public String getFilter() {
    	return filter;
    }
    
    public void setFilter(String filter) {
    	this.filter = filter;
    }
    
	public boolean isFilterDisabled() {
        return StringUtils.isBlank(filter);
	}
    
    public BeanModel<UserBean> getModel() 
    {
        BeanModel<UserBean> model = beanModelSource.createDisplayModel(UserBean.class, resources.getMessages());

        model.exclude("userLocality");
        model.add("delete", null);
        model.add("editCertificate", null);
        model.add("select", null);
        
        return model;
    }
    
    @SetupRender
    public void setupRender() 
    {
        if (selected == null) {
            selected = Collections.synchronizedSet(new HashSet<String>());
        }
        else {
            selected.clear();
        }

    	/*
    	 * set initial sorting if sorting is not yet set
    	 */
    	if (grid.getSortModel().getColumnSort(EMAIL_COLUMN) == ColumnSort.UNSORTED) 
    	{
    		grid.getSortModel().updateSort(EMAIL_COLUMN);
    	}
    }
	
	public boolean getSelect() {
		return selected.contains(user.getEmail());
	}
		
	public void setSelect(boolean value) 
	{
		/*
		 * Ignored. Checkbox values will be handled using EventCheckbox (see onEventActionLink)
		 */
	}
    
	public boolean isInternalUser() 
	throws WebServiceCheckedException, HierarchicalPropertiesException
	{
	    if (internalUser == null) {
	        internalUser = user.getUserLocality() == UserLocality.INTERNAL;
	    }
	    
	    return internalUser;
	}
	
	@Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_USER_MANAGER})
    @OnEvent(component = "deleteUser")
    protected void deleteUser(String email) 
    throws WebServiceCheckedException 
    {
    	userManager.deleteUser(email);
    }

	public UserGridDataSource getGridDataSource() {
		return new UserGridDataSource(userManager, filter);
	}
	
    /*
     * Event handler called when the checkbox is clicked. 
     */
	@Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_USER_MANAGER})
    public void onCheckboxEvent()
    {
    	String data = request.getParameter("data");

    	if (StringUtils.isEmpty(data)) {
    		throw new IllegalArgumentException("data is missing.");
    	}
    	
    	JSONObject json = new JSONObject(data);
    	    	    	
    	String element = json.getString("element");
    	boolean checked = json.getBoolean("checked"); 
    	
    	if (checked) {
    		selected.add(element);
    	}
    	else {
    		selected.remove(element);
    	}
    }
    
	@Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_USER_MANAGER})
    @OnEvent(component = "deleteSelected")
    protected void deleteSelected() 
    throws WebServiceCheckedException 
    {
        /*
         * Note: we need to clone the set to make sure that concurrent modifications
         * are possible.
         */
        String[] emails = selected.toArray(new String[]{});

        for (String email : emails) {
	        userManager.deleteUser(email);
	    }
    }

    public int getRowsPerPage()
    {
        return rowsPerPage != null ? rowsPerPage : defaultRowsPerPage;
    }
    
    public void setRowsPerPage(int rowsPerPage) {
        this.rowsPerPage = rowsPerPage;
    }

    public UserBean getUser() {
        return user;
    }

    public void setUser(UserBean user)
    {
        this.user = user;
        
        internalUser = null;
    }

    public Boolean getInternalUser() {
        return internalUser;
    }

    public void setInternalUser(Boolean internalUser) {
        this.internalUser = internalUser;
    }
}
