/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.grid;

import java.util.List;

import mitm.common.util.Check;
import mitm.djigzo.web.beans.X509CertificateBean;

import org.apache.tapestry5.grid.SortConstraint;

/**
 * Combination of a dynamic Certificate source and a static source. The static list is shown before
 * the dynamic list.
 * 
 * @author Martijn Brinkers
 *
 */
public class DelegatedStaticCertificateGridDataSource extends AbstractCertificateGridDataSource
{
	/*
	 * Source to delegate to
	 */
	private final AbstractCertificateGridDataSource delegate;
	
	/*
	 * Static list of X509CertificateBean's
	 */
	private final List<X509CertificateBean> staticCertificateBeans;
	
	public DelegatedStaticCertificateGridDataSource(List<X509CertificateBean> staticCertificateBeans,
			AbstractCertificateGridDataSource delegate)
	{
		Check.notNull(staticCertificateBeans, "staticCertificateBeans");
		Check.notNull(delegate, "delegate");
		
		this.staticCertificateBeans = staticCertificateBeans;
		this.delegate = delegate;
	}
	
    @Override
	public int getAvailableRows()
	{
		return delegate.getAvailableRows() + staticCertificateBeans.size();
	}

    @Override
	public X509CertificateBean getRowValue(int index) 
	{
		X509CertificateBean value = null;
		
		if (index < staticCertificateBeans.size()) 
		{
			/*
			 * we need to return items from the static list 
			 */
			value = staticCertificateBeans.get(index);
		}
		else {
			/*
			 * Because we have also a static list we need to lower the index by the number of
			 * items in the static list
			 */
			index = index - staticCertificateBeans.size();
			
			value = delegate.getRowValue(index);
		}
		
		return value;
	}

    @Override
	public void prepare(int startIndex, int endIndex, List<SortConstraint> sortConstraints) 
	{
		/*
		 * We need to correct the indexes because of the extra items from the static list
		 */
		startIndex = startIndex - staticCertificateBeans.size();
		endIndex = endIndex - staticCertificateBeans.size();
		
		if (endIndex < 0) 
		{
			/*
			 * The static list is long enough so we do not need to prepare something.
			 */
			return;
		}

		if (startIndex < 0) {
			startIndex = 0;
		}

		delegate.prepare(startIndex, endIndex, sortConstraints);
	}
}
