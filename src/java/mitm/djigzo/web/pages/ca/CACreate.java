/*
 * Copyright (c) 2009-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.pages.ca;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import mitm.application.djigzo.admin.FactoryRoles;
import mitm.application.djigzo.ws.CABuilderParametersDTO;
import mitm.application.djigzo.ws.CAWS;
import mitm.application.djigzo.ws.X500PrincipalDTO;
import mitm.common.ws.WebServiceCheckedException;
import mitm.djigzo.web.common.SignatureAlgorithm;
import mitm.djigzo.web.components.X500PrincipalEdit;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.tapestry5.PersistenceConstants;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.corelib.components.Form;
import org.apache.tapestry5.corelib.components.Select;
import org.apache.tapestry5.corelib.components.TextField;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.annotation.Secured;

@Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_PKI_MANAGER})
@IncludeStylesheet("context:styles/pages/ca/caCreate.css")
public class CACreate
{
    private final static Logger logger = LoggerFactory.getLogger(CACreate.class);
	
    @Inject
    private CAWS caWS;
    
    /*
     * The validity of the root certificate (in days)
     */
    private int rootValidity;

    /*
     * The validity of the intermediate certificate (in days)
     */
    private int intermediateValidity;

    /*
     * The key length of the root (in bits)
     */
    private int rootKeyLength;

    /*
     * The key length of the intermediate (in bits)
     */
    private int intermediateKeyLength;

    /*
     * If true the newly created CA will be the default CA
     */
    private boolean makeDefault;
    
    /*
     * Algorithm for signing the certificate
     */
    private SignatureAlgorithm signatureAlgorithm;
    
    /*
     * The CRL distribution point for the intermediate certificate
     */
    private String crlDistributionPoint;
    
    /*
     * True if and error has occurred
     */
    @Persist(PersistenceConstants.FLASH)
    private boolean error;

	/*
     * Error message if an error has occurred
     */
    @Persist(PersistenceConstants.FLASH)
    private String errorMessage;
    
    @Component
    private Form form;
    
    @Component(id = "rootPrincipal")
    private X500PrincipalEdit rootPrincipal;

    @Component(id = "intermediatePrincipal")
    private X500PrincipalEdit intermediatePrincipal;
    
    @SuppressWarnings("unused")
    @Component(id = "rootValidity", parameters = {"value = rootValidity", "validate = required, min=1, max=14600"})
    private TextField rootValidityField;

    @SuppressWarnings("unused")
    @Component(id = "intermediateValidity", parameters = {"value = intermediateValidity", "validate = required, min=1, max=14600"})
    private TextField intermediateValidityField;

    @SuppressWarnings("unused")
    @Component(id = "rootKeyLength", parameters = {"value = rootKeyLength"})
    private Select rootKeyLengthField;

    @SuppressWarnings("unused")
    @Component(id = "intermediateKeyLength", parameters = {"value = intermediateKeyLength"})
    private Select intermediateKeyLengthField;

    @SuppressWarnings("unused")
    @Component(id = "signatureAlgorithm", parameters = {"value = signatureAlgorithm", "blankOption = NEVER", "validate = required"})
    private Select signatureAlgorithmField;
    
    @Component(id = "crlDistributionPoint", parameters = {"value = crlDistributionPoint", "validate = maxLength=1024"})
    private TextField crlDistributionPointField;
    
    /*
     * True if changes were applied
     */
    @Persist(PersistenceConstants.FLASH)
    private boolean applied;
    
    @Persist
    private boolean advanced;
    
    @SetupRender
    @Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_PKI_MANAGER})
    public void setupRender()
    {
        rootValidity = 1825;
        intermediateValidity = 1825;
        rootKeyLength = 2048;
        intermediateKeyLength = 2048;
        signatureAlgorithm = SignatureAlgorithm.SHA1_WITH_RSA;
        makeDefault = true;
    }
    
    private X500PrincipalDTO createX500Principal(X500PrincipalEdit principalEdit)
    {
        X500PrincipalDTO principal = new X500PrincipalDTO();
        
        if (StringUtils.isNotEmpty(principalEdit.getEmail())) {
            principal.setEmail(Arrays.asList(principalEdit.getEmail()));
        }
        
        if (StringUtils.isNotEmpty(principalEdit.getOrganisation())) {
            principal.setOrganisation(Arrays.asList(principalEdit.getOrganisation()));
        }
        
        if (StringUtils.isNotEmpty(principalEdit.getCommonName())) {
            principal.setCommonName(Arrays.asList(principalEdit.getCommonName()));
        }
        
        if (StringUtils.isNotEmpty(principalEdit.getFirstName())) {
            principal.setGivenName(Arrays.asList(principalEdit.getFirstName()));
        }
        
        if (StringUtils.isNotEmpty(principalEdit.getLastName())) {
            principal.setSurname(Arrays.asList(principalEdit.getLastName()));
        }
        
        return principal;
    }
    
    private void buildCA()
    throws WebServiceCheckedException
    {
        X500PrincipalDTO rootSubject = createX500Principal(rootPrincipal);
        X500PrincipalDTO intermediateSubject = createX500Principal(intermediatePrincipal);
        
        List<String> crlDistributionPoints = null;
        
        if (StringUtils.isNotBlank(crlDistributionPoint)) {
            crlDistributionPoints = Collections.singletonList(crlDistributionPoint.trim());
        }
        
        CABuilderParametersDTO parameters = new CABuilderParametersDTO(rootSubject, intermediateSubject, rootValidity,
                intermediateValidity, rootKeyLength, intermediateKeyLength, signatureAlgorithm.getName(), 
                crlDistributionPoints);
        
        String thumbprint = caWS.buildCA(parameters);
        
        if (makeDefault) {
            caWS.setActiveCA(thumbprint);
        }
    }
    
    public void onValidateForm() 
    {
        /*
         * Root and intermediate subject should not be the same
         */
        if (rootPrincipal != null && StringUtils.isNotBlank(rootPrincipal.getCommonName()) 
                && rootPrincipal.equals(intermediatePrincipal))
        {
            form.recordError("The common name of the root and intermediate should not be equal.");
            
            return;
        }
        
        if (StringUtils.isNotBlank(crlDistributionPoint))
        {
            /*
             * Check if the crlDistributionPoint is a valid URI
             */
            try {
                new URL(crlDistributionPoint);
            }
            catch (MalformedURLException e)
            {
                form.recordError(crlDistributionPointField, "CRL distribution point is not a valid URL.");
                
                return;
            }
        }
    }
    
    public void onSuccess()
    throws WebServiceCheckedException
    {
    	try {
	        buildCA();
	        
	        applied = true;
    	}
    	catch (Exception e)
    	{
            logger.error("Error creating CA", e);
            
            error = true;
            errorMessage = ExceptionUtils.getRootCauseMessage(e);
    	}
    }
    
    public boolean isApplied() {
        return applied;
    }

    public int getRootValidity() {
        return rootValidity;
    }

    public void setRootValidity(int rootValidity) {
        this.rootValidity = rootValidity;
    }

    public int getIntermediateValidity() {
        return intermediateValidity;
    }

    public void setIntermediateValidity(int intermediateValidity) {
        this.intermediateValidity = intermediateValidity;
    }

    public int getRootKeyLength() {
        return rootKeyLength;
    }

    public void setRootKeyLength(int rootKeyLength) {
        this.rootKeyLength = rootKeyLength;
    }

    public int getIntermediateKeyLength() {
        return intermediateKeyLength;
    }

    public void setIntermediateKeyLength(int intermediateKeyLength) {
        this.intermediateKeyLength = intermediateKeyLength;
    }

    public boolean isMakeDefault() {
        return makeDefault;
    }

    public void setMakeDefault(boolean makeDefault) {
        this.makeDefault = makeDefault;
    }
    
    /*
     * Event handler called when the cancel button is pressed.
     */
    protected Object onCancel() {
        return CAIssue.class;
    }

    public boolean isAdvanced() {
        return advanced;
    }

    public void setAdvanced(boolean advanced) {
        this.advanced = advanced;
    }

    public String getCrlDistributionPoint() {
        return crlDistributionPoint;
    }

    public void setCrlDistributionPoint(String crlDistributionPoint) {
        this.crlDistributionPoint = crlDistributionPoint;
    }

    public SignatureAlgorithm getSignatureAlgorithm() {
        return signatureAlgorithm;
    }

    public void setSignatureAlgorithm(SignatureAlgorithm signatureAlgorithm) {
        this.signatureAlgorithm = signatureAlgorithm;
    }
    
    public boolean isError() {
		return error;
	}

	public String getErrorMessage() {
		return errorMessage;
	}
}
