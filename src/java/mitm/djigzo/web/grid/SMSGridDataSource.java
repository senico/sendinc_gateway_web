/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.grid;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import mitm.application.djigzo.DjigzoRuntimeException;
import mitm.application.djigzo.ws.SMSDTO;
import mitm.application.djigzo.ws.SMSGatewayWS;
import mitm.common.hibernate.SortDirection;
import mitm.common.sms.SortColumn;
import mitm.common.util.Check;
import mitm.common.ws.WebServiceCheckedException;
import mitm.djigzo.web.beans.SMSBean;
import mitm.djigzo.web.beans.impl.SMSBeanImpl;

import org.apache.tapestry5.beaneditor.PropertyModel;
import org.apache.tapestry5.grid.ColumnSort;
import org.apache.tapestry5.grid.GridDataSource;
import org.apache.tapestry5.grid.SortConstraint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SMSGridDataSource implements GridDataSource 
{
	private final static Logger logger = LoggerFactory.getLogger(SMSGridDataSource.class);
	
	private List<SMSBean> smsBeans;
	
	/*
	 * we need to keep track of base index because the index in getRowValue is the absolute
	 * index and not relative.
	 */
	private int startIndex;
	
	private final SMSGatewayWS smsGatewayWS;
	
	private final Map<String, SortColumn> propertySortColumnMap;
	
	public SMSGridDataSource(SMSGatewayWS smsGatewayWS, Map<String, SortColumn> propertySortColumnMap)
	{
		Check.notNull(smsGatewayWS, "smsGatewayWS");
		Check.notNull(propertySortColumnMap, "propertySortColumnMap");
		
		this.smsGatewayWS = smsGatewayWS;
		this.propertySortColumnMap = propertySortColumnMap;
	}
	
    @Override
	public int getAvailableRows() 
	{
		try {
			return smsGatewayWS.getCount();
		} 
		catch (WebServiceCheckedException e) {
			throw new DjigzoRuntimeException(e);
		}
	}

    @Override
	public Class<SMSBean> getRowType()
	{
		return SMSBean.class;
	}

    @Override
	public SMSBean getRowValue(int index) 
	{
		int rowIndex = index - startIndex;
		
		SMSBean value = null;
		
		if (smsBeans != null && rowIndex < smsBeans.size()) {
			value = smsBeans.get(rowIndex);
		}
		else {
			logger.warn("Not enough rows.");
			/* 
			 * We do not have enough Bean objects. This can happen when AvailableRows
			 * returned more rows than were actually available when prepared was called. 
			 */ 
			value = null;
		}
		
		return value;
	}

    @Override
	public void prepare(int startIndex, int endIndex, List<SortConstraint> sortConstraints) 
	{
		try {
			SortColumn sortColumn = null;
			SortDirection sortDirection = null;
			
			if (sortConstraints.size() > 0)
			{
				/*
				 * We only support sorting on one column
				 */
				SortConstraint sortConstraint = sortConstraints.get(0);
				
				sortDirection = sortConstraint.getColumnSort() == ColumnSort.DESCENDING ? 
							SortDirection.DESC : SortDirection.ASC;
							
				PropertyModel propertyModel = sortConstraint.getPropertyModel();
				
				sortColumn = propertySortColumnMap.get(propertyModel.getPropertyName().toLowerCase());
			}
			
			List<SMSDTO> smsDTOs = smsGatewayWS.getAll(startIndex, endIndex - startIndex + 1, 
					sortColumn, sortDirection);

			if (smsDTOs == null) {
			    /*
			     * Can happen because of a race condition where getAvailableRows said that
			     * there are rows available but the items were already removed when prepare
			     * was called.
			     */
			    smsDTOs = Collections.emptyList();
			}
			
			smsBeans = new ArrayList<SMSBean>(smsDTOs.size());
			
			for (SMSDTO dto : smsDTOs) {
				smsBeans.add(new SMSBeanImpl(dto));
			}
		} 
		catch (WebServiceCheckedException e) {
			throw new DjigzoRuntimeException(e);
		} 
		
		this.startIndex = startIndex;
	}
}
