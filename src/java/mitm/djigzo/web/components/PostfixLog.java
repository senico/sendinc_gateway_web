/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.components;

import java.io.IOException;
import java.util.regex.Pattern;

import mitm.application.djigzo.ws.PostfixLogManagerWS;
import mitm.djigzo.web.common.PostfixLogType;
import mitm.djigzo.web.grid.PostfixLogGridDataSource;
import mitm.djigzo.web.mixins.AjaxEvent;
import mitm.djigzo.web.render.FragmentMarkupRendererRegistry;
import mitm.djigzo.web.render.SystemRendererTags;
import mitm.djigzo.web.render.impl.FragmentMarkupRendererRegistryImpl;

import org.apache.tapestry5.ComponentResources;
import org.apache.tapestry5.annotations.BeginRender;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.IncludeJavaScriptLibrary;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.beaneditor.BeanModel;
import org.apache.tapestry5.corelib.components.Grid;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.ioc.annotations.Value;
import org.apache.tapestry5.json.JSONObject;
import org.apache.tapestry5.services.BeanModelSource;
import org.apache.tapestry5.services.Request;

@IncludeStylesheet("context:styles/components/postfixLog.css")
@IncludeJavaScriptLibrary("postfixLog.js")
public class PostfixLog 
{
	/*
	 * The current logLine being shown
	 */
	private String logLine;
	
	@Inject
	private PostfixLogManagerWS logManager;
	
	@Inject
    private BeanModelSource beanModelSource;

    @Inject
    private ComponentResources resources;
	
    @Inject
    private Request request;
    
    @Inject
    @Value("${postfixLog.rowsPerPage}")
    private int defaultRowsPerPage; 
    
    @Persist
    private Integer rowsPerPage;    
    
    /*
     * Whether the logs should be GROUPED, RAW etc. 
     */
    @Persist
    @Property
    private PostfixLogType logType;
    
	@SuppressWarnings("unused")
	@Component(id = "postfixLogGrid", parameters = {"source=gridDataSource", "row=logLine", "volatile=true", 
			"model=model", "rowsPerPage=prop:rowsPerPage"})
    private Grid grid;
	
	@Component(id = "postfixLogFilter")
	private RegExprFilter filter;
	
	@SuppressWarnings("unused")
	@Component(id = "textFragmentMarkup", parameters = {"fragmentRegistry=fragmentRegistry"})
	private TextFragmentMarkup textFragmentMarkup;
	
	private FragmentMarkupRendererRegistry fragmentRegistry;
	
	@SetupRender
	protected void setupRender()
	{
	    if (logType == null) {
	        logType = PostfixLogType.RAW;
	    }
	}
	
	@BeginRender
	public void beginRender()
	{
		fragmentRegistry = new FragmentMarkupRendererRegistryImpl();
		
		fragmentRegistry.getMap().put(SystemRendererTags.FIILTER, getSearchPattern(true));
	}
	
	private Pattern getSearchPattern(boolean escapeHTML) 
	{
		return filter.getPattern(escapeHTML);
	}
	
	public PostfixLogGridDataSource getGridDataSource()	{
		return new PostfixLogGridDataSource(logManager, logType, getSearchPattern(false));
	}

    public BeanModel<Void> getModel() 
    {
        BeanModel<Void> model = beanModelSource.createDisplayModel(Void.class, resources.getMessages());
        
        model.add("row", null);
        
        return model;
    }
	
    @OnEvent(component = "logType", value="AjaxEvent")
    public void onLogTypeSelected()
    throws IOException
    {
        String data = request.getParameter(AjaxEvent.REQUEST_PARAM_NAME);

        JSONObject json = new JSONObject(data);
        
        logType = PostfixLogType.valueOf(json.getString("value"));
    }    
    
	public String getLogLine() 
	{
		return logLine;
	}

	public void setLogLine(String logLine) {
		this.logLine = logLine;
	}

	public FragmentMarkupRendererRegistry getFragmentRegistry() {
		return fragmentRegistry;
	}

    public int getRowsPerPage()
    {
        return rowsPerPage != null ? rowsPerPage : defaultRowsPerPage;
    }
    
    public void setRowsPerPage(int rowsPerPage) {
        this.rowsPerPage = rowsPerPage;
    }
}
