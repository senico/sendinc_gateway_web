/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.pages;

import java.util.IllegalFormatException;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import mitm.application.djigzo.admin.FactoryRoles;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.util.RegExprUtils;
import mitm.common.ws.WebServiceCheckedException;
import mitm.djigzo.web.beans.UserPreferencesBean;
import mitm.djigzo.web.beans.impl.UserPreferencesBeanImp;
import mitm.djigzo.web.components.propertyeditor.TextPropertyEdit;
import mitm.djigzo.web.components.propertyeditor.UserPropertiesEdit;
import mitm.djigzo.web.entities.GlobalPreferencesManager;

import org.apache.commons.lang.StringUtils;
import org.apache.tapestry5.PersistenceConstants;
import org.apache.tapestry5.annotations.Cached;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.corelib.components.Form;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.springframework.security.annotation.Secured;

@IncludeStylesheet("context:styles/pages/globalPreferences.css")
@Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_GLOBAL_MANAGER})
public class GlobalPreferences
{
	private UserPreferencesBean globalPreferences;

	@Inject
	private GlobalPreferencesManager globalPreferencesManager;
	
    /*
     * True if changes were applied
     */
    @Persist(PersistenceConstants.FLASH)
    private boolean applied;
	
	@Component(parameters = { "userProperties = globalPreferences.properties" })
	private UserPropertiesEdit globalPropertiesEdit;

    @SuppressWarnings("unused")
    @Component(id = "securityInfoDecryptedTag", parameters = {
            "value = globalPreferences.properties.securityInfoDecryptedTag.value", 
            "checked = globalPreferences.properties.securityInfoDecryptedTag.inherit"}
    )
    private TextPropertyEdit securityInfoDecryptedTag;

    @SuppressWarnings("unused")
    @Component(id = "securityInfoSignedValidTag", parameters = {
            "value = globalPreferences.properties.securityInfoSignedValidTag.value", 
            "checked = globalPreferences.properties.securityInfoSignedValidTag.inherit"}
    )
    private TextPropertyEdit securityInfoSignedValidTag;

    @Component(id = "securityInfoSignedByValidTag", parameters = {
            "value = globalPreferences.properties.securityInfoSignedByValidTag.value", 
            "checked = globalPreferences.properties.securityInfoSignedByValidTag.inherit"}
    )
    private TextPropertyEdit securityInfoSignedByValidTag;

    @SuppressWarnings("unused")
    @Component(id = "securityInfoSignedInvalidTag", parameters = {
            "value = globalPreferences.properties.securityInfoSignedInvalidTag.value", 
            "checked = globalPreferences.properties.securityInfoSignedInvalidTag.inherit"}
    )
    private TextPropertyEdit securityInfoSignedInvalidTag;

    @Component(id = "subjectFilter", parameters = {
            "value = globalPreferences.properties.subjectFilterRegEx.value", 
            "checked = globalPreferences.properties.subjectFilterRegEx.inherit"}
    )
    private TextPropertyEdit subjectFilter;
    
    @Component(id = "form")
    private Form form;
    
    public UserPropertiesEdit getGlobalPropertiesEdit() {
        return globalPropertiesEdit;
    }
    
    public boolean isApplied() {
        return applied;
    }
	
	@SetupRender
	@Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_GLOBAL_MANAGER})
	protected void setupRender() {
		/*
		 * Empty on purpose
		 */
	}

	@Cached
	public UserPreferencesBean getGlobalPreferences() 
	throws WebServiceCheckedException 
	{
		if (globalPreferences == null) {
			globalPreferences = new UserPreferencesBeanImp(globalPreferencesManager.getPreferences());
		}
		
		return globalPreferences;
	}

	protected void onValidateForm()
	throws WebServiceCheckedException, HierarchicalPropertiesException
	{
	    /*
	     * The server secret is mandatory
	     */
	    if (StringUtils.isBlank(getGlobalPreferences().getProperties().getServerSecret().getUserValue()))
	    {
	        form.recordError(globalPropertiesEdit.getServerSecret().getField(), 
	                "Server secret must be set");
	    }
	    
	    /*
	     * Test whether securityInfoSignedByValidTag is a valid format string
	     */
	    try {
            String.format(StringUtils.defaultString(getGlobalPreferences().getProperties().
                    getSecurityInfoSignedByValidTag().getUserValue()), "test@example.com");
	    }
	    catch (IllegalFormatException e)
	    {
            form.recordError(securityInfoSignedByValidTag.getField(), "Signed by tag does not contain " +
            		"a valid format string.");
	    }
	    
        /*
         * If set, test whether subject filter is a valid filter. The format should be /REG-EX/ replace
         */
	    if (StringUtils.isNotEmpty(getGlobalPreferences().getProperties().getSubjectFilterRegEx().getUserValue()))
	    {
	        String[] filter = RegExprUtils.splitRegExp(getGlobalPreferences().getProperties().
	                getSubjectFilterRegEx().getUserValue());
	        
	        if (filter != null)
	        {
	            /*
	             * Should be a valid regular expression
	             */
	            try {
	                Pattern.compile(filter[0]);
	            }
	            catch (PatternSyntaxException e)
	            {
	                form.recordError(subjectFilter.getField(), "Subject filter regular expression is not a valid " +
	                		"regular expression");
	            }
	        }
	        else {
	            form.recordError(subjectFilter.getField(), "Subject filter is not a valid filter");
	        }
	    }
	}
	
	public void onSuccess()
	throws HierarchicalPropertiesException, WebServiceCheckedException 
	{
		getGlobalPreferences().save();
		
		applied = true;
	}
	
    /*
     * Event handler called when the cancel button is pressed.
     */
    protected Object onCancel() {
        return Users.class;
    }	
}
