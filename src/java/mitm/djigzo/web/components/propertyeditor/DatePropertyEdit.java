/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.components.propertyeditor;

import java.util.Date;

import org.apache.tapestry.commons.components.DateTimeField;
import org.apache.tapestry5.BindingConstants;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.annotations.SupportsInformalParameters;

/**
 * @author Martijn Brinkers
 */
@SupportsInformalParameters
public class DatePropertyEdit
{	
	@Parameter(required = true)
	private Date value;
	
	@Parameter(defaultPrefix = BindingConstants.LITERAL)
    private String validate;
    
	@Parameter(required = true, defaultPrefix = BindingConstants.LITERAL)
	private String property;

	@Parameter(defaultPrefix = BindingConstants.LITERAL)
	private String prefix;
	
	@Parameter(required = true)
	private boolean checked;
	
	@Component(parameters = {"field=prop:field", "validate=inherit:validate", "prefix=inherit:prefix", 
			"checked=inherit:checked", "idsToDisable=prop:idsToDisable"})
	private PropertyEdit propertyEdit;

	/*
	 * We want to validate the property so the fieldValidator parameter will provide the requirements (this is a bit
	 * clumsy but I do not yet know how to get the @Validate annotation of the parent for this component). The label
	 * parameter is need to give a proper name when a validation error occurs (otherwise the name will be Field).
	 * 
	 * Note: the onchange does not work when the icon is clicked. Also disabling the control does
	 * not disable the icon 
	 * 
	 * Todo: file a request for enhancement.
	 */
	@Component(id = "field", parameters = {"validate=prop:propertyEdit.fieldValidator", 
			"label=prop:property", "timePicker=true", "timePickerAdjacent=true", "datePattern=MM/dd/yyyy HH:mm"})
	private DateTimeField field;
	
	public String getIdsToDisable()
	{
		/*
		 * TODO: add icon id when the DateTimeField control is fixed 
		 * (see http://code.google.com/p/tapestry5-components/issues/detail?id=92)
		 */
		return field.getClientId();
	}
	
	public DateTimeField getField() {
		return field;
	}
	
	public void setValue(Date value) {
		this.value = value;
	}
	
	public Date getValue() {
		return value;
	}
	
	public PropertyEdit getPropertyEdit() {
		return propertyEdit;
	}

	public String getValidate() {
		return validate;
	}

	public void setValidate(String validate) {
		this.validate = validate;
	}

	public String getProperty() {
		return property;
	}

	public void setProperty(String property) {
		this.property = property;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}
}