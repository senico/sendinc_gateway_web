var Disable = Class.create();

Disable.prototype = {
        initialize: function(element, enabled) 
        {
        	$(element).disabled = !enabled;
        }
}