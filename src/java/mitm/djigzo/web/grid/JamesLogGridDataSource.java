/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.grid;

import java.util.List;
import java.util.regex.Pattern;

import mitm.application.djigzo.DjigzoRuntimeException;
import mitm.application.djigzo.ws.JamesManagerWS;
import mitm.common.util.Check;
import mitm.common.ws.WebServiceCheckedException;

import org.apache.tapestry5.grid.GridDataSource;
import org.apache.tapestry5.grid.SortConstraint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JamesLogGridDataSource implements GridDataSource 
{
	private final static Logger logger = LoggerFactory.getLogger(JamesLogGridDataSource.class);
	
	private List<String> logLines;
	
	/*
	 * we need to keep track of base index because the index in getRowValue is the absolute
	 * index and not relative.
	 */
	private int startIndex;
	
	/*
	 * The web service client for managing James
	 */
	private final JamesManagerWS jameManager;
	
	/*
	 * The regular expression pattern used for filtering log lines
	 */
	private final Pattern searchPattern;
	
	/**
	 * The searchPattern may be null (meaning return all)
	 */
	public JamesLogGridDataSource(JamesManagerWS jameManager, Pattern searchPattern)
	{
		Check.notNull(jameManager, "jameManager");
		
		this.jameManager = jameManager;
		this.searchPattern = searchPattern;
	}
	
    @Override
	public int getAvailableRows() 
	{
		try {
			return jameManager.getLogLineCount(searchPattern != null ? searchPattern.pattern() : null);
		} 
		catch (WebServiceCheckedException e) {
			throw new DjigzoRuntimeException(e);
		}
	}

    @Override
	public Class<String> getRowType()
	{
		return String.class;
	}

    @Override
	public String getRowValue(int index) 
	{
		int rowIndex = index - startIndex;
		
		String value = null;
		
		if (logLines != null && rowIndex < logLines.size()) {
			value = logLines.get(rowIndex);
		}
		else {
			logger.warn("Not enough rows.");
			/* 
			 * We do not have enough Bean objects. This can happen when AvailableRows
			 * returned more rows than were actually available when prepared was called. 
			 */ 
			value = null;
		}
		
		return value;
	}

    @Override
	public void prepare(int startIndex, int endIndex, List<SortConstraint> sortConstraints) 
	{
		try {
			logLines = jameManager.getLogLines(startIndex, endIndex - startIndex + 1, 
					searchPattern != null ? searchPattern.pattern() : null);
		} 
		catch (WebServiceCheckedException e) {
			throw new DjigzoRuntimeException(e);
		} 
		
		this.startIndex = startIndex;
	}
}
