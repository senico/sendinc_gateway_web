/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.common;

import java.io.IOException;
import java.lang.reflect.ParameterizedType;

import mitm.common.util.Check;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.tapestry5.Link;
import org.apache.tapestry5.internal.services.LinkFactory;
import org.apache.tapestry5.services.RequestExceptionHandler;
import org.apache.tapestry5.services.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Martijn Brinkers
 *
 * Class must be abstract because we need to extend it (to support exceptionClass)
 */
public abstract class RedirectRequestExceptionHandler<T extends Throwable> 
		implements RequestExceptionHandler
{
	private final static Logger logger = LoggerFactory.getLogger(RedirectRequestExceptionHandler.class);
	
	private final RequestExceptionHandler delegate;
	private final Response response;
	private final LinkFactory linkFactory;
	
	/*
	 * The page to redirect to
	 */
	private final String redirectToPage;

	/*
	 * The context for the page redirect
	 */
	private final Object[] context;
	
    private final Class<T> exceptionClass;
    
    @SuppressWarnings("unchecked")
    public RedirectRequestExceptionHandler(RequestExceptionHandler delegate, Response response, 
    		LinkFactory linkFactory, String redirectToPage, Object... context)
    {
    	Check.notNull(delegate, "delegate");
    	Check.notNull(response, "response");
    	Check.notNull(linkFactory, "linkFactory");
    	Check.notNull(redirectToPage, "redirectToPage");
    	    	
    	this.delegate = delegate;
    	this.response = response;
    	this.linkFactory = linkFactory;
    	this.redirectToPage = redirectToPage;
    	this.context = context;
    	
        this.exceptionClass = (Class<T>) ((ParameterizedType) getClass().
                getGenericSuperclass()).getActualTypeArguments()[0];
    }
	
    /**
     * Subclasses can use this to filter for specific messages etc.
     */
    protected boolean isMatch(T exception) {
    	return true;
    }
    
    @SuppressWarnings("unchecked")
    @Override
	public void handleRequestException(Throwable t) 
    throws IOException
    {
    	boolean handled = false;
    	
    	/*
    	 * Check for specified exception
    	 */
    	int exceptionIndex = ExceptionUtils.indexOfType(t, exceptionClass);
    	
    	if (exceptionIndex != -1)
    	{
    		/*
    		 * Check if the exception is caused by authentication failure.
    		 */
    		T exception = (T) ExceptionUtils.getThrowables(t)[exceptionIndex];
    		
    		if (isMatch(exception)) 
    		{
    			logger.warn("Exception " + exceptionClass + " handled. Redirecting to page: " + 
    					redirectToPage);

    			Link pageLink = linkFactory.createPageRenderLink(redirectToPage, false, context);
    			
                response.sendRedirect(pageLink.toRedirectURI());
                
    			handled = true;
    		}
    	}
    	
    	if (!handled) 
    	{
    		/*
    		 * Pass the exception to the next handler
    		 */
    		((RequestExceptionHandler)delegate).handleRequestException(t);
    	}
    }
}
