Tapestry.activateZone = function ( zoneId, url ) {
        var zoneManager = Tapestry.findZoneManagerByZoneId( zoneId );
        if ( zoneManager != null ) {
                zoneManager.updateFromURL( url );
        }
};

Tapestry.findZoneManagerByZoneId = function( zoneId ) {
        var zoneElement = $(zoneId);
        if (!zoneElement) {
                Tapestry.ajaxError("Unable to locate Ajax Zone '#{id}' for dynamic update.", { id:zoneId});
                return null;
        }
        var manager = $T(zoneElement).zoneManager;
        if (!manager) {
                Tapestry.ajaxError("Ajax Zone '#{id}' does not have an associated Tapestry.ZoneManager object.", { id :zoneId });
                return null;
        }
        return manager;
};
