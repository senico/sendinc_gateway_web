/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.pages;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import mitm.application.djigzo.admin.FactoryRoles;
import mitm.application.djigzo.ws.SMSGatewayWS;
import mitm.common.sms.SortColumn;
import mitm.common.ws.WebServiceCheckedException;
import mitm.djigzo.web.beans.SMSBean;
import mitm.djigzo.web.grid.SMSGridDataSource;

import org.apache.commons.lang.StringUtils;
import org.apache.tapestry5.Asset;
import org.apache.tapestry5.ComponentResources;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Path;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.beaneditor.BeanModel;
import org.apache.tapestry5.corelib.components.Grid;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.json.JSONObject;
import org.apache.tapestry5.services.BeanModelSource;
import org.apache.tapestry5.services.Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.annotation.Secured;

@IncludeStylesheet("context:styles/pages/sms.css")
@Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_SMS_MANAGER})
public class SMS
{
	private static final Logger logger = LoggerFactory.getLogger(SMS.class);
	
	/*
	 * Mapping from SMSBean property name to sort column. Used by SMSGridDataSource to sort
	 * on a specific column
	 */
	private final Map<String, SortColumn> propertySortColumnMap = new HashMap<String, SortColumn>();
	
	@Inject
	private SMSGatewayWS smsGateway;
	
	@Inject
	private Request request;
	
	/*
	 * The SMS currently being drawn by the grid (is set by the grid component)
	 */
	@Property
	private SMSBean sms;
	
	/*
	 * Set of the selected SMS ID's
	 */
	@Persist
	private Set<Long> selected;
		
	@SuppressWarnings("unused")
	@Component(parameters = {"source = gridDataSource", "model = model", "row = sms", "volatile = true", 
			"reorder = select,delete,id,phonenumber,created,lastTry,message"})
    private Grid grid;

	@Inject
    private BeanModelSource beanModelSource;

    @Inject
    private ComponentResources resources;

    @SuppressWarnings("unused")
	@Inject
    @Path("context:icons/cross.png")
    @Property
    private Asset deleteAsset;
    
    public BeanModel<SMSBean> getModel() 
    {
        BeanModel<SMSBean> model = beanModelSource.createDisplayModel(SMSBean.class, resources.getMessages());
        
        model.add("delete", null);
        model.add("select", null);
        
        model.get("id").sortable(false);
        model.get("message").sortable(false);
        model.get("lastError").sortable(false);
        
        return model;
    }
    
    @SetupRender
	@Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_SMS_MANAGER})
    public void setupGrid() 
    {
        if (selected == null) {
            selected = Collections.synchronizedSet(new HashSet<Long>());
        }
        else {
            selected.clear();
        }
    	
    	if (propertySortColumnMap.size() == 0)
    	{
    		propertySortColumnMap.put("phonenumber", SortColumn.PHONENUMBER);
    		propertySortColumnMap.put("created", SortColumn.CREATED);
    		propertySortColumnMap.put("lasttry", SortColumn.LAST_TRY);
    	}
    }
    
	public boolean getSelect() {
		return selected.contains(sms.getID());
	}
	
	public void setSelect(boolean value) 
	{
		/*
		 * Ignored. Checkbox values will be handled using EventCheckbox (see onEventActionLink)
		 */
	}
    
    @OnEvent(component = "deleteSMS")
    protected void deleteSMS(long smsID) 
    {
    	try {
    		smsGateway.delete(smsID);
		} 
    	catch (WebServiceCheckedException e) {
    		logger.error("Error deleting SMS with ID: " + smsID, e);
		}
    }
    
	public SMSGridDataSource getGridDataSource()	{
		return new SMSGridDataSource(smsGateway, propertySortColumnMap);
	}
	
    /*
     * Event handler called when the checkbox is clicked. 
     */
    public void onCheckboxEvent()
    {
    	String data = request.getParameter("data");

    	if (StringUtils.isEmpty(data)) {
    		throw new IllegalArgumentException("data is missing.");
    	}
    	
    	JSONObject json = new JSONObject(data);
    	    	    	
    	Long element = json.getLong("element");
    	boolean checked = json.getBoolean("checked"); 
    	
    	if (checked) {
    		selected.add(element);
    	}
    	else {
    		selected.remove(element);
    	}
    }
    
    @OnEvent(component = "deleteSelected")
    protected void deleteSelected() 
    {
        /*
         * Note: we need to clone the set to make sure that concurrent modifications
         * are possible.
         */
        Long[] ids = selected.toArray(new Long[]{});
        
        for (Long smsID : ids) {
            deleteSMS(smsID);
        }
    }
}
