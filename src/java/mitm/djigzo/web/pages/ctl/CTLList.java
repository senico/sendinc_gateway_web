/*
 * Copyright (c) 2009-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.pages.ctl;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import mitm.application.djigzo.admin.FactoryRoles;
import mitm.application.djigzo.ws.CTLEntryDTO;
import mitm.application.djigzo.ws.CTLWS;
import mitm.common.security.ctl.CTLManager;
import mitm.common.ws.WebServiceCheckedException;
import mitm.djigzo.web.grid.CTLGridDataSource;
import mitm.djigzo.web.grid.StaticCTLGridDataSource;

import org.apache.commons.lang.StringUtils;
import org.apache.tapestry5.Asset;
import org.apache.tapestry5.ComponentResources;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Path;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.beaneditor.BeanModel;
import org.apache.tapestry5.corelib.components.Grid;
import org.apache.tapestry5.grid.GridDataSource;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.ioc.annotations.Value;
import org.apache.tapestry5.json.JSONObject;
import org.apache.tapestry5.services.BeanModelSource;
import org.apache.tapestry5.services.Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.annotation.Secured;

@IncludeStylesheet("context:styles/pages/ctl/ctlList.css")
public class CTLList
{
    private final static Logger logger = LoggerFactory.getLogger(CTLList.class);
    
    private final static String CTL_NAME = CTLManager.DEFAULT_CTL;
    
    @Inject
    private CTLWS ctlWS;
    
    @Inject
    private Request request;
    
    @Inject
    @Value("${ctls.rowsPerPage}")
    private int rowsPerPage;    
    
    /*
     * The CTL entry currently being drawn by the grid (is set by the grid component)
     */
    private CTLEntryDTO ctlEntry;
    
    /*
     * Set of the selected CTLs (thumbprints).
     */
    @Persist
    private Set<String> selected;
    
    /*
     * The thumbprint of the CTL entry. Only used if we want to show just entry with the given thumbprint
     */
    private String thumbprint;
    
    @SuppressWarnings("unused")
    @Component(parameters = {"source = source", "model = model", "row = ctlEntry", "volatile = true",
            "reorder = select,delete,status,allowExpired,thumbprint", "rowsPerPage = prop:rowsPerPage"})
    private Grid grid;

    @Inject
    private BeanModelSource beanModelSource;

    @Inject
    private ComponentResources resources;

    @Inject
    @Path("context:icons/cross.png")
    private Asset deleteAsset;

    @SetupRender
    public void setupGrid() 
    {
        if (selected == null) {
            selected = Collections.synchronizedSet(new HashSet<String>());
        }
        else {
            selected.clear();
        }
    }
    
    public void onActivate(String thumbprint)
    {
        this.thumbprint = thumbprint;
        
        try {
            ctlEntry = ctlWS.getEntry(CTL_NAME, thumbprint);
        }
        catch (WebServiceCheckedException e) {
            logger.error("Error loading CRL entry.", e);
        }
    }
        
    public void setCTLEntry(CTLEntryDTO ctlEntry) {
        this.ctlEntry = ctlEntry;
    }
    
    public CTLEntryDTO getCTLEntry() {
        return ctlEntry;
    }
    
    public boolean getSelect() {
        return selected.contains(ctlEntry.getThumbprint());
    }
    
    public void setSelect(boolean value) 
    {
        /*
         * Ignored. Checkbox values will be handled using EventCheckbox (see onCheckboxClick)
         */
    }
    
    public GridDataSource getSource()
    {
        return thumbprint != null ? new StaticCTLGridDataSource(ctlEntry) : 
            new CTLGridDataSource(ctlWS, CTL_NAME); 
    }

    public Asset getDeleteAsset() {
        return deleteAsset;
    }
    
    public BeanModel<CTLEntryDTO> getModel()
    {
        BeanModel<CTLEntryDTO> model = beanModelSource.createDisplayModel(CTLEntryDTO.class, 
                resources.getMessages());
        
        model.add("delete", null);
        model.add("select", null);

        /*
         * Disable all sorting
         */
        for (String column : model.getPropertyNames()) {
            model.get(column).sortable(false);
        }
        
        return model;
    }

    @OnEvent(component = "deleteCTLEntry")
    @Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_PKI_MANAGER})
    protected void deleteCTLEntry(String thumbprint) 
    throws WebServiceCheckedException 
    {
        ctlWS.deleteEntry(CTL_NAME, thumbprint);
    }

    /*
     * Event handler called when the checkbox is clicked. 
     */
    public void onCheckboxEvent()
    {
        String data = request.getParameter("data");

        if (StringUtils.isEmpty(data)) {
            throw new IllegalArgumentException("data is missing.");
        }
        
        JSONObject json = new JSONObject(data);
                        
        String element = json.getString("element");
        boolean checked = json.getBoolean("checked"); 
        
        if (checked) {
            selected.add(element);
        }
        else {
            selected.remove(element);
        }
    }
    
    @OnEvent(component = "deleteSelected")
    @Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_PKI_MANAGER})
    protected void deleteSelected()
    throws WebServiceCheckedException 
    {
        /*
         * Note: we need to clone the set to make sure that concurrent modifications
         * are possible.
         */
        String[] thumbprints = selected.toArray(new String[]{});
        
        /*
         * We assume that the id's of the checkboxes are thumbprints of certificates
         */
        for (String thumbprint : thumbprints) {
            ctlWS.deleteEntry(CTL_NAME, thumbprint);
        }
    }

    public int getRowsPerPage() {
        return rowsPerPage;
    }

    public String getThumbprint() {
        return thumbprint;
    }
}
