/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.services.security;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import mitm.application.djigzo.DjigzoRuntimeException;
import mitm.common.util.Check;
import mitm.djigzo.web.asos.HMAC;

import org.apache.commons.lang.StringUtils;
import org.apache.tapestry5.Link;
import org.apache.tapestry5.MarkupWriter;
import org.apache.tapestry5.dom.Document;
import org.apache.tapestry5.dom.Element;
import org.apache.tapestry5.dom.Node;
import org.apache.tapestry5.internal.services.LinkFactory;
import org.apache.tapestry5.internal.services.RequestPageCache;
import org.apache.tapestry5.internal.structure.Page;
import org.apache.tapestry5.json.JSONObject;
import org.apache.tapestry5.services.ApplicationStateManager;
import org.apache.tapestry5.services.ComponentEventRequestHandler;
import org.apache.tapestry5.services.ComponentEventRequestParameters;
import org.apache.tapestry5.services.MarkupRenderer;
import org.apache.tapestry5.services.PartialMarkupRenderer;
import org.apache.tapestry5.services.Request;
import org.apache.tapestry5.services.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Filter that calculates the HMAC of a elements value and add the HMAC to the generated page
 * as a hidden element. When a page is activated (for example by a post) the HMAC from the request
 * is checked against a (newly) calculated HMAC. If the HMACs differ it means that the value
 * has been changed and the user is redirected to another page that to report this. 
 * 
 * @author Martijn Brinkers
 *
 */
public class HMACFilterImpl implements HMACFilter
{
	private final static Logger logger = LoggerFactory.getLogger(HMACFilterImpl.class);
	
	/*
	 * The set of all element names that need to be protected by a checksum
	 */
	private Set<String> protectedElements = new HashSet<String>();
	
	public static String HMAC_PARAMETER = "hmac-checksum";

	private final ApplicationStateManager asm;
	private final Request request;
	private final Response response;
	private final LinkFactory linkFactory;
	private final RequestPageCache requestPageCache;
	
	/*
	 * The page to redirect to when secureID is incorrect
	 */
	private final String redirectTo;
	
	/*
	 * Exception thrown when HMAC is incorrect
	 */
	private static class IncorrectHMACException extends Exception 
	{
		private static final long serialVersionUID = -8133828090623176301L;

	    public IncorrectHMACException(String message) {
	        super(message);
	    }
	}
	
	public HMACFilterImpl(ApplicationStateManager asm, Request request, Response response, 
			LinkFactory linkFactory, RequestPageCache requestPageCache, String redirectTo,
			String... protectedElements)
	{
		Check.notNull(asm, "asm");
		Check.notNull(request, "request");
		Check.notNull(response, "response");
		Check.notNull(linkFactory, "linkFactory");
		Check.notNull(requestPageCache, "requestPageCache");
		Check.notNull(redirectTo, "redirectTo");
		
		this.asm = asm;
		this.request = request;
		this.response = response;
		this.linkFactory = linkFactory;
		this.requestPageCache = requestPageCache;
		this.redirectTo = redirectTo;

		for (String protectedElement : protectedElements)
		{
			if (protectedElement == null) {
				continue;
			}
			
			protectedElement = protectedElement.trim().toLowerCase();
			
			this.protectedElements.add(protectedElement);
		}
	}
	
	private void calculateHMACs(MarkupWriter writer, List<String> hmacs)
	{
        Document document = writer.getDocument();
        
        if (document != null)
        {
	        Element root = document.getRootElement(); 
	        
	        if (root != null)
	        {
	        	LinkedList<Element> queue = new LinkedList<Element>();

	        	queue.add(root);
		
		        while (!queue.isEmpty())
		        {
		            Element element = queue.removeFirst();
		
		            if (element == null) {
		            	continue;
		            }
		            
		            String elementName = element.getAttribute("name");
		
		            if (elementName != null) {
		            	elementName = elementName.trim().toLowerCase();
		            }
		            
		            if (protectedElements.contains(elementName))
		            {
		            	/*
		            	 * It's a protected item so we should calculate the HMAC of the value
		            	 */
		            	String value = element.getAttribute("value");
		            	
		            	String hmac;
		            	
						try {
							hmac = calculateHMAC(value, true /* create ASO if not exist */);
						} 
						catch (InvalidKeyException e) {
							throw new DjigzoRuntimeException(e);
						} 
						catch (NoSuchAlgorithmException e) {
							throw new DjigzoRuntimeException(e);
						} 
		            	
						if (hmac == null) {
							throw new DjigzoRuntimeException("hmac is null.");
						}

						if (hmacs != null) {
							hmacs.add(hmac);
						}
						else {
			            	/*
			            	 * Add the HMAC checksum as a hidden element
			            	 */
							element.element("input",
			                        "type", "hidden",
			                        "name", HMAC_PARAMETER,
			                        "value", hmac);
						}
		            }
		            
		            for (Node n : element.getChildren())
		            {
		            	Element child = null;
		            	
		            	if (n instanceof Element) {
		            		child = (Element) n;
		            	}
		
		                if (child != null) queue.addLast(child);
		            }
		        }
	        }
        }
	}
	
    @Override
    public void renderMarkup(MarkupWriter writer, JSONObject reply, PartialMarkupRenderer renderer)
    {
        renderer.renderMarkup(writer, reply);

        List<String> hmacs = new LinkedList<String>();
        
        calculateHMACs(writer, hmacs);
        
        String content = reply.getString("content");
        
        if (content != null)
        {
        	String inputs = "";
        	/*
        	 * Add the calculated hmacs as hidden inputs
        	 */
        	for (String hmac : hmacs) {
        		inputs = inputs + "<input type=\"hidden\" name=\"" + HMAC_PARAMETER + "\" value=\"" + hmac + "\"/>";
        	}

        	content = content.replaceAll("</form>\\s*$", inputs + "</form>");
        	
        	reply.put("content", content);
        }
    }
    
    @Override
    public void renderMarkup(MarkupWriter writer, MarkupRenderer renderer)
    {
        renderer.renderMarkup(writer);
        
        calculateHMACs(writer, null);
    }
    
    @Override
	public void handle(ComponentEventRequestParameters parameters, 
			ComponentEventRequestHandler handler)
	throws IOException 
	{
        Page page = requestPageCache.get(parameters.getActivePageName());

        try {
	        if (isHMACProtected(page)) 
	        {
	        	/*
	        	 * We will build a set of all the HMACS we can find in the request
	        	 */
	        	String[] hmacParameters = request.getParameters(HMAC_PARAMETER);
	        	
	        	Set<String> hmacs = new HashSet<String>();
	        	
	        	if (hmacParameters != null) 
	        	{
	        		for (String hmac : hmacParameters) {
	        			hmacs.add(hmac);
	        		}
	        	}
	        	
	        	for (String protectedElement : protectedElements)
	        	{
	        		/*
	        		 * There can be more than one protected value per element
	        		 */
	        		String[] protectedValues = request.getParameters(protectedElement);
	        		
	        		if (protectedValues != null)
	        		{
		        		for (String protectedValue : protectedValues)
		        		{
		        			/*
		        			 * If a form fragment is used there can be an empty t:formdata value. Skip if value is empty
		        			 */
		        			if (StringUtils.isNotEmpty(protectedValue))
		        			{
			        			String hmac = calculateHMAC(protectedValue, false /* do not create ASO if not exist */);
			        			
			        			if (hmac == null || !hmacs.contains(hmac)) {
			        				throw new IncorrectHMACException("The hmac " + hmac + " is incorrect");
			        			}
		        			}
		        		}
	        		}
	        	}
	        }
	        
	        /*
	         * Not protected or checksum is correct so continue
	         */
	        handler.handle(parameters);
        }
        catch(IncorrectHMACException e)
        {
        	logger.warn(e.getMessage());
        	
        	Link link = linkFactory.createPageRenderLink(redirectTo, false);

        	response.sendRedirect(link);
        } 
        catch (InvalidKeyException e) {
        	throw new IOException(e);
		} 
        catch (NoSuchAlgorithmException e) {
        	throw new IOException(e);
		}
	}
	
	private String calculateHMAC(String input, boolean createASOIfNotExist) 
	throws InvalidKeyException, NoSuchAlgorithmException
	{
	    /*
	     * Canonicalize the input before calculating the HMAC
	     */
	    input = StringUtils.deleteWhitespace(StringUtils.defaultString(input));

        String calculated = null;
	    
		if (createASOIfNotExist || asm.exists(HMAC.class)) {
			calculated = asm.get(HMAC.class).calculateHMAC(input);
		}
		
    	return calculated;
	}
	
	private boolean isHMACProtected(Page page)
	{
		/*
		 * For now all actions are protected. We can always create a special Annotation when
		 * we need to specify which pages/actions are protected.
		 */
		return true;
	}
}
