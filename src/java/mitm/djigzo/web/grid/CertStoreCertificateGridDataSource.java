/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.grid;

import java.util.ArrayList;
import java.util.List;

import mitm.application.djigzo.DjigzoRuntimeException;
import mitm.application.djigzo.ws.X509CertStoreWS;
import mitm.application.djigzo.ws.X509CertificateDTO;
import mitm.common.security.certstore.Expired;
import mitm.common.security.certstore.Match;
import mitm.common.security.certstore.MissingKeyAlias;
import mitm.common.util.Check;
import mitm.common.ws.WebServiceCheckedException;
import mitm.djigzo.web.beans.X509CertificateBean;
import mitm.djigzo.web.beans.impl.X509CertificateBeanImpl;
import mitm.djigzo.web.common.CertificateFilterSettings;

import org.apache.commons.lang.StringUtils;
import org.apache.tapestry5.grid.SortConstraint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CertStoreCertificateGridDataSource extends AbstractCertificateGridDataSource
{
	private final static Logger logger = LoggerFactory.getLogger(CertStoreCertificateGridDataSource.class);
	
	private List<X509CertificateBean> certificateBeans;
	
	/*
	 * we need to keep track of base index because the index in getRowValue is the absolute
	 * index and not relative.
	 */
	private int startIndex;
		
	/*
	 * The certificate store from which certificates will be retrieved.
	 */
	private final X509CertStoreWS certStore;

	/*
	 * The filter settings with which we will filter the result
	 */
	private final CertificateFilterSettings filterSettings;
	
	public CertStoreCertificateGridDataSource(X509CertStoreWS certStore, CertificateFilterSettings filterSettings)
	{
		Check.notNull(certStore, "certStore");
		
		if (filterSettings == null) {
			filterSettings = new CertificateFilterSettings();
		}
		
		this.certStore = certStore;
		this.filterSettings = filterSettings;
	}
	
    @Override
	public int getAvailableRows() 
	{
		try {
			/*
			 * Cast to int which should be safe because we will never
			 * have this many certificates.
			 */
			return (int) getStoreSize();
		} 
		catch (WebServiceCheckedException e) {
			throw new DjigzoRuntimeException(e);
		}
	}

	@Override
	public X509CertificateBean getRowValue(int index) 
	{
		int rowIndex = index - startIndex;
		
		X509CertificateBean value = null;
		
		if (certificateBeans != null  && rowIndex < certificateBeans.size()) {
			value = certificateBeans.get(rowIndex);
		}
		else {
			logger.warn("Not enough rows.");
			/* 
			 * We do not have enough certificateBean objects. This can happen when AvailableRows
			 * returned more rows than were actually available when prepared was called. 
			 */ 
			value = null;
		}
		
		return value;
	}

    @Override
	public void prepare(int startIndex, int endIndex, List<SortConstraint> sortConstraints) 
	{
		try {
			List<X509CertificateDTO> certificates = getCertificates(startIndex, endIndex);
			
			/*
			 * The web service returns null when the collection returns no items.
			 */
			if (certificates != null)
			{
				certificateBeans =  new ArrayList<X509CertificateBean>(certificates.size());
				
				for (X509CertificateDTO certificateDTO : certificates) {
					certificateBeans.add(new X509CertificateBeanImpl(certificateDTO));
				}
			}
			else {
				certificateBeans = new ArrayList<X509CertificateBean>();
			}

			/*
			 * We need to save the startIndex because we need in to get the correct index of 
			 * certificateBeans in getRowValue (the index value is absolute but we need to 
			 * make it relative)
			 */
			this.startIndex = startIndex;
		} 
		catch (WebServiceCheckedException e) {
			throw new DjigzoRuntimeException(e);
		}
	}

	private MissingKeyAlias getMissingKeyAlias() {
		return filterSettings.isAllowMissingKeyAlias() ? MissingKeyAlias.ALLOWED : 
			MissingKeyAlias.NOT_ALLOWED;
	}

	private Expired getExpired() {
		return filterSettings.isAllowExpired() ? Expired.ALLOWED : 
			Expired.NOT_ALLOWED;
	}
	
	private List<X509CertificateDTO> getAllCertificates(int startIndex, int endIndex) 
	throws WebServiceCheckedException 
	{
		return certStore.getCertificates(getExpired(), getMissingKeyAlias(), startIndex, 
				endIndex - startIndex + 1);
	}

	private List<X509CertificateDTO> getCertificatesByEmail(int startIndex, int endIndex) 
	throws WebServiceCheckedException 
	{
		return certStore.getByEmail(getFilter(), Match.LIKE, getExpired(), 
				getMissingKeyAlias(), startIndex, endIndex - startIndex + 1);
	}

	private List<X509CertificateDTO> getCertificatesBySubject(int startIndex, int endIndex) 
	throws WebServiceCheckedException 
	{
		return certStore.searchBySubject(getFilter(), getExpired(), 
				getMissingKeyAlias(), startIndex, endIndex - startIndex + 1);
	}

	private List<X509CertificateDTO> getCertificatesByIssuer(int startIndex, int endIndex) 
	throws WebServiceCheckedException 
	{
		return certStore.searchByIssuer(getFilter(), getExpired(), 
				getMissingKeyAlias(), startIndex, endIndex - startIndex + 1);
	}

	private long getNoFilterCount() 
	throws WebServiceCheckedException 
	{
		return certStore.size(getExpired(), getMissingKeyAlias());
	}
	
	private long getByEmailCount()
	throws WebServiceCheckedException 
	{
		return certStore.getByEmailCount(getFilter(), Match.LIKE, getExpired(), 
				getMissingKeyAlias());
	}

	private long getBySubjectCount() 
	throws WebServiceCheckedException 
	{
		return certStore.getSearchBySubjectCount(getFilter(), getExpired(), 
				getMissingKeyAlias());
	}

	private long getByIssuerCount() 
	throws WebServiceCheckedException 
	{
		return certStore.getSearchByIssuerCount(getFilter(), getExpired(), 
				getMissingKeyAlias());
	}
	
	private long getStoreSize()
	throws WebServiceCheckedException
	{
		switch(filterSettings.getFilterBy())
		{
		case NO_FILTER : return getNoFilterCount();
		case EMAIL     : return getByEmailCount();
		case SUBJECT   : return getBySubjectCount();
		case ISSUER    : return getByIssuerCount();
		default:
			throw new IllegalArgumentException("Unknown FilterBy.");
		}
	}
	
	private List<X509CertificateDTO> getCertificates(int startIndex, int endIndex)
	throws WebServiceCheckedException
	{
		switch(filterSettings.getFilterBy())
		{
		case NO_FILTER : return getAllCertificates(startIndex, endIndex);
		case EMAIL     : return getCertificatesByEmail(startIndex, endIndex);
		case SUBJECT   : return getCertificatesBySubject(startIndex, endIndex);
		case ISSUER    : return getCertificatesByIssuer(startIndex, endIndex);
		default:
			throw new IllegalArgumentException("Unknown FilterBy.");
		}
	}
	
	private String getFilter() 
	{
		String filter = filterSettings.getFilter();
		
		if (filter == null) 
		{
			/*
			 * If search is empty search for all everything
			 */
			filter = "%%";
		}
		else {
			/*
			 * Treat searching for # as a special character to indicate that searching for null
			 * field should be done.
			 */
			if (filter.equals("#")) {
				filter = null;
			}
			else {
				/*
				 * If the filter string does not contain a LIKE special symbol ('%' or '_') the search
				 * string will be embedded in %%.
				 */
				if (!StringUtils.containsAny(filter, "%_")) {
					filter = "%" + filter + "%";
				}
			}
		}
		
		return filter;
	}
}
