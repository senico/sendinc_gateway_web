/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.components;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.tapestry5.Block;
import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.json.JSONObject;
import org.apache.tapestry5.services.Request;

public class MultiSelectCertificateGrid extends CertificateGrid
{
	@Inject
	private Request request;
		    
	/*
	 * Set of the selected certificates (thumbprints).
	 */
	@Persist
	private Set<String> selected;

    @Inject
    @Property
    private Block defaultLegendBlock;
    
    @Parameter(defaultPrefix="inherit")
    private Block legendBlock;
	
	public Block getLegendBlock() {
	    return legendBlock;
	}
	
	public Block defaultLegendBlock() {
	    return defaultLegendBlock;
	}
	
	public Set<String> getSelected()
	{
		if (selected == null) {
    		selected = Collections.synchronizedSet(new HashSet<String>());
		}
		
		return selected;
	}

    public void clearSelected()
    {
        if (selected != null) {
            selected.clear();
        }
    }
    
	public boolean isSelect() {
		return getSelected().contains(getCertificate().getThumbprint());
	}
	
	public void setSelect(boolean value) 
	{
		/*
		 * Ignored. Checkbox values will be handled using EventCheckbox (see onCheckboxClick)
		 */
	}
	
    /*
     * Event handler called when the checkbox is clicked. 
     */
    public void onCheckboxEvent()
    {
    	String data = request.getParameter("data");

    	if (StringUtils.isEmpty(data)) {
    		throw new IllegalArgumentException("data is missing.");
    	}
    	
    	JSONObject json = new JSONObject(data);
    	    	    	
    	String element = json.getString("element");
    	boolean checked = json.getBoolean("checked"); 
    	
    	/*
    	 * If the Grid is inside a Zone the Id is no longer just the thumbprint. Some :.* part
    	 * is added. We should therefore remove everything after :.* to get the thumbprint back 
    	 */
    	element = StringUtils.substringBefore(element, ":");
    	
    	if (checked) {
    	    getSelected().add(element);
    	}
    	else {
    	    getSelected().remove(element);
    	}
    }
}
