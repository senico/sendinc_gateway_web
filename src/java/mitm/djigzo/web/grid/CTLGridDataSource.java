/*
 * Copyright (c) 2009-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.grid;

import java.util.Collections;
import java.util.List;

import mitm.application.djigzo.DjigzoRuntimeException;
import mitm.application.djigzo.ws.CTLEntryDTO;
import mitm.application.djigzo.ws.CTLWS;
import mitm.common.util.Check;
import mitm.common.ws.WebServiceCheckedException;

import org.apache.tapestry5.grid.GridDataSource;
import org.apache.tapestry5.grid.SortConstraint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CTLGridDataSource implements GridDataSource 
{
	private final static Logger logger = LoggerFactory.getLogger(CTLGridDataSource.class);
	
	private List<CTLEntryDTO> ctlEntries;
	
	/*
	 * we need to keep track of base index because the index in getRowValue is the absolute
	 * index and not relative.
	 */
	private int startIndex;
	
	private final CTLWS ctlWS;

    private final String ctlName;

	public CTLGridDataSource(CTLWS ctlWS, String ctlName)
	{
        Check.notNull(ctlWS, "ctlWS");
        Check.notNull(ctlName, "ctlName");
		
		this.ctlWS = ctlWS;
		this.ctlName = ctlName;
	}
	
    @Override
	public int getAvailableRows() 
	{
		try {
			return ctlWS.getSize(ctlName);
		} 
		catch (WebServiceCheckedException e) {
			throw new DjigzoRuntimeException(e);
		}
	}

    @Override
	public Class<CTLEntryDTO> getRowType()
	{
		return CTLEntryDTO.class;
	}

    @Override
	public CTLEntryDTO getRowValue(int index) 
	{
		int rowIndex = index - startIndex;
		
		CTLEntryDTO value = null;
		
		if (ctlEntries != null && rowIndex < ctlEntries.size()) {
			value = ctlEntries.get(rowIndex);
		}
		else {
			logger.warn("Not enough rows.");
			/* 
			 * We do not have enough objects. This can happen when AvailableRows
			 * returned more rows than were actually available when prepared was called. 
			 */ 
			value = null;
		}
		
		return value;
	}

    @Override
	public void prepare(int startIndex, int endIndex, List<SortConstraint> sortConstraints) 
	{
		try {
			ctlEntries = ctlWS.getEntries(ctlName, startIndex, endIndex - startIndex + 1);

            if (ctlEntries == null) {
                /*
                 * Can happen because of a race condition where getAvailableRows said that
                 * there are rows available but the items were already removed when prepare
                 * was called.
                 */
                ctlEntries = Collections.emptyList();
            }
		} 
		catch (WebServiceCheckedException e) {
			throw new DjigzoRuntimeException(e);
		} 
		
		this.startIndex = startIndex;
	}
}
