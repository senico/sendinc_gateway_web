/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.common.security;

import java.util.List;

import mitm.application.djigzo.admin.PasswordEncoding;
import mitm.application.djigzo.ws.AdminDTO;
import mitm.application.djigzo.ws.AdminWS;
import mitm.common.security.crypto.RandomGenerator;
import mitm.common.util.Check;
import mitm.common.util.MiscStringUtils;
import mitm.common.ws.WebServiceCheckedException;

import org.apache.commons.codec.binary.Base64;
import org.springframework.security.GrantedAuthority;
import org.springframework.security.GrantedAuthorityImpl;
import org.springframework.security.providers.dao.SaltSource;
import org.springframework.security.providers.encoding.PasswordEncoder;

/**
 * Implementation of Admin for gateway Administrators
 * 
 * @author Martijn Brinkers
 *
 */
public class AdminImpl implements Admin
{
	private static final long serialVersionUID = 8171069249037533310L;

	/*
	 * Size of the salt in bytes
	 */
	private static final int SALT_SIZE = 16;

	/*
	 * The DTO returned from the SOAP call
	 */
	private final AdminDTO adminDTO;
	
	/*
	 * Used to encode the password when the password is not encoded
	 */
	private final PasswordEncoder passwordEncoder;
	
	/*
	 * Used to salt the password when the password is not encoded
	 */
	private final SaltSource saltSource;
	
	/*
	 * Used to generate the salt
	 */
	private final RandomGenerator randomGenerator;
	
	/*
	 * (optional) Service for updating the user 
	 */
	private final AdminWS adminWS;
	
	private String salt;
	
	boolean accountNonExpired = true;
	boolean accountNonLocked = true;
	boolean credentialsNonExpired = true;
	boolean enabled = true;
	
	
	public AdminImpl(AdminDTO adminDTO, PasswordEncoder passwordEncoder, SaltSource saltSource,
			RandomGenerator randomGenerator, AdminWS adminWS)
	{
		Check.notNull(adminDTO, "adminDTO");
		Check.notNull(passwordEncoder, "passwordEncoder");
		Check.notNull(saltSource, "saltSource");

		this.adminDTO = adminDTO;
		this.passwordEncoder = passwordEncoder;
		this.saltSource = saltSource;
		this.randomGenerator = randomGenerator;
		this.adminWS = adminWS;
		
		/*
		 * We need to be able to change the salt
		 */
		this.salt = adminDTO.getSalt();
	}

    @Override
	public String getUsername() {
		return adminDTO.getUsername();
	}

    @Override
	public GrantedAuthority[] getAuthorities() 
	{
		List<String> roles = adminDTO.getRoles();
		
		if (roles == null) {
			throw new IllegalStateException("roles is empty.");
		}
		
		GrantedAuthority[] authorities = new GrantedAuthority[roles.size()];
		
		for (int i = 0; i < authorities.length; i++)
		{
			authorities[i] = new GrantedAuthorityImpl(roles.get(i));
		}
		
		return authorities;
	}

    @Override
	public String getPassword() 
	{
		String password = adminDTO.getPassword();
		
		if (adminDTO.getPasswordEncoding() == PasswordEncoding.NONE) 
		{
			/*
			 * The password is not encoded but Spring excpect it to be encoded so we need to 
			 * encode it
			 */
			password = encodePassword(password);
		}
		
		return password;
	}

	public String getSalt() {
		return salt;
	}
	
    @Override
	public boolean isAccountNonExpired() {
		return accountNonExpired;
	}

    @Override
	public boolean isAccountNonLocked() {
		return accountNonLocked;
	}
    @Override
	public boolean isCredentialsNonExpired() {
		return credentialsNonExpired;
	}

    @Override
	public boolean isEnabled() {
		return adminDTO.isEnabled();
	}
	
    @Override
	public boolean isBuiltIn() {
		return adminDTO.isBuiltIn();
	}
	
    @Override
	public void setRoles(List<String> roles)
	throws WebServiceCheckedException
	{
		checkAdminWS();
		
		adminWS.setRoles(getUsername(), roles);
	}
	
    @Override
	public void setPassword(String password)
	throws WebServiceCheckedException
	{
		checkAdminWS();
		
		if (randomGenerator == null) {
			throw new IllegalStateException("randomGenerator is not set.");
		}
		
		adminWS.setPasswordEncoding(getUsername(), PasswordEncoding.ENCODED);
		
		/*
		 * We must generate the salt and set it before encoding the password
		 */
		byte[] saltBytes = randomGenerator.generateRandom(SALT_SIZE);
		
		/*
		 * Convert the binary salt to a String
		 */
		this.salt = MiscStringUtils.toAsciiString(Base64.encodeBase64(saltBytes));
		
		adminWS.setSalt(getUsername(), salt);
		
		password = encodePassword(password);
		
		adminWS.setPassword(getUsername(), password);
	}

    @Override
	public void setEnabled(boolean enabled)
	throws WebServiceCheckedException
	{
		checkAdminWS();
		
		adminWS.setEnabled(getUsername(), enabled);
	}
	
	private void checkAdminWS() 
	throws WebServiceCheckedException
	{
		if (adminWS == null) {
			throw new WebServiceCheckedException("adminWS is not set.");
		}
	}
	
	private String encodePassword(String password) {
		return passwordEncoder.encodePassword(password, saltSource.getSalt(this));
	}
	
	@Override
	public String toString()
	{
	    return "AdminImpl. Username: " + adminDTO != null ? adminDTO.getUsername() : "unknown user";
	}
}
