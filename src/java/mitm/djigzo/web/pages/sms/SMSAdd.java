/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.pages.sms;

import mitm.application.djigzo.admin.FactoryRoles;
import mitm.application.djigzo.ws.SMSDTO;
import mitm.application.djigzo.ws.SMSGatewayWS;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.ws.WebServiceCheckedException;
import mitm.djigzo.web.pages.SMS;

import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.IncludeJavaScriptLibrary;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.beaneditor.Validate;
import org.apache.tapestry5.corelib.components.TextArea;
import org.apache.tapestry5.corelib.components.TextField;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.springframework.security.annotation.Secured;

@IncludeJavaScriptLibrary("classpath:mitm/djigzo/web/validators/phonenumber.js") 
@IncludeStylesheet("context:styles/pages/sms/smsAdd.css")
@Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_SMS_MANAGER})
public class SMSAdd 
{
	@Inject
	private SMSGatewayWS smsGateway;
	
	@Property
	@Validate("required,maxlength=255,phonenumber")
	private String phoneNumber;
	
	@Property
	@Validate("required,maxlength=160")
	private String message;

	@SuppressWarnings("unused")
	@Component(id = "phoneNumber", parameters = {"value = phoneNumber"})
	private TextField phonenumberField;

	@SuppressWarnings("unused")
	@Component(id = "message", parameters = {"value = message"})
	private TextArea messageField;

	@SetupRender
	@Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_SMS_MANAGER})
	protected void setupRender() {
		/*
		 * Empty on purpose
		 */
	}
	
	public Object onSuccess()
	throws HierarchicalPropertiesException, WebServiceCheckedException 
	{
		SMSDTO sms = new SMSDTO(phoneNumber, message);
		
		smsGateway.sendSMS(sms);
		
		return SMS.class;
	}

    /*
     * Event handler called when the cancel button is pressed.
     */
    public Object onCancel() {
		return SMS.class;
	}
}
