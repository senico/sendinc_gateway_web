/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.services;

import java.util.List;

import mitm.common.util.Check;
import mitm.djigzo.web.utils.HttpServletUtils;

import org.apache.tapestry5.model.MutableComponentModel;
import org.apache.tapestry5.services.ClassTransformation;
import org.apache.tapestry5.services.ComponentClassTransformWorker;
import org.apache.tapestry5.services.ComponentMethodAdvice;
import org.apache.tapestry5.services.ComponentMethodInvocation;
import org.apache.tapestry5.services.Response;
import org.apache.tapestry5.services.TransformMethodSignature;

/**
 * ComponentClassTransformWorker that disables the HTTP cache when the method is annotated with {@link #DisableHttpCache}
 * 
 * @author Martijn Brinkers
 *
 */public class DisableHttpCacheWorker implements ComponentClassTransformWorker 
{
	private final Response response;
	
    private final ComponentMethodAdvice advice = new ComponentMethodAdvice()
    {
        @Override
        public void advise(ComponentMethodInvocation invocation)
        {
        	invocation.proceed();
        	
        	HttpServletUtils.disableHTTPCaching(response);
        }
    };
	
    public DisableHttpCacheWorker(Response response)
    {
    	Check.notNull(response, "response");
    	
        this.response = response;
    }

    @Override
    public final void transform(final ClassTransformation transformation, final MutableComponentModel model) 
    {
    	/*
    	 * Find all methods with the DisableHttpCache annotation
    	 */
    	List<TransformMethodSignature> methods = transformation.findMethodsWithAnnotation(DisableHttpCache.class);
    	
        for (TransformMethodSignature method : methods) {
            transformation.advise(method, advice);
        }
    }
}