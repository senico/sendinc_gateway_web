var DHTMLEvent = Class.create();

DHTMLEvent.prototype = 
{
    initialize: function(elementID, event, toEval)
    {
        this.elementID = elementID;
        this.event = event;
        this.toEval = toEval;

        Event.observe($(this.elementID), event, this.onEvent.bindAsEventListener(this));
    },
    onEvent: function(e)
    {
    	eval(this.toEval);
    }
}