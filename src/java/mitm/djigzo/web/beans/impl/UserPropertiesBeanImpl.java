/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 *
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar,
 * wsdl4j-1.6.1.jar (or modified versions of these libraries),
 * containing parts covered by the terms of Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to
 * convey the resulting work.
 */
package mitm.djigzo.web.beans.impl;

import mitm.application.djigzo.EncryptMode;
import mitm.application.djigzo.PropertyRegistry;
import mitm.application.djigzo.UserLocality;
import mitm.application.djigzo.UserProperties;
import mitm.application.djigzo.ws.PropertySelectorDTO;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.util.Check;
import mitm.djigzo.web.beans.BooleanProperty;
import mitm.djigzo.web.beans.EnumProperty;
import mitm.djigzo.web.beans.StringProperty;
import mitm.djigzo.web.beans.UserPropertiesBean;
import mitm.djigzo.web.entities.SaveableUserProperties;


/**
 *
 * @author jon
 */
public class UserPropertiesBeanImpl implements UserPropertiesBean
{
	private final EnumProperty<UserLocality> userLocality = new EnumProperty<UserLocality>();
	private final EnumProperty<EncryptMode> encryptMode = new EnumProperty<EncryptMode>();
	private final BooleanProperty sendEncryptionNotification = new BooleanProperty();

	private final StringProperty subjectTrigger = new StringProperty();
	private final BooleanProperty subjectTriggerEnabled = new BooleanProperty();
	private final BooleanProperty subjectTriggerIsRegExpr = new BooleanProperty();
	private final BooleanProperty subjectTriggerRemovePattern = new BooleanProperty();

	private final StringProperty serverSecret = new StringProperty();
	private final BooleanProperty forceEncryptAllowed = new BooleanProperty();
	private final StringProperty forceEncryptHeaderTrigger = new StringProperty();

  private final BooleanProperty addSecurityInfo = new BooleanProperty();
  private final StringProperty securityInfoDecryptedTag = new StringProperty();
  private final StringProperty securityInfoSignedValidTag = new StringProperty();
  private final StringProperty securityInfoSignedByValidTag = new StringProperty();
  private final StringProperty securityInfoSignedInvalidTag = new StringProperty();

  private final BooleanProperty subjectFilterEnabled = new BooleanProperty();
  private final StringProperty subjectFilterRegEx = new StringProperty();

    /*
     * All properties will be read/written to this properties instance
     */
	private final SaveableUserProperties properties;

	public UserPropertiesBeanImpl(SaveableUserProperties properties)
	throws HierarchicalPropertiesException
	{
		Check.notNull(properties, "properties");

		this.properties = properties;

		initProperties();
	}

	private boolean isInherited(String property)
	throws HierarchicalPropertiesException
	{
		return properties.isInherited(properties.getFullPropertyName(property));
	}

	private PropertySelectorDTO createPropertySelector(String basicPropertyName)
	{
	    String fullName = properties.getFullPropertyName(basicPropertyName);

	    return new PropertySelectorDTO(fullName, PropertyRegistry.getInstance().isEncrypted(fullName));
	}

	private void initProperties()
	throws HierarchicalPropertiesException
	{
	    /*
	     * Preload some properties to speedup the loading process (preloaded properties will be loaded with a single
	     * soap call).
	     */
	    properties.preload(
	              createPropertySelector(UserProperties.LOCALITY),
                createPropertySelector(UserProperties.ENCRYPT_MODE),
                createPropertySelector(UserProperties.SEND_ENCRYPTION_NOTIFICATION),

                createPropertySelector(UserProperties.SUBJECT_TRIGGER),
                createPropertySelector(UserProperties.SUBJECT_TRIGGER_ENABLED),
                createPropertySelector(UserProperties.SUBJECT_TRIGGER_IS_REGEXPR),
                createPropertySelector(UserProperties.SUBJECT_TRIGGER_REMOVE_PATTERN),

                createPropertySelector(UserProperties.SERVER_SECRET),
                createPropertySelector(UserProperties.FORCE_ENCRYPT_ALLOWED),
                createPropertySelector(UserProperties.FORCE_ENCRYPT_HEADER_TRIGGER),

                createPropertySelector(UserProperties.ADD_SECURITY_INFO),
                createPropertySelector(UserProperties.SECURITY_INFO_DECRYPTED_TAG),
                createPropertySelector(UserProperties.SECURITY_INFO_SIGNED_VALID_TAG),
                createPropertySelector(UserProperties.SECURITY_INFO_SIGNED_BY_VALID_TAG),
                createPropertySelector(UserProperties.SECURITY_INFO_SIGNED_INVALID_TAG),

                createPropertySelector(UserProperties.SUBJECT_FILTER_ENABLED),
                createPropertySelector(UserProperties.SUBJECT_FILTER_REGEX));

		userLocality.setValue(properties.getUserLocality());
		userLocality.setInherit(isInherited(UserProperties.LOCALITY));

		encryptMode.setValue(properties.getEncryptMode());
		encryptMode.setInherit(isInherited(UserProperties.ENCRYPT_MODE));

        forceEncryptHeaderTrigger.setValue(properties.getForceEncryptHeaderTrigger());
        forceEncryptHeaderTrigger.setInherit(isInherited(UserProperties.FORCE_ENCRYPT_HEADER_TRIGGER));

        forceEncryptAllowed.setValue(properties.isForceEncryptAllowed());
        forceEncryptAllowed.setInherit(isInherited(UserProperties.FORCE_ENCRYPT_ALLOWED));

		subjectTrigger.setValue(properties.getSubjectTrigger());
		subjectTrigger.setInherit(isInherited(UserProperties.SUBJECT_TRIGGER));

		subjectTriggerEnabled.setValue(properties.isSubjectTriggerEnabled());
		subjectTriggerEnabled.setInherit(isInherited(UserProperties.SUBJECT_TRIGGER_ENABLED));

		subjectTriggerIsRegExpr.setValue(properties.isSubjectTriggerRegExpr());
		subjectTriggerIsRegExpr.setInherit(isInherited(UserProperties.SUBJECT_TRIGGER_IS_REGEXPR));

		subjectTriggerRemovePattern.setValue(properties.isSubjectTriggerRemovePattern());
		subjectTriggerRemovePattern.setInherit(isInherited(UserProperties.SUBJECT_TRIGGER_REMOVE_PATTERN));

		sendEncryptionNotification.setValue(properties.isSendEncryptionNotification());
		sendEncryptionNotification.setInherit(isInherited(UserProperties.SEND_ENCRYPTION_NOTIFICATION));

		serverSecret.setValue(properties.getServerSecret());
		serverSecret.setInherit(isInherited(UserProperties.SERVER_SECRET));

        addSecurityInfo.setValue(properties.isAddSecurityInfo());
        addSecurityInfo.setInherit(isInherited(UserProperties.ADD_SECURITY_INFO));

        securityInfoDecryptedTag.setValue(properties.getSecurityInfoDecryptedTag());
        securityInfoDecryptedTag.setInherit(isInherited(UserProperties.SECURITY_INFO_DECRYPTED_TAG));

        securityInfoSignedValidTag.setValue(properties.getSecurityInfoSignedValidTagTag());
        securityInfoSignedValidTag.setInherit(isInherited(UserProperties.SECURITY_INFO_SIGNED_VALID_TAG));

        securityInfoSignedByValidTag.setValue(properties.getSecurityInfoSignedByValidTagTag());
        securityInfoSignedByValidTag.setInherit(isInherited(UserProperties.SECURITY_INFO_SIGNED_BY_VALID_TAG));

        securityInfoSignedInvalidTag.setValue(properties.getSecurityInfoSignedInvalidTagTag());
        securityInfoSignedInvalidTag.setInherit(isInherited(UserProperties.SECURITY_INFO_SIGNED_INVALID_TAG));

        subjectFilterEnabled.setValue(properties.isSubjectFilterEnabled());
        subjectFilterEnabled.setInherit(isInherited(UserProperties.SUBJECT_FILTER_ENABLED));

        subjectFilterRegEx.setValue(properties.getSubjectFilterRegEx());
        subjectFilterRegEx.setInherit(isInherited(UserProperties.SUBJECT_FILTER_REGEX));
	}

    @Override
	public EnumProperty<UserLocality> getUserLocality() {
		return userLocality;
	}

    @Override
	public EnumProperty<EncryptMode> getEncryptMode() {
		return encryptMode;
	}

    @Override
    public StringProperty getForceEncryptHeaderTrigger() {
        return forceEncryptHeaderTrigger;
    }

    @Override
    public BooleanProperty getForceEncryptAllowed() {
        return forceEncryptAllowed;
    }

    @Override
	public StringProperty getSubjectTrigger() {
		return subjectTrigger;
	}

    @Override
	public BooleanProperty getSubjectTriggerEnabled() {
		return subjectTriggerEnabled;
	}

    @Override
	public BooleanProperty getSubjectTriggerIsRegExpr() {
		return subjectTriggerIsRegExpr;
	}

    @Override
	public BooleanProperty getSubjectTriggerRemovePattern() {
		return subjectTriggerRemovePattern;
	}

    @Override
	public BooleanProperty getSendEncryptionNotification() {
		return sendEncryptionNotification;
	}

    @Override
    public StringProperty getServerSecret() {
    	return serverSecret;
    }

    @Override
    public BooleanProperty getAddSecurityInfo() {
        return addSecurityInfo;
    }

    @Override
    public StringProperty getSecurityInfoDecryptedTag() {
        return securityInfoDecryptedTag;
    }

    @Override
    public StringProperty getSecurityInfoSignedValidTag() {
        return securityInfoSignedValidTag;
    }

    @Override
    public StringProperty getSecurityInfoSignedByValidTag() {
        return securityInfoSignedByValidTag;
    }

    @Override
    public StringProperty getSecurityInfoSignedInvalidTag() {
        return securityInfoSignedInvalidTag;
    }

    @Override
    public BooleanProperty getSubjectFilterEnabled() {
        return subjectFilterEnabled;
    }

    @Override
    public StringProperty getSubjectFilterRegEx() {
        return subjectFilterRegEx;
    }

    @Override
	public void save()
	throws HierarchicalPropertiesException
	{
		properties.setUserLocality(userLocality.getUserValue());
		properties.setEncryptMode(encryptMode.getUserValue());

        properties.setForceEncryptHeaderTrigger(forceEncryptHeaderTrigger.getUserValue());
        properties.setForceEncryptAllowed(forceEncryptAllowed.getUserValue());

		properties.setSubjectTrigger(subjectTrigger.getUserValue());
		properties.setSubjectTriggerEnabled(subjectTriggerEnabled.getUserValue());
		properties.setSubjectTriggerIsRegExpr(subjectTriggerIsRegExpr.getUserValue());
		properties.setSubjectTriggerRemovePattern(subjectTriggerRemovePattern.getUserValue());
		properties.setSendEncryptionNotification(sendEncryptionNotification.getUserValue());

		properties.setServerSecret(serverSecret.getUserValue());

        properties.setAddSecurityInfo(addSecurityInfo.getUserValue());
        properties.setSecurityInfoDecryptedTag(securityInfoDecryptedTag.getUserValue());
        properties.setSecurityInfoSignedValidTag(securityInfoSignedValidTag.getUserValue());
        properties.setSecurityInfoSignedByValidTag(securityInfoSignedByValidTag.getUserValue());
        properties.setSecurityInfoSignedInvalidTag(securityInfoSignedInvalidTag.getUserValue());

        properties.setSubjectFilterEnabled(subjectFilterEnabled.getUserValue());
        properties.setSubjectFilterRegEx(subjectFilterRegEx.getUserValue());

	properties.save();
	}
}
