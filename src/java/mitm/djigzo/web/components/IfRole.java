/*
 * Copyright 2007 Robin Helgelin
 * Copyright 2008 Jonathan Barker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package mitm.djigzo.web.components;

import mitm.djigzo.web.common.security.Authorizer;

import org.apache.tapestry5.Block;
import org.apache.tapestry5.annotations.Parameter;

/**
 * Render it's body depending whether the user is in a specific role or not.
 * 
 * @author Jonathan Barker
 * @author Robin Helgelin
 * @author Tapestry Project (doc comments)
 * 
 * Martijn Brinkers
 * 	- 15/07/08 Updated to make it compatible with Spring security and Tapestry 5.0.13
 *  - 05/11/08 Moved parts to Authorizer so we can reuse parts of it for a Mixin
 */
public class IfRole
{
    /** 
     * If the logged in user matches this role, then the body of the IfRole component is rendered. If false, the body is
     * omitted.  This is retained for backward compatibility, and corresponds to a single entry in ifAnyGranted
     */
    @Parameter(required = false, defaultPrefix = "literal", principal=true)
    private String role;

    /**
     * A comma-separated list of roles is supplied to one or more of the
     * following parameters. If none are supplied, the default behavior is to
     * permit access. Behavior should be self-explanatory.
     */
    @Parameter(required = false, defaultPrefix = "literal")
    private String ifAllGranted;

    @Parameter(required = false, defaultPrefix = "literal")
    private String ifAnyGranted;

    @Parameter(required = false, defaultPrefix = "literal")
    private String ifNotGranted;

    /**
     * Optional parameter to invert the test. If true, then the body is rendered when the test
     * parameter is false (not true).
     */
    @Parameter
    private boolean negate;

    /**
     * An alternate {@link Block} to render if the test parameter is false. The default, null, means
     * render nothing in that situation.
     */
    @Parameter(name = "else")
    private Block elseBlock;

    private boolean test;

    void setupRender()
    {
    	Authorizer authorizer = new Authorizer();
    	
    	authorizer.setIfAllGranted(ifAllGranted);
    	authorizer.setIfAnyGranted(ifAnyGranted);
    	authorizer.setIfNotGranted(ifNotGranted);
    	authorizer.setRole(role);
    	
        test = authorizer.isAuthorized();
    }

    /**
     * Returns null if the test method returns true, which allows normal
     * rendering (of the body). If the test parameter is false, returns the else
     * parameter (this may also be null).
     */
    Object beginRender() {
        return test != negate ? null : elseBlock;
    }

    /**
     * If the test method returns true, then the body is rendered, otherwise not. The component does
     * not have a template or do any other rendering besides its body.
     */
    boolean beforeRenderBody() {
        return test != negate;
    }
}
