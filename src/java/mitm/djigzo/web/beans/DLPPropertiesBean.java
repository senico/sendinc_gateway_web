/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.beans;

import mitm.common.properties.HierarchicalPropertiesException;

/**
 * DLP specific user properties
 * 
 * @author Martijn Brinkers
 *
 */
public interface DLPPropertiesBean
{    
    /**
     * True if DLP is enabled
     */
    public BooleanProperty getEnabled();

    /**
     * The (base) URL used to get access to the quarantine
     */
    public StringProperty getQuarantineURL();

    /**
     * comma separated list email addresses to which DLP notifications will be sent.
     */
    public StringProperty getDLPManagers();

    /**
     * If true, warnings, are send to the originator
     */
    public BooleanProperty getSendWarningToOriginator();

    /**
     * If true, warnings, are send to the DLP managers
     */
    public BooleanProperty getSendWarningToDLPManagers();

    /**
     * If true, Quarantine warnings, are send to the originator
     */
    public BooleanProperty getSendQuarantineToOriginator();

    /**
     * If true, Quarantine warnings, are send to the DLP managers
     */
    public BooleanProperty getSendQuarantineToDLPManagers();
    
    /**
     * If true, Block warnings, are send to the originator
     */
    public BooleanProperty getSendBlockToOriginator();

    /**
     * If true, Block warnings, are send to the DLP managers
     */
    public BooleanProperty getSendBlockToDLPManagers();
    
    /**
     * If true, Error warnings, are send to the originator
     */
    public BooleanProperty getSendErrorToOriginator();

    /**
     * If true, Error warnings, are send to the DLP managers
     */
    public BooleanProperty getSendErrorToDLPManagers();

    /**
     * If true, a user may download a message from the quarantine
     */
    public BooleanProperty getAllowDownloadMessage();

    /**
     * If true, a user may release a message from the quarantine
     */
    public BooleanProperty getAllowReleaseMessage();

    /**
     * If true, a user may release a message from the quarantine with force encrypt
     */
    public BooleanProperty getAllowReleaseEncryptMessage();

    /**
     * If true, a user may release a message directly to outgoing transport
     */
    public BooleanProperty getAllowReleaseAsIsMessage();

    /**
     * If true, a user may delete a message from the quarantine
     */
    public BooleanProperty getAllowDeleteMessage();

    /**
     * If true, and an error occurs during DLP, the message will be put in quarantine
     */
    public BooleanProperty getQuarantineOnError();

    /**
     * If true, and a message cannot be encrypted and encryption is mandatory, the message will be quarantined
     */
    public BooleanProperty getQuarantineOnFailedEncryption();

    /**
     * If true, a notification is send to the originator when the email is released from quarantine.
     */
    public BooleanProperty getSendReleaseNotifyToOriginator();
    
    /**
     * If true, a notification is send to the DLP Managers when the email is released from quarantine.
     */
    public BooleanProperty getSendReleaseNotifyToDLPManagers();
    
    /**
     * If true, a notification is send to the originator when the email is deleted from quarantine.
     */
    public BooleanProperty getSendDeleteNotifyToOriginator();
    
    /**
     * If true, a notification is send to the DLP Managers when the email is deleted from quarantine.
     */
    public BooleanProperty getSendDeleteNotifyToDLPManagers();
    
    /**
     * If true, a notification is send to the originator when the email expires from quarantine.
     */
    public BooleanProperty getSendExpireNotifyToOriginator();
    
    /**
     * If true, a notification is send to the DLP Managers when the email expires from quarantine.
     */
    public BooleanProperty getSendExpireNotifyToDLPManagers();
        
    /**
     * Saves the pdf properties (makes persistent)
     */
	public void save()
	throws HierarchicalPropertiesException;
}
