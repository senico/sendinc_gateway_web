/*
 * Copyright (c) 2011, Martijn Brinkers, Djigzo.
 *
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar,
 * wsdl4j-1.6.1.jar (or modified versions of these libraries),
 * containing parts covered by the terms of Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to
 * convey the resulting work.
 */
package mitm.djigzo.web.beans.impl;

import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.util.Check;
import mitm.djigzo.web.beans.BooleanProperty;
import mitm.djigzo.web.beans.SMSPropertiesBean;
import mitm.djigzo.web.beans.StringProperty;
import mitm.djigzo.web.entities.SaveableUserProperties;


/**
 *
 * @author jon
 */
public class SMSPropertiesBeanImpl implements SMSPropertiesBean
{
	private final StringProperty smsPhoneNumber = new StringProperty();
	private final BooleanProperty smsSendAllowed = new BooleanProperty();
    private final BooleanProperty smsReceiveAllowed = new BooleanProperty();
    private final BooleanProperty smsPhoneNumberSetAllowed = new BooleanProperty();
    private final StringProperty phoneDefaultCountryCode = new StringProperty();

	private final SaveableUserProperties userProperties;

	public SMSPropertiesBeanImpl(SaveableUserProperties userProperties)
	throws HierarchicalPropertiesException
	{
		Check.notNull(userProperties, "userProperties");

		this.userProperties = userProperties;

		loadProperties();
	}

	private void loadProperties()
	throws HierarchicalPropertiesException
	{
	}

    @Override
	public StringProperty getSMSPhoneNumber() {
		return smsPhoneNumber;
	}

    @Override
	public BooleanProperty getSMSSendAllowed() {
		return smsSendAllowed;
	}

    @Override
	public BooleanProperty getSMSReceiveAllowed() {
		return smsReceiveAllowed;
	}

    @Override
    public BooleanProperty getSMSPhoneNumberSetAllowed() {
        return smsPhoneNumberSetAllowed;
    }

    @Override
    public StringProperty getPhoneDefaultCountryCode() {
        return phoneDefaultCountryCode;
    }

    @Override
	public void save()
	throws HierarchicalPropertiesException
	{
		userProperties.save();
	}
}
