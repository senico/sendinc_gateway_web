/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.entities.impl;

import java.util.LinkedList;
import java.util.List;

import mitm.application.djigzo.ws.HierarchicalPropertiesWS;
import mitm.application.djigzo.ws.UserDTO;
import mitm.application.djigzo.ws.UserPreferencesWS;
import mitm.application.djigzo.ws.UserWS;
import mitm.application.djigzo.ws.UsersWS;
import mitm.common.hibernate.SortDirection;
import mitm.common.util.Check;
import mitm.common.ws.WebServiceCheckedException;
import mitm.djigzo.web.entities.User;
import mitm.djigzo.web.entities.UserManager;

public class UserManagerImpl implements UserManager
{
	private final UsersWS usersWS;
	private final UserWS userWS;
	private final UserPreferencesWS userPreferencesWS;
	private final HierarchicalPropertiesWS hierarchicalPropertiesWS;
	
	public UserManagerImpl(UsersWS usersWS, UserWS userWS, UserPreferencesWS userPreferencesWS,
			HierarchicalPropertiesWS hierarchicalPropertiesWS)
	{
		Check.notNull(usersWS, "usersWS");
		Check.notNull(userWS, "userWS");
		Check.notNull(userPreferencesWS, "userPreferencesWS");
		Check.notNull(hierarchicalPropertiesWS, "hierarchicalPropertiesWS");

		this.usersWS = usersWS;
		this.userWS = userWS;
		this.userPreferencesWS = userPreferencesWS;
		this.hierarchicalPropertiesWS = hierarchicalPropertiesWS;
	}
	
    @Override
	public void addUser(String email) 
	throws WebServiceCheckedException 
	{
	    usersWS.addUser(email);
	}

    @Override
	public boolean deleteUser(String email) 
	throws WebServiceCheckedException 
	{
		boolean deleted = false;
		
		if (usersWS.isUser(email))
		{
			usersWS.deleteUser(email);
			
			deleted = true;
		}
		
		return deleted;
	}

    @Override
	public User getUser(String email, boolean dummyIfNotExist) 
	throws WebServiceCheckedException 
	{
		User user = null;
		
		if (dummyIfNotExist || usersWS.isUser(email))
		{
			user = new UserImpl(userWS, userPreferencesWS, hierarchicalPropertiesWS, 
					email, null /* we do not yet now the locality */);
		}
		
		return user;
	}

    @Override
	public int getUserCount() 
	throws WebServiceCheckedException 
	{
		return usersWS.getUserCount();
	}

    @Override
	public List<User> getUsers(Integer firstResult, Integer maxResults, SortDirection sortDirection)
	throws WebServiceCheckedException 
	{
		List<User> users = new LinkedList<User>();
		
		List<UserDTO> usersDTO = usersWS.getUsers(firstResult, maxResults, sortDirection);
		
		/*
		 * The webservices return null when the collection returns no items.
		 */
		if (usersDTO != null)
		{
			for (UserDTO user : usersDTO) 
			{
				users.add(new UserImpl(userWS, userPreferencesWS, hierarchicalPropertiesWS, 
				        user.getEmail(), user.getUserLocality()));
			}
		}
		
		return users;
	}

    @Override
    public List<User> searchUsers(String search, Integer firstResult, Integer maxResults, SortDirection sortDirection)
    throws WebServiceCheckedException 
    {
        List<User> users = new LinkedList<User>();
        
        List<UserDTO> usersDTO = usersWS.searchUsers(search, firstResult, maxResults, sortDirection);
        
        /*
         * The webservices return null when the collection returns no items.
         */
        if (usersDTO != null)
        {
            for (UserDTO user : usersDTO) 
            {
                users.add(new UserImpl(userWS, userPreferencesWS, hierarchicalPropertiesWS, 
                        user.getEmail(), user.getUserLocality()));
            }
        }
        
        return users;
    }
	    
    @Override
    public int getSearchUsersCount(String search)
    throws WebServiceCheckedException
    {
    	return usersWS.getSearchUsersCount(search);
    }
	
    @Override
	public boolean isUser(String email) 
	throws WebServiceCheckedException 
	{
		return usersWS.isUser(email);
	}
}
