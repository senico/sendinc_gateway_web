/*
 * Copyright (c) 2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.utils;


/**
 * Password encoder/decoder
 * 
 * @author Martijn Brinkers
 *
 */
public interface PasswordCodec
{
    public static interface PasswordAndSalt
    {
        public String getPassword();
        public String getSalt();
    }
  
    /**
     * Extracts the salt and the hashed password from the encoded password
     */
    public PasswordAndSalt getPasswordAndSalt(String encodedPassword);

    /**
     * Encodes the password. It returns a salted and hashes password. The salt is
     * encoded into the password
     */
    public String encodePassword(String password);
    
    /**
     * Returns true if the rawPassword is the same as the current password
     */
    public boolean isPasswordValid(String currentEncodedPassword, String rawPassword);
}
