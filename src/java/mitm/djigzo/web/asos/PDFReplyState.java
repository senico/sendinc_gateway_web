/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 *
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar,
 * wsdl4j-1.6.1.jar (or modified versions of these libraries),
 * containing parts covered by the terms of Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to
 * convey the resulting work.
 */
package mitm.djigzo.web.asos;

import mitm.application.djigzo.james.mailets.PDFReplyURLBuilder;
import mitm.common.mail.EmailAddressUtils;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.util.Check;
import mitm.common.util.StandardHttpURLBuilder;
import mitm.common.util.URLBuilder;
import mitm.common.util.URLBuilderException;
import mitm.common.ws.WebServiceCheckedException;
import mitm.djigzo.web.entities.User;
import mitm.djigzo.web.entities.UserManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * PDFReplyState is used to handle the reply link from a PDF (when an external user clicks the reply link
 * in the PDF).
 *
 * @author Martijn Brinkers
 *
 */
public class PDFReplyState
{
	private final static Logger logger = LoggerFactory.getLogger(PDFReplyState.class);

	/*
	 * Manages users (getting the user from his/her email address)
	 */
	private final UserManager userManager;

    /*
     * The user that initiated the PDF (ie. the creator of the PDF message)
     */
    private String userEmail;

	/*
	 * Reply is sent to this email address
	 */
	private String recipientEmail;

	/*
	 * The email address of the user that replies to the message
	 */
	private String fromEmail;

	/*
	 * The subject of the reply
	 */
	private String subject;

	/*
	 * The time (in milliseconds) the source message was created. This is used to check if a reply can not longer be sent
	 */
	private long time;

	/*
	 * The message id
	 */
	private String messageID;

	/*
	 * The calculated hmac of the reply parameters
	 */
	private String hmac;

	/*
	 * True if the request is valid (mac correct etc.), false is not valid, null if undecided
	 */
	private boolean validRequest;

	/*
	 * A key that uniquely identifies the PDF message
	 */
	private String messageKey;

	public PDFReplyState(UserManager userManager)
	{
		Check.notNull(userManager, "userManager");

		this.userManager = userManager;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public String getRecipientEmail() {
		return recipientEmail;
	}

	public String getFromEmail() {
		return fromEmail;
	}

	public String getSubject() {
		return subject;
	}

	public long getTime() {
		return time;
	}

	public String getMessageID() {
		return messageID;
	}

	public String getHmac() {
		return hmac;
	}

	public void loadFromEnvelope(String envelope, String hmac)
	{
		validRequest = false;

		URLBuilder uRLBuilder = new StandardHttpURLBuilder();

		PDFReplyURLBuilder replyBuilder = new PDFReplyURLBuilder(uRLBuilder);

    	PDFReplyURLBuilder.KeyProvider keyProvider = new PDFReplyURLBuilder.KeyProvider()
    	{
			@Override
            public String getKey(PDFReplyURLBuilder builder)
			throws URLBuilderException
			{
				return PDFReplyState.this.getKey(builder);
			}
    	};

    	try {
			replyBuilder.loadFromEnvelope(envelope, hmac, keyProvider);

			this.userEmail = replyBuilder.getUser();
			this.recipientEmail = replyBuilder.getRecipient();
			this.fromEmail = replyBuilder.getFrom();
			this.subject = replyBuilder.getSubject();
			this.time = replyBuilder.getTime();
			this.hmac = hmac;

	    	validRequest = true;
		}
    	catch (PDFReplyURLBuilder.NullKeyURLBuilderException e) {
    		logger.warn(e.getMessage());
		}
    	catch (URLBuilderException e) {
    		logger.error("Error loading envelope.", e);
		}
	}

	private String getKey(PDFReplyURLBuilder builder)
	throws URLBuilderException
	{
		String serverSecret = null;

		try {
			if (builder.getUser() == null || builder.getFrom() == null)
			{
				 logger.warn("Some envelope parameters are missing.");

				 return null;
			}

			User sender = getUser(builder.getUser());
			User from = getUser(builder.getFrom());

			if (isReplyAlowed(sender) && isReplyAlowed(from))
			{
				if (sender != null)	{
					serverSecret = sender.getPreferences().getProperties().getServerSecret();
				}
			}
			else {
				logger.warn("User " + builder.getUser() + " or recipient " + builder.getFrom() + " does not allow PDF reply.");
			}
		}
		catch(WebServiceCheckedException e) {
			throw new URLBuilderException(e);
		}
		catch (HierarchicalPropertiesException e) {
			throw new URLBuilderException(e);
		}

		return serverSecret;
	}

	public String getMessageKey()
	{
		if (!isValidRequest()) {
			throw new IllegalStateException("The request was not valid.");
		}

		if (messageKey == null) {
			messageKey = recipientEmail + fromEmail + time + messageID + hmac;
		}

		return messageKey;
	}

	private User getUser(String email)
	throws WebServiceCheckedException
	{
		email = EmailAddressUtils.canonicalizeAndValidate(email, true);

		if (email == null) {
			return null;
		}

		return userManager.getUser(email, true /* dummy if not exist */);
	}

	private boolean isReplyAlowed(User user)
	throws HierarchicalPropertiesException, WebServiceCheckedException
	{
      return false;
	}

	public boolean isValidRequest()	{
		return validRequest;
	}
}
