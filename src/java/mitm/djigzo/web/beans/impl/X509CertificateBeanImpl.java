/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.beans.impl;

import java.math.BigInteger;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import mitm.application.djigzo.ws.X509CertificateDTO;
import mitm.common.util.Check;
import mitm.djigzo.web.beans.CertificateType;
import mitm.djigzo.web.beans.CertificateTypeMarker;
import mitm.djigzo.web.beans.X509CertificateBean;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.text.StrBuilder;

public class X509CertificateBeanImpl implements X509CertificateBean, CertificateTypeMarker
{
	private final static String SEPARATOR = ", ";
	
	private final X509CertificateDTO certificateDTO;
	
	private final CertificateType certificateType;

	public X509CertificateBeanImpl(X509CertificateDTO certificateDTO)
	{
		this(certificateDTO, CertificateType.GENERAL);
	}
	
	public X509CertificateBeanImpl(X509CertificateDTO certificateDTO, CertificateType certificateType)
	{
		Check.notNull(certificateDTO, "certificateDTO");
		Check.notNull(certificateType, "certificateType");
		
		this.certificateDTO = certificateDTO;
		this.certificateType = certificateType;
	}
	
    @Override
    public String getEmail()
	{
		StrBuilder sb = new StrBuilder();

		Set<String> emails = new HashSet<String>();
		
		if (certificateDTO.getEmailFromAltNames() != null) {
			emails.addAll(certificateDTO.getEmailFromAltNames());
		}
		
		if (certificateDTO.getEmailFromDN() != null) {
			emails.addAll(certificateDTO.getEmailFromDN());
		}
		
		sb.appendWithSeparators(emails, SEPARATOR);
		
		return sb.toString();
	}

    @Override
    public String getSubject() {
		return certificateDTO.getSubjectFriendly();
	}

    @Override
    public Date getNotBefore() {
		return certificateDTO.getNotBefore();
	}

    @Override
    public Date getNotAfter() {
		return certificateDTO.getNotAfter();
	}
    
    @Override
    public boolean isExpired() {
		return certificateDTO.isExpired();
	}
        
    @Override
    public String getKeyUsage() 
	{
		StrBuilder sb = new StrBuilder();

		sb.appendWithSeparators(certificateDTO.getKeyUsage(), SEPARATOR);
		
		return sb.toString();
	}

    @Override
    public String getExtendedKeyUsage()
	{
		StrBuilder sb = new StrBuilder();

		sb.appendWithSeparators(certificateDTO.getExtendedKeyUsage(), SEPARATOR);
		
		return sb.toString();
	}
    
    @Override
    public String getIssuer() {
		return certificateDTO.getIssuerFriendly();
	}

    @Override
    public String getSerialNumberHex() {
		return certificateDTO.getSerialNumberHex();
	}

    @Override
    public String getThumbprintSHA1() {
		return certificateDTO.getThumbprintSHA1();
	}
    
    @Override
    public boolean isCA() {
		return certificateDTO.isCA();
	}
    
    @Override
    public BigInteger getPathLengthConstraint() {
        return certificateDTO.getPathLengthConstraint();
    }

    @Override
    public String getSubjectKeyIdentifier() {
		return certificateDTO.getSubjectKeyIdentifier();
	}
    
    @Override
    public String getThumbprint() {
		return certificateDTO.getThumbprint();
	}

    @Override
    public String getSignatureAlgorithm() {
        return certificateDTO.getSignatureAlgorithm();
    }
    
    @Override
    public int getPublicKeyLength() {
        return certificateDTO.getPublicKeyLength();
    }

    @Override
    public String getPublicKeyAlgorithm() {
        return certificateDTO.getPublicKeyAlgorithm();
    }
    
    @Override
    public String getURIDistributionPointNames() 
	{
		StrBuilder sb = new StrBuilder();

		sb.appendWithSeparators(certificateDTO.getURIDistributionPointNames(), SEPARATOR);

		return sb.toString();
	}
    
    @Override
    public String getKeyAlias() {
		return certificateDTO.getKeyAlias();
	}


	@Override
	public boolean isPrivateKeyAvailable() {
		return certificateDTO.isPrivateKeyAvailable();
	}

	@Override
	public boolean isPrivateKeyAccessible() {
		return certificateDTO.isPrivateKeyAccessible();
	}
	
	@Override
	public boolean isInherited() {
		return certificateDTO.isInherited();
	}    
    
    /**
     * Certificate equals iff thumbprint is equal.
     */    
    @Override
    public boolean equals(Object obj)
    {
        if (!(obj instanceof X509CertificateBean)) {
            return false;
        }
        
        if (this == obj) {
            return true;
        }
        
        X509CertificateBean rhs = (X509CertificateBean) obj;
        
        return new EqualsBuilder()
            .append(getThumbprint(), rhs.getThumbprint())
            .isEquals();    
    }
    
    @Override
    public int hashCode() 
    {
        return new HashCodeBuilder()
            .append(getThumbprint())
            .toHashCode();    
    }

    @Override
	public CertificateType getCertificateType() {
		return certificateType;
	}
}
