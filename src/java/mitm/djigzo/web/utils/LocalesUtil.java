package mitm.djigzo.web.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LocalesUtil
{
    private final static Logger logger = LoggerFactory.getLogger(LocalesUtil.class);
    
    public static List<Locale> convertToLocales(String supportedLocaleCodesString)
    {

        String[] localeCodes = supportedLocaleCodesString.split(",");

        List<Locale> locales = new ArrayList<Locale>(localeCodes.length);
        
        for (String localeCode : localeCodes)
        {
            Locale locale = convertToLocale(localeCode);
            
            if (locale != null) {
                locales.add(locale);
            }
        }

        return locales;
    }

    public static Locale convertToLocale(String localeCode)
    {
        if (StringUtils.isEmpty(localeCode)) {
            return null;
        }
        
        Locale locale = null;
        
        String[] elements = localeCode.split("_");

        switch (elements.length) {
        case 1:
            locale = new Locale(elements[0]);
            break;
        case 2:
            locale = new Locale(elements[0], elements[1]);
            break;
        case 3:
            locale = new Locale(elements[0], elements[1], elements[2]);
            break;
        case 4:
            locale = new Locale(elements[0], elements[1], elements[2] + "_" + elements[3]);
            break;
        default:
            logger.warn("Can't handle localeCode = \"" + localeCode + "\".  " +
            		"Elements.length = " + elements.length);
        }

        return locale;
    }
}
