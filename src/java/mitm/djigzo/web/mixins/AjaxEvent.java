/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.mixins;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.tapestry5.BindingConstants;
import org.apache.tapestry5.ClientElement;
import org.apache.tapestry5.ComponentResources;
import org.apache.tapestry5.Link;
import org.apache.tapestry5.MarkupWriter;
import org.apache.tapestry5.RenderSupport;
import org.apache.tapestry5.annotations.Environmental;
import org.apache.tapestry5.annotations.IncludeJavaScriptLibrary;
import org.apache.tapestry5.annotations.InjectContainer;
import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.json.JSONObject;

@IncludeJavaScriptLibrary({"ajaxEvent.js", "../components/activateZone.js"})
public class AjaxEvent 
{
    /**
     * The name of the event
     */
    public static final String DEFAULT_EVENT_NAME = "AjaxEvent";

    /**
     * The name of the request object
     */
    public static final String REQUEST_PARAM_NAME = "data";

    /**
     * The name of the params object
     */
    public static final String PARAMS_NAME = "params";

    /**
     * The name of the before event param
     */
    public static final String BEFORE_EVENT_PARAM_NAME = "beforeEvent";

    /**
     * The name of the after event param
     */
    public static final String AFTER_EVENT_PARAM_NAME = "afterEvent";
    
    /**
     * The name of the context param
     */
    public static final String CONTEXT_PARAM_NAME = "context";

    /**
     * The name of the event param
     */
    public static final String EVENT_PARAM_NAME = "event";
    
    /**
     * The name of the client id param
     */
    public static final String CLIENT_ID_PARAM_NAME = "clientid";
    
    @Parameter(value = "click", defaultPrefix = BindingConstants.LITERAL)
    private String event;

    @Parameter(defaultPrefix = BindingConstants.LITERAL)
    private String zone;

    @Parameter
    private String context;
    
    @Parameter(value = DEFAULT_EVENT_NAME, defaultPrefix = BindingConstants.LITERAL)
    private String eventName;

    /**
     * The Javascript function to call before the event (optional).
     */
    @Parameter(required = false, defaultPrefix = "literal")
    private String beforeEvent;
    
    /**
     * The Javascript function to call after the event (optional).
     */
    @Parameter(required = false, defaultPrefix = "literal")
    private String afterEvent;

    @Inject
    private ComponentResources resources;
    
    @Environmental
    private RenderSupport renderSupport;

    @InjectContainer
    private ClientElement element;
    
    void afterRender(MarkupWriter writer)
    {
        Link link = resources.createEventLink(eventName);

        JSONObject json = new JSONObject();
        
        json.put(CLIENT_ID_PARAM_NAME, StringEscapeUtils.escapeJavaScript(element.getClientId()));
        json.put("event", event);
        json.put("uri", link.toAbsoluteURI());
        json.put("zone", zone);
        json.put(BEFORE_EVENT_PARAM_NAME, beforeEvent);
        json.put(AFTER_EVENT_PARAM_NAME, afterEvent);
        json.put(CONTEXT_PARAM_NAME, context);
        
        renderSupport.addScript(String.format("new AjaxEvent('%s');", json.toString()));
    }
}
