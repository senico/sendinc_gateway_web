/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.pages;

import java.io.IOException;
import java.util.Arrays;

import mitm.application.djigzo.ws.CertificateStore;
import mitm.application.djigzo.ws.CertificateValidatorResult;
import mitm.application.djigzo.ws.CertificateValidatorWS;
import mitm.application.djigzo.ws.KeyAndCertStoreWS;
import mitm.application.djigzo.ws.KeyAndCertificateWorkflowWS;
import mitm.common.ws.WebServiceCheckedException;
import mitm.djigzo.web.beans.X509CertificateBean;
import mitm.djigzo.web.common.ValidityCheck;
import mitm.djigzo.web.common.YesNo;
import mitm.djigzo.web.components.CertificateGrid;
import mitm.djigzo.web.components.CertificateStoreGrid;
import mitm.djigzo.web.pages.certificate.CertificateDownloadKey;
import mitm.djigzo.web.services.CertificateStoreMarker;

import org.apache.tapestry5.Link;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.InjectPage;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.internal.services.LinkFactory;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.ioc.annotations.Value;
import org.apache.tapestry5.services.Response;

@IncludeStylesheet("context:styles/pages/certificates.css")
public class Certificates
{
	@Inject
	@CertificateStoreMarker
	private KeyAndCertStoreWS certStore;

	@Inject
	private KeyAndCertificateWorkflowWS keyAndCertificateWorkflow;
	
	@Inject
	private CertificateValidatorWS certificateValidatorWS; 
	
	@Inject
	private Response response;
	
	@Inject
	private LinkFactory linkFactory;

    @Inject
    @Value("${certificates.rowsPerPage}")
    private int defaultRowsPerPage;
    
    @Persist
    private Integer rowsPerPage;    

    @Inject
    @Value("${certificates.checkInUse}")
    private boolean checkInUse;    
    
	@InjectPage
	private CertificateDownloadKey downloadKeyPage;
	
	@Component(parameters = {"certStore = certStore", "rowsPerPage = prop:rowsPerPage"})
    private CertificateStoreGrid certificateStoreGrid;
	
	@OnEvent(CertificateGrid.CERTIFICATE_VALIDATOR_RESULT_EVENT)
	protected CertificateValidatorResult onGetCertificateValidatorResult(
			X509CertificateBean certificate) 
	throws WebServiceCheckedException
	{
		return certificateValidatorWS.checkValidity(CertificateStore.CERTIFICATES, 
				certificate.getThumbprint());
	}
	
	@OnEvent(CertificateGrid.SUBJECT_CLICKED_EVENT)
	protected void onSubjectClicked(String thumbprint) 
	throws IOException
	{
        Link link = linkFactory.createPageRenderLink("certificate/view", false, CertificateStore.CERTIFICATES,
        		thumbprint, ValidityCheck.TRUST);

        response.sendRedirect(link);
	}
	
    @OnEvent(CertificateStoreGrid.DOWNLOAD_KEYS_CLICKED_EVENT)
    protected Object downloadSelected() 
    throws WebServiceCheckedException 
    {
        /*
         * Note: we need to clone the set to make sure that concurrent modifications
         * are possible.
         */
        String[] thumbprints = certificateStoreGrid.getCertificateGrid().getSelected().toArray(new String[]{});
        
        /*
         * We assume that the id's of the checkboxes are thumbprints of certificates
         */

        if (thumbprints.length > 0) {
            downloadKeyPage.setThumbprints(Arrays.asList(thumbprints));
        }

        /*
         * We need to clear the selected. This is not visually reflected (we cannot refresh the page
         * and download the certificates afaik) so the check boxes are unchecked using Javascript.
         * Another option would be to redirect to the same page and persist (flash) the  certificate 
         * byte array.
         */
        certificateStoreGrid.getCertificateGrid().clearSelected();
    	    	
    	return downloadKeyPage;
    }

	@OnEvent(CertificateGrid.IN_USE_EVENT)
	protected YesNo isInUse(String thumbprint) 
	throws WebServiceCheckedException 
	{
		YesNo inUse = YesNo.NO;
		
		if (checkInUse && thumbprint != null && keyAndCertificateWorkflow.isInUse(thumbprint)) {
			inUse = YesNo.YES;
		}
		
		return inUse;
	}
    
	public KeyAndCertStoreWS getCertStore() {
		return certStore;
	}

	public void setCertStore(KeyAndCertStoreWS certStore) {
		this.certStore = certStore;
	}

    public int getRowsPerPage()
    {
        return rowsPerPage != null ? rowsPerPage : defaultRowsPerPage;
    }
    
    public void setRowsPerPage(int rowsPerPage) {
        this.rowsPerPage = rowsPerPage;
    }
}
