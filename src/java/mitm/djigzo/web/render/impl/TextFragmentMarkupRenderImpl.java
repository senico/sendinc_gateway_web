/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.render.impl;

import mitm.common.util.Check;
import mitm.djigzo.web.render.FragmentMarkupRenderContent;
import mitm.djigzo.web.render.FragmentMarkupRenderer;
import mitm.djigzo.web.render.FragmentMarkupRendererRegistry;
import mitm.djigzo.web.render.TextFragmentMarkupRender;

import org.apache.commons.lang.text.StrBuilder;

/**
 * Default implementation of TextFragmentMarkupRender
 * 
 * @author Martijn Brinkers
 *
 */
public class TextFragmentMarkupRenderImpl implements TextFragmentMarkupRender 
{
    /*
     * The FragmentMarkupRenderer which is used to render the text
     */
	private final FragmentMarkupRenderer fragmentMarkupRenderer;
	
	/*
	 * The FragmentMarkupRenderContent
	 */
	private final FragmentMarkupRenderContent renderedContent;
	
	public TextFragmentMarkupRenderImpl(FragmentMarkupRenderer fragmentMarkupRenderer, 
			FragmentMarkupRenderContent renderedContent)
	{
		Check.notNull(fragmentMarkupRenderer, "fragmentMarkupRenderer");
		Check.notNull(renderedContent, "renderedContent");
		
		this.fragmentMarkupRenderer = fragmentMarkupRenderer;
		this.renderedContent = renderedContent;
	}
	
    @Override
	public String renderMarkup(String input, FragmentMarkupRendererRegistry registry) 
	{
		StrBuilder builder = new StrBuilder(input);
		
		/*
		 * fragmentMarkupRenderer will render the input and add the result to the FragmentMarkupRenderContent
		 */
		fragmentMarkupRenderer.renderMarkup(builder, registry);
		
		return renderedContent.getContent();
	}
}
