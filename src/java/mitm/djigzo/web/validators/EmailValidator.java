/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.validators;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import mitm.common.mail.EmailAddressUtils;

import org.apache.tapestry5.Field;
import org.apache.tapestry5.MarkupWriter;
import org.apache.tapestry5.ValidationException;
import org.apache.tapestry5.ioc.MessageFormatter;
import org.apache.tapestry5.services.FormSupport;
import org.apache.tapestry5.validator.AbstractValidator;

/**
 * Tapestry validator for the validation of an email address
 * 
 * @author Martijn Brinkers
 *
 */
public class EmailValidator extends AbstractValidator<Integer, String>
{
	/*
	 * Because Tapestry now has it's own email validator I create a unique name
	 */
	public static final String NAME = "emailmitm";
	
    public EmailValidator(String messageKey) 
    {
    	super(Integer.class, String.class, messageKey);
    }

    public EmailValidator() 
    {
    	super(Integer.class, String.class, "validate-email");
    }

    @Override
    public void validate(Field field, Integer constraintValue, MessageFormatter formatter, String value) 
    throws ValidationException
    {
        if (value != null)
        {
            try {
                InternetAddress[] addresses = InternetAddress.parse(value, true);
                
                if (addresses == null) {
                    throw new ValidationException(formatter.format(field.getLabel()));
                }

                int maxAddresses = 1;
                
                if (constraintValue != null) {
                    maxAddresses = constraintValue;
                }
                
                if (addresses.length > maxAddresses) {
                    throw new ValidationException(formatter.format(field.getLabel()));
                }
                
                for (InternetAddress address : addresses)
                {
                    if (!EmailAddressUtils.isValid(address)) {
                        throw new ValidationException(formatter.format(field.getLabel()));
                    }
                }
            }
            catch (AddressException e) {
                throw new ValidationException(formatter.format(field.getLabel()));
            }
        }
    }

    @Override
	public void render(Field field, Integer constraintValue, MessageFormatter formatter, MarkupWriter writer,
            FormSupport formSupport)
	{
		/*
		 * Empty on purpose because we do not have a Javascript validator
		 */
	}
}
