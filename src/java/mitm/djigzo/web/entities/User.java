/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.entities;

import java.util.List;

import mitm.application.djigzo.UserLocality;
import mitm.application.djigzo.ws.X509CertificateDTO;
import mitm.common.ws.WebServiceCheckedException;

public interface User 
{
    /**
     * a user is uniquely identified by his/her email. The email address should be a valid email 
     * address.
     */
	public String getEmail();
	
	public UserLocality getUserLocality()
    throws WebServiceCheckedException;
	
    public List<X509CertificateDTO> getAutoSelectEncryptionCertificates()
    throws WebServiceCheckedException;
    
    public X509CertificateDTO getSigningKeyAndCertificate()
    throws WebServiceCheckedException;

    public void setSigningKeyAndCertificate(String thumbprint)
    throws WebServiceCheckedException;
    
    public UserPreferences getPreferences()
    throws WebServiceCheckedException;
}
