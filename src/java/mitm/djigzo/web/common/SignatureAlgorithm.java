package mitm.djigzo.web.common;


public enum SignatureAlgorithm
{
    SHA1_WITH_RSA   ("SHA1WithRSAEncryption"),
    SHA256_WITH_RSA ("SHA256WithRSAEncryption"),
    SHA512_WITH_RSA ("SHA512WithRSAEncryption");
    
    private String name;
    
    private SignatureAlgorithm(String name) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    }
    
    public static SignatureAlgorithm fromName(String name)
    {
        for (SignatureAlgorithm algorithm : SignatureAlgorithm.values()) 
        {
            if (algorithm.getName().equalsIgnoreCase(name)) 
            {
                return algorithm;
            }
        }
        
        return null;
    }    
}
