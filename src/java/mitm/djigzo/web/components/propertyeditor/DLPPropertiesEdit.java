/*
 * Copyright (c) 2010-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.components.propertyeditor;

import mitm.application.djigzo.DLPProperties;
import mitm.djigzo.web.beans.BooleanProperty;
import mitm.djigzo.web.beans.DLPPropertiesBean;
import mitm.djigzo.web.beans.UserPropertiesBean;

import org.apache.commons.lang.StringUtils;
import org.apache.tapestry5.Block;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.IncludeJavaScriptLibrary;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.json.JSONObject;
import org.apache.tapestry5.services.Request;

@SuppressWarnings("unused")
@IncludeStylesheet("context:styles/components/propertyeditor/dlpPropertiesEdit.css")
public class DLPPropertiesEdit
{
	@Property
	@Parameter(required = true)
	private DLPPropertiesBean properties;
	
    @Inject
    private Request request;
	
	@Component(id = "enabled", parameters = {
			"value = properties.enabled.value", 
			"checked = properties.enabled.inherit"}
	)
    private BooleanPropertyEdit enabled;

	@Component(id = "quarantineURL", parameters = {
            "validate = uri=base",
			"value = properties.quarantineURL.value", 
    		"checked = properties.quarantineURL.inherit"}
	)
    private TextPropertyEdit quarantineURL;
	
    @Component(id = "dlpManagers", parameters = {
            "validate = emailmitm=5",
            "value = properties.dlpManagers.value", 
            "checked = properties.dlpManagers.inherit"}
    )
    private TextPropertyEdit dlpManagers;
    
    @Component(id = "sendWarningToOriginator", parameters = {
            "value = properties.sendWarningToOriginator.value", 
            "checked = properties.sendWarningToOriginator.inherit"}
    )
    private BooleanPropertyEdit sendWarningToOriginator;

    @Component(id = "sendWarningToDLPManagers", parameters = {
            "value = properties.sendWarningToDLPManagers.value", 
            "checked = properties.sendWarningToDLPManagers.inherit"}
    )
    private BooleanPropertyEdit sendWarningToDLPManagers;
        
    @Component(id = "sendQuarantineToOriginator", parameters = {
            "value = properties.sendQuarantineToOriginator.value", 
            "checked = properties.sendQuarantineToOriginator.inherit"}
    )
    private BooleanPropertyEdit sendQuarantineToOriginator;

    @Component(id = "sendQuarantineToDLPManagers", parameters = {
            "value = properties.sendQuarantineToDLPManagers.value", 
            "checked = properties.sendQuarantineToDLPManagers.inherit"}
    )
    private BooleanPropertyEdit sendQuarantineToDLPManagers;
    
    @Component(id = "sendBlockToOriginator", parameters = {
            "value = properties.sendBlockToOriginator.value", 
            "checked = properties.sendBlockToOriginator.inherit"}
    )
    private BooleanPropertyEdit sendBlockToOriginator;

    @Component(id = "sendBlockToDLPManagers", parameters = {
            "value = properties.sendBlockToDLPManagers.value", 
            "checked = properties.sendBlockToDLPManagers.inherit"}
    )
    private BooleanPropertyEdit sendBlockToDLPManagers;
    
    @Component(id = "sendErrorToOriginator", parameters = {
            "value = properties.sendErrorToOriginator.value", 
            "checked = properties.sendErrorToOriginator.inherit"}
    )
    private BooleanPropertyEdit sendErrorToOriginator;

    @Component(id = "sendErrorToDLPManagers", parameters = {
            "value = properties.sendErrorToDLPManagers.value", 
            "checked = properties.sendErrorToDLPManagers.inherit"}
    )
    private BooleanPropertyEdit sendErrorToDLPManagers;
        
    @Component(id = "allowDownloadMessage", parameters = {
            "value = properties.allowDownloadMessage.value", 
            "checked = properties.allowDownloadMessage.inherit"}
    )
    private BooleanPropertyEdit allowDownloadMessage;

    @Component(id = "allowReleaseMessage", parameters = {
            "value = properties.allowReleaseMessage.value", 
            "checked = properties.allowReleaseMessage.inherit"}
    )
    private BooleanPropertyEdit allowReleaseMessage;

    @Component(id = "allowReleaseEncryptMessage", parameters = {
            "value = properties.allowReleaseEncryptMessage.value", 
            "checked = properties.allowReleaseEncryptMessage.inherit"}
    )
    private BooleanPropertyEdit allowReleaseEncryptMessage;

    @Component(id = "allowReleaseAsIsMessage", parameters = {
            "value = properties.allowReleaseAsIsMessage.value", 
            "checked = properties.allowReleaseAsIsMessage.inherit"}
    )
    private BooleanPropertyEdit allowReleaseAsIsMessage;
    
    @Component(id = "allowDeleteMessage", parameters = {
            "value = properties.allowDeleteMessage.value", 
            "checked = properties.allowDeleteMessage.inherit"}
    )
    private BooleanPropertyEdit allowDeleteMessage;    
    
    @Component(id = "quarantineOnError", parameters = {
            "value = properties.quarantineOnError.value", 
            "checked = properties.quarantineOnError.inherit"}
    )
    private BooleanPropertyEdit quarantineOnError;

    @Component(id = "quarantineOnFailedEncryption", parameters = {
            "value = properties.quarantineOnFailedEncryption.value", 
            "checked = properties.quarantineOnFailedEncryption.inherit"}
    )
    private BooleanPropertyEdit quarantineOnFailedEncryption;

    @Component(id = "sendReleaseNotifyToOriginator", parameters = {
            "value = properties.sendReleaseNotifyToOriginator.value", 
            "checked = properties.sendReleaseNotifyToOriginator.inherit"}
    )
    private BooleanPropertyEdit sendReleaseNotifyToOriginator;    
        
    @Component(id = "sendReleaseNotifyToDLPManagers", parameters = {
            "value = properties.sendReleaseNotifyToDLPManagers.value", 
            "checked = properties.sendReleaseNotifyToDLPManagers.inherit"}
    )
    private BooleanPropertyEdit sendReleaseNotifyToDLPManagers;    
        
    @Component(id = "sendDeleteNotifyToOriginator", parameters = {
            "value = properties.sendDeleteNotifyToOriginator.value", 
            "checked = properties.sendDeleteNotifyToOriginator.inherit"}
    )
    private BooleanPropertyEdit sendDeleteNotifyToOriginator;    
    
    @Component(id = "sendDeleteNotifyToDLPManagers", parameters = {
            "value = properties.sendDeleteNotifyToDLPManagers.value", 
            "checked = properties.sendDeleteNotifyToDLPManagers.inherit"}
    )
    private BooleanPropertyEdit sendDeleteNotifyToDLPManagers;    
    
    @Component(id = "sendExpireNotifyToOriginator", parameters = {
            "value = properties.sendExpireNotifyToOriginator.value", 
            "checked = properties.sendExpireNotifyToOriginator.inherit"}
    )
    private BooleanPropertyEdit sendExpireNotifyToOriginator;    
    
    @Component(id = "sendExpireNotifyToDLPManagers", parameters = {
            "value = properties.sendExpireNotifyToDLPManagers.value", 
            "checked = properties.sendExpireNotifyToDLPManagers.inherit"}
    )
    private BooleanPropertyEdit sendExpireNotifyToDLPManagers;    
}
