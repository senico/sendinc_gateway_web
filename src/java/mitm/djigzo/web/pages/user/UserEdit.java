/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.pages.user;

import mitm.application.djigzo.admin.FactoryRoles;
import mitm.common.mail.EmailAddressUtils;
import mitm.common.properties.HierarchicalPropertiesException;
import mitm.common.ws.WebServiceCheckedException;
import mitm.djigzo.web.beans.UserBean;
import mitm.djigzo.web.beans.impl.UserBeanImpl;
import mitm.djigzo.web.components.propertyeditor.UserPropertiesEdit;
import mitm.djigzo.web.entities.User;
import mitm.djigzo.web.entities.UserManager;
import mitm.djigzo.web.pages.Users;

import org.apache.tapestry5.PersistenceConstants;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.springframework.security.annotation.Secured;

@IncludeStylesheet("context:styles/pages/user/userEdit.css")
@Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_USER_MANAGER})
public class UserEdit 
{
	@Inject
	private UserManager userManager;

	@Property
	private UserBean userBean;

    /*
     * True if changes were applied
     */
    @Persist(PersistenceConstants.FLASH)
    private boolean applied;
	
	@SuppressWarnings("unused")
	@Component(parameters = { "userProperties = userBean.preferences.properties" })
	private UserPropertiesEdit userPropertiesEdit;
	
	public boolean isApplied() {
	    return applied;
	}
	
	@SetupRender
	@Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_USER_MANAGER})
	protected void setupRender() {
		/*
		 * Empty on purpose
		 */
	}
	
	private UserBean createUserBean(String email)
	throws WebServiceCheckedException
	{
		String filteredEmail = EmailAddressUtils.canonicalizeAndValidate(email, true);
		
		if (filteredEmail == null) {
			return null;
		}
		
		User user = userManager.getUser(filteredEmail, false /* null if not exist */);
		
		if (user == null) {
			return null;			
		}
		
		return new UserBeanImpl(user);
	}
	
	public void setEmail(String email)
	throws WebServiceCheckedException 
	{
		userBean = createUserBean(email);
	}
	
    @Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_USER_MANAGER})
	public Object onActivate(String email)
	throws WebServiceCheckedException 
	{
		userBean = createUserBean(email);
		
		if (userBean == null) {
			return Users.class;
		}
		
		return null;
	}

	public String onPassivate() 
	{
		String email = null;
		
		if (userBean != null) {
			email = userBean.getEmail();
		}
		
		return email;
	}
	
	public void onSuccess()
	throws HierarchicalPropertiesException, WebServiceCheckedException 
	{
		assert(userBean != null);

		userBean.save();
		
		applied = true;
	}

    /*
     * Event handler called when the cancel button is pressed.
     */
	protected Object onCancel() {
		return Users.class;
	}
}
