/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.components;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.WordUtils;
import org.apache.tapestry5.ComponentResources;
import org.apache.tapestry5.MarkupWriter;
import org.apache.tapestry5.RenderSupport;
import org.apache.tapestry5.annotations.AfterRender;
import org.apache.tapestry5.annotations.BeginRender;
import org.apache.tapestry5.annotations.Environmental;
import org.apache.tapestry5.annotations.IncludeJavaScriptLibrary;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.ioc.annotations.Inject;

/**
 * Tooltip component based on Javascript from:
 * 
 * http://blog.innerewut.de/files/tooltip/tooltip-v0.1.js
 * 
 * @author Martijn Brinkers
 *
 */
@IncludeJavaScriptLibrary("tooltip.js") 
@IncludeStylesheet("context:styles/components/dhtmlTooltip.css")
public class DHTMLTooltip 
{
    @Parameter(required = false, defaultPrefix = "literal")
    private int minLength = 0;

    @Parameter(required = false, defaultPrefix = "literal")
    private int breakupLength = 50;
    
	@Parameter(value = "", required = false, defaultPrefix = "literal")
    private String value;
	
    @Inject
    private ComponentResources componentResources;
	
    @Environmental
    private RenderSupport renderSupport;
    
    private String clientId;
	
    private String getBodyId() {
    	return clientId + "_body";
    }
    
    @BeginRender
    void beginRender(MarkupWriter writer)
    {
    	/*
    	 * Only tooltip if minimal length is reached
    	 */
    	if (value != null &&  (value.length() > minLength))
    	{
        	clientId = renderSupport.allocateClientId(componentResources.getId());

        	/*
        	 * The value need to be split up when the text becomes too long (we do not want
        	 * a extremely long tooltip). We therefore need to insert <br/> so we need to
        	 * do the HTML escaping ourselves and <br/> and use writeRaw.
        	 */
        	String tooltip = StringEscapeUtils.escapeHtml(value);

        	tooltip = WordUtils.wrap(tooltip, breakupLength, "<br/>", true);
        	
        	/*
        	 * Somehow FF requires the display: none style to be inline to prevent some flickering
        	 */
        	writer.element("span", "id", clientId, "class", "dhtmlTooltip", "style", "display: none;");
        	writer.writeRaw(tooltip);
        	writer.end(); /* span */
        	writer.element("span", "id", getBodyId());
    	}
    }
    
    @AfterRender
    void afterRender(MarkupWriter writer)
    {
    	/*
    	 * Only tooltip if minimal length is reached
    	 */
    	if (value != null && (value.length() > minLength))
    	{
	    	writer.end(); /* div */
	    	
			renderSupport.addScript(String.format("new Tooltip('%s', '%s')",
					StringEscapeUtils.escapeJavaScript(getBodyId()),
					StringEscapeUtils.escapeJavaScript(clientId)));
    	}
    }
}