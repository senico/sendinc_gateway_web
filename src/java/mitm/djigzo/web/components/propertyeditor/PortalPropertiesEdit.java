/*
 * Copyright (c) 2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.components.propertyeditor;

import mitm.djigzo.web.beans.MobilePropertiesBean;
import mitm.djigzo.web.beans.PortalPropertiesBean;
import mitm.djigzo.web.beans.UserPropertiesBean;

import org.apache.commons.lang.StringUtils;
import org.apache.tapestry5.Block;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.IncludeJavaScriptLibrary;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.json.JSONObject;
import org.apache.tapestry5.services.Request;

@SuppressWarnings("unused")
@IncludeStylesheet("context:styles/components/propertyeditor/userPropertiesEdit.css")
public class PortalPropertiesEdit
{
	@Property
	@Parameter(required = true)
	private PortalPropertiesBean properties;
	
	/*
	 * Block which can be used to add additional components/html
	 */
    @Parameter
    private Block additionalBlock;

    @Component(id = "portalPassword", parameters = {
            "value = properties.password.value", 
            "checked = properties.password.inherit"}
    )
    private PasswordPropertyEdit portalPassword;

    @Component(id = "portalEnabled", parameters = {
            "value = properties.enabled.value", 
            "checked = properties.enabled.inherit"}
    )
    private BooleanPropertyEdit portalEnabled;

    @Component(id = "portalAutoInvite", parameters = {
            "value = properties.autoInvite.value", 
            "checked = properties.autoInvite.inherit"}
    )
    private BooleanPropertyEdit portalAutoInvite;

    public Block getAdditionalBlock() {
        return additionalBlock;
    }
}
