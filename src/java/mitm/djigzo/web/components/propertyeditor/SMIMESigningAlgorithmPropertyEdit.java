/*
 * Copyright (c) 2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.components.propertyeditor;

import mitm.common.security.smime.SMIMESigningAlgorithm;

import org.apache.tapestry5.SelectModel;
import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.util.EnumSelectModel;

/**
 * Somehow I cannot create a generic version of EnumPropery so I need to extend it for every enum
 * I need a property for :(
 *  
 * @author Martijn Brinkers
 *
 */
public class SMIMESigningAlgorithmPropertyEdit extends AbstractEnumPropertyEdit
{	
    private static final SMIMESigningAlgorithm[] SUPPORTED_ALGORITHMS = {
        SMIMESigningAlgorithm.SHA1WITHRSAENCRYPTION,
        SMIMESigningAlgorithm.SHA256WITHRSAENCRYPTION,
        SMIMESigningAlgorithm.SHA512WITHRSAENCRYPTION,
        SMIMESigningAlgorithm.RIPEMD160WITHRSAENCRYPTION};
    
	@Parameter(required = true)
	private SMIMESigningAlgorithm value;
	
	public void setValue(SMIMESigningAlgorithm value) {
		this.value = value;
	}

	public SMIMESigningAlgorithm getValue() {
		return value;
	}
	
	@Override
	protected Class<SMIMESigningAlgorithm> getEnumClass() {
		return SMIMESigningAlgorithm.class;
	}
	
	/*
	 * We need to override getModel because we only want to support a sub selection of all signing algorithms.
	 * 
	 * @see mitm.djigzo.web.components.propertyeditor.AbstractEnumPropertyEdit#getModel()
	 */
    @Override
    public SelectModel getModel()
    {
        return new EnumSelectModel(getEnumClass(), getResources().getMessages(), SUPPORTED_ALGORITHMS);
    }	
}