/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.pages;

import mitm.application.djigzo.admin.FactoryRoles;
import mitm.common.ws.WebServiceCheckedException;
import mitm.djigzo.web.beans.AdminBean;
import mitm.djigzo.web.common.security.AdminManager;
import mitm.djigzo.web.grid.AdminGridDataSource;

import org.apache.tapestry5.Asset;
import org.apache.tapestry5.Block;
import org.apache.tapestry5.ComponentResources;
import org.apache.tapestry5.PersistenceConstants;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Path;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.beaneditor.BeanModel;
import org.apache.tapestry5.corelib.components.Grid;
import org.apache.tapestry5.grid.ColumnSort;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.ioc.annotations.Value;
import org.apache.tapestry5.services.BeanModelSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.annotation.Secured;

@IncludeStylesheet("context:styles/pages/admins.css")
@Secured({FactoryRoles.ROLE_ADMIN})
public class Admins 
{
	private static final Logger logger = LoggerFactory.getLogger(Admins.class);
	
	private final static String USERNAME_COLUMN = "username";
	
	@Inject
	private AdminManager adminManager;
	
	/*
	 * The Admin currently being drawn by the grid (is set by the grid component)
	 */
	private AdminBean admin;
	
	/*
	 * True if an error occurred deleting an Admin
	 */
	@Persist(PersistenceConstants.FLASH)
	private boolean deletionError;
	
	@Component(parameters = {"source = gridDataSource", "model = model", "row = admin", "volatile = true", 
			"reorder = delete,username,enabled,roles", "exclude = builtin"})
    private Grid grid;

	@Inject
    private BeanModelSource beanModelSource;

    @Inject
    private ComponentResources resources;

    @SuppressWarnings("unused")
	@Inject
    @Path("context:icons/cross.png")
    @Property
    private Asset deleteAsset;
    
	@Inject
	private Block deletionErrorBlock;
    
    /*
     * If true the Fetchmail pages are enabled
     */
    @Inject 
    @Value("${fetchmail.enabled}")
    private boolean fetchmailEnabled;
	
    @SetupRender
	@Secured({FactoryRoles.ROLE_ADMIN})
    public void setupGrid() 
    {
    	/*
    	 * set initial sorting if sorting is not yet set
    	 */
    	if (grid.getSortModel().getColumnSort(USERNAME_COLUMN) == ColumnSort.UNSORTED) 
    	{
    		grid.getSortModel().updateSort(USERNAME_COLUMN);
    	}
    }
	
    public BeanModel<AdminBean> getModel() 
    {
        BeanModel<AdminBean> model = beanModelSource.createDisplayModel(AdminBean.class, resources.getMessages());
        
        model.add("delete", null);
        
        /*
         * Explicitly add the roles column because it is not automatically added because the property
         * is a List
         */
        model.add("roles", null);
        
        return model;
    }
        
    @OnEvent(component = "deleteAdmin")
    protected void deleteAdmin(String username) 
    {
    	try {
			adminManager.deleteAdmin(username);
		} 
    	catch (WebServiceCheckedException e) 
    	{
    		logger.error("Error deleting admin " + username, e);
    		
    		deletionError = true;
		}
    }
    
	public AdminGridDataSource getGridDataSource()	{
		return new AdminGridDataSource(adminManager);
	}
	
    public Block getDeletionErrorBlock()
    {
    	/*
    	 * Only return the error block if an error actually occurred
    	 */
    	return deletionError ? deletionErrorBlock : null;
    }

	public AdminBean getAdmin() {
		return admin;
	}

	public void setAdmin(AdminBean admin) {
		this.admin = admin;
	}

    public boolean isFetchmailEnabled() {
        return fetchmailEnabled;
    }
}
