/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 *
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar,
 * wsdl4j-1.6.1.jar (or modified versions of these libraries),
 * containing parts covered by the terms of Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to
 * convey the resulting work.
 */
package mitm.djigzo.web.components.propertyeditor;

import mitm.djigzo.web.beans.UserPropertiesBean;

import org.apache.commons.lang.StringUtils;
import org.apache.tapestry5.Block;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.IncludeJavaScriptLibrary;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.json.JSONObject;
import org.apache.tapestry5.services.Request;

@SuppressWarnings("unused")
@IncludeStylesheet("context:styles/components/propertyeditor/userPropertiesEdit.css")
public class UserPropertiesEdit
{
	@Property
	@Parameter(required = true)
	private UserPropertiesBean userProperties;

    /*
     * Block which can be used to add an additional advanced block
     */
    @Parameter
    private Block additionalAdvancedBlock;

    /*
     * Block which can be used to add additional security info settings
     */
    @Parameter
    private Block additionalSecurityInfoBlock;

    /*
     * Block which can be used to add additional subject filter settings
     */
    @Parameter
    private Block additionalSubjectFilterBlock;

	@Persist
    private boolean advancedSettings;

    @Inject
    private Request request;

	public boolean isAdvancedSettings() {
		return advancedSettings;
	}

	public void setAdvancedSettings(boolean advancedSettings) {
		this.advancedSettings = advancedSettings;
	}

	@Component(id = "locality", parameters = {
			"value = userProperties.userLocality.value",
    		"checked = userProperties.userLocality.inherit"}
	)
    private UserLocalityPropertyEdit locality;

	@Component(id = "encryptMode", parameters = {
			"value = userProperties.encryptMode.value",
    		"checked = userProperties.encryptMode.inherit"}
	)
    private EncryptModePropertyEdit encryptMode;

	@Component(id = "sendEncryptionNotification", parameters = {
			"value = userProperties.sendEncryptionNotification.value",
			"checked = userProperties.sendEncryptionNotification.inherit"}
	)
    private BooleanPropertyEdit sendEncryptionNotification;

	@Component(id = "serverSecret", parameters = {
	        "bytesRandom=64",
			"value = userProperties.serverSecret.value",
    		"checked = userProperties.serverSecret.inherit"}
	)
    private SecretKeyPropertyEdit serverSecret;

	  @Component(id = "forceEncryptHeaderTrigger", parameters = {
            "value = userProperties.forceEncryptHeaderTrigger.value",
            "checked = userProperties.forceEncryptHeaderTrigger.inherit"}
    )
    private TextPropertyEdit forceEncryptHeaderTrigger;

    @Component(id = "forceEncryptAllowed", parameters = {
            "value = userProperties.forceEncryptAllowed.value",
            "checked = userProperties.forceEncryptAllowed.inherit"}
    )
    private BooleanPropertyEdit forceEncryptAllowed;

	@Component(id = "subjectTrigger", parameters = {
			"value = userProperties.subjectTrigger.value",
			"checked = userProperties.subjectTrigger.inherit"}
	)
    private TextPropertyEdit subjectTrigger;

	@Component(id = "subjectTriggerEnabled", parameters = {
			"value = userProperties.subjectTriggerEnabled.value",
			"checked = userProperties.subjectTriggerEnabled.inherit"}
	)
    private BooleanPropertyEdit subjectTriggerEnabled;

	@Component(id = "subjectTriggerIsRegExpr", parameters = {
			"value = userProperties.subjectTriggerIsRegExpr.value",
			"checked = userProperties.subjectTriggerIsRegExpr.inherit"}
	)
    private BooleanPropertyEdit subjectTriggerIsRegExpr;

	@Component(id = "subjectTriggerRemovePattern", parameters = {
			"value = userProperties.subjectTriggerRemovePattern.value",
			"checked = userProperties.subjectTriggerRemovePattern.inherit"}
	)
    private BooleanPropertyEdit subjectTriggerRemovePattern;

    @Component(id = "addSecurityInfo", parameters = {
            "value = userProperties.addSecurityInfo.value",
            "checked = userProperties.addSecurityInfo.inherit"}
    )
    private BooleanPropertyEdit addSecurityInfo;

    @Component(id = "subjectFilterEnabled", parameters = {
            "value = userProperties.subjectFilterEnabled.value",
            "checked = userProperties.subjectFilterEnabled.inherit"}
    )
    private BooleanPropertyEdit subjectFilterEnabled;

    /*
     * Ajax event handler called when the advanced settings checkbox is clicked. We want to store
     * the selected value without requiring a submit
     */
    @OnEvent(value="checkboxEvent", component="advancedSettings")
    public void onCheckboxEvent()
    {
        String data = request.getParameter("data");

        if (StringUtils.isEmpty(data)) {
            throw new IllegalArgumentException("data is missing.");
        }

        JSONObject json = new JSONObject(data);

        boolean checked = json.getBoolean("checked");

        advancedSettings = checked;
    }

    public Block getAdditionalAdvancedBlock() {
        return additionalAdvancedBlock;
    }

    public Block getAdditionalSecurityInfoBlock() {
        return additionalSecurityInfoBlock;
    }

    public Block getAdditionalSubjectFilterBlock() {
        return additionalSubjectFilterBlock;
    }

    public SecretKeyPropertyEdit getServerSecret() {
        return serverSecret;
    }
}
