/*
 * Copyright (c) 2008-2012, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.pages.certificate;

import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import mitm.application.djigzo.admin.FactoryRoles;
import mitm.application.djigzo.ws.KeyAndCertificateWorkflowWS;
import mitm.djigzo.web.common.ContentTypes;
import mitm.djigzo.web.common.streamresponse.ByteArrayFileResponse;
import mitm.djigzo.web.pages.Certificates;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.tapestry5.ComponentResources;
import org.apache.tapestry5.Link;
import org.apache.tapestry5.PersistenceConstants;
import org.apache.tapestry5.RenderSupport;
import org.apache.tapestry5.StreamResponse;
import org.apache.tapestry5.annotations.BeginRender;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.beaneditor.Validate;
import org.apache.tapestry5.corelib.components.PasswordField;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.annotation.Secured;

@IncludeStylesheet("context:styles/pages/certificate/certificateDownloadKey.css")
@Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_PKI_MANAGER})
public class CertificateDownloadKey
{
    private final static Logger logger = LoggerFactory.getLogger(CertificateDownloadKey.class);
    
	private final static String PFX_FILENAME = "keys.pfx";
	
	private final static String DOWNLOAD_EVENT = "download";
	
	@Inject
	@Property
	private KeyAndCertificateWorkflowWS keyAndCertificateWorkflowWS;

    @Inject
    private ComponentResources resources;

	@Inject
	private RenderSupport renderSupport;

	@Inject
	private Response response;
    
    /*
     * The thumbprints of all the certificates that need to be downloaded
     */
	@Persist
    private List<String> thumbprints;
    
	/*
	 * The password for the private key file. We need to persist the password because we will
	 * redirect after submit in order to clean-up the form. I do not like the fact the we need to
	 * store the password in the session but it's the only approach that works for IE and FF. I tried 
	 * to reset the password just after the download but the IE7 popup blocker prevented the download
	 * of the file so there had to be a direct download link and this requires the password. The 
	 * password will be cleared when the user presses the close button or when new thumbprints are set.
	 */
	@Validate("required")
    @Persist
	private String password;

	@Property
    @Persist
	private boolean submitted;
	
    /*
     * True if and error has occurred
     */
    @Persist(PersistenceConstants.FLASH)
    private boolean error;

    /*
     * Error message if an error has occurred
     */
    @Persist(PersistenceConstants.FLASH)
    private String errorMessage;
	
	@SuppressWarnings("unused")
	@Component(id = "password", parameters = {"value=password"})
	private PasswordField passwordField;
	
	@SetupRender
	@Secured({FactoryRoles.ROLE_ADMIN, FactoryRoles.ROLE_PKI_MANAGER})
	protected void setupRender() {
		/*
		 * Empty on purpose
		 */
	}
	
	@BeginRender
	public void beginRender() 
	throws IOException 
	{
	    if (!isError())
	    {
    		if (!isHasCertificates())
    		{
    	        Link link = resources.createPageLink(Certificates.class, false);
    	     
    	        response.sendRedirect(link);
    		}
    		else if (submitted)
    		{
    	        String script = String.format("window.location='%s'", 
    	        		StringEscapeUtils.escapeJavaScript(getDownloadURI()));
    	        
    			renderSupport.addScript(script);
    		}
	    }
	}

    /*
     * Called when the form is submitted.
     */
    public void onSuccess() {
    	submitted = true;
    }

	public List<String> getThumbprints() {
		return thumbprints;
	}

	public void setThumbprints(Collection<String> thumbprints) 
	{
		/*
		 * We will reset submitted and password when new thumbprints are set
		 */
		reset();
		
		this.thumbprints = new LinkedList<String>(thumbprints);
	}
	
	public boolean isHasCertificates() {
		return thumbprints != null && thumbprints.size() > 0;
	}
	
	public String getDownloadURI() 
	{
		Link link = resources.createEventLink(DOWNLOAD_EVENT);

        return link.toAbsoluteURI();
	}
	
	private void reset() 
	{
		thumbprints = null;
		password = null;
		submitted = false;
	}
	
    /*
     * Event handler called when the download should be started
     */
    @OnEvent(DOWNLOAD_EVENT)
	public Object onDownload() 
	{
        try {
    		if (!isHasCertificates() || password == null)
    		{
    			return Certificates.class;
    		}
    		
    		byte[] encodedPFX = keyAndCertificateWorkflowWS.getPFX(thumbprints, password.toCharArray());
    	    	
        	StreamResponse response = new ByteArrayFileResponse(encodedPFX, ContentTypes.X_DOWNLOAD,
        			PFX_FILENAME);
        	
        	return response;
        }
        catch (Exception e)
        {
            logger.error("Error downloading keys", e);
            
            error = true;
            errorMessage = ExceptionUtils.getRootCauseMessage(e);
            
            return null;
        }
	}
	
	/*
	 * Called by the Close button
	 */
	public Object onClose() 
	{
		reset();
		
		return Certificates.class;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
    public boolean isError() {
        return error;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}
