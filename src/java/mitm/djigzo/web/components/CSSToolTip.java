/*
 * Copyright (c) 2008-2011, Martijn Brinkers, Djigzo.
 * 
 * This file is part of Djigzo email encryption.
 *
 * Djigzo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3, 19 November 2007 as published by the Free Software 
 * Foundation.
 *
 * Djigzo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public 
 * License along with Djigzo. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 * 
 * If you modify this Program, or any covered work, by linking or 
 * combining it with saaj-api-1.3.jar, saaj-impl-1.3.jar, 
 * wsdl4j-1.6.1.jar (or modified versions of these libraries), 
 * containing parts covered by the terms of Common Development and 
 * Distribution License (CDDL), Common Public License (CPL) the 
 * licensors of this Program grant you additional permission to 
 * convey the resulting work.
 */
package mitm.djigzo.web.components;

import org.apache.tapestry5.MarkupWriter;
import org.apache.tapestry5.annotations.AfterRender;
import org.apache.tapestry5.annotations.BeginRender;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.annotations.SupportsInformalParameters;
import org.apache.tapestry5.dom.Element;

/**
 * Tooltip component showing a CSS based tooltip.
 * 
 * @author Martijn Brinkers
 *
 */
@IncludeStylesheet("context:styles/components/tooltip.css")
@SupportsInformalParameters
public class CSSToolTip 
{
    @Parameter(required = false, defaultPrefix = "literal")
    private int minLength = 0;

	@Parameter(value = "", required = false, defaultPrefix = "literal")
    private String value;
	
    @BeginRender
    void beginRender(MarkupWriter writer)
    {
    	/*
    	 * Only tooltip if minimal length is reached
    	 */
    	if (value != null &&  (value.length() > minLength))
    	{
	    	Element element = writer.element("a");
	    	element.attribute("href", "#");
	    	element.attribute("class", "tooltip");
    	}
    }
    
    @AfterRender
    void afterRender(MarkupWriter writer)
    {
    	/*
    	 * Only tooltip if minimal length is reached
    	 */
    	if (value != null && (value.length() > minLength))
    	{
	    	writer.element("b");
	    	writer.element("em", "class", "outer");
	    	writer.end(); /* em */
	    	writer.element("em", "class", "inner");
	    	writer.end(); /* em */
	    	writer.write(value);
	    	writer.end(); /* b */
	    	writer.end(); /* a */
    	}
    }
}
